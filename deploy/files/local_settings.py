# -*- coding: utf-8 -*-
from settings import INSTALLED_APPS, MIDDLEWARE_CLASSES

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.mysql',
        'NAME': '{{ db_name }}',
        'USER': '{{ db_user }}',
        'PASSWORD': '{{ db_password }}',
        'HOST': '{{ db_host }}',
        'OPTIONS': {"init_command": "SET foreign_key_checks = 0;SET SESSION TRANSACTION ISOLATION LEVEL READ COMMITTED;"},
    }
}

ALLOWED_HOSTS = ['{{ server_name }}']

COMPRESS_ENABLED = False

CACHES = {
    'default': {
        'BACKEND': 'redis_cache.RedisCache',
        'LOCATION': '{{ redis_host }}:{{ redis_port }}',
        'TIMEOUT': 300,
        'OPTIONS': {
            'PARSER_CLASS': 'redis.connection.HiredisParser'
        },
    }
}

SESSION_REDIS_HOST = '{{ redis_host }}'
SESSION_REDIS_PORT = {{ redis_port }}
SESSION_REDIS_DB = 1
SESSION_REDIS_PREFIX = 'session'

DEFAULT_FILE_STORAGE = '{{ file_storage }}'
STATICFILES_STORAGE = '{{ staticfiles_storage }}'
STATIC_URL = '{{ static_url }}'

RAVEN_CONFIG = {
    'dsn': '{{ raven_dsn }}',
}

INSTALLED_APPS += (
    'gunicorn',
)

DEBUG = {{ debug }}
SOCIAL_AUTH_RAISE_EXCEPTIONS = DEBUG
STATIC_ROOT = '{{ project_static }}'
MEDIA_ROOT = '{{ project_media }}'
MEDIA_URL = '{{ project_media_url }}'
CACHEBUST = '?cachebust={{ deploy_ts.stdout }}'

if DEBUG:
    DEBUG_TOOLBAR_PANELS = [
        'debug_toolbar.panels.versions.VersionsPanel',
        'debug_toolbar.panels.timer.TimerPanel',
        'debug_toolbar.panels.settings.SettingsPanel',
        'debug_toolbar.panels.headers.HeadersPanel',
        'debug_toolbar.panels.request.RequestPanel',
        'debug_toolbar.panels.sql.SQLPanel',
        'debug_toolbar.panels.staticfiles.StaticFilesPanel',
        'debug_toolbar.panels.templates.TemplatesPanel',
        'debug_toolbar.panels.cache.CachePanel',
        'debug_toolbar.panels.signals.SignalsPanel',
        'debug_toolbar.panels.logging.LoggingPanel',
        'debug_toolbar.panels.redirects.RedirectsPanel',
    ]
    DEBUG_TOOLBAR_CONFIG = {
        'SHOW_TOOLBAR_CALLBACK': 'inlife.toolbar.show_toolbar',
    }
    MIDDLEWARE_CLASSES += (
        'debug_toolbar.middleware.DebugToolbarMiddleware',
    )

CACHEOPS_REDIS = {
    'host': '{{ redis_host }}',
    'port': {{ redis_port }},
    'db': 1,
    'socket_timeout': 3,
}

# NEO4J
NEO4J_URL = 'http://localhost:{{ neo4j_port}}/db/data/'
NEO4J_LOGIN = '{{ neo4j_login }}'
NEO4J_PASSWORD = '{{ neo4j_password }}'

HAYSTACK_CONNECTIONS = {
    'default': {
        'ENGINE': 'inlife.search_backends.ConfigurableElasticSearchEngine',
        'URL': '{{ haystack_url }}',
        'INDEX_NAME': '{{ haystack_index_name }}',
    },
}
INTERNAL_IPS = ('127.0.0.1',
                '195.64.208.211', '89.31.16.64')
PRINT_TIMINGS = False
IGNORED_TEMPLATES = ["debug_toolbar/*"]

TEMPLATE_LOADERS = (
    ('django.template.loaders.cached.Loader', (
        'django.template.loaders.filesystem.Loader',
        'django.template.loaders.app_directories.Loader',
    )),
)

APPEND_SLASH = False

EMAIL_BACKEND = 'django_ses.SESBackend'
DEFAULT_FROM_EMAIL = u'Achiever - твои достижения <site@achiever.ru>'

AWS_ACCESS_KEY_ID = '{{ aws_access_key_id }}'
AWS_SECRET_ACCESS_KEY = '{{ aws_secret_access_key }}'
AWS_SECRET_ACCESS_KEY_URLENCODED = '{{ aws_secret_access_key_urlencoded }}'
AWS_QUERYSTRING_AUTH = False
AWS_S3_SECURE_URLS = False

# Celery config
CELERY_ACCEPT_CONTENT = ['json']
CELERY_TASK_SERIALIZER = 'json'
BROKER_TRANSPORT_OPTIONS = {
    'region': 'eu-west-1',
    'polling_interval': int('{{ celery_polling_interval }}'),
    'queue_name_prefix': '{{ celery_queue_prefix }}-',
}
BROKER_URL = 'sqs://%s:%s@' % (AWS_ACCESS_KEY_ID, AWS_SECRET_ACCESS_KEY_URLENCODED)
BROKER_TRANSPORT = 'sqs'
CELERY_DEFAULT_QUEUE = 'achiever'
CELERY_QUEUES = {
    CELERY_DEFAULT_QUEUE: {
        'exchange': CELERY_DEFAULT_QUEUE,
        'binding_key': CELERY_DEFAULT_QUEUE,
    }
}
SENTRY_SITE = '{{ server_name }}'

# SOCIAL AUTH
VK_APP_ID = '{{ vk_app_id }}'
VK_API_SECRET = '{{ vk_api_secret }}'
FACEBOOK_APP_ID = '{{ facebook_app_id }}'
FACEBOOK_API_SECRET = '{{ facebook_api_secret }}'
TWITTER_CONSUMER_KEY = '{{ twitter_consumer_key }}'
TWITTER_CONSUMER_SECRET = '{{ twitter_consumer_secret }}'
GOOGLE_OAUTH2_CLIENT_ID = '{{ google_oauth2_client_id }}'
GOOGLE_OAUTH2_CLIENT_SECRET = '{{ google_oauth2_client_secret }}'

# Instagram
INSTAGRAM_CLIENTID = '{{ instagram_clientid }}'
INSTAGRAM_CLIENTSECRET = '{{ instagram_clientsecret }}'
INSTAGRAM_REDIRECTURI = '{{ instagram_redirecturi }}'
