[webservers]
pre.achiever.ru

[webservers:vars]
project_name=pre_achiever
app_port=8002

[haystack]
pre.achiever.ru

[haystack:vars]
haystack_index_name=pre_achiever
haystack_url=http://localhost:9200

[neo4jservers]
pre.achiever.ru

[neo4jservers:vars]
neo4j_port=7476
neo4j_https_port=7475

[appservers]
pre.achiever.ru

[appservers:vars]
debug=False
app_port=8002
project_name=pre_achiever
project_media_url=http://pre.achiever.ru/media/
branch=stable
server_name=pre.achiever.ru
haystack_index_name=test_achiever

# SOCIAL AUTH
vk_app_id = 3492399
vk_api_secret = OMyuR4tZd6CHqpaLxZax
facebook_app_id = 514681578573636
facebook_api_secret = 2211d1c8217e961e2943ce72e86c60ec
twitter_consumer_key = 3v2VctYMOJuHuWV9ZEkJg
twitter_consumer_secret = svqJZJ8JuxLWXJC2CnoUluAFtPqCzbjUFtauxRenvg
google_oauth2_client_id = 335376993573-1jom4jnksiha1hm6liavf6rhueb0ttom.apps.googleusercontent.com
google_oauth2_client_secret = H3b0UCA4YtJDcK5N4GPAubNg

# AWS
aws_access_key_id=AKIAISNXUQ6IIRKDBGGA
aws_secret_access_key=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
aws_secret_access_key_urlencoded = ubrFrVx2QOWX7WmOI6Uy8EpzcT2%2BM%2FzhD65qQ8CJ

# Database
db_name=preprod
db_user=preroot
db_password=c5vOt186pgJ3
db_host=preprod.cplcb3d07due.eu-west-1.rds.amazonaws.com
prod_db_name=achive
prod_db_user=root
prod_db_password=ag9Q1bmysql
prod_db_host=achiever-stable.cplcb3d07due.eu-west-1.rds.amazonaws.com

# Memcache
memcache_host=first.48sfcw.cfg.euw1.cache.amazonaws.com:11211

# Redis
redis_host=test-redis.48sfcw.0001.euw1.cache.amazonaws.com
redis_port=6379

# Celery
celery_polling_interval=10
celery_queue_prefix=pre

# Static files
staticfiles_storage=django.contrib.staticfiles.storage.StaticFilesStorage
file_storage=django.core.files.storage.FileSystemStorage
static_url=/static/

# Sentry
raven_dsn=http://f39be2ae928044d1b91199a2fa57b31a:d4fea97adc344674aebd66fb1fa83999@sentry.achiever.ru/4

#Instagram
instagram_clientid = 'c72e89a0ee294a10af7fc395662e92e4'
instagram_clientsecret = '72f80c3181a64d7aa4e08e1013a677d1'
instagram_redirecturi = 'http://pre.achiever.ru/instagramback/'
