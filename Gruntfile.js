module.exports = function(grunt) {

  // Project configuration.
  grunt.initConfig({
    pkg: grunt.file.readJSON('package.json'),

    clean: {
      build: {
        src: [ 'static/build/js/output.min.js', 'static/css/output.min.css' ]
      }
    },

    uglify: {
      options: {
        banner: '/*! <%= pkg.name %> <%= grunt.template.today("yyyy-mm-dd HH:MM:ss") %> */\n'
      },
      compress : {
          src:  ['static/js/jquery-1.10.2.min.js',
                 'static/js/vendor/**/*.js',
                 'static/js/all.js',
                 'static/js/guide.js',
                 'static/js/scripts.js',
                 'static/js/timeline.js',
                 'static/js/character.js',
                 'static/js/comment.js',
                 'static/js/group_discussion_detail.js',
                 'static/js/conversations.js',
                 'static/js/friends.js',
                 'static/js/header.js',
          ],
          dest: 'static/build/js/output.min.js'
      }
  },
  cssmin: {
  add_banner: {
    options: {
      banner: '/*! <%= pkg.name %> <%= grunt.template.today("yyyy-mm-dd HH:MM:ss") %> */\n'
    },
    src: ['static/css/bootstrap.css',
          'static/css/screen.css',
          'static/js/vendor/fotorama/fotorama.css'
    ]
    ,
    dest: 'static/css/output.min.css'
  }
}
  });


  grunt.loadNpmTasks('grunt-contrib-uglify');
  grunt.loadNpmTasks('grunt-contrib-cssmin');
  grunt.loadNpmTasks('grunt-contrib-clean');

  grunt.registerTask('default', ['clean', 'uglify', 'cssmin']);

};
