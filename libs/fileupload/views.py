import trans
import json
from PIL import Image as pImage
from cStringIO import StringIO

from django.views.generic import CreateView
from django.shortcuts import render
from django.http import HttpResponse, HttpResponseRedirect
from django.utils import simplejson
from django.core.urlresolvers import reverse
from django.core.files.storage import default_storage
from django.core.files.base import ContentFile
from django.conf import settings

from fileupload.models import Picture


def image(request):
    success = False
    f, path = request.FILES.get('tinymce_image_input'), ''
    if getattr(f, 'name', False):
        fname = f.name.encode('trans').replace(" ", "_")
        if fname.split('.')[-1].lower() in ('png', 'jpg', 'jpeg', ):
            im = pImage.open(f)
            w, h = im.size
            w, h = 500, 500 * (h / float(w))
            im.thumbnail((w, h), pImage.ANTIALIAS)
            temp_handle = StringIO()
            im.save(temp_handle, 'png')
            temp_handle.seek(0)
            path = default_storage.save("pictures/" + fname,
                                        ContentFile(temp_handle.read()))
            path = default_storage.base_url + path
            success = True
    return HttpResponse(json.dumps({'success':success, 'image_path': path}),
                        content_type="application/json")


def response_mimetype(request):
    if "application/json" in request.META['HTTP_ACCEPT']:
        return "application/json"
    else:
        return "text/plain"

class PictureCreateView(CreateView):
    model = Picture

    def form_valid(self, form):
        f = self.request.FILES.get('file')
        path = default_storage.save(
            "pictures/" + f.name.replace(" ", "_"), ContentFile(f.read()))

        data = [{'name': f.name,
                 'url': default_storage.base_url + path,
                 'thumbnail_url': default_storage.base_url + path,
                 'delete_url': 'delete'
                 }]

        response = JSONResponse(data, {}, response_mimetype(self.request))
        response['Content-Disposition'] = 'inline; filename=files.json'
        return response


class JSONResponse(HttpResponse):
    """JSON response class."""
    def __init__(self,obj='',json_opts={},mimetype="application/json",*args,**kwargs):
        content = simplejson.dumps(obj,**json_opts)
        super(JSONResponse,self).__init__(content,mimetype,*args,**kwargs)
