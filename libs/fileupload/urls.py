from django.conf.urls import patterns, url
from fileupload.views import PictureCreateView, image

urlpatterns = patterns('',
    (r'^new/$', PictureCreateView.as_view(), {}, 'upload-new'),
    url(r'^image/$', image, name='image'),
)

