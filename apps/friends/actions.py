# coding=utf-8
from django.core.mail import EmailMessage
from apps.friends.models import EmailInvite
from apps.users.models import Friendship


def add(friend, user, response):
    status = user.add_friend(friend)
    if status == 'accepted':
        response['friendship'] = status


def remove(friend, user):
    user.remove_friend(friend)


def invite(email, user):
    # TODO: rewrite with EmailTemplate
    subject = u'Приглашение на Achiver'
    body = u'Ваш друг %s пригалашает Вас присоединиться к http://achiver.ru' % user.get_full_name()
    mail = EmailMessage(subject, body, to=(email,))
    mail.send()
    EmailInvite(sender=user, email=email).save()


def accept(friend, user):
    user.change_friendship_status(friend,
                                  Friendship.WAITING_FOR_ANSWER,
                                  Friendship.ACCEPTED)


def decline(friend, user):
    user.change_friendship_status(friend,
                                  Friendship.WAITING_FOR_ANSWER,
                                  Friendship.DECLINED)


def reject(friend, user):
    user.change_friendship_status(friend,
                                  Friendship.WAITING_FOR_ANSWER,
                                  Friendship.REJECTED)
