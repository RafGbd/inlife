from django.conf.urls import url, patterns

urlpatterns = patterns(
    'apps.friends.views',
    url(r'^$', 'friends', name='friends'),
    url(r'^action/$', 'friends_action', name='friends_action'),
    url(r'^data/$', 'friends_data', name='friends_data'),
    url(r'^search/$', 'friends_search', name='friends_search'),
    url(r'^page/$', 'friends_page', name='friends_page'),
    url(r'^social_auth/(?P<backend>[^/]+)/search/$',
        'social_auth_search',
        name='social_friends_auth_search'),
    url(r'^social_auth/(?P<backend>[^/]+)/invite/$',
        'social_auth_invite',
        name='social_friends_auth_invite'),
    url(r'^social_auth/invite/(?P<backend>[^/]+)/data/$',
        'social_auth_invite_data',
        name='social_auth_invite_data'),
)
