# coding=utf-8
from django.db import models


class EmailInvite(models.Model):
    """
    Высланные email приглашения
    """

    class Meta:
        verbose_name = u'Email приглашение'
        verbose_name_plural = u'Email приглашения'

    sender = models.ForeignKey('users.InLifeUser', related_name='email_invites')
    email = models.EmailField()
