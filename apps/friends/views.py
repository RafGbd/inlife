import json

from django.contrib.auth.decorators import login_required
from django.contrib.auth import get_user_model
from django.core.cache import cache
from django.core.urlresolvers import reverse
from django.db.models import Q
from django.http import HttpResponse, QueryDict
from django.shortcuts import render, redirect
from django.views.decorators.csrf import csrf_exempt
from django.conf import settings
from rest_framework import permissions, renderers
from rest_framework.response import Response
from rest_framework.views import APIView

from social_auth.decorators import dsa_view
from apps.friends.models import EmailInvite
from social_auth.views import auth_process

from apps.app.serializers import FriendsInLifeUserSerializer, InLifeUserSerializer
from apps.users.models import Friendship

from .utils import update_possible_friendships, cache_invite_data, MergedQS, FriendsMergedQS
from . import actions

InLifeUser = get_user_model()

@login_required
def friends(request):
    friendships_provider = request.GET.get('update_friendships', None)
    invite_provider = request.GET.get('invite', None)
    if friendships_provider:
       update_possible_friendships(request.user, friendships_provider)
    elif invite_provider and invite_provider in settings.INVITE_PROVIDERS:
        cache_invite_data(request.user, invite_provider)
    return render(request, 'users/friends.html')


@login_required
def friends_data(request):
    data = {}
    friends = request.user.friends.all().exclude(first_name=u'', last_name=u'')
    friends_serializer = FriendsInLifeUserSerializer(
        friends,
        many=True,
        context={'user': request.user,
                 'status': 'friend'}
    )
    data['friends'] = friends_serializer.data
    friendship_incoming = request.user.friendship_incoming.filter(status=Friendship.WAITING_FOR_ANSWER)
    friendship_offerings = InLifeUser.objects.filter(
        pk__in=[i[0] for i in friendship_incoming.values_list('sender')]
    )
    friendship_offerings_serializer = FriendsInLifeUserSerializer(
        friendship_offerings,
        many=True,
        context={'user': request.user,
                 'status': 'want_to_friendship'}
    )
    data['friendship_offerings'] = friendship_offerings_serializer.data

    friendship_outcoming = request.user.friendship_outcoming.filter(
        status=Friendship.WAITING_FOR_ANSWER).values_list('reciever', flat=True)
    users = FriendsMergedQS(request.user.get_possible_friends(),
                            InLifeUser.objects.get_not_related(request.user))
    users_serializer = FriendsInLifeUserSerializer(users[:settings.PAGINATE_FRIEND],
                                                   many=True,
                                                   context={'user': request.user,
                                                            'status': 'not_friend',
                                                            'friendship_outcoming': friendship_outcoming})
    data['users'] = users_serializer.data
    data['users_count'] = users.count()
    data['users_limit'] = settings.PAGINATE_FRIEND
    return HttpResponse(json.dumps(data), content_type="application/json")


@login_required
def friends_page(request):
    page = request.GET.get('page')
    users = FriendsMergedQS(request.user.get_possible_friends(page=page),
                            InLifeUser.objects.get_not_related(request.user),
                            page)
    data = {}
    friendship_outcoming = request.user.friendship_outcoming.filter(
        status=Friendship.WAITING_FOR_ANSWER).values_list('reciever', flat=True)
    users_serializer = FriendsInLifeUserSerializer(users,
                                                   many=True,
                                                   context={'user': request.user,
                                                            'status':'not_friend',
                                                            'friendship_outcoming': friendship_outcoming})
    data['users'] = users_serializer.data
    return HttpResponse(json.dumps(data), content_type="application/json")


@login_required
def friends_search(request):
    data = {}
    query = request.GET.get('q')
    #active, not stuff, not request user, not already friends
    if query:
        users = InLifeUser.objects.get_not_related(user=request.user)
        users = users.filter(Q(first_name__icontains=query)
                             | Q(middle_name__icontains=query)
                             | Q(last_name__icontains=query))
    else:
        users = MergedQS(InLifeUser.objects.possible_friendships(request.user),
                         InLifeUser.objects.get_not_related(request.user))
    users_serializer = FriendsInLifeUserSerializer(
        users,
        many=True,
        context={'user': request.user,
                 'status': 'not_friend'})
    data['users'] = users_serializer.data
    return HttpResponse(json.dumps(data), content_type="application/json")


@login_required
@csrf_exempt
def friends_action(request):
    if request.method == 'POST':
        response = {}
        action = request.POST.get('action')
        friend_id = request.POST.get('id', request.user.id)
        friend = InLifeUser.objects.get(id=friend_id)

        if action == 'add':
            actions.add(friend, request.user, response)
        elif action == 'accept':
            actions.accept(request.user, friend)
        elif action == 'decline':
            actions.decline(friend, request.user)
        elif action == 'reject':
            actions.reject(friend, request.user)
        elif action == 'remove':
            actions.remove(friend, request.user)
        elif action == 'invite':
            email = request.POST.get('email')
            actions.invite(email, request.user)

        response['success'] = True
        return HttpResponse(json.dumps(response), content_type="application/json")


def add_backend_name_to_args(func):

    def wrapper(request, backend, *args, **kwargs):
        return func(request, backend, backend, *args, **kwargs)

    return wrapper

@add_backend_name_to_args
@dsa_view(getattr(settings,
                  'SOCIAL_AUTH_COMPLETE_URL_NAME',
                  'socialauth_complete'))
def social_auth_search(request, backend, backend_name):
    request.GET = request.GET.copy()
    request.GET['next'] = u'%s?update_friendships=%s' % (reverse('friends'), backend_name)
    return auth_process(request, backend)

@add_backend_name_to_args
@dsa_view(getattr(settings,
                  'SOCIAL_AUTH_COMPLETE_URL_NAME',
                  'socialauth_complete'))
def social_auth_invite(request, backend, backend_name):
    request.GET = request.GET.copy()
    request.GET['next'] = u'%s?invite=%s#%s' % (reverse('friends'), backend_name, backend_name)
    return auth_process(request, backend)


class SocialAuthInviteDataView(APIView):

    renderer_classes = (renderers.JSONRenderer,)
    permission_classes = (permissions.IsAuthenticated,)

    def get(self, request, backend):
        data = cache.get('social_invite_data:%s:%s' % (request.user.pk, backend))
        if not data:
            return Response([])
        inlife_friends = update_possible_friendships(request.user, backend)
        invited_data = EmailInvite.objects.filter(sender=request.user).values('email')
        invited_emails = [d['email'] for d in invited_data]
        result_data = []
        for friend_data in data:
            if friend_data['email'] in invited_emails:
                continue
            try:
                inlife_friend = inlife_friends.get(social_auth__uid__in=friend_data['id'])
                friend_data['url'] = reverse('friends_action')
                friend_data['action'] = 'add'
                friend_data['add_action'] = True
            except InLifeUser.DoesNotExist:
                friend_data['url'] = reverse('friends_action')
                friend_data['action'] = 'invite'
                friend_data['invite_action'] = True
            result_data.append(friend_data)
        return Response(result_data)

social_auth_invite_data = SocialAuthInviteDataView.as_view()

