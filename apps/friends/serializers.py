from rest_framework import serializers


class TwitterUserSerializer(serializers.Serializer):
    id = serializers.Field()
    name = serializers.Field()


class FacebookUserSerializer(serializers.Serializer):
    id = serializers.Field()
    name = serializers.Field()


class FacebookUserSerializerWrapper(object):

    def __init__(self):
        self.serializer = FacebookUserSerializer

    def __call__(self, data):
        #  TODO: add pagination support
        return self.serializer(data['data'])


class GoogleUserSerializer(serializers.Serializer):
    id = serializers.Field(source='ns1_email.address')
    email = serializers.Field(source='ns1_email.address')
    first_name = serializers.Field(source='ns1_givenname')
    last_name = serializers.Field(source='ns1_familyname')
    full_name = serializers.Field(source='ns1_fullname')


class VKUserSerializerMock(object):

    def __init__(self, data):
        self.data = [{'id': x} for x in data]
