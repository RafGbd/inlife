from django.conf import settings
from django.contrib.auth import get_user_model
from django.core.cache import cache
from django.db.models import Q
from inlife import graph

from social_auth.models import UserSocialAuth
from social_friends_finder.backends.facebook_backend import FacebookFriendsProvider
from social_friends_finder.backends.google_backend import GoogleFriendsProvider
from social_friends_finder.backends.twitter_backend import TwitterFriendsProvider
from social_friends_finder.backends.vkontakte_backend import VKontakteFriendsProvider


from .serializers import (TwitterUserSerializer, FacebookUserSerializerWrapper,
                          VKUserSerializerMock, GoogleUserSerializer)

InLifeUser = get_user_model()

SOCIAL_FRIENDSHIP_SETTINGS = {
    'providers': {
        'twitter': TwitterFriendsProvider(),
        'facebook': FacebookFriendsProvider(),
        'vk-oauth': VKontakteFriendsProvider(),
        'google-oauth2': GoogleFriendsProvider(),
    },

    'serializers': {
        'twitter': TwitterUserSerializer,
        'facebook': FacebookUserSerializerWrapper(),
        'vk-oauth': VKUserSerializerMock,
        'google-oauth2': GoogleUserSerializer,
    }

}

def update_possible_friendships(user, backend=None):

    friends_data = fetch_friends_data(user, backend)

    inlife_friends = InLifeUser.objects.filter(
        social_auth__provider=backend,
        social_auth__uid__in=[d['id'] for d in friends_data]
    )
    if inlife_friends:
        query_tpl = """match (u:User{_id: %s}), (f:User)
                       where f._id in [%s]
                       merge (u)-[:SOCIAL_FRIEND{provider: "%s"]->(f)"""
        query = query_tpl % (user.id,
                             ', '.join([f.id for f in inlife_friends]),
                             backend)
        graph.db.query(query)
    user.friends_imported(backend)
    return inlife_friends


def cache_invite_data(user, backend, update=False):
    friends_data = fetch_friends_data(user, backend)
    cache_key = 'social_invite_data:%s:%s' % (user.pk, backend)
    if cache.get(cache_key) and not update:
        return
    cache.set(cache_key,
              filter(lambda f: f['id'], friends_data),
              timeout=24*60*60)

def fetch_friends_data(user, backend):

    config = SOCIAL_FRIENDSHIP_SETTINGS

    try:
        social_account = user.social_auth.all().get(provider=backend)
    except UserSocialAuth.DoesNotExist, e:
        raise e

    if backend == 'facebook':
        social_friends = config['providers'][backend].fetch_friends(social_account, paginate=True)
    else:
        social_friends = config['providers'][backend].fetch_friends(social_account)
    return config['serializers'][backend](social_friends).data


class MergedQS(list):

    def __init__(self, q1, q2, page=1):
        self._q1_count = q1.count()
        self._q2_count = q2.count()

        limit_end = settings.PAGINATE_FRIEND*int(page)
        limit_start = limit_end - settings.PAGINATE_FRIEND

        if limit_start > self._q1_count:
            limit_start = limit_start - self._q1_count
            limit_end = limit_end - self._q1_count
            super(MergedQS, self).__init__(q2[limit_start:limit_end])
        elif limit_end <= self._q1_count:
            super(MergedQS, self).__init__(q1[limit_start:limit_end])
        else:
            limit_end = limit_end - self._q1_count
            result1 = q1[limit_start:]
            result2 = q2[:limit_end]
            q = []
            for r in result1:
                q.append(r)
            for r in result2:
                q.append(r)
            super(MergedQS, self).__init__(q)

    def count(self):
        return self._q2_count + self._q1_count

class FriendsMergedQS(MergedQS):

    def __init__(self, q1, q2, page=1):
        self._q1_count = q1.count()
        self._q2_count = q2.count()

        limit_end = settings.PAGINATE_FRIEND*int(page)
        limit_start = limit_end - settings.PAGINATE_FRIEND

        if limit_start > self._q1_count:
            limit_start = limit_start - self._q1_count
            limit_end = limit_end - self._q1_count
            super(MergedQS, self).__init__(q2[limit_start:limit_end])
        elif limit_end <= self._q1_count:
            super(MergedQS, self).__init__(q1)
        else:
            limit_end = limit_end - self._q1_count
            result1 = list(q1)
            result2 = list(q2[:limit_end])
            q = sum([result1, result2], [])
            sorted_q = sorted(q,
                              key=lambda el: getattr(el, 'mf_count', 0),
                              reverse=True)
            super(MergedQS, self).__init__(sorted_q)
