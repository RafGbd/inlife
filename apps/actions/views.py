# -*- coding: utf-8 -*-
import json
import datetime
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from django.views.generic import ListView, TemplateView, View
from django.shortcuts import HttpResponse, get_object_or_404
from django.contrib.auth import get_user_model
from actions.models import UserAchievementAction
from rest_framework import generics
from rest_framework.response import Response
from app.models import UserAchievement, WatchUserAchievement
from app.serializers import UserAchievementSerializer
from app.mixins import LoginRequiredMixin
from apps.configs import achiever_config_value
from .serializers import PaginatedUserAchievementTabSerializer
InLifeUser = get_user_model()


class ActionsDataMixin(object):

    def dispatch(self, *args, **kwargs):
        self.request_user_id = 0
        if self.request.user.is_authenticated():
            self.request_user_id = self.request.user.id
        return super(ActionsDataMixin, self).dispatch(*args, **kwargs)


    def exclude_hidden(self, query, user):
        """Добавляет фильтр, исключающий скрытые записи таймлайна"""
        if self.request.user != user:
            return query.exclude(is_hidden=True)
        # для владельца запрос остается прежним
        return query

    def mention_tab(self, action, tab):
        """Добавляет таб в массив tabs в экшне"""
        try:
            action['tabs'].append(tab)
        except (KeyError, AttributeError):
            action['tabs'] = [tab]
        return action

    def timeline_queryset(self, qs):
        """Возвращает qs UserAchievement, с презапрошенными данными"""
        qs = qs.filter(
            status__in=[UserAchievement.ACTIVE, UserAchievement.CONFIRMATION,
                        UserAchievement.RECEIVED, UserAchievement.FAILED]
            ).extra(
                select={
                    # добавляем к объектам qs флаг, определяющий, лайкнул request.user данное достижение или нет
                    'request_user_liked': '%s IN (SELECT app_userachievementlike.who_like_id FROM app_userachievementlike WHERE app_userachievementlike.user_achievement_id = app_userachievement.id)',
                }, select_params=[self.request_user_id, ]
            ).extra(
                select={
                    # добавляем к объектам qs кол-во лайков и комментов
                    'likes_count': 'SELECT COUNT(*) FROM app_userachievementlike WHERE app_userachievementlike.user_achievement_id = app_userachievement.id',
                    'comments_count': 'SELECT COUNT(*) FROM app_userachievementcomment WHERE app_userachievementcomment.user_achievement_id = app_userachievement.id',
                    },
            ).select_related(
                'achievement',
                'proof',
                'user'
            ).prefetch_related(
                'proof__photos',
            ).order_by('-date')
        return qs

    def get_paginated_tab_data(self, queryset=None, context={}):
        actions_count = achiever_config_value('news_timeline_num')
        serializer_class = self.get_serializer_class()
        paginator = Paginator(queryset, actions_count)
        page = self.request.QUERY_PARAMS.get('page')
        try:
            items = paginator.page(page)
        except PageNotAnInteger:
            # If page is not an integer, deliver first page.
            items = paginator.page(1)
        except EmptyPage:
            # If page is out of range (e.g. 9999),
            # deliver last page of results.
            items = paginator.page(paginator.num_pages)
        serializer = PaginatedUserAchievementTabSerializer(
            items,
            context=context
        )
        return serializer.data


class JournalDataView(ActionsDataMixin, LoginRequiredMixin, generics.ListAPIView):
    tabs = ('interesting', 'friends', 'all_other', )
    serializer_class = UserAchievementSerializer

    def get_tab_data(self, tab, *args, **kwargs):
        tabs_methods = {
            'interesting': self.interesting_actions_tab_data,
            'friends': self.friends_actions_tab_data,
            'all_other': self.all_users_actions_tab_data,
        }
        if tab not in tabs_methods:
            return

        actions = tabs_methods[tab]()
        return actions

    def get(self, request, *args, **kwargs):
        self.request_user = self.request.user
        self.serializer_context = {'request_user': self.request_user, 'request': self.request}
        data = []
        request_tab = request.GET.get('tab', None)

        if request_tab and request_tab in self.tabs:
            #requesting specific tab
            data.append(self.get_tab_data(request_tab))
        else:
            #requesting data for all tabs
            for tab in self.tabs:
                data.append(self.get_tab_data(tab))  # для каждой вкладки свой метод

        return Response(data)

    def get_friends_ids(self):
        friends_ids = list(self.request.user.friends.filter().values_list('id', flat=True))
        return friends_ids

    def get_interesting_ids(self):
        return list(InLifeUser.objects.filter(is_interesting=True)
                                      .values_list('id', flat=True))

    def to_date(self):
        time_offset = self.request.GET.get('time_offset', None)
        if time_offset:
            date = datetime.datetime.fromtimestamp(int(time_offset) - 1)
        else: 
            date = datetime.datetime.now()
        return date

    def user_achievement(self):
        reg_achievs_recieved = achiever_config_value('post_registration_recieved_achievements')
        ua = UserAchievement.objects.exclude(
            achievement__in=reg_achievs_recieved.all()).select_related()
        return ua

    def interesting_actions_tab_data(self):
        interesting_ids = self.get_interesting_ids()
        seen = list(WatchUserAchievement.objects.filter(user=self.request.user).values_list('user_achievement_id', flat=True))
        achievement_actions = self.user_achievement().filter(user__in=interesting_ids).exclude(pk__in=seen)
        achievement_actions = self.timeline_queryset(achievement_actions)
        WatchUserAchievement.objects.bulk_create(
            [
                WatchUserAchievement(
                    user=self.request.user,
                    user_achievement= user_achievement
                )
                for user_achievement in achievement_actions
            ]
        )

        context = self.serializer_context
        context.update({'tab_id': 'interesting', 'tab_name': u'Интересное'})
        return self.get_paginated_tab_data(queryset=achievement_actions,
                                           context=context)

    def friends_actions_tab_data(self):
        friends_ids = self.get_friends_ids()
        achievement_actions = self.user_achievement().filter(user__in=friends_ids)
        achievement_actions = self.timeline_queryset(achievement_actions)
        
        context = self.serializer_context
        context.update({'tab_id': 'friends', 'tab_name': u'Друзья'})
        return self.get_paginated_tab_data(queryset=achievement_actions,
                                           context=context)
    
    def all_users_actions_tab_data(self):
        friends_ids = self.get_friends_ids()
        friends_ids.append(self.request.user.id)
        achievement_actions = self.user_achievement().filter().exclude(user__in=friends_ids)
        achievement_actions = self.timeline_queryset(achievement_actions)
        context = self.serializer_context
        context.update({'tab_id': 'all_other', 'tab_name': u'Все остальные'})
        return self.get_paginated_tab_data(queryset=achievement_actions,
                                           context=context)


class AchievementsJournal(LoginRequiredMixin, TemplateView):
    template_name = 'news/achievements_journal.html'

    def get_context_data(self, **kwargs):
        context = super(AchievementsJournal, self).get_context_data(**kwargs)
        if self.request.user.is_authenticated():
            context['request_user'] = self.request.user
        context['big_actions_num'] = achiever_config_value('news_timeline_num')
        return context


class JournalCountView(LoginRequiredMixin, View):

    def dispatch(self, request, *args, **kwargs):
        interesting_ids = list(InLifeUser.objects.filter(
                is_interesting=True).values_list('id', flat=True))

        """
        РЕВЬЮ: переменная WatchUserAchievementAction не определена
        """
        # check = WatchUserAchievementAction.objects.filter(
        #         user=self.request.user.id).values_list('action_id', flat=True)
        count = UserAchievementAction.objects.filter(
            user__in=interesting_ids).count()#.exclude(pk__in=check).count()
        return HttpResponse(count)


class TimelineDataView(ActionsDataMixin, generics.ListAPIView):
    #tabs = ('all', 'activated', 'pending', 'recieved', 'categorys') #существующие вкладки. сейчас только "Все", но могут добавиться
    tabs = [('all', u'Все')]
    serializer_class = UserAchievementSerializer

    def get(self, request, *args, **kwargs):
        ach_id = kwargs.get('ach_id')
        self.request_user = self.request.user
        self.user = get_object_or_404(InLifeUser, id=kwargs['user_id'])
        self.serializer_context = {'request_user': self.request_user, 'request': self.request}

        response = [self.get_data(),]
        return Response(response)

    def get_queryset(self):
        achievement_actions = UserAchievement.objects.filter(user=self.user)
        achievement_actions = self.timeline_queryset(achievement_actions)
        achievement_actions = self.exclude_hidden(achievement_actions, self.user)
        return achievement_actions

    def get_data(self):
        context = self.serializer_context
        context.update({'tab_id': 'all', 'tab_name': u'Все'})
        return self.get_paginated_tab_data(queryset=self.get_queryset(),
                                           context=context)


class TimelineTemplateView(TemplateView):
    template_name = 'timeline.html'

    def get_context_data(self, *args, **kwargs):
        context = super(TimelineTemplateView, self).get_context_data(**kwargs)
        user_id = int(kwargs['user_id'])
        user = get_object_or_404(InLifeUser, id=user_id)
        context['user_timeline'] = user
        context['num_comments'] = achiever_config_value('timeline_num_comments')
        return context


class ActionMixin(object):
    def get_action_id(self):
        return int(self.kwargs['action_id'])

    def get_action_class(self):
        action_type = self.kwargs['action_type']
        if action_type == 'ach':
            action_class = UserAchievementAction
        return action_class
    
    def get_action(self):
        cls = self.get_action_class()
        pk = self.get_action_id()
        obj = get_object_or_404(cls, pk=pk)
        return obj


class HideAction(ActionMixin, LoginRequiredMixin, View):
    """
        Скрыть свое достижение из таймлайна.
    """
    def get(self, request, *args, **kwargs):
        action = self.get_action()
        if action.user_id != self.request.user.id:
            return HttpResponse('You can not hide not your action')
        action.is_hidden = True
        action.save()
        return HttpResponse('hidden')


class ShowAction(ActionMixin, LoginRequiredMixin, View):
    def get(self, request, *args, **kwargs):
        action = self.get_action()
        if action.user_id != self.request.user.id:
            return HttpResponse('You can not hide not your action')
        action.is_hidden = False
        action.save()
        return HttpResponse('showed')
