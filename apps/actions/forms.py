# coding: utf-8
from datetime import datetime, timedelta
from django import forms

class ActionCommentForm(forms.Form):
    comment = forms.CharField(widget=forms.TextInput(
        attrs={
            'class': u'add_comment',
            'placeholder':u'написать комментарий',
            'autocomplete': 'off',
        }),
    )