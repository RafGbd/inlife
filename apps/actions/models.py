# -*- coding: utf-8 -*-
from django.db import models
from django.utils import timezone
from django.core.exceptions import ObjectDoesNotExist
from app.const import ACHIEVEMENT_STATUS, SEX, ICON_COLOR, ACTION_VERBS


class Action(models.Model):
    class Meta:
        verbose_name = u'Action'
        verbose_name_plural = u'Actions'
        ordering = ['-date']
        abstract = True

    user = models.ForeignKey('users.InLifeUser')
    action = models.IntegerField(max_length=23, choices=ACTION_VERBS['PERFECT_FORM'].items())
    date = models.DateTimeField(default=timezone.now)

    def is_activated(self):
        return self.action == ACHIEVEMENT_STATUS.ACTIVE

    def is_peinding_confirmation(self):
        return self.action == ACHIEVEMENT_STATUS.CONFIRMATION

    def is_recieved(self):
        return self.action == ACHIEVEMENT_STATUS.RECEIVED

    def is_failed(self):
        return self.action == ACHIEVEMENT_STATUS.FAILED

    def get_action_male(self):
        return ACTION_VERBS[SEX.MALE][self.action]

    def get_action_verb(self):
        return ACTION_VERBS[self.user.sex][self.action]

    def get_perfect_verb(self):
        return ACTION_VERBS['PERFECT_FORM'][self.action]

    def get_past_verb(self):
        return ACTION_VERBS['PAST_FORM'][self.action]

    def get_cname(self):
        return self.__class__.__name__

    def get_proof_annotation(self):
        obj = self
        annotation = {
            'icon_class': u'',
            'data_title': u"",
            'span_class': u'',
        }
        if obj.is_failed():
            annotation['icon_class'] = u'icon-remove-sign'
            annotation['span_class'] = u'icon_sign rejected_sign hint'
            try:
                proof = obj.user_achievement.proof
            except Exception, e:
                proof = None
            if proof:
                #Провалено, потому что админ отклонил подтверждение
                annotation['data_title'] = u"Отклонено"
            else:
                #просто провалено
                annotation['data_title'] = u"Провалено"
        elif obj.is_recieved():
            try:
                proof = obj.user_achievement.proof
                if proof.is_accepted():
                    annotation['icon_class'] = u'icon-ok-sign'
                    annotation['span_class'] = u'icon_sign trusted_sign hint'
                    annotation['data_title'] = u"Подтверждено"
                elif proof.is_doubtfully():
                    annotation['icon_class'] = u'icon-ok-sign'
                    annotation['span_class'] = u'icon_sign not_trusted_sign hint'
                    annotation['data_title'] = u"Сомнительно"
            except Exception, e:
                pass
        return annotation


class UserAchievementAction(Action):
    achievement = models.ForeignKey('app.Achievement')
    user_achievement = models.ForeignKey('app.UserAchievement', blank=True, null=True)
    is_hidden = models.BooleanField('', default=False)

    ACHIEVEMENT_FORMS = {
        ACHIEVEMENT_STATUS.ACTIVE: u'достижение',
        ACHIEVEMENT_STATUS.RECEIVED: u'достижение',
        ACHIEVEMENT_STATUS.FAILED: u'достижение',
        ACHIEVEMENT_STATUS.CONFIRMATION: u'достижения',
        ACHIEVEMENT_STATUS.ADDED: u'достижение',
    }

    class Meta:
        verbose_name = u'Действие с достижением'
        verbose_name_plural = u'Действия с достижениями'
        ordering = ['-date']

    def __unicode__(self):
        try:
            achievement = self.achievement
        except ObjectDoesNotExist:
            achievement = "Not exist, id: " + str(self.achievement_id)
        return u'[%s] %s %s %s' % (self.date, self.user, self.get_action_verb(), achievement,)

    def get_achievement_action_verb(self):
        return u"%s %s" % (self.get_action_verb(), self.ACHIEVEMENT_FORMS[self.action])

    def get_full_achievement_action_verb(self):
        return u"%s %s %s" % (self.user.get_full_name(), self.get_achievement_action_verb(), self.achievement.name)

    def get_proof_comment(self):
        try:
            return self.user_achievement.proof.comment
        except Exception, e:
            return u''
    
    def get_image(self):
        from apps.app.models import Proof
        if self.achievement.created_by_user:
            image = self.achievement.get_image()
            if image:
                return image
        if self.is_recieved() or self.is_peinding_confirmation():
            try:
                if self.user_achievement.proof.is_photo():
                    return self.user_achievement.proof.get_photo()
            except AttributeError:
                pass
            except ObjectDoesNotExist:
                pass
            except Proof.MultipleObjectsReturned:
                proof = Proof.objects.filter(user_achievement=self.user_achievement, proof_type=Proof.P_PHOTO)[0]
                return proof.photo
        return self.achievement.get_image()

    def get_cropping(self):
        if self.is_activated() or self.is_failed() or self.achievement.created_by_user:
            return self.achievement.cropping

    def get_timeline_thumb(self):
        if self.is_recieved() or self.is_peinding_confirmation() or self.is_failed():
            try:
                return self.user_achievement.proof.get_photo_timeline_thumb()
            except Exception, e:
                pass
        return self.achievement.get_timeline_thumb()


    @classmethod
    def create_action(cls, user_achievemnt):
        """Создает событие на снове достижения пользователя"""
        if user_achievemnt.date:
            action_date = user_achievemnt.date
        else:
            action_date = timezone.now()

        UserAchievementAction.objects.get_or_create(user=user_achievemnt.user,
                                             achievement=user_achievemnt.achievement,
                                             user_achievement=user_achievemnt,
                                             action =user_achievemnt.status, 
                                             defaults={'date': action_date})

