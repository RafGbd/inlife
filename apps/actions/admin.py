from django.contrib import admin
from .models import *


class UserAchievementActionAdmin(admin.ModelAdmin):
    list_select_related = True
    readonly_fields = ('achievement', 'user_achievement',)

admin.site.register(UserAchievementAction, UserAchievementActionAdmin)
