from django.conf.urls import patterns, include, url
from django.contrib import admin
from actions.views import TimelineTemplateView, TimelineDataView, \
                          AchievementsJournal, JournalDataView, \
                          JournalCountView


admin.autodiscover()

urlpatterns = patterns('',
    url(r'^journal/$', AchievementsJournal.as_view(), name='journal'),
    url(r'^journal/data/$', JournalDataView.as_view(), name='journal_data'),
    url(r'^journal/count/$', JournalCountView.as_view(), name='journal_count'),
    url(r'^(?P<user_id>\d+)/$', TimelineTemplateView.as_view(), name='timeline'),
    url(r'^(?P<user_id>\d+)/data/$', TimelineDataView.as_view(), name='timeline_data'),
    
    # url(r'^actions/(?P<action_type>\w+)_(?P<action_id>\d+)/like/$', LikeAction.as_view(), name='like_action'),
    # url(r'^actions/(?P<action_type>\w+)_(?P<action_id>\d+)/unlike/$', UnlikeAction.as_view(), name='unlike_action'),
    # url(r'^actions/(?P<action_type>\w+)_(?P<action_id>\d+)/hide/$', HideAction.as_view(), name='hide_action'),
    # url(r'^actions/(?P<action_type>\w+)_(?P<action_id>\d+)/show/$', ShowAction.as_view(), name='show_action'),
    # url(r'^actions/(?P<action_type>\w+)_(?P<action_id>\d+)/delete/$', DeleteAction.as_view(), name='delete_action'),
    # url(r'^actions/(?P<action_type>\w+)_(?P<action_id>\d+)/accept/$', AcceptAction.as_view(), name='accept_action'),
)
