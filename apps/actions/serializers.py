from rest_framework import serializers
from rest_framework.pagination import PaginationSerializer
from rest_framework.templatetags.rest_framework import replace_query_param
from app.serializers import UserAchievementSerializer


class NextPageField(serializers.Field):
    """
    Field that returns a link to the next page in paginated results.
    """
    page_field = 'page'

    def to_native(self, value):
        if not value.has_next():
            return None
        tab_id = self.context.get("tab_id")
        page = value.next_page_number()
        request = self.context.get('request')
        url = request and request.build_absolute_uri() or ''
        url = replace_query_param(url, 'tab', tab_id)
        return replace_query_param(url, self.page_field, page)


class PaginatedUserAchievementTabSerializer(PaginationSerializer):
    results_field = 'items'
    tab_id = serializers.SerializerMethodField('get_tab_id')
    tab_name = serializers.SerializerMethodField('get_tab_name')

    next = NextPageField(source='*')

    def get_tab_id(self, obj):
        return self.context.get("tab_id")

    def get_tab_name(self, obj):
        return self.context.get("tab_name")

    class Meta:
        object_serializer_class = UserAchievementSerializer