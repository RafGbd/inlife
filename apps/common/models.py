# -*- coding: utf-8 -*-
from django.db import models
from mptt.models import MPTTModel, TreeForeignKey
from app.const import EMAIL_EVENT
from inlife.utils import landing_image


class EmailTemplate(models.Model):
    class Meta:
        verbose_name = u'Шаблон письма'
        verbose_name_plural = u'Шаблоны писем'

    def __unicode__(self):
        if self.active:
            return u"%s (Активен): %s" % (self.get_event_display(), self.subject)
        else:
            return u"%s: %s" % (self.get_event_display(), self.subject)

    event = models.IntegerField(u'Событие', choices=EMAIL_EVENT.CHOICES)
    subject = models.CharField(u'Тема', max_length=255)
    body_text = models.TextField(u'Шаблон в plaintext', blank=True, null=True)
    body_html = models.TextField(u'Шаблон в html')
    active = models.BooleanField(u'Активен', help_text=u'Если активен, для выбранного события будет использоваться этот шаблон', default=False)

    def save(self, *args, **kwargs):
        if self.active:
            try:
                email_tpl = EmailTemplate.objects.get(event=self.event, active=True)
            except EmailTemplate.DoesNotExist:
                pass
            else:
                email_tpl.active = False
                email_tpl.save()
        super(EmailTemplate, self).save(*args, **kwargs)


class LandingItem(MPTTModel):
    class Meta:
        verbose_name = u'элемент лэндинга'
        verbose_name_plural = u'элементы лэндинга'

    parent = TreeForeignKey('self', null=True, blank=True, related_name='children')
    title = models.CharField(u'заголовок', max_length=100)
    description = models.TextField(u'описание', blank=True, null=True)
    image = models.ImageField(u'изображение', upload_to=landing_image, blank=True, null=True)

    def __unicode__(self):
        return self.title
