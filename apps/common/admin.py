# coding: utf-8
from django.contrib import admin
from feincms.admin import tree_editor
from .models import EmailTemplate, LandingItem


class EmailTemplateAdmin(admin.ModelAdmin):
    list_filter = ('event', 'active')

admin.site.register(EmailTemplate, EmailTemplateAdmin)
admin.site.register(LandingItem, tree_editor.TreeEditor)