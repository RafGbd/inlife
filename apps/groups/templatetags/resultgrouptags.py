# -*- coding: utf-8 -*-
from django import template
from django.utils.safestring import mark_safe
from django.contrib.admin.templatetags.admin_list import items_for_result,\
    result_list as admin_rl


def result_list(cl):
    results = []
    if cl.model._meta.object_name == 'GroupDiscussion':
        for discussion in cl.result_list:
            results.append(list(items_for_result(cl, discussion, None)))
            for d_reply in discussion.replies.all():
                reply = list(items_for_result(cl, d_reply, None))
                reply[0] = mark_safe('<td></td>')
                reply[1] = mark_safe('<td>%s</td>' % d_reply.__unicode__())
                results.append(reply)
    resp = admin_rl(cl)
    if results:
        resp['results'] = results
    return resp

register = template.Library()
register.inclusion_tag('admin/groups/change_list_results.html')(result_list)
