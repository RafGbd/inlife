from haystack import indexes

from celery_haystack.indexes import CelerySearchIndex

from groups.models import Group


class GroupIndex(CelerySearchIndex, indexes.Indexable):
    text = indexes.EdgeNgramField(document=True, use_template=True)

    rendered = indexes.CharField(use_template=True, indexed=False)
    
    def get_model(self):
        return Group

    def index_queryset(self, using=None):
        """Used when the entire index for model is updated."""
        return self.get_model().objects.all()
