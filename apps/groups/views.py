# -*- coding: utf-8 -*-
import json
import datetime
from dateutil import parser as date_parser

from django.utils import timezone
from django.shortcuts import render, HttpResponse, HttpResponseRedirect
from django.shortcuts import redirect
from django.shortcuts import get_object_or_404
from django.db.models import Q
from django.views.generic import ListView, DetailView, TemplateView, \
    RedirectView, View
from django.views.generic.edit import CreateView, FormView, DeleteView
from django.views.generic.dates import MonthArchiveView, _date_from_string
from django.views.decorators.csrf import csrf_exempt
from django.utils.decorators import method_decorator
from django.http import Http404
from django.core.urlresolvers import reverse
from rest_framework import generics
from rest_framework.response import Response
from users.models import InLifeUser
from app.mixins import LoginRequiredMixin, StaffRequiredMixin
from app.models import Achievement, UserAchievement, Proof
from app.serializers import UserAchievementSerializer
from .models import Group, GroupDiscussion, DiscussionReply, \
    WallAction, AchievementOfMonth, GroupEvent, \
    GroupMemebreship

from .forms import GroupDiscussionCreateForm, DiscussionReplyCreateForm, \
    WallActionCreateForm, AchievementOfMonthCreateForm, \
    GroupEventCreateForm, AchievementOfMonthSelectForm

from .serializers import DiscussionReplySerializer


class GroupMixin(object):
    def get_group(self):
        """возвращает текущую группу"""
        if not hasattr(self, 'group'):
            group_pk = self.kwargs['group_pk']
            self.group = get_object_or_404(Group, pk=group_pk)
        return self.group

    def dispatch(self, *args, **kwargs):
        """
            добавляте в request.user аттрибут is_group_admin - является ли пользователь
            админом текущей группы
        """
        self.request.user.is_group_admin = self.is_admin()
        return super(GroupMixin, self).dispatch(*args, **kwargs)

    def get_context_data(self, **kwargs):
        context = super(GroupMixin, self).get_context_data(**kwargs)
        context['group'] = group = self.get_group()
        context['all_groups'] = Group.objects.all()
        context['group_members'] = group.members.exclude( (Q(first_name=u'') & Q(last_name=u'') )).order_by('?')
        context['group_members_count'] = len(context['group_members'])
        context['group_achievements_count'] = group.wallaction_set.count()
        return context

    def is_admin(self):
        group = self.get_group()
        user = self.request.user
        if group and user.is_authenticated():
            return user.id in group.admins_ids()
        return False


class GroupAdminRequiredMixin(object):
    """ 
        Действия которые может совершать только админ группы
        Использовать вместе с GroupMixin
    """

    def dispatch(self, *args, **kwargs):
        if not self.is_admin():
            raise Http404
        return super(GroupAdminRequiredMixin, self).dispatch(*args, **kwargs)


class GroupsView(LoginRequiredMixin, ListView):
    template_name = 'groups/group_list.html'
    model = Group
    context_object_name = 'group_list'

    def get_queryset(self):
        return Group.objects.filter().prefetch_related('members')


class GroupDetailView(LoginRequiredMixin, GroupMixin, DetailView):
    model = Group
    context_object_name = 'group'
    template_name = 'groups/group_detail.html'

    def get_context_data(self, **kwargs):
        context = super(GroupDetailView, self).get_context_data(**kwargs)
        group = self.get_group()
        context['events'] = group.event_achievements.filter().order_by('-date').select_related()
        context['members'] = group.members.all().order_by('trust')
        return context

    def get_group(self):
        if not hasattr(self, 'group'):
            self.group = self.get_object()
        return self.group


class GroupJoin(LoginRequiredMixin, GroupMixin, DetailView):
    model = Group
    context_object_name = 'group'

    def get_redirect_url(self):
        group = self.get_group()
        return reverse('group_detail', args=[group.pk, ])

    def get(self, request, *args, **kwargs):
        group = self.get_group()
        user = self.request.user
        GroupMemebreship.objects.get_or_create(group=group, user=user)
        return HttpResponseRedirect(self.get_redirect_url())

    def get_group(self):
        if not hasattr(self, 'group'):
            self.group = self.get_object()
        return self.group


class GroupWallData(LoginRequiredMixin, GroupMixin, generics.ListAPIView):
    tabs = ['all']

    def get_queryset(self):
        group = self.get_group()
        actions = group.get_actions()
        return actions.filter(
                status__in=[UserAchievement.ACTIVE, UserAchievement.CONFIRMATION, UserAchievement.RECEIVED, UserAchievement.FAILED]
            ).extra(
                select={
                    'request_user_liked': '%s IN (SELECT app_userachievementlike.who_like_id FROM app_userachievementlike WHERE app_userachievementlike.user_achievement_id = app_userachievement.id)',
                }, select_params=[self.request.user.id, ]
            ).extra(
                select={
                    'likes_count': 'SELECT COUNT(*) FROM app_userachievementlike WHERE app_userachievementlike.user_achievement_id = app_userachievement.id',
                    'comments_count': 'SELECT COUNT(*) FROM app_userachievementcomment WHERE app_userachievementcomment.user_achievement_id = app_userachievement.id',
                },
            ).select_related(
                'achievement',
                'proof',
                'user'
            ).prefetch_related(
                'proof__photos',
            )

    def mention_tab(self, action, tab):
        """Добавляет таб в массив tabs в экшне"""
        try:
            action['tabs'].append(tab)
        except (KeyError, AttributeError):
            action['tabs'] = [tab]
        return action

    def tab_data(self, request, username, tab):
        achievement_actions = self.get_queryset()
        serializer = UserAchievementSerializer(achievement_actions,
                                               many=True,
                                               context={'request': request})
        actions = serializer.data
        return {'items': actions}

    def get(self, request, *args, **kwargs):
        data = []
        for tab in self.tabs:
            data.append(self.tab_data(self.request, self.request.user.username, tab))
        return HttpResponse(json.dumps(data), content_type="application/json")


class AddAchievementOfMonth(LoginRequiredMixin, GroupMixin, GroupAdminRequiredMixin, TemplateView):
    model = AchievementOfMonth
    template_name = 'groups/add_achievement.html'

    def get_context_data(self, **kwargs):
        context = super(AddAchievementOfMonth, self).get_context_data(**kwargs)
        context["group"] = self.get_group()
        select_form = AchievementOfMonthSelectForm(prefix='select')
        select_form.fields["achievement"].queryset = Achievement.objects.created_by_staff()
        context["select_form"] = select_form
        context["create_form"] = AchievementOfMonthCreateForm(prefix='create')
        return context


class SelectAchievementOfMonth(LoginRequiredMixin, GroupMixin, GroupAdminRequiredMixin, CreateView):
    model = AchievementOfMonth
    form_class = AchievementOfMonthSelectForm
    template_name = 'groups/select_achievement_of_month.html'
    prefix = 'select'

    def form_valid(self, form):
        form.instance.group = self.get_group()
        return super(SelectAchievementOfMonth, self).form_valid(form)

    def get_form(self, form_class):
        form = form_class(prefix=self.prefix, **self.get_form_kwargs())
        form.fields["achievement"].queryset = Achievement.objects.created_by_staff()
        return form

    def get_context_data(self, *args, **kwargs):
        context = super(SelectAchievementOfMonth, self).get_context_data(**kwargs)
        context["group"] = self.get_group()
        return context

    def get_success_url(self):
        group = self.get_group()
        return reverse('group_achievement_of_month_list', args=[group.pk, ])


class CreateAchievementOfMonth(LoginRequiredMixin, GroupMixin, GroupAdminRequiredMixin, CreateView):
    model = Achievement
    form_class = AchievementOfMonthCreateForm
    template_name = 'groups/create_achievement_of_month.html'
    prefix = 'create'

    def get_form(self, form_class):
        form = form_class(prefix=self.prefix, **self.get_form_kwargs())
        return form

    def form_valid(self, form):
        result = super(CreateAchievementOfMonth, self).form_valid(form)
        self.object.created_by_user = True
        self.object.save()
        AchievementOfMonth.objects.create(group=self.get_group(), achievement=self.object)
        return result

    def form_invalid(self, form):
        return super(CreateAchievementOfMonth, self).form_invalid(form)

    def get_context_data(self, *args, **kwargs):
        context = super(CreateAchievementOfMonth, self).get_context_data(**kwargs)
        context["group"] = self.get_group()
        return context

    def get_success_url(self):
        group = self.get_group()
        return reverse('group_achievement_of_month_list', args=[group.pk, ])


class AchievementOfMonthList(LoginRequiredMixin, GroupMixin, ListView):
    model = AchievementOfMonth
    context_object_name = 'achievement_of_month_list'
    template_name = 'groups/achievement_of_month_list.html'

    def get_queryset(self):
        group = self.get_group()
        return self.model.objects.filter(group=group).select_related()

    def get_context_data(self, **kwargs):
        context = super(AchievementOfMonthList, self).get_context_data(**kwargs)
        context["group"] = self.get_group()
        return context


class AchievementOfMonthDelete(LoginRequiredMixin, GroupMixin, GroupAdminRequiredMixin, DeleteView):
    model = AchievementOfMonth
    context_object_name = 'achievement_of_month'
    template_name = 'groups/achievement_of_month_delete.html'

    def get_success_url(self):
        group = self.get_group()
        return reverse('group_achievement_of_month_list', args=[group.pk, ])


class SetAchievementOfMonthToGroupFace(LoginRequiredMixin, GroupMixin, GroupAdminRequiredMixin, DetailView):
    model = AchievementOfMonth
    template_name = 'groups/achievement_of_month_set_to_face.html'

    def set_to_face(self, request, *args, **kwargs):
        success_url = self.get_success_url()
        group = self.get_group()
        group.achievements_of_month.all().update(on_face=False)
        achievement_of_month = self.get_object()
        achievement_of_month.on_face = True
        achievement_of_month.save()
        return HttpResponseRedirect(success_url)

    def post(self, request, *args, **kwargs):
        return self.set_to_face(request, *args, **kwargs)

    def get_success_url(self):
        group = self.get_group()
        return reverse('group_detail', args=[group.pk, ])


class AddEvenet(LoginRequiredMixin, GroupMixin, GroupAdminRequiredMixin, CreateView):
    model = Achievement
    template_name = 'groups/add_event.html'
    form_class = GroupEventCreateForm

    def get_context_data(self, **kwargs):
        context = super(AddEvenet, self).get_context_data(**kwargs)
        return context


    def form_valid(self, form):
        event_date = form.cleaned_data['event_date']
        result = super(AddEvenet, self).form_valid(form)
        self.object.created_by_user = True
        self.object.time_to_complete = 30
        self.object.is_event = True
        self.object.is_active = True
        self.object.save()

        GroupEvent.objects.create(
            group=self.get_group(),
            achievement=self.object,
            end_date=self.object.end_date,
            event_date=event_date
        )
        return result

    def form_invalid(self, form, *args, **kwargs):
        return super(AddEvenet, self).form_invalid(form, *args, **kwargs)

    def get_success_url(self):
        group = self.get_group()
        return reverse('group_event_list', args=[group.pk, ])


class EvenetList(LoginRequiredMixin, GroupMixin, MonthArchiveView):
    model = GroupEvent
    date_field = "event_date"
    make_object_list = True
    allow_future = True
    allow_empty = True
    context_object_name = 'event_list'
    template_name = 'groups/event_list.html'

    def get_queryset(self):
        group = self.get_group()
        return GroupEvent.objects.filter(group=group).select_related()

    def get_context_data(self, **kwargs):
        context = super(EvenetList, self).get_context_data(**kwargs)
        context["group"] = self.get_group()
        context["next_month_url"] = self.next_month_url()
        context["previous_month_url"] = self.previous_month_url()
        return context

    def get_year(self):
        """
        Return the year for which this view should display data.
        """
        return (self.year
                or self.kwargs.get('year', None)
                or self.request.GET.get('year', None)
                or datetime.date.today().strftime("%Y"))

    def get_month(self):
        """
        Return the month for which this view should display data.
        """
        return (self.month
                or self.kwargs.get('month', None)
                or self.request.GET.get('month', None)
                or datetime.date.today().strftime("%b"))

    def next_month_url(self):
        year = self.get_year()
        month = self.get_month()

        date_field = self.get_date_field()
        date = _date_from_string(year, self.get_year_format(),
                                 month, self.get_month_format())
        next_month = self.get_next_month(date)
        group = self.get_group()

        return reverse('group_event_list_archive',
                       args=[group.pk, next_month.strftime("%Y"), next_month.strftime("%b")])

    def previous_month_url(self):
        year = self.get_year()
        month = self.get_month()

        date_field = self.get_date_field()
        date = _date_from_string(year, self.get_year_format(),
                                 month, self.get_month_format())
        next_month = self.get_previous_month(date)
        group = self.get_group()

        return reverse('group_event_list_archive',
                       args=[group.pk, next_month.strftime("%Y"), next_month.strftime("%b")])


class EventDelete(LoginRequiredMixin, GroupMixin, GroupAdminRequiredMixin, DeleteView):
    model = GroupEvent
    context_object_name = 'event'
    template_name = 'groups/event_delete.html'

    def get_success_url(self):
        group = self.get_group()
        return reverse('group_event_list', args=[group.pk, ])


class AddActionToGroupWall(LoginRequiredMixin, GroupMixin, CreateView):
    model = WallAction
    template_name = 'groups/add_action_to_wall.html'
    form_class = WallActionCreateForm

    def form_valid(self, form):
        form.instance.group = self.get_group()
        return super(AddActionToGroupWall, self).form_valid(form)

    def get_form(self, form_class):
        form = form_class(**self.get_form_kwargs())
        form.fields["action"].queryset = UserAchievementAction.objects.filter(user=self.request.user)
        return form

    def get_success_url(self):
        group = self.get_group()
        return reverse('group_detail', args=[group.pk, ])


class GroupWallDeleteAction(LoginRequiredMixin, GroupMixin, GroupAdminRequiredMixin, DeleteView):
    response = {"success": True, "message": ""}

    def respond(self):
        return HttpResponse(json.dumps(self.response), content_type="application/json")

    def error(self, message='', *args, **kwargs):
        self.response['success'] = False
        self.response['message'] = message
        return self.respond()

    def get(self, request, *args, **kwargs):
        group_pk = kwargs.get('group_pk')
        action_id = kwargs.get('action_id')

        if not (group_pk and action_id):
            return self.error()

        action_type, pk = action_id.split('_')

        if action_type == 'ach':
            try:
                wall_act = WallAction.objects.get(group_id=group_pk, action_id=pk)
                wall_act.delete()
                return self.respond()
            except Exception, e:
                pass
        return self.error()


class GroupDescussionCreateView(LoginRequiredMixin, GroupMixin, CreateView):
    model = GroupDiscussion
    template_name = 'groups/discussion_create_form.html'
    form_class = GroupDiscussionCreateForm

    def form_valid(self, form):
        form.instance.group = self.get_group()
        form.instance.author = self.request.user
        form.save(commit=True)
        reply_form = DiscussionReplyCreateForm(self.request.POST)
        if reply_form.is_valid():
            reply_form.instance.discussion = form.instance
            reply_form.instance.author = self.request.user
            reply_form.save()
        else:
            return self.form_invalid(form, reply_form)
        return super(GroupDescussionCreateView, self).form_valid(form)

    def form_invalid(self, form, reply_form=None):
        return self.render_to_response(self.get_context_data(form=form, reply_form=reply_form))

    def get_success_url(self):
        group = self.get_group()
        return reverse('group_dicussion', args=[group.pk, self.object.pk])

    def get_context_data(self, **kwargs):
        context = super(GroupDescussionCreateView, self).get_context_data(**kwargs)
        if not context.get('group_reply_form'):
            context['group_reply_form'] = DiscussionReplyCreateForm()
        return context


class GroupDescussionListView(LoginRequiredMixin, GroupMixin, ListView):
    model = GroupDiscussion
    template_name = 'groups/discussion_list.html'
    context_object_name = 'discussion_list'

    def get_context_data(self, **kwargs):
        context = super(GroupDescussionListView, self).get_context_data(**kwargs)
        context['group'] = self.get_group()
        return context


class GroupDescussionDetailData(LoginRequiredMixin, GroupMixin, View):
    def get_queryset(self):
        group = self.get_group()
        discussion_pk = self.kwargs['pk']
        qs = DiscussionReply.objects.filter(discussion_id=discussion_pk).select_related('author')
        date = self.request.GET.get('date', None)
        if date:
            date = date_parser.parse(date)
            qs = qs.filter(date__gt=date)
        return qs

    def get_serializer_data(self):
        queryset = self.get_queryset()
        serializer = DiscussionReplySerializer(queryset, many=True)
        return {'replies': serializer.data}

    def get(self, request, *args, **kwargs):
        data = self.get_serializer_data()
        return HttpResponse(json.dumps(data))


class GroupDescussionDetailView(LoginRequiredMixin, GroupMixin, DetailView):
    model = GroupDiscussion
    context_object_name = 'discussion'
    template_name = 'groups/discussion_detail.html'

    def get_context_data(self, **kwargs):
        context = super(GroupDescussionDetailView, self).get_context_data(**kwargs)
        group = self.get_group()
        context['group'] = group
        context['another_discussions'] = group.discussions.exclude(id=self.object.id)[:3]
        context['reply_form'] = DiscussionReplyCreateForm()
        return context


class GroupDescussionDeletelView(LoginRequiredMixin, GroupMixin, GroupAdminRequiredMixin, DeleteView):
    model = GroupDiscussion
    context_object_name = 'discussion'
    template_name = 'groups/discussion_delete.html'

    def get_success_url(self):
        group = self.get_group()
        return reverse('group_discussions_list', args=[group.pk, ])


class GroupDescussionReplyView(LoginRequiredMixin, GroupMixin, CreateView):
    model = DiscussionReply
    template_name = 'groups/discussion_reply_form.html'
    form_class = DiscussionReplyCreateForm

    @method_decorator(csrf_exempt)
    def dispatch(self, *args, **kwargs):
        return super(GroupDescussionReplyView, self).dispatch(*args, **kwargs)

    def form_valid(self, form):
        form.instance.discussion = self.get_discussion()
        form.instance.author = self.request.user
        form.instance.save()
        if self.request.is_ajax():
            return HttpResponse(json.dumps({'success': True}))
        return super(GroupDescussionReplyView, self).form_valid(form)

    def get_discussion(self):
        if not hasattr(self, 'discussion'):
            discussion_pk = self.kwargs['pk']
            self.discussion = get_object_or_404(GroupDiscussion, pk=discussion_pk)
        return self.discussion

    def get_success_url(self):
        group = self.get_group()
        discussion = self.get_discussion()
        return reverse('group_dicussion', args=[group.pk, discussion.pk])


class GroupDescussionReplyDelete(LoginRequiredMixin, GroupMixin, GroupAdminRequiredMixin, DeleteView):
    model = DiscussionReply
    context_object_name = 'reply'
    template_name = 'groups/discussion_reply_delete.html'

    def get_success_url(self):
        discussion_pk = self.kwargs['disc_pk']
        group = self.get_group()
        return reverse('group_dicussion', args=[group.pk, discussion_pk])
