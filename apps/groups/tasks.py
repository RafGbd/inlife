# -*- coding: utf-8 -*-
from celery import task


@task()
def delete_achievement_of_month(achievement_of_month_id):
    from groups.models import AchievementOfMonth
    try:
        achievement_of_month = AchievementOfMonth.objects.get(
            id=achievement_of_month_id)
        achievement_of_month.delete()
    except AchievementOfMonth.DoesNotExist, e:
        print e
        pass
