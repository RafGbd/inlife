# -*- coding: utf-8 -*-
from datetime import datetime, timedelta
from django.db import models
from django.db.models import Q
from django.utils import timezone
from image_cropping import ImageCropField, ImageRatioField
from easy_thumbnails.files import get_thumbnailer
from easy_thumbnails.exceptions import EasyThumbnailsError
from inlife.utils import group_image
from app.models import UserAchievement
from .tasks import delete_achievement_of_month


class GroupManager(models.Manager):
    def config_list(self):
        return [(group.pk, group.name) for group in self.all()]


class Group(models.Model):
    class Meta:
        verbose_name = u'группа'
        verbose_name_plural = u'группы'

    def __unicode__(self):
        return self.name

    name = models.CharField(u'название', max_length=100)
    info = models.TextField(u'информация', blank=True, null=True)
    actions = models.ManyToManyField('app.UserAchievement', through='WallAction') #события на стене
    members = models.ManyToManyField('users.InLifeUser', blank=True, through='GroupMemebreship', related_name='my_groups')

    image = ImageCropField(verbose_name=u'картинка группы', upload_to=group_image, blank=True, null=True)
    cropping = ImageRatioField('image', '2x3', size_warning=True)

    objects = GroupManager()

    def admins_ids(self):
        return self.groupmemebreship_set.filter(is_admin=True).values_list('user_id', flat=True)

    def face_achievement(self):
        achievement = None
        try:
            achievement = self.achievements_of_month.filter(on_face=True).select_related()[0]
        except IndexError, e:
            try:
                achievement = self.achievements_of_month.filter().select_related()[0]
            except IndexError, e:
                pass
        return achievement

    def get_members(self):
        return self.members.exclude( (Q(first_name=u'') & Q(last_name=u'') ))
    
    def get_actions(self):
        return self.actions.all().order_by('-wallaction__date')

    def get_image_thumb(self):
        if self.image:
            try:
                thumb = get_thumbnailer(self.image).get_thumbnail({
                    'size': (200, 300),
                    'box': self.cropping,
                    'crop': True,
                    'detail': True,
                })
                return thumb
            except EasyThumbnailsError, e:
                #что-то не так с изображением, обнуляем его
                print e
                #self.image = None
                #self.save()
        return None

class GroupMemebreship(models.Model):
    class Meta:
        verbose_name = u'участник группы'
        verbose_name_plural = u'участники группы'
        unique_together = (('group', 'user'),)
        
    group = models.ForeignKey('Group', verbose_name=u'Группа')
    user = models.ForeignKey('users.InLifeUser')
    is_admin = models.BooleanField(u'админ', default=False)
    date = models.DateTimeField(default=timezone.now)


class WallAction(models.Model):
    group = models.ForeignKey('Group', verbose_name=u'Группа')
    action = models.ForeignKey('app.UserAchievement')
    date = models.DateTimeField(default=timezone.now)


class AchievementOfMonthBonus(models.Model):
    name = models.CharField(u'название', max_length=100)
    #implement later


class GroupEvent(models.Model):
    class Meta:
        verbose_name = u'событие группы'
        verbose_name_plural = u'события группы'
        ordering = ['event_date']

    group = models.ForeignKey('Group', verbose_name=u'Группа', related_name='event_achievements')
    achievement = models.ForeignKey('app.Achievement')
    date = models.DateTimeField(default=timezone.now)
    end_date = models.DateTimeField(verbose_name=u'Дата завершения', blank=True, null=True)
    event_date = models.DateTimeField(verbose_name=u'Дата события', blank=True, null=True)


class AchievementOfMonth(models.Model):

    class Meta:
        verbose_name = u'достижение месяца'
        verbose_name_plural = u'достижения месяца'

    group = models.ForeignKey('Group', verbose_name=u'Группа', related_name='achievements_of_month')
    achievement = models.ForeignKey('app.Achievement', verbose_name=u'Достижение')
    bonus = models.ForeignKey('AchievementOfMonthBonus', blank=True, null=True)
    date = models.DateTimeField(default=timezone.now)
    end_date = models.DateTimeField(verbose_name=u'Дата завершения', blank=True, null=True)
    active = models.BooleanField(default=True)
    on_face = models.BooleanField(default=False)

    def count(self):
        return UserAchievement.objects.filter(
            achievement=self.achievement, 
            date__range=[self.date, self.get_end_date()]
        ).count()

    def get_end_date(self):
        if self.end_date:
            return self.end_date
        else:
            return self.date + timedelta(days=30)

    def save(self, *args, **kwargs):
        if self.id is None:
            super(AchievementOfMonth, self).save(*args, **kwargs)
            delete_achievement_of_month.apply_async(args=[self.id,], eta=self.get_end_date())
        else:
            super(AchievementOfMonth, self).save(*args, **kwargs)


class GroupDiscussion(models.Model):
    class Meta:
        verbose_name = u'обсуждение группы'
        verbose_name_plural = u'обсуждения группы'
        ordering = ['-date']
        
    group = models.ForeignKey('Group', verbose_name=u'Группа', related_name='discussions')
    author = models.ForeignKey('users.InLifeUser', verbose_name=u'Автор')
    theme = models.CharField(verbose_name=u'Тема', max_length=100)
    date = models.DateTimeField(default=timezone.now)

    def __unicode__(self):
        return u'Група: %s | Тема: %s' % (self.group.name, self.theme, )


class DiscussionReply(models.Model):
    class Meta:
        verbose_name = u'ответ в обсуждении'
        verbose_name_plural = u'ответы в обсуждении'
        ordering = ['date']

    discussion = models.ForeignKey('GroupDiscussion', related_name='replies')
    author = models.ForeignKey('users.InLifeUser', verbose_name=u'Автор')
    message = models.TextField(blank=True)
    date = models.DateTimeField(default=timezone.now)

    def __unicode__(self):
        return '( %s ) \n %s' % (self.author.get_full_name(), self.message, )
