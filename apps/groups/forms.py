# coding: utf-8
import datetime
from django import forms
from django.utils import timezone
from app.forms import AchievementAddForm
from app.widgets import DatePickerWidget, WYSIWYGTextWidget, IconColorWidget
from app.models import Achievement
from .models import GroupDiscussion, DiscussionReply, \
					WallAction, AchievementOfMonth, \
                    GroupEvent
from .utils import last_day_of_month, tommorow
from inlife.utils import AchieverFormMixin

class GroupDiscussionCreateForm(forms.ModelForm):
    class Meta:
        model = GroupDiscussion
        fields = ('theme',)
    
    theme = forms.CharField(widget=forms.TextInput(
        attrs = {
            'placeholder': u'Тема',
            'class': u"form-control",
        }),
    )


class DiscussionReplyCreateForm(forms.ModelForm):
    class Meta:
        model = DiscussionReply
        fields = ('message', )

    message = forms.CharField(widget=forms.Textarea(
        attrs={
            'rows': 4,
            'cols': 40,
            'class': u"form-control",
        }),
    )


class WallActionCreateForm(forms.ModelForm):
    class Meta:
        model = WallAction
        fields = ('action', )


class AchievementOfMonthCreateForm(AchieverFormMixin, forms.ModelForm):
    class Meta:
        model = Achievement
        fields = ('name', 'description', 'short_decscription', 'end_date', 'cropping', 'image')
    
    
    name = forms.CharField(
        label = u'Достижение',
        widget=forms.TextInput(
            attrs={
                'placeholder': u'Введите название достижения',
                'class': u"form-control",
            }),
        )

    end_date = forms.DateField(
        label = u'Дата завершения',
        widget=DatePickerWidget(
            attrs={
                'class': u"form-control",
                'data-date-format': u"dd.mm.yyyy"
            }), 
        required=False,
        initial=last_day_of_month()
        
     )

    short_decscription = forms.CharField(
        label = u'Краткое описание',
        widget=forms.Textarea(
            attrs={
                'rows': 4,
                'cols': 40,
                'class': u"form-control",
            }),
        required=False,
    )
    
    description = forms.CharField(
        label = u'Описание',
        widget=WYSIWYGTextWidget(
            attrs={
                'rows': 14,
                'cols': 40,
                'class': u"form-control",
            }),
        required=False,
    )

    cropping = forms.CharField(widget=forms.HiddenInput(
        attrs={
            'class': u"crop_field_class",
        })
    )

class AchievementOfMonthSelectForm(AchieverFormMixin, forms.ModelForm):
    class Meta:
        model = AchievementOfMonth
        fields = ('achievement', 'end_date')

    end_date = forms.DateField(
        label = u'Дата завершения',
        widget=DatePickerWidget(
            attrs={
                'class': u"form-control",
                'data-date-format': u"dd.mm.yyyy"
            }), 
        required=False,
        initial=last_day_of_month()
     )


class GroupEventCreateForm(AchieverFormMixin, forms.ModelForm):
    class Meta:
        model = Achievement
        fields = ('name', 'description', 'short_decscription', 'event_date', 'end_date', 'cropping', 'image')
    
    
    name = forms.CharField(
        label = u'Достижение',
        widget=forms.TextInput(
            attrs={
                'placeholder': u'Введите название достижения',
                'class': u"form-control",
            }),
    )
    

    event_date = forms.DateField(
        label = u'Дата события',
        widget=DatePickerWidget(
            attrs={
                'class': u"form-control",
                'data-date-format': u"dd.mm.yyyy"
            }), 
        required=True,
        #initial=tommorow()
        
     )

    end_date = forms.DateField(
        label = u'Дата завершения',
        widget=DatePickerWidget(
            attrs={
                'class': u"form-control",
                'data-date-format': u"dd.mm.yyyy"
            }), 
        required=True,
        initial=tommorow()
        
     )

    short_decscription = forms.CharField(
        label = u'Краткое описание',
        widget=forms.Textarea(
            attrs={
                'rows': 4,
                'cols': 40,
                'class': u"form-control",
            }),
        required=False,
    )
    
    description = forms.CharField(
        label = u'Описание',
        widget=WYSIWYGTextWidget(
            attrs={
                'rows': 14,
                'cols': 40,
                'class': u"form-control",
            }),
        required=False,
    )

    cropping = forms.CharField(widget=forms.HiddenInput(
        attrs={
            'class': u"crop_field_class",
        })
    )


    def clean_event_date(self):
        event_date = self.cleaned_data['event_date']
        if event_date:
            if event_date < datetime.date.today():
                raise forms.ValidationError(u'Дата события не должна быть раньше сегодняшнего дня')
        return event_date


    def clean_end_date(self):
        event_date = self.cleaned_data.get('event_date')
        end_date = self.cleaned_data.get('end_date')
        if end_date and event_date:
            if end_date < datetime.date.today():
                raise forms.ValidationError(u'Дата завершения события не должна быть раньше сегодняшнего дня')
            if end_date < event_date:
                raise forms.ValidationError(u'Дата завершения события не может быть раньше даты события')
        return end_date


