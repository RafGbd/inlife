# -*- coding: utf-8 -*-
from django.utils.html import linebreaks
from django.utils.dateformat import DateFormat
from django.core.urlresolvers import reverse
from rest_framework import serializers
from django.contrib.auth import get_user_model
from apps.configs import achiever_config_value
from .models import DiscussionReply

InLifeUser = get_user_model()


class DiscussionReplyAuthorSerializer(serializers.ModelSerializer):
    avatar = serializers.SerializerMethodField('get_avatar')
    avatar_class = serializers.SerializerMethodField('get_avatar_class')
    full_name = serializers.SerializerMethodField('get_full_name')
    link = serializers.SerializerMethodField('get_link')

    def get_avatar_class(self, obj):
        return 'avatar ' + obj.avatar_class()

    def get_avatar(self, obj):
        thumb = obj.get_cropped_avatar()
        if not thumb:
            thumb = achiever_config_value('default_user_avatar')
        return thumb.url

    def get_full_name(self, obj):
        return obj.get_full_name()

    def get_link(self, obj):
        return reverse('timeline', args=(obj.id,))

    class Meta:
        model = InLifeUser
        fields = ('id', 'full_name', 'first_name', 'last_name', 'avatar',
                  'avatar_class', 'link',)


class DiscussionReplySerializer(serializers.ModelSerializer):
    author = DiscussionReplyAuthorSerializer()
    date = serializers.DateTimeField(format='iso-8601')
    date_repr = serializers.SerializerMethodField('get_date_repr')
    message = serializers.SerializerMethodField('get_message')

    def get_message(self, obj):
        return linebreaks(obj.message)

    def get_date_repr(self, obj):
        date = DateFormat(obj.date)
        return '%s %s, %s:%s' % (date.format('j'), date.format('E'), date.format('H'), date.format('i'))

    class Meta:
        model = DiscussionReply
        fields = ('id', 'message', 'date', 'date_repr', 'author')