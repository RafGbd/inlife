# -*- coding: utf-8 -*-
from django.contrib import admin
from django.conf import settings
from django.core.urlresolvers import reverse
from image_cropping import ImageCroppingMixin
from .models import Group, GroupDiscussion, DiscussionReply, WallAction, \
    AchievementOfMonth, GroupMemebreship, GroupEvent


class DeleteMixin(object):

    def delete_link(self, obj):
        info = obj._meta.app_label, obj._meta.module_name
        url = reverse('admin:%s_%s_delete' % info, args=(obj.id,))
        return '<a href="%s" class="deletelink">Delete</a>' % url

    delete_link.allow_tags = True
    delete_link.short_description = 'Delete'


class GroupMemebreshipInlineAdmin(admin.TabularInline):
    model = GroupMemebreship
    extra = 2


class GroupAdmin(ImageCroppingMixin, admin.ModelAdmin):
    inlines = [GroupMemebreshipInlineAdmin, ]


class DiscussionReplyAdmin(DeleteMixin, admin.ModelAdmin):
    list_display = ('discussion', 'author', 'date', 'delete_link',)
    list_filter = ('discussion', 'date', 'author',)


class DiscussionReplyInlineAdmin(admin.TabularInline):
    model = DiscussionReply
    extra = 0


class GroupDiscussionAdmin(DeleteMixin, admin.ModelAdmin):
    inlines = [DiscussionReplyInlineAdmin, ]
    list_display = ('__unicode__', 'delete_link',)


class GroupEventAdmin(DeleteMixin, admin.ModelAdmin):
    list_display = ('group', 'achievement', 'date', 'end_date', 'event_date',
                    'delete_link',)

admin.site.register(Group, GroupAdmin)
admin.site.register(GroupDiscussion, GroupDiscussionAdmin)
admin.site.register(DiscussionReply, DiscussionReplyAdmin)
admin.site.register(GroupEvent, GroupEventAdmin)
admin.site.register(WallAction)
admin.site.register(AchievementOfMonth)
