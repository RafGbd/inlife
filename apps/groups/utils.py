import calendar
import datetime
from django.utils import timezone

def last_day_of_month(date=None):
    if not date:
        date = timezone.now()
    last_day = calendar.monthrange(date.year, date.month)[1]
    return datetime.date(year=date.year, month=date.month, day=last_day)

def tommorow():
    today = datetime.date.today()
    return today + datetime.timedelta(days=1)