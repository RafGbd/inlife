# -*- coding: utf-8 -*-

__author__ = 'katya'
from django.db import models


class GuideStep(models.Model):
    class Meta:
        verbose_name = u'Шаг обучалки'
        verbose_name_plural = u'Шаги обучалки'

    number = models.PositiveIntegerField(unique=True)
    url = models.CharField(max_length=255)
    is_url = models.BooleanField(default=False)
    user_achievement = models.ForeignKey('app.UserAchievement', blank=True, null=True)
    immediately_next = models.BooleanField(default=False)
    finish = models.BooleanField(default=False)
    def __unicode__(self):
        return str(self.number)


class GuideUser(models.Model):
    class Meta:
        verbose_name = u'Обучалка пользователя'
        verbose_name_plural = u'Обучалка пользователей'

    start = models.DateTimeField(auto_now_add=True)
    user = models.OneToOneField('users.InLifeUser', related_name='guide')
    step = models.ForeignKey(GuideStep)
    is_finished = models.BooleanField(default=False)
    is_close = models.BooleanField(default=False)

    def __unicode__(self):
        return self.user.username

    def next_step(self):
        if self.step.finish:
            self.is_finished = True
        else:
            new_step_number = self.step.number + 1
            self.step = GuideStep.objects.get(number=new_step_number)
            if self.step.immediately_next:
                self.next_step()
        self.is_close = False
        self.save()
        return self.step