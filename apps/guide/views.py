# -*- coding: utf-8 -*-
from django.http import HttpResponse
from guide.models import GuideUser
import json

__author__ = 'katya'


def next_step(request):
    user_id = request.GET.get('user_id', None)
    step = int(request.GET.get('step', 0))
    if user_id and step > 0:
        guide = GuideUser.objects.get(user__id=user_id)
        if guide.step.number == step:
            step = guide.next_step()
            if guide.is_finished:
                response = {}
            else:
                response = {'step':step.number}
            if step.is_url:
                response['url'] = step.url
            elif step.number == 4:
                response['category_bar'] = True
            else:
                pass
            return HttpResponse(json.dumps(response), content_type='application/json')
        else:
            raise Exception("bad step")

    else:
        raise Exception("get error")


def guide_close(request):
    user_id = request.GET.get('user_id', None)
    if user_id:
        guide = GuideUser.objects.get(user__id=user_id)
        # guide.next_step()
        guide.is_close = True
        guide.save()
        return HttpResponse(json.dumps({'result': True}), content_type='application/json')

    else:
        raise Exception("get error")