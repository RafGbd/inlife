# -*- coding: utf-8 -*-

__author__ = 'katya'
from django.contrib import admin
from django import forms
from guide.models import GuideStep, GuideUser
from chosen import forms as chosenforms
from app.models import UserAchievement


class GuideStepAdminForm(forms.ModelForm):
    user_achievement = chosenforms.ChosenModelChoiceField(
        label=u'достижение',
        help_text=u'Полученое достижение на 3м шаге',
        required=False,
        queryset=UserAchievement.objects.all())

    class Meta:
        model = GuideStep


class GuideStepAdmin(admin.ModelAdmin):
    form = GuideStepAdminForm


class GuideUserAdmin(admin.ModelAdmin):
    pass


admin.site.register(GuideUser, GuideUserAdmin)
admin.site.register(GuideStep, GuideStepAdmin)