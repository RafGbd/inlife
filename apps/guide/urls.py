from django.conf.urls import patterns, include, url
from django.contrib import admin


admin.autodiscover()

urlpatterns = patterns('',
                       url(r'^next_step/$', 'guide.views.next_step', name='next_step'),
                       url(r'^guide_close/$', 'guide.views.guide_close', name='guide_close'),

)