from django.conf.urls import patterns, url
from .views import (SectionDetail,
                    CategoryDetail,
                    CategoryDetailData,
                    )


urlpatterns = patterns(
    'apps.achievements.views',
    url(r'^section/(?P<pk>\d+)/$',
        SectionDetail.as_view(),
        name='section_detail'),
    url(r'^category/(?P<pk>\d+)/$',
        CategoryDetail.as_view(),
        name='category_detail'),
    url(r'^category/(?P<pk>\d+)/data/$',
        CategoryDetailData.as_view(),
        name='category_detail_data'),
)
