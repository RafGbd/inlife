# -*- coding: utf-8 -*-
from django.views.generic import (DetailView, )
from rest_framework import generics
from rest_framework.response import Response
from apps.app.models import (Category, Achievement)
from .serializers import CategorySerializer


class SectionDetail(DetailView):
    """меню раздела - список подкатегорий"""
    model = Category
    context_object_name = 'section'
    template_name = 'section_detail.html'
    queryset = Category.objects.filter(level=0)

    def get_context_data(self, **kwargs):
        context = super(SectionDetail, self).get_context_data(**kwargs)
        context['subsections'] = self.object.get_descendants().filter(level__lt=3)
        return context


class CategoryDetail(DetailView):
    model = Category
    context_object_name = 'category'
    template_name = 'category_detail.html'
    queryset = Category.objects.filter(level=2)


class CategoryDetailData(generics.ListAPIView):
    serializer_class = CategorySerializer
    response = {"children": []}

    def get_queryset(self):
        parent_pk = self.kwargs['pk']
        return Category.objects.filter(
            parent_id=parent_pk
            ).prefetch_related('achievements')

    def get(self, reqest, *args, **kwargs):
        self.queryset = self.get_queryset()
        serializer = self.get_serializer(self.queryset, many=True)
        self.response["children"] = serializer.data
        return Response(self.response)
