from rest_framework import serializers
from apps.app.models import (Category, Achievement)


class CategoryAchievementSerializer(serializers.ModelSerializer):
    url = serializers.SerializerMethodField('get_url')

    def get_url(self, obj):
        return obj.get_absolute_url()

    class Meta:
        model = Achievement
        fields = ('name', 'url',)


class CategorySerializer(serializers.ModelSerializer):
    parent = serializers.PrimaryKeyRelatedField()
    achievements = CategoryAchievementSerializer(many=True)
    image = serializers.SerializerMethodField('get_image')

    def get_image(self, obj):
        return obj.image.url if obj.image else None

    class Meta:
        model = Category
        fields = ('id', 'name', 'info', 'parent', 'is_disabled', 'achievements', 'image')