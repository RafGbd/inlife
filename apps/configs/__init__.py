# -*- coding: utf-8 -*-


def achiever_config():
    """return AchieverConfig model instance"""
    from apps.configs.models import AchieverConfig
    return AchieverConfig.get_solo()


def achiever_config_value(key):
    """return konfig value from AchieverConfig model"""
    obj = achiever_config()
    return getattr(obj, key)
