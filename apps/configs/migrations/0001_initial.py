# -*- coding: utf-8 -*-
import datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding model 'AchieverConfig'
        db.create_table(u'configs_achieverconfig', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('default_achievement_image', self.gf('django.db.models.fields.files.ImageField')(default='/static/img/timeline_pic.png', max_length=100)),
            ('default_user_avatar', self.gf('django.db.models.fields.files.ImageField')(default='/static/img/avatar.jpg', max_length=100)),
            ('news_timeline_num', self.gf('django.db.models.fields.IntegerField')(default=10)),
            ('news_day_num', self.gf('django.db.models.fields.IntegerField')(default=10)),
            ('sidebar_groups_num', self.gf('django.db.models.fields.IntegerField')(default=10)),
            ('email_for_register', self.gf('django.db.models.fields.EmailField')(default='reg@example.com', max_length=75)),
            ('site_closed', self.gf('django.db.models.fields.BooleanField')(default=False)),
            ('timeline_num_comments', self.gf('django.db.models.fields.IntegerField')(default=3)),
            ('instagram_num_photos', self.gf('django.db.models.fields.IntegerField')(default=10)),
            ('message_from_admin_author', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['users.InLifeUser'], null=True, blank=True)),
            ('message_from_admin', self.gf('django.db.models.fields.TextField')(default=u'\u0414\u043e\u0431\u0440\u043e \u043f\u043e\u0436\u0430\u043b\u043e\u0432\u0430\u0442\u044c \u043d\u0430 \u043f\u043e\u0440\u0442\u0430\u043b!', blank=True)),
        ))
        db.send_create_signal(u'configs', ['AchieverConfig'])

        # Adding M2M table for field post_registration_recieved_achievements on 'AchieverConfig'
        db.create_table(u'configs_achieverconfig_post_registration_recieved_achievements', (
            ('id', models.AutoField(verbose_name='ID', primary_key=True, auto_created=True)),
            ('achieverconfig', models.ForeignKey(orm[u'configs.achieverconfig'], null=False)),
            ('achievement', models.ForeignKey(orm[u'app.achievement'], null=False))
        ))
        db.create_unique(u'configs_achieverconfig_post_registration_recieved_achievements', ['achieverconfig_id', 'achievement_id'])

        # Adding M2M table for field registration_groups on 'AchieverConfig'
        db.create_table(u'configs_achieverconfig_registration_groups', (
            ('id', models.AutoField(verbose_name='ID', primary_key=True, auto_created=True)),
            ('achieverconfig', models.ForeignKey(orm[u'configs.achieverconfig'], null=False)),
            ('group', models.ForeignKey(orm[u'groups.group'], null=False))
        ))
        db.create_unique(u'configs_achieverconfig_registration_groups', ['achieverconfig_id', 'group_id'])

        # Adding model 'ApiConfig'
        db.create_table(u'configs_apiconfig', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('PhotoSize', self.gf('django.db.models.fields.IntegerField')(default=1024)),
            ('CompressionQuality', self.gf('django.db.models.fields.IntegerField')(default=80)),
            ('ReportStorageDaysDuration', self.gf('django.db.models.fields.IntegerField')(default=14)),
            ('GPSPollingInterval', self.gf('django.db.models.fields.IntegerField')(default=5)),
            ('RequireGPSLocation', self.gf('django.db.models.fields.BooleanField')(default=False)),
            ('UseGeocoder', self.gf('django.db.models.fields.BooleanField')(default=False)),
        ))
        db.send_create_signal(u'configs', ['ApiConfig'])


    def backwards(self, orm):
        # Deleting model 'AchieverConfig'
        db.delete_table(u'configs_achieverconfig')

        # Removing M2M table for field post_registration_recieved_achievements on 'AchieverConfig'
        db.delete_table('configs_achieverconfig_post_registration_recieved_achievements')

        # Removing M2M table for field registration_groups on 'AchieverConfig'
        db.delete_table('configs_achieverconfig_registration_groups')

        # Deleting model 'ApiConfig'
        db.delete_table(u'configs_apiconfig')


    models = {
        u'app.achievement': {
            'Meta': {'object_name': 'Achievement'},
            'author': ('django.db.models.fields.related.ForeignKey', [], {'blank': 'True', 'related_name': "'created_achievements'", 'null': 'True', 'to': u"orm['users.InLifeUser']"}),
            'bonuses': ('django.db.models.fields.related.ManyToManyField', [], {'to': u"orm['users.MetricBonus']", 'symmetrical': 'False', 'blank': 'True'}),
            'created_by_admin': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'created_by_user': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'cropping': ('django.db.models.fields.CharField', [], {'max_length': '255', 'blank': 'True'}),
            'date': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now'}),
            'description': ('ckeditor.fields.RichTextField', [], {'blank': 'True'}),
            'end_date': ('django.db.models.fields.DateTimeField', [], {'null': 'True', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'image': ('django.db.models.fields.files.ImageField', [], {'max_length': '100', 'null': 'True', 'blank': 'True'}),
            'interests': ('django.db.models.fields.related.ManyToManyField', [], {'to': u"orm['app.Interest']", 'symmetrical': 'False', 'blank': 'True'}),
            'is_event': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '50'}),
            'points_weight': ('django.db.models.fields.IntegerField', [], {'default': '0'}),
            'short_decscription': ('django.db.models.fields.TextField', [], {'blank': 'True'}),
            'stars_number': ('django.db.models.fields.IntegerField', [], {'default': '1'})
        },
        u'app.category': {
            'Meta': {'object_name': 'Category'},
            'achievements': ('django.db.models.fields.related.ManyToManyField', [], {'symmetrical': 'False', 'to': u"orm['app.Achievement']", 'null': 'True', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'image': ('django.db.models.fields.files.ImageField', [], {'max_length': '100', 'null': 'True', 'blank': 'True'}),
            'info': ('tinymce.models.HTMLField', [], {'blank': 'True'}),
            'is_disabled': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'level': ('django.db.models.fields.PositiveIntegerField', [], {'db_index': 'True'}),
            'lft': ('django.db.models.fields.PositiveIntegerField', [], {'db_index': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'parent': ('mptt.fields.TreeForeignKey', [], {'blank': 'True', 'related_name': "'children'", 'null': 'True', 'to': u"orm['app.Category']"}),
            'rght': ('django.db.models.fields.PositiveIntegerField', [], {'db_index': 'True'}),
            'svg_icon': ('django.db.models.fields.files.FileField', [], {'max_length': '100', 'null': 'True', 'blank': 'True'}),
            'tree_id': ('django.db.models.fields.PositiveIntegerField', [], {'db_index': 'True'})
        },
        u'app.interest': {
            'Meta': {'object_name': 'Interest'},
            'achievements': ('django.db.models.fields.related.ManyToManyField', [], {'to': u"orm['app.Achievement']", 'symmetrical': 'False', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '150'})
        },
        u'app.userachievement': {
            'Meta': {'unique_together': "(('user', 'achievement'),)", 'object_name': 'UserAchievement'},
            'accepted_by_user': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'achievement': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['app.Achievement']"}),
            'activate_time': ('django.db.models.fields.DateTimeField', [], {'null': 'True', 'blank': 'True'}),
            'comment': ('django.db.models.fields.CharField', [], {'max_length': '1024', 'null': 'True', 'blank': 'True'}),
            'date': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now', 'db_index': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'intensity': ('django.db.models.fields.IntegerField', [], {'null': 'True', 'blank': 'True'}),
            'is_derivative_page_viewed': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'is_hidden': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'recieve_time': ('django.db.models.fields.DateTimeField', [], {'null': 'True', 'blank': 'True'}),
            'status': ('django.db.models.fields.IntegerField', [], {'default': '1', 'db_index': 'True'}),
            'user': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'achievements'", 'to': u"orm['users.InLifeUser']"})
        },
        u'auth.group': {
            'Meta': {'object_name': 'Group'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '80'}),
            'permissions': ('django.db.models.fields.related.ManyToManyField', [], {'to': u"orm['auth.Permission']", 'symmetrical': 'False', 'blank': 'True'})
        },
        u'auth.permission': {
            'Meta': {'ordering': "(u'content_type__app_label', u'content_type__model', u'codename')", 'unique_together': "((u'content_type', u'codename'),)", 'object_name': 'Permission'},
            'codename': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'content_type': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['contenttypes.ContentType']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '50'})
        },
        u'configs.achieverconfig': {
            'Meta': {'object_name': 'AchieverConfig'},
            'default_achievement_image': ('django.db.models.fields.files.ImageField', [], {'default': "'/static/img/timeline_pic.png'", 'max_length': '100'}),
            'default_user_avatar': ('django.db.models.fields.files.ImageField', [], {'default': "'/static/img/avatar.jpg'", 'max_length': '100'}),
            'email_for_register': ('django.db.models.fields.EmailField', [], {'default': "'reg@example.com'", 'max_length': '75'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'instagram_num_photos': ('django.db.models.fields.IntegerField', [], {'default': '10'}),
            'message_from_admin': ('django.db.models.fields.TextField', [], {'default': "u'\\u0414\\u043e\\u0431\\u0440\\u043e \\u043f\\u043e\\u0436\\u0430\\u043b\\u043e\\u0432\\u0430\\u0442\\u044c \\u043d\\u0430 \\u043f\\u043e\\u0440\\u0442\\u0430\\u043b!'", 'blank': 'True'}),
            'message_from_admin_author': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['users.InLifeUser']", 'null': 'True', 'blank': 'True'}),
            'news_day_num': ('django.db.models.fields.IntegerField', [], {'default': '10'}),
            'news_timeline_num': ('django.db.models.fields.IntegerField', [], {'default': '10'}),
            'post_registration_recieved_achievements': ('django.db.models.fields.related.ManyToManyField', [], {'symmetrical': 'False', 'to': u"orm['app.Achievement']", 'null': 'True', 'blank': 'True'}),
            'registration_groups': ('django.db.models.fields.related.ManyToManyField', [], {'symmetrical': 'False', 'to': u"orm['groups.Group']", 'null': 'True', 'blank': 'True'}),
            'sidebar_groups_num': ('django.db.models.fields.IntegerField', [], {'default': '10'}),
            'site_closed': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'timeline_num_comments': ('django.db.models.fields.IntegerField', [], {'default': '3'})
        },
        u'configs.apiconfig': {
            'CompressionQuality': ('django.db.models.fields.IntegerField', [], {'default': '80'}),
            'GPSPollingInterval': ('django.db.models.fields.IntegerField', [], {'default': '5'}),
            'Meta': {'object_name': 'ApiConfig'},
            'PhotoSize': ('django.db.models.fields.IntegerField', [], {'default': '1024'}),
            'ReportStorageDaysDuration': ('django.db.models.fields.IntegerField', [], {'default': '14'}),
            'RequireGPSLocation': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'UseGeocoder': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'})
        },
        u'contenttypes.contenttype': {
            'Meta': {'ordering': "('name',)", 'unique_together': "(('app_label', 'model'),)", 'object_name': 'ContentType', 'db_table': "'django_content_type'"},
            'app_label': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'model': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '100'})
        },
        u'groups.group': {
            'Meta': {'object_name': 'Group'},
            'actions': ('django.db.models.fields.related.ManyToManyField', [], {'to': u"orm['app.UserAchievement']", 'through': u"orm['groups.WallAction']", 'symmetrical': 'False'}),
            'cropping': ('django.db.models.fields.CharField', [], {'max_length': '255', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'image': ('django.db.models.fields.files.ImageField', [], {'max_length': '100', 'null': 'True', 'blank': 'True'}),
            'info': ('django.db.models.fields.TextField', [], {'null': 'True', 'blank': 'True'}),
            'members': ('django.db.models.fields.related.ManyToManyField', [], {'symmetrical': 'False', 'related_name': "'my_groups'", 'blank': 'True', 'through': u"orm['groups.GroupMemebreship']", 'to': u"orm['users.InLifeUser']"}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '100'})
        },
        u'groups.groupmemebreship': {
            'Meta': {'unique_together': "(('group', 'user'),)", 'object_name': 'GroupMemebreship'},
            'date': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now'}),
            'group': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['groups.Group']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'is_admin': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'user': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['users.InLifeUser']"})
        },
        u'groups.wallaction': {
            'Meta': {'object_name': 'WallAction'},
            'action': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['app.UserAchievement']"}),
            'date': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now'}),
            'group': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['groups.Group']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'})
        },
        u'users.inlifeuser': {
            'Meta': {'object_name': 'InLifeUser'},
            'birthdate': ('django.db.models.fields.DateField', [], {'null': 'True', 'blank': 'True'}),
            'char_to_premium': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['app.Category']", 'null': 'True', 'blank': 'True'}),
            'cropping': ('django.db.models.fields.CharField', [], {'max_length': '255', 'blank': 'True'}),
            'date_joined': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now', 'blank': 'True'}),
            'email': ('django.db.models.fields.EmailField', [], {'max_length': '254', 'null': 'True', 'blank': 'True'}),
            'facebook_friends_imported': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'first_name': ('django.db.models.fields.CharField', [], {'max_length': '30', 'blank': 'True'}),
            'first_view': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'friends': ('django.db.models.fields.related.ManyToManyField', [], {'to': u"orm['users.InLifeUser']", 'symmetrical': 'False', 'blank': 'True'}),
            'google_friends_imported': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'groups': ('django.db.models.fields.related.ManyToManyField', [], {'to': u"orm['auth.Group']", 'symmetrical': 'False', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'is_active': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'is_interesting': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'is_premium': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'is_public_action_in_fb': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'is_public_action_in_tw': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'is_public_action_in_vk': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'is_show_location': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'is_staff': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'is_superuser': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'last_clear_pg_date': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now'}),
            'last_login': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now'}),
            'last_name': ('django.db.models.fields.CharField', [], {'max_length': '30', 'blank': 'True'}),
            'latitude': ('django.db.models.fields.FloatField', [], {'null': 'True', 'blank': 'True'}),
            'level': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['users.Level']", 'null': 'True', 'on_delete': 'models.SET_NULL', 'blank': 'True'}),
            'longitude': ('django.db.models.fields.FloatField', [], {'null': 'True', 'blank': 'True'}),
            'middle_name': ('django.db.models.fields.CharField', [], {'max_length': '30', 'blank': 'True'}),
            'password': ('django.db.models.fields.CharField', [], {'max_length': '128'}),
            'photo': ('django.db.models.fields.files.ImageField', [], {'max_length': '100', 'blank': 'True'}),
            'registration_method': ('django.db.models.fields.IntegerField', [], {'default': '0'}),
            'sex': ('django.db.models.fields.IntegerField', [], {'default': '0'}),
            'social_network_page': ('django.db.models.fields.URLField', [], {'max_length': '200', 'null': 'True', 'blank': 'True'}),
            'trust': ('django.db.models.fields.IntegerField', [], {'default': '50', 'blank': 'True'}),
            'twitter_friends_imported': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'user_permissions': ('django.db.models.fields.related.ManyToManyField', [], {'to': u"orm['auth.Permission']", 'symmetrical': 'False', 'blank': 'True'}),
            'username': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '30'}),
            'vk_friends_imported': ('django.db.models.fields.BooleanField', [], {'default': 'False'})
        },
        u'users.level': {
            'Meta': {'object_name': 'Level'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'number': ('django.db.models.fields.IntegerField', [], {'unique': 'True'}),
            'required_points': ('django.db.models.fields.IntegerField', [], {'default': '0'})
        },
        u'users.metricbonus': {
            'Meta': {'object_name': 'MetricBonus'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'metric': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['users.UserMetric']"}),
            'points': ('django.db.models.fields.IntegerField', [], {'default': '0'})
        },
        u'users.usermetric': {
            'Meta': {'object_name': 'UserMetric'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'title': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'type': ('django.db.models.fields.IntegerField', [], {'default': '0'})
        }
    }

    complete_apps = ['configs']