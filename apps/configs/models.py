# -*- coding: utf-8 -*-
from django.db import models
from solo.models import SingletonModel
from inlife.utils import achievement_image, inlifeuser_image


class AchieverConfig(SingletonModel):
    #achievements
    default_achievement_image = models.ImageField(
        upload_to=achievement_image,
        default='/static/img/timeline_pic.png',
        verbose_name=u'картинка достижения по-умолчанию',
    )
    post_registration_recieved_achievements = models.ManyToManyField(
        'app.Achievement',
        limit_choices_to={'created_by_user': False},
        blank=True, null=True,
        verbose_name=u'Достижения активированные после регистрации',
        help_text=u'Достижения которые активируются\
                    пользователю после регистрации',
    )
    #users
    default_user_avatar = models.ImageField(
        upload_to=inlifeuser_image,
        default='/static/img/avatar.jpg',
        verbose_name=u'аватар пользователя по-умолчанию',
    )
    #news
    news_timeline_num = models.IntegerField(
        default=10,
        verbose_name=u'Колличество новостей с картинкой',
    )
    news_day_num = models.IntegerField(
        default=10,
        verbose_name=u'Колличество дней',
        help_text=u'За сколько последних дней показывать новости',
    )
    #groups
    sidebar_groups_num = models.IntegerField(
        default=10,
        verbose_name=u'Колличество групп в левой панели'
    )
    registration_groups = models.ManyToManyField(
        'groups.Group',
        blank=True, null=True,
        verbose_name=u'Группы при регистрации',
        help_text=u'Группы, которые добавляются по умолчанию',
    )
    #Email
    email_for_register = models.EmailField(
        verbose_name=u'Почта для регистрации',
        help_text=u'По-умолчанию',
        default='reg@example.com',
    )
    #site
    site_closed = models.BooleanField(
        verbose_name=u'Сайт закрыт',
        default=False,
    )
    #timeline
    timeline_num_comments = models.IntegerField(
        default=3,
        verbose_name=u'Число комментариев к достижению',
        help_text=u'По-умолчанию выводится вот столько последних комментариев.\
                    Остальные скрыты ссылкой Показать предыдущие.'
    )
    instagram_num_photos = models.IntegerField(
        default=10,
        verbose_name=u'Количество фотографий из инстаграма',
    )
    #messages
    message_from_admin_author = models.ForeignKey(
        'users.InLifeUser',
        limit_choices_to={'is_staff': True, 'is_active': True},
        verbose_name=u'Автор сообщения',
        null=True, blank=True,
        help_text=u'Админ, от имени которого посылается сообщение',
    )
    message_from_admin = models.TextField(
        verbose_name=u'Сообщение от админа',
        help_text=u'Сообщение, которое пользователь получает при регистрации',
        default=u'Добро пожаловать на портал!',
        blank=True
    )

    def __unicode__(self):
        return u"Настройки сайта"

    class Meta:
        verbose_name = "Настройки сайта"
        verbose_name_plural = "Настройки сайта"


class ApiConfig(SingletonModel):
    PhotoSize = models.IntegerField(default=1024)
    CompressionQuality = models.IntegerField(default=80)
    ReportStorageDaysDuration = models.IntegerField(default=14)
    GPSPollingInterval = models.IntegerField(default=5)
    RequireGPSLocation = models.BooleanField(default=False)
    UseGeocoder = models.BooleanField(default=False)

    def __unicode__(self):
        return u"Настройки api"

    class Meta:
        verbose_name = "Настройки api"
        verbose_name_plural = "Настройки api"
