# -*- coding: utf-8 -*-
from django.contrib import admin
from solo.admin import SingletonModelAdmin
from .models import (AchieverConfig, ApiConfig)


class AchieverConfigAdmin(SingletonModelAdmin):
    filter_horizontal = (
        'registration_groups', 'post_registration_recieved_achievements')

    fieldsets = (
        (
            u"Сайт",
            {
                'fields': ('site_closed',)
            }
        ),
        (
            u"Группы",
            {
                'fields': ('sidebar_groups_num', 'registration_groups',)
            }
        ),
        (
            u"Достижения",
            {
                'fields': ('default_achievement_image',
                           'post_registration_recieved_achievements',)
            }
        ),
        (
            u"Пользователи",
            {
                'fields': ('default_user_avatar',)
            }
        ),
        (
            u"Новости",
            {
                'fields': ('news_timeline_num',
                           'news_day_num',)
            }
        ),
        (
            u"Почта",
            {
                'fields': ('email_for_register',)
            }
        ),
        (
            u"timeline",
            {
                'fields': ('timeline_num_comments', 'instagram_num_photos')
            }
        ),
        (
            u"Соообщения",
            {
                'fields': ('message_from_admin_author', 'message_from_admin')
            }
        ),
    )


admin.site.register(AchieverConfig, AchieverConfigAdmin)
admin.site.register(ApiConfig, SingletonModelAdmin)
