# -*- coding: utf-8 -*-
from django.template import Context, Template
from django.core.mail import EmailMultiAlternatives, EmailMessage
from django.db import models
from django.utils import timezone
from django.db.models.signals import post_save
from django.dispatch import receiver

from postman.models import Message
from postman.api import pm_write

from inlife.utils import notification_image
from inlife.settings import DEFAULT_FROM_EMAIL
from .const import NOTIFICATION_SUBJECT, NOTIFICATION_SUBJECT_CHOICES
from app.const import EMAIL_EVENT
from apps.configs import achiever_config_value


class Notification(models.Model):
    user = models.ForeignKey('users.InLifeUser', related_name='notifications')
    subject_id = models.PositiveIntegerField(db_index=True)
    subject_type = models.IntegerField(choices=NOTIFICATION_SUBJECT_CHOICES,
                                       db_index=True)
    date = models.DateTimeField(default=timezone.now)
    viewed = models.BooleanField(u'Показано', default=False, db_index=True)
    theme = models.CharField(max_length=255, null=True, blank=True)
    body = models.TextField(blank=True)
    icon = models.ImageField(upload_to=notification_image,
                             blank=True, null=True)

    @classmethod
    def message(cls, message):
        """Создает нотификацию о сообщении"""
        notification = cls.objects.create(
            user=message.recipient,
            subject_id=message.id,
            subject_type=NOTIFICATION_SUBJECT.MESSAGE,
            body=' '.join(message.body.split()[:10]),
            theme=message.sender.get_full_name(),
            icon=message.sender.photo,
        )


class MessageConf(models.Model):
    ht = u'Если активен, для выбранного события использоваться этот шаблон'
    notification = 0
    email = 1
    browser = 2
    other_notification = 3
    vk_notification = 4
    TYPE_CHOICES = (
        (notification, u'Отправляем в личные сообщения'),
        (email, u'Отправляем на почту (если указана)'),
        (browser, u'Отправляем в браузер (если онлайн)'),
        (other_notification, u'Другая индикация'),
        (vk_notification, u'Отправляем в vk'),
    )

    class Meta:
        verbose_name = u'Шаблон уведомления'
        verbose_name_plural = u'Шаблоны уведомлений'
        ordering = ['-active']

    def __unicode__(self):
        if self.active:
            return u"%s(Активен): %s" % (self.get_event_display(), self.subject)
        else:
            return "%s: %s" % (self.get_event_display(), self.subject)

    event = models.IntegerField(u'Событие', choices=EMAIL_EVENT.CHOICES)
    notification_type = models.PositiveIntegerField(u'Тип уведомления',
                                                    choices=TYPE_CHOICES)
    subject = models.CharField(u'Тема', max_length=255)
    body_text = models.TextField(u'Шаблон в plaintext', blank=True, null=True)
    body_html = models.TextField(u'Шаблон в html')
    active = models.BooleanField(u'Активен', default=False, help_text=ht)

    def save(self, *args, **kwargs):
        if self.active:
            try:
                msg = MessageConf.objects.get(
                    notification_type=self.notification_type,
                    event=self.event, active=True)
            except MessageConf.DoesNotExist:
                pass
            else:
                msg.active = False
                msg.save()
        super(MessageConf, self).save(*args, **kwargs)

    def send_notification(self, recipient, **kwargs):
        admin_from = achiever_config_value('message_from_admin_author')
        message = self.body_text or achiever_config_value('message_from_admin')
        if admin_from and message:
            pm_write(
                sender=admin_from,
                recipient=recipient,
                subject='< subject >',
                body=message,
                skip_notification = True
                )

    def send_email(self, user, **kwargs):
        to = user.email
        if not to:
            return False            
        t_html = Template(self.body_html)
        c = Context(kwargs)
        html = t_html.render(c)
        if isinstance(to, basestring):
            to = [to,]
        if self.body_text:
            t_text = Template(self.body_text)
            text = t_text.render(c)
            mail = EmailMultiAlternatives(self.subject, text,
                                          DEFAULT_FROM_EMAIL, to)
            mail.attach_alternative(html, 'text/html')
        else:
            mail = EmailMessage(self.subject, html, DEFAULT_FROM_EMAIL, to)
            mail.content_subtype = 'html'
        try:
            mail.send()
        except:
            pass
        return True

    def send_browser(self, user, **kwargs):
        notification = Notification.objects.create(
            user=user,
            subject_id=10,
            subject_type=NOTIFICATION_SUBJECT.MESSAGE,
            body=' '.join(self.body_text.split()[:10]),
            theme=user.get_full_name(),
            icon=user.photo,
            )

    @classmethod
    def message(cls, user=None, **kwargs):
        method_dict = {cls.notification: 'send_notification',
                       cls.email: 'send_email',
                       cls.browser: 'send_browser'}
        other_notification_dict = {
            EMAIL_EVENT.ACHIEVEMENT_COMMENT: True,
            EMAIL_EVENT.MESSAGE: user and user.get_message_count(),
            EMAIL_EVENT.FRIENDSHIP: user and user.get_friendship_offers_count(),
            EMAIL_EVENT.INTEREST_NEWS: user and user.get_news_count()
            }
        kwargs['active'] = True
        try:
            mconf = cls.objects.get(**kwargs)
        except cls.DoesNotExist:
            pass
        else:
            if kwargs['notification_type'] == cls.other_notification:
                return other_notification_dict.get(kwargs['event'], '')
            elif kwargs['notification_type'] == cls.browser and \
                    kwargs['event'] == EMAIL_EVENT.MESSAGE:
                return True
            func = getattr(mconf, method_dict[kwargs['notification_type']])
            func and func(user, event=kwargs['event'])


def events_and_types(user, events, types):
    for enent in events:
        for n_type in types:
            MessageConf.message(user, event=enent, notification_type=n_type)


@receiver(post_save, sender=Message, dispatch_uid="apps.notifications.models")
def post_write_actions(sender, instance, created, **kwargs):
    mc = MessageConf.message(instance.recipient, event=EMAIL_EVENT.MESSAGE,
                             notification_type=MessageConf.browser)
    if instance.subject == "<no subject>" and mc:
        Notification.message(instance)
