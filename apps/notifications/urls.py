
from django.conf.urls import patterns, include, url


urlpatterns = patterns(
    'notifications.views',
    url(r'^check/$', 'notifications_check', name='notifications_check'),
    url(r'^counts/$', 'data_counts', name='data_counts'),
)
