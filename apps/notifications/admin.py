# coding: utf-8
from django.contrib import admin
from .models import MessageConf


class MessageConfAdmin(admin.ModelAdmin):
    list_display = ('id', 'event', 'notification_type', 'subject',
                    'body_text', 'body_html', 'active', )
    list_filter = ('event', 'notification_type', 'active', )

admin.site.register(MessageConf, MessageConfAdmin)
