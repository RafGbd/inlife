# -*- coding: utf-8 -*-
import json

from django.shortcuts import HttpResponse
from django.contrib.auth.decorators import login_required
from django.core.urlresolvers import reverse

from app.const import EMAIL_EVENT
from users.models import Friendship

from .serializers import NotificationSerializer
from .models import MessageConf


def notifications_check(request):
    response = { "notifications": [] }
    if request.user.is_authenticated():
        notifications = request.user.notifications.filter(viewed=False)
        serializer = NotificationSerializer(notifications, many=True)
        response["notifications"] = serializer.data
        notifications.update(viewed=True)
    return HttpResponse(json.dumps(response), content_type="application/json")


@login_required
def data_counts(request):
    user, n_type = request.user, MessageConf.other_notification
    response = [
        {'title': u'Новости', 'class': 'icon-rss hint',
         'url': reverse('journal'), 'count': MessageConf.message(
                user, event=EMAIL_EVENT.INTEREST_NEWS,
                notification_type=n_type)},
        {'title': u'Друзья', 'class': 'icon-group hint',
         'url': reverse('friends'), 'count': MessageConf.message(
                user, event=EMAIL_EVENT.FRIENDSHIP, notification_type=n_type)},
        {'title': u'Сообщения', 'class': 'icon-comments hint',
         'url': reverse('conversations'), 'count': MessageConf.message(
                user, event=EMAIL_EVENT.MESSAGE, notification_type=n_type)},
        {'title': u'Группы', 'class': 'hint svg-hint',
         'url': reverse('groups'), 'count': 0}]
    return HttpResponse(json.dumps(response), content_type="application/json")
