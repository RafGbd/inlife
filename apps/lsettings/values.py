# -*- coding: utf-8 -*-
from PIL import Image
import os
from cStringIO import StringIO

from django import forms
from django.contrib.admin.widgets import AdminFileWidget
from django.utils.translation import ugettext as _
from django.utils.safestring import mark_safe
from django.conf import settings
from django.core.files.storage import default_storage as storage
from django.core.files.base import ContentFile

from livesettings.values import Value, NOTSET


__all__ = ['ImageValue']

def thumbnail(image_path):
    try:
        d, f = os.path.split(image_path)
        fname, ftype = f.split('.')
        if ftype.lower() not in ('png', 'jpg', 'jpeg', ):
            raise(Exception)
        temp_handle = StringIO()
        with storage.open(image_path, 'rb') as fobject:
            im = Image.open(fobject)
            w, h = im.size
            w, h = 100, 100 * (h / float(w))
            im.thumbnail((w, h), Image.ANTIALIAS)
            im.save(temp_handle, 'png')
        temp_handle.seek(0)
        path = storage.save(os.path.join(d, fname + '100x100.png'),
                            ContentFile(temp_handle.read()))
        path = storage.base_url + path
    except:
        path = '/static/img/avatar.jpg'
    return u'<img src="%s" alt="%s" />' % (path, path)


class AdminImageWidget(AdminFileWidget):
    """
    A FileField Widget that displays an image instead of a file path
    if the current file is an image.
    """
    def render(self, name, value, attrs=None):
        output = []
        file_name = str(value).replace(storage.base_url, '')
        if file_name:
            try:            # is image
                output.append('<a target="_blank" href="%s">%s</a><br />%s <a target="_blank" href="%s">%s</a><br />%s ' % \
                                  (value, thumbnail(file_name), _('Currently:'), value, file_name, _('Change:')))
            except IOError: # not image
                output.append('%s <a target="_blank" href="%s">%s</a> <br />%s ' % \
                    (_('Currently:'), value, file_name, _('Change:')))
            
        output.append(super(AdminFileWidget, self).render(name, value, attrs))
        return mark_safe(u''.join(output))


class ImageValue(Value):

    class field(forms.ImageField):
        def __init__(self, *args, **kwargs):
            kwargs['required'] = False
            kwargs['widget'] = AdminImageWidget
            forms.ImageField.__init__(self, *args, **kwargs)

    def to_python(self, value):
        if value == NOTSET:
            value = 0
        return os.path.join(storage.base_url, value)

    to_editor = to_python
