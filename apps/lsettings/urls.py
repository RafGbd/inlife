from django.conf.urls import patterns, include, url
from .views import site_settings, export_as_python, group_settings


urlpatterns = patterns('',
    (r'^$', site_settings, {}, 'satchmo_site_settings'),
    (r'^export/$', export_as_python, {}, 'settings_export'),
    (r'^(?P<group>[^/]+)/$', group_settings),
)
