# coding: utf-8
class REGISTRATION_METHOD:
    EMAIL = 0
    SOCIAL_NETWORK = 1

REGISTRATION_METHOD_CHOICES = [(REGISTRATION_METHOD.EMAIL, u'Почта'),
                               (REGISTRATION_METHOD.SOCIAL_NETWORK, u'Социальная сеть')]

class SEX:
    UNDEFINED = 0
    MALE = 1
    FEMALE = 2

SEX_CHOICES = [(SEX.UNDEFINED, u'Не указан'),
               (SEX.MALE, u'Мужской'),
               (SEX.FEMALE, u'Женский')]


class EMAIL_EVENT:
    REGISTRATION = 0
    LEVEL_UP = 1
    FRIENDSHIP = 2
    MESSAGE = 3
    ACHIEVEMENT_ADDITION = 4
    ADMIN_FEEDBACK = 5
    ACHIEVEMENT_TIME_EXPIRES = 6
    ACHIEVEMENT_COMMENT = 7
    ACHIEVEMENT_LIKE = 8
    INTEREST_NEWS = 9
    EVENT_GROUP = 10
    EVENT_GROUP_END = 11
    CHOICES = ((REGISTRATION, u'Регистрация пользователя'),
               (LEVEL_UP, u'Достижение уровня'),
               (FRIENDSHIP, u'Дружба'),
               (MESSAGE, u'Сообщение'),
               (ACHIEVEMENT_ADDITION, u'Добавление достижения'),
               (ADMIN_FEEDBACK, u'Отзыв админа'),
               (ACHIEVEMENT_TIME_EXPIRES, u'Истечение времени достижения'),
               (ACHIEVEMENT_COMMENT, u'Комментарий к достижению'),
               (ACHIEVEMENT_LIKE, u'Лайк к достижению'),
               (INTEREST_NEWS, u'Интересные новости'),
               (EVENT_GROUP, u'Новое событие групы'),
               (EVENT_GROUP_END, u'1 день доконца события'),
               )


class ACHIEVEMENT_STATUS:
    NOT_AVAILABLE = 0
    NOT_ACTIVE = 1
    ACTIVE = 2
    CONFIRMATION = 3
    RECEIVED = 4
    FAILED = 5
    ADDED = 6
    STATUS_CHOICES = (
        (NOT_AVAILABLE, u'Недоступно'),
        (NOT_ACTIVE, u'Неактивно'),
        (ACTIVE, u'Активированно'),
        (CONFIRMATION, u'Подтверждается'),
        (RECEIVED, u'Получено'),
        (FAILED, u'Провалено'),
        (ADDED, u'Добавленно'),
    )

class ICON_COLOR:
    RED_ICON = 'achiv'
    GREEN_ICON = 'achiv achiv-green'
    COLLECT_ICON = 'achiv achiv-collect'
    VIOLET_ICON = 'achiv achiv-violet'
    GRAY_ICON = 'achiv achiv-gray'
    COLORS_CLASS = (
              (RED_ICON, u'Красный'),
              (GREEN_ICON, u'Зеленый'),
              (COLLECT_ICON, u'Синий'),
              (VIOLET_ICON, u'Фиолетовый'),
              (GRAY_ICON, u'Серый'),
              )

ACTION_VERBS = {
    SEX.UNDEFINED: {
        ACHIEVEMENT_STATUS.NOT_AVAILABLE: u'не доступно',
        ACHIEVEMENT_STATUS.NOT_ACTIVE: u'не активно',
        ACHIEVEMENT_STATUS.ACTIVE: u'активировало',
        ACHIEVEMENT_STATUS.RECEIVED: u'получило',
        ACHIEVEMENT_STATUS.FAILED: u'провалило',
        ACHIEVEMENT_STATUS.CONFIRMATION: u'ожидает подтверждения',
        ACHIEVEMENT_STATUS.ADDED: u'добавило',
    },
    SEX.MALE: {
        ACHIEVEMENT_STATUS.NOT_AVAILABLE: u'не доступно',
        ACHIEVEMENT_STATUS.NOT_ACTIVE: u'не активно',
        ACHIEVEMENT_STATUS.ACTIVE: u'активировал',
        ACHIEVEMENT_STATUS.RECEIVED: u'получил',
        ACHIEVEMENT_STATUS.FAILED: u'провалил',
        ACHIEVEMENT_STATUS.CONFIRMATION: u'ожидает подтверждения',
        ACHIEVEMENT_STATUS.ADDED: u'добавил',
    },
    SEX.FEMALE: {
        ACHIEVEMENT_STATUS.NOT_AVAILABLE: u'не доступно',
        ACHIEVEMENT_STATUS.NOT_ACTIVE: u'не активно',
        ACHIEVEMENT_STATUS.ACTIVE: u'активировала',
        ACHIEVEMENT_STATUS.RECEIVED: u'получила',
        ACHIEVEMENT_STATUS.FAILED: u'провалила',
        ACHIEVEMENT_STATUS.CONFIRMATION: u'ожидает подтверждениия',
        ACHIEVEMENT_STATUS.ADDED: u'добавила',
    },
    'PERFECT_FORM': {
        ACHIEVEMENT_STATUS.NOT_AVAILABLE: u'не доступно',
        ACHIEVEMENT_STATUS.NOT_ACTIVE: u'не активно',
        ACHIEVEMENT_STATUS.ACTIVE: u'активировано',
        ACHIEVEMENT_STATUS.RECEIVED: u'получено',
        ACHIEVEMENT_STATUS.FAILED: u'провалено',
        ACHIEVEMENT_STATUS.CONFIRMATION: u'ожидает подтверждения',
        ACHIEVEMENT_STATUS.ADDED: u'добавленно',
    },
    'PAST_FORM': {
        ACHIEVEMENT_STATUS.NOT_AVAILABLE: u'не доступно',
        ACHIEVEMENT_STATUS.NOT_ACTIVE: u'не активно',
        ACHIEVEMENT_STATUS.ACTIVE: u'активирован',
        ACHIEVEMENT_STATUS.RECEIVED: u'получен',
        ACHIEVEMENT_STATUS.FAILED: u'провален',
        ACHIEVEMENT_STATUS.CONFIRMATION: u'ожидает подтверждения',
        ACHIEVEMENT_STATUS.ADDED: u'добавлен',
    },
}


ACHIEVEMENT_FORMS = {
    ACHIEVEMENT_STATUS.NOT_AVAILABLE: u'достижение',
    ACHIEVEMENT_STATUS.NOT_ACTIVE: u'достижение',
    ACHIEVEMENT_STATUS.ACTIVE: u'достижение',
    ACHIEVEMENT_STATUS.RECEIVED: u'достижение',
    ACHIEVEMENT_STATUS.FAILED: u'достижение',
    ACHIEVEMENT_STATUS.CONFIRMATION: u'достижения',
    ACHIEVEMENT_STATUS.ADDED: u'достижение',
}
