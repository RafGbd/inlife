from django.template import Context, Template
from django.core.mail import EmailMultiAlternatives, EmailMessage
from inlife.settings import DEFAULT_FROM_EMAIL
from common.models import EmailTemplate
from django.core.files.uploadedfile import InMemoryUploadedFile, TemporaryUploadedFile
from PIL import Image
from cStringIO import StringIO
from PIL.ExifTags import TAGS
import os


def orientation_rotation(im):
    #take pil Image insctance and if need rotate it
    orientation = None
    #image file is truncated bug
    try:
        im.load()
    except IOError:
        pass
    try:
        exifdict = im._getexif()
    except AttributeError:
        exifdict = {}
    if exifdict:
        for k in exifdict.keys():
            if k in TAGS.keys():
                if TAGS[k] == 'Orientation':
                   orientation =  exifdict[k]
    if orientation in (3, 6, 8):
        if orientation == 6:
            im = im.rotate(-90)
        elif orientation == 8:
            im = im.rotate(90)
        elif orientation == 3:
            im = im.rotate(180)
    return im

def rotate_in_memory(image):
    #take inmemory file and return rotated(if need) Inmemory file
    image.seek(0)
    f = StringIO(image.read()) #user image
    img = StringIO() #result image
    im = Image.open(f) #PIL processing image
    im = orientation_rotation(im)
    im.save(img, 'JPEG')
    img.seek(0, os.SEEK_END)
    img_len = img.tell()
    img.seek(0)
    return InMemoryUploadedFile(img, image.field_name, image.name, image.content_type, img_len, image.charset)

def rotate_temporary(image):
    #take temporary file and return rotated(if need) temporary file
    file_path = image.temporary_file_path()
    im = Image.open(file_path)
    im = orientation_rotation(im)
    im.save(file_path, 'JPEG')
    return image

def fix_photo_orientation(image):
    if isinstance(image, InMemoryUploadedFile):
        image = rotate_in_memory(image)
    if isinstance(image, TemporaryUploadedFile):
        image = rotate_temporary(image)
    return image

def send_templated_email(event, to, **kwargs):
    print "send_templated_email", event, to, kwargs
    try:
        tpl = EmailTemplate.objects.get(event=event, active=True)
    except EmailTemplate.DoesNotExist, e:
        print e
        return False
    else:
        t_html = Template(tpl.body_html)
        c = Context(kwargs)
        html = t_html.render(c)
        if isinstance(to, basestring):
            to = [to,]
        if tpl.body_text:
            t_text = Template(tpl.body_text)
            text = t_text.render(c)
            mail = EmailMultiAlternatives(tpl.subject, text, DEFAULT_FROM_EMAIL, to)
            mail.attach_alternative(html, 'text/html')
        else:
            mail = EmailMessage(tpl.subject, html, DEFAULT_FROM_EMAIL, to)
            mail.content_subtype = 'html'
        print mail
        mail.send()
        return True

def get_or_none(model, **kwargs):
    try:
        return model.objects.get(**kwargs)
    except model.DoesNotExist:
        return None
