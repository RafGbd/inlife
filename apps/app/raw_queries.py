# -*- coding: utf-8 -*-
trees_with_categorys_achievement_count = """
SELECT *, app_achievement.tree_id, achievements_need, achievements_recieved, reached_categorys
FROM app_achievement
LEFT JOIN
    (
        SELECT count(*) as achievements_need, tree_id 
        FROM app_achievement 
        WHERE id in (
            SELECT achievement_id 
            FROM app_category_achievements
            WHERE category_id in (
                SELECT category_id
                FROM app_usercategory
                WHERE user_id = %s)
            ) GROUP BY tree_id
    ) tree_achievements_need
ON app_achievement.tree_id = tree_achievements_need.tree_id
LEFT JOIN
    (
        SELECT COUNT(*) as achievements_recieved, tree_id 
        FROM 
            (
                SELECT app_achievement.id as achievement_id, tree_id
                FROM app_achievement
                INNER JOIN
                    app_userachievement
                ON app_achievement.id = app_userachievement.achievement_id
                WHERE app_userachievement.user_id = %s and app_userachievement.status = 4 
            ) recieved
            INNER JOIN
            (
                SELECT * 
                FROM app_category_achievements
                WHERE category_id in (
                    SELECT category_id 
                    FROM app_usercategory
                    WHERE user_id = %s
                    )
            ) char_ach
            ON recieved.achievement_id = char_ach.achievement_id
            GROUP BY tree_id
    ) tree_achievements_recieved
ON app_achievement.tree_id = tree_achievements_recieved.tree_id
LEFT JOIN
    (
        SELECT COUNT(*) as reached_categorys, tree_id 
        FROM
            (
                SELECT DISTINCT app_category_achievements.category_id, app_achievement.tree_id
                FROM app_category_achievements
                LEFT JOIN app_achievement
                ON app_category_achievements.achievement_id = app_achievement.id
                WHERE app_category_achievements.category_id in 
                (
                    SELECT category_id 
                    FROM app_usercategory 
                    WHERE reached = 1 and user_id = %s
                )
            ) categorys_trees
        GROUP BY tree_id
    ) tree_reached_categorys
ON app_achievement.tree_id = tree_reached_categorys.tree_id
WHERE app_achievement.level = 0 AND app_achievement.is_active = 1 AND app_achievement.created_by_user = False
"""
'''
Возвращает корневые элементы дерева + в каждом из них поля achievements_need и achievements_recieved
achievements_need - сколько пользователю необходимо получить достижений связанных с целями пользователя
achievements_recieved - сколько уже получено
reached_categorys - количество получунных целей, связанных с деревом
Если нет целей связанных с деревом, возращается NULL
'''



achievement_children = """
SELECT *
    FROM app_achievement INNER JOIN (
            SELECT id as user_ach_id, status, user_id, achievement_id, activate_time
            FROM app_userachievement
            WHERE user_id = %s
        ) userachievement
    ON (app_achievement.id = userachievement.achievement_id)
    WHERE
        (
        parent_id = %s
        )
    ORDER BY app_achievement.tree_id ASC, app_achievement.lft ASC

"""
'''
Возвращает детей заданного достижения + достижения пользователя
'''

tree_base = """
SELECT *
    FROM app_achievement INNER JOIN (
            SELECT id as user_ach_id, status, user_id, achievement_id, activate_time
            FROM app_userachievement
            WHERE user_id = %s
        ) userachievement
    ON (app_achievement.id = userachievement.achievement_id)
    WHERE
        (
        lft <= %s
        AND
        lft >= %s
        AND
        tree_id = %s
        AND
        level <= 2
        )
    ORDER BY app_achievement.tree_id ASC, app_achievement.lft ASC
"""
'''
Возвращает Первые два уровня дерева + достижения пользователя
'''