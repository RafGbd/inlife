# -*- coding: utf-8 -*-
from django.core.management.base import BaseCommand, CommandError


from django.db.models import Q, F

class Command(BaseCommand):
    help = 'Closes the specified poll for voting'

    def handle(self, *args, **options):
        from app.models import Achievement, UserAchievement
        from users.models import InLifeUser

        created = []
        users = InLifeUser.objects.active()
        achievements = Achievement.objects.filter(Q(created_by_user=False) | Q(is_on_user_tree=True))
        for user in users:
            existing_achievements = UserAchievement.objects.filter(user=user, achievement__in=achievements).values_list('achievement_id', flat=True)
            #existing_achievements достижения - для котороых у пользовтеля есть соотв. UserAchievement
            missed_achievements = achievements.exclude(id__in=existing_achievements)
            missed_achievements

            user_achievements = []
            for missed_achievement in missed_achievements:
                user_achiev = UserAchievement(user=user,
                                              achievement=missed_achievement,
                                              status=UserAchievement.NOT_ACTIVE)
                user_achievements.append(user_achiev)
            UserAchievement.objects.bulk_create(user_achievements)
            if user_achievements:
                created.append(user_achievements)
        print "created achievements: "
        print created