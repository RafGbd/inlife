# -*- coding: utf-8 -*-
from django.core.management.base import BaseCommand, CommandError


from django.db.models import Q, F

class Command(BaseCommand):
    help = 'Closes the specified poll for voting'

    def handle(self, *args, **options):
        from app.models import Proof, ProofPhoto

        proof_photos = ProofPhoto.objects.filter(Q(proof=None) | Q (proof__status=Proof.STATUS_HIDDEN))
        print proof_photos
        proof_photos.delete()


