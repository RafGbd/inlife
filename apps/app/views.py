# -*- coding: utf-8 -*-
import json
import random
import os
import sys
import urllib, urllib2
import re
from cStringIO import StringIO
from django.core.files.storage import default_storage

from poster.encode import multipart_encode
from poster.streaminghttp import register_openers
from django.views.generic import ListView, DetailView, TemplateView, \
                                 RedirectView, View
from django.views.generic.edit import CreateView, UpdateView, FormView
from django.shortcuts import render, HttpResponse, HttpResponseRedirect, \
    get_object_or_404, redirect
from django.template import Context, loader
from django.http import Http404
from django.core.urlresolvers import reverse
from django.core.cache import cache
from django.conf import settings
from django.contrib.auth.decorators import login_required
from django.contrib.auth import logout
from django.contrib.auth import get_user_model
from django.utils.timezone import now
from django.db.models import Q, Count
from django.utils.datastructures import SortedDict, MultiValueDict, \
                                        MultiValueDictKeyError
from postman.models import Message
from postman.api import pm_write
from social_auth.utils import setting
from social_auth.decorators import dsa_view
from social_auth.views import auth_process
from haystack.query import SearchQuerySet
from apps.configs import achiever_config_value
from app.models import Achievement, UserAchievement, Category,\
                       Proof, UserProofVote, ProofPhoto,\
                       UserAchievementLike, UserAchievementComment
from actions.models import UserAchievementAction
from common.models import LandingItem
from app.forms import ProofForm, ReplyForm, \
    CategoryActivateForm, ProofPhotoFormset, \
    AchievementAddForm, AddAchievementToUserForm, \
    AddAchievementToUserGenTreeForm, SearchForm, \
    ProofPhotoForm, SimpleProofPhotoForm, UserAchievementCommentForm
from .raw_queries import achievement_children
from .serializers import MessageSerializer, \
    FriendsInLifeUserSerializer, \
    UserAchievementSerializer,\
    UserAchievementCommentSerializer
from .const import EMAIL_EVENT
from .utils import fix_photo_orientation, get_or_none
from .mixins import LoginRequiredMixin, StaffRequiredMixin
from users.forms import InlifeRegistrationForm, SubscriberForm
from groups.models import Group
from instagram.client import InstagramAPI
from notifications.models import events_and_types, MessageConf
InLifeUser = get_user_model()

def closesite(request):
    status = 'erorr'
    form = SubscriberForm()
    if request.method == 'POST':
        form = SubscriberForm(data=request.POST)
        if form.is_valid():
            status = 'ok'
            form.save()

    return render(request, 'cap.html', {'form': form, 'status': status})


CONFIG = {
    'client_id': settings.INSTAGRAM_CLIENTID,
    'client_secret': settings.INSTAGRAM_CLIENTSECRET,
    'redirect_uri': settings.INSTAGRAM_REDIRECTURI,
}

unauthenticated_api = InstagramAPI(**CONFIG)


@login_required
def instagram(request):
    request.session['instagramback'] = request.META.get('HTTP_REFERER')
    redirect_uri = unauthenticated_api.get_authorize_login_url(scope = ['basic'])
    return redirect(redirect_uri)


@login_required
def instagramback(request):
    code, photos, query_args = request.GET.get('code'), [], {}
    access_token, u_i = unauthenticated_api.exchange_code_for_access_token(code)
    count = achiever_config_value('instagram_num_photos')
    api = InstagramAPI(access_token=access_token)
    recent_media, next = api.user_recent_media(count=count)
    for media in recent_media:
        photos.append((media.images['standard_resolution'].url,
                       media.images['thumbnail'].url, ))
    query_args['inst_photo'] = json.dumps(photos)
    query_args['max_id'] = next.split('max_id=')[-1] if next else 'not'
    query_args['access_token'] = access_token
    encoded_args = urllib.urlencode(query_args)
    return redirect(request.session.pop('instagramback') + '?' + encoded_args)


@login_required
def nextphotos(request):
    access_token = request.GET.get('access_token')
    max_id = request.GET.get('max_id')
    count = achiever_config_value('instagram_num_photos')
    api = InstagramAPI(access_token=access_token)
    recent_media, next_m = api.user_recent_media(count=count, max_id=max_id)
    loading, photos = {}, []
    for media in recent_media:
        photos.append([media.images['standard_resolution'].url,
                       media.images['thumbnail'].url])
    loading['max_id'] = next_m.split('max_id=')[-1] if next_m else 'not'
    loading['photos'] = photos
    return HttpResponse(json.dumps(loading))


@login_required
def clear_cache(request):
    cache.clear()
    return redirect('/admin/')


@dsa_view(setting('SOCIAL_AUTH_COMPLETE_URL_NAME', 'socialauth_complete'))
def inlife_auth(request, backend):
    logout(request)
    return auth_process(request, backend)

class StartView(TemplateView):
    template_name = 'start.html'

    def get_context_data(self, **kwargs):
        context = super(StartView, self).get_context_data(**kwargs)
        if not self.request.user.is_authenticated():
            if UserAchievement.objects.count() < 8:
                categorys = Category.objects.all()
            else:
                categorys = Category.objects.all()[0:8]
            context['categorys'] = categorys
        return context


class ABView(TemplateView):
    template_name = 'start.html'

    def get(self, request, *args, **kwargs):
        # print request.session.pop("index")
        index = request.session.get("index")
        select_list = ['landing_A.html', 'landing_B.html']
        if index:
            landing = select_list[int(index)] 
        else:
            landing = random.choice(select_list)

        self.template_name = 'landing/' + landing
        context = self.get_context_data(**kwargs)
        request.session["index"] = index or select_list.index(landing)
        #тут всегда будет второй вариант, потому что 0 это False :-)
        return self.render_to_response(context)


class HomeView(TemplateView):
    ab_test_referers = ['vk.com', 'facebook.com']

    def get_context_data(self, **kwargs):
        context = super(HomeView, self).get_context_data(**kwargs)
        context['form'] = InlifeRegistrationForm()
        context['landing_itmes'] = LandingItem.objects.filter(level=0)
        return context

    def ab_test(self, request, *args, **kwargs):
        index = request.session.get("index", None)
        select_list = [
            'landing_A.html',
            'landing_B.html',
            ]
        if index is None:
            #first time
            landing = random.choice(select_list)
            request.session["index"] = select_list.index(landing)
        else:
            #not first time
            landing = select_list[int(index)]
            request.session["index"] = index

        self.template_name = 'landing/' + landing

    def get(self, request, *args, **kwargs):
        if request.user.is_authenticated():
            return HttpResponseRedirect(reverse('timeline',
                                                args=[request.user.id]))

        self.ab_test(request)
        context = self.get_context_data(**kwargs)
        return self.render_to_response(context)

class RulesView(TemplateView):
    template_name = 'rules.html'

def achievement_detail(request, ach_id):
    """Страница исходного достижения"""
    response = {}
    response['achievement'] = get_object_or_404(Achievement, id=ach_id)
    if request.user.is_authenticated():
        response['user_achievement'] = get_or_none(UserAchievement,
                                                   user=request.user,
                                                   achievement_id=ach_id)
    return render(request, 'achievement/achievement_detail.html', response)

@login_required
def activate_achievement(request, ach_id):
    user_achievement, created = UserAchievement.objects.get_or_create(
        user=request.user, achievement_id=ach_id)
    # TODO: проверять создан ли пруф, или перенести в другую функцию
    try:
        user_achievement.likes.all().delete()
        user_achievement.comments.all().delete()
        user_achievement.proof.delete()
    except Exception, e:
        pass
    try:
        action = user_achievement.get_achievement_action()
        action.comments.all().delete()
        action.likes.all().delete()
    except Exception, e:
        pass
    user_achievement.activate()
    imgPath = ''
    imgUrl = ''
    msg = u'Я активировал достижение: "%s"' % user_achievement.achievement.name
    if user_achievement.achievement.image:
        imgPath = str(user_achievement.achievement.image)
        imgUrl = str(user_achievement.achievement.image.url)
    callback = {}
    if request.user.is_public_action_in_vk:
        callback.update({
                         'vk':
                         {
                            'function': 'VkWallPostWithImage',
                            'params':
                            {
                                'imagePath': imgPath,
                                'message': msg
                            }
                          }
                         })
    if request.user.is_public_action_in_fb:
        callback.update({
                         'fb': {
                                'function': 'FbWallPostWithImage',
                                'params': {
                                           'imagePath': imgUrl,
                                           'message': msg
                                           }
                                }
                         })
    return redirect(reverse('achievement_with_callback', args=(ach_id, json.dumps(callback),)))

@login_required
def unactivate_achievement(request, ach_id):
    user_achievement = get_object_or_404(UserAchievement, user=request.user, achievement_id=ach_id)
    user_achievement.unactivate()
    return redirect(reverse('achievement', args=(ach_id,)))

@login_required
def confirm_achievement(request, ach_id):
    user_achievement = get_object_or_404(
        UserAchievement,
        user=request.user,
        achievement_id=ach_id,
        status__in=[
            UserAchievement.ACTIVE,
            UserAchievement.CONFIRMATION,
            UserAchievement.RECEIVED
            ])
    try:
        proof = user_achievement.proof
    except Exception, e:
        proof = Proof()
    proof_photos = photo_bool = proof.photos.all()
    photo_formset = ProofPhotoFormset(queryset=proof_photos)

    photos = []
    for media in json.loads(request.GET.get('inst_photo', '[]')):
        photos.append(media)

    inst_photo = photos
    max_id = request.GET.get('max_id', 'not')
    access_token = request.GET.get('access_token', 'not')

    if request.method == 'GET':
        form = ProofForm(instance=proof)

    elif request.method == 'POST':
        form = ProofForm(data=request.POST, files=request.FILES, instance=proof)
        try:
            photo_formset = ProofPhotoFormset(request.POST, request.FILES)
        except MultiValueDictKeyError, e:
            #user send wrong INITIAL_FORMS or TOTAL_FORMS
            post_data = request.POST
            forms_ids = [k for k in post_data.keys() if re.match('form-(\d+)-id', k)]
            forms_count = len(forms_ids)
            post_data[u'form-INITIAL_FORMS'] = forms_count
            post_data[u'form-TOTAL_FORMS'] = forms_count
            photo_formset = ProofPhotoFormset(post_data, request.FILES)
        if all(_form.is_valid() for _form in [form, photo_formset] ): #чтобы .is_valid у photo_formset вызвался в любом случае
            proof = form.save(commit=False)
            proof.user_achievement = user_achievement
            proof.status = Proof.STATUS_NOT_VERIFIED
            proof.save()
            proof_photos = photo_formset.save()
            for proof_ph in proof_photos:
                proof_ph.proof = proof
                proof_ph.save()

            return redirect(reverse('user_achievement_confirmed_detail', args=[request.user.id, user_achievement.achievement.pk]))
    return render(request, 'achievement/achievement_confirm.html', {
                                            'form': form,
                                            'photo_formset': photo_formset,
                                            'photo_bool': photo_bool,
                                            'user_achievement': user_achievement,
                                            'inst_photo': inst_photo,
                                            'max_id': max_id,
                                            'access_token': access_token
                                            })


@login_required
def upload_proof_photo(request, ach_id):
    from image_cropping.widgets import get_attrs
    response = {'success':False}
    user_achievement = get_object_or_404(
        UserAchievement, 
        user=request.user, 
        achievement_id=ach_id)
    try:
        proof = user_achievement.proof
    except Proof.DoesNotExist, e:
        proof = Proof.objects.create(user_achievement=user_achievement, status=Proof.STATUS_HIDDEN)
    photo_form = SimpleProofPhotoForm(request.POST, request.FILES)
    if photo_form.is_valid():
        photo = photo_form.cleaned_data['photo']
        photo = fix_photo_orientation(photo)
        response['success'] = True
        proof_ph = ProofPhoto(proof=proof, photo=photo, position=10)
        proof_ph.save()
        response['photo'] = {
            'id': proof_ph.id,
            'url': proof_ph.photo.url,
            'attrs': get_attrs(proof_ph.photo, 'photo')
        }
    return HttpResponse(json.dumps(response), content_type="application/json")

@login_required
def upload_temp_proof_photo(request):
    from image_cropping.widgets import get_attrs
    from django.core.files.uploadedfile import SimpleUploadedFile

    response = {'success':False}
    files = request.FILES
    if not files:
        _file = StringIO(urllib2.urlopen(request.POST.get('photo')).read())
        img = SimpleUploadedFile(u'temp.jpg', _file.read(),
                                 content_type=u'image/jpeg')
        files = MultiValueDict({u'photo': [img]})

    photo_form = SimpleProofPhotoForm(request.POST, files)
    if photo_form.is_valid():
        photo = photo_form.cleaned_data['photo']
        photo = fix_photo_orientation(photo)
        response['success'] = True
        proof_ph = ProofPhoto(user=request.user, photo=photo, position=10)
        proof_ph.save()
        response['photo'] = {
            'id': proof_ph.id,
            'url': proof_ph.photo.url,
            'attrs': get_attrs(proof_ph.photo, 'photo')
        }
    return HttpResponse(json.dumps(response), content_type="application/json")


@login_required
def delete_temp_proof_photo(request, obj_id):
    status = 'ok'
    try:
        ProofPhoto.objects.get(id=obj_id).delete()
    except:
        status = 'error'
    return HttpResponse(json.dumps({'status': status}),
                        content_type="application/json")


class GetAchievementChildren(LoginRequiredMixin, View):
    def dispatch(self, request, *args, **kwargs):
        pk = self.kwargs.get('ach_id', None)
        achievement = get_object_or_404(Achievement, pk=pk)
        children_template = loader.get_template('tree/achievement_children.html')
        raw_children = Achievement.objects.raw(achievement_children, [request.user.id, achievement.id])
        children = []
        for child in raw_children:
            blocking_count = child.blocking_achievements.count()
            if blocking_count > 0:
                blocking_achievements = child.blocking_achievements.all()
                child.blocking_user_achievements = list(UserAchievement.objects.filter(
                        user_id=self.request.user,
                        achievement__in=blocking_achievements
                    ).exclude(status=UserAchievement.RECEIVED))
            children.append(child)
        res = children_template.render(Context({'children': children}))
        return HttpResponse(res)


"""
РЕВЬЮ: не определена переменная ProfileForm, непонятное описание
"""
@login_required
def profile(request):
    """
        REPLACED TO users
    """
    if request.method == 'GET':
        form = ProfileForm(instance=request.user)
    elif request.method == 'POST':
        form = ProfileForm(request.POST, request.FILES, instance=request.user)
        if form.is_valid():
            form.save()
            return redirect(reverse('home'))

    return render(request, 'profile.html', {'form': form})


@login_required
def category_group_list(request, category):
    context = Context()
    context['group'] = category
    return render(request, 'category/category_group_list.html', context)


@login_required
def conversations(request):
    context = Context()
    if request.method == "GET":
        chat_users = list(request.user.get_interlocutors())
        context['chat_users'] = chat_users
        user_id = None
        if 'u' in request.GET:
            user_id = request.GET['u']
        elif len(chat_users) > 0:
            user_id = chat_users[0].id
        if user_id:
            chat_user = get_object_or_404(InLifeUser, id=user_id)
            context['chat_user'] = chat_user
            context['reply_form'] = ReplyForm(initial={'recipient': chat_user.id})
        return render(request, 'messages/conversations.html', context)
    if request.method == "POST":
        reply_form = ReplyForm(request.POST)
        if reply_form.is_valid():
            chat_user = reply_form.cleaned_data['recipient']
            text = reply_form.cleaned_data['text']
            skip_notification = Message.objects.filter(sender=request.user,
                                                       recipient=chat_user,
                                                       read_at=None).count() > 0
            if skip_notification:
                MessageConf.message(chat_user, event=EMAIL_EVENT.MESSAGE,
                                    notification_type=MessageConf.email)

            MessageConf.message(chat_user, event=EMAIL_EVENT.MESSAGE,
                                notification_type=MessageConf.browser)
            pm_write(
                sender=request.user,
                recipient=chat_user,
                subject="<no subject>",
                body=text,
                skip_notification = skip_notification
                )

        if request.is_ajax():
            return HttpResponse(json.dumps({"success": True}), content_type="application/json")
        redirect_url = "%s?u=%s" % (reverse('conversations'), chat_user.id)
        return redirect(redirect_url)


@login_required
def conversations_check(request):

    response = {
        "success": False
    }

    if 'chat_user_id' in request.GET:
        chat_user_id = request.GET['chat_user_id']
        user = request.user
        
        messages_qs = Message.objects.filter(
                Q(Q(sender=user) & Q(recipient_id=chat_user_id))
                |
                Q(Q(sender_id=chat_user_id) & Q(recipient=user))
                ).order_by('sent_at').distinct()
        
        time = request.GET.get('time', None)
        if time:
            messages_qs = messages_qs.filter(sent_at__gt=time)
        
        messages_qs.filter(recipient=user).update(read_at=now())

        response['user'] = request.user.username
        serializer = MessageSerializer(messages_qs, many=True)
        response["success"] = True
        response["user"] = request.user.username
        response["messages"] = serializer.data
        response['sender_count'] =  list(Message.objects.filter(
            recipient=request.user.id, read_at__isnull=True).values('sender_id')\
            .annotate(Count('sender')))
    return HttpResponse(json.dumps(response), content_type="application/json")


class ConversationsCountView(LoginRequiredMixin, View):

    def dispatch(self, request, *args, **kwargs):
        count = Message.objects.filter(recipient=self.request.user.id,
                                       read_at__isnull=True).count()
        return HttpResponse(count)


class UserAchievmentDataView(DetailView):
    def get(self, request, user_id, ach_id):
        user_achievement = get_object_or_404(UserAchievement, user_id=user_id, achievement_id=ach_id)
        comments = user_achievement.comments.all()
        serializer_context = {'request_user': self.request.user, 'request': self.request}
        user_ach_serializer = UserAchievementSerializer(user_achievement, context=serializer_context)
        comments_serializer = UserAchievementCommentSerializer(comments, many=True, context=serializer_context)
        data = user_ach_serializer.data
        data['comments'] = comments_serializer.data
        return HttpResponse(json.dumps(data), content_type="application/json")


class CommentDeleteView(LoginRequiredMixin, View):

    def dispatch(self, request, *args, **kwargs):
        obj_type = self.kwargs.get('obj_type', None)
        id = self.kwargs.get('pk', None)
        if obj_type and id:
            if obj_type == 'ach':
                try:
                    obj = UserAchievementActionComment.objects.get(pk=id)
                    if obj.user == self.request.user:
                        obj.delete()
                except UserAchievementActionComment.DoesNotExist:
                    pass

        return HttpResponse('')


class LoginRedirectView(LoginRequiredMixin, RedirectView):

    def get_redirect_url(self):
        if not self.request.user.first_view:
            self.request.user.first_view = True
            self.request.user.save()
            return reverse('profile')
        return reverse('timeline', args=[self.request.user.id])


class LikeAchievementView(LoginRequiredMixin, View):

    def dispatch(self, request, user_ach_id):
        user_achievement = get_object_or_404(UserAchievement, id=user_ach_id)

        try:
            UserAchievementLike.objects.get(user_achievement=user_achievement,
                                            who_like=self.request.user)
        except UserAchievementLike.DoesNotExist:
            p = UserAchievementLike(user_achievement=user_achievement,
                                    who_like=self.request.user)
            p.save()

        events_and_types(user_achievement.user,
                         events=(EMAIL_EVENT.ACHIEVEMENT_LIKE, ),
                         types=(MessageConf.browser, MessageConf.email, ))

        count = UserAchievementLike.objects.filter(user_achievement=user_achievement).count()
        res = '<a href="#"><div class="i_like red"><i class="icon-heart"></i><div class="qty">%s</div></div></a>' % count

        return HttpResponse(res)


class UnLikeAchievementView(LoginRequiredMixin, View):

    def dispatch(self, request, user_ach_id):
        user_achievement = get_object_or_404(UserAchievement, id=user_ach_id)

        try:
            UserAchievementLike.objects.get(user_achievement=user_achievement,
                                            who_like=self.request.user).delete()
        except UserAchievementLike.DoesNotExist:
            pass

        count = UserAchievementLike.objects.filter(user_achievement=user_achievement).count()
        res = '<a href="#"><div class="i_like"><i class="icon-heart"></i><div class="qty">%s</div></div></a>' % count

        return HttpResponse(res)

class CommentAchievementView(LoginRequiredMixin, View):

    def post(self, request, user_ach_id):
        res = {"success": False}
        user_achievement = get_object_or_404(UserAchievement, id=user_ach_id)
        comment_form = UserAchievementCommentForm(request.POST)
        if comment_form.is_valid():
            comment_text = comment_form.cleaned_data['comment']
            comment = UserAchievementComment(
                user=request.user,
                user_achievement=user_achievement,
                comment=comment_text,
                )
            comment.save()
            
            res["success"] = True
            serializer = UserAchievementCommentSerializer(comment, context={'request_user': request.user})
            events_and_types(user_achievement.user,
                             events=(EMAIL_EVENT.ACHIEVEMENT_COMMENT, ),
                             types=(MessageConf.browser, MessageConf.email, ))
            
            res["data"] = serializer.data
        return HttpResponse(json.dumps(res), content_type="application/json")


class AcceptAchievementView(LoginRequiredMixin, View):
    """
    Принять достижение добавленное админом
    """
    def post(self, request, user_ach_id):
        response = {"success": True, "message": ""}
        user_achievement = get_object_or_404(UserAchievement, id=user_ach_id)
        # только владелец может принять
        if user_achievement.user_id != request.user.id:
            response["success"] = False
            response["message"] = 'You can not accept foreign action'
            return HttpResponse(json.dumps(response), content_type='application/json')

        user_achievement.accepted_by_user = True
        user_achievement.save()

        return HttpResponse(json.dumps(response), content_type='application/json')

class DeleteAchievementView(LoginRequiredMixin, View):
    """
    Удалить достижение.
    """
    def post(self, request, user_ach_id):
        response = {"success": True, "message": ""}
        user_achievement = get_object_or_404(UserAchievement, id=user_ach_id)
        # только владелец может удалить
        if user_achievement.user_id != request.user.id:
            response["success"] = False
            response["message"] = 'You can not delete foreign action'
            return HttpResponse(json.dumps(response), content_type='application/json')

        # LF-427.6
        reg_achievemnets = achiever_config_value('post_registration_recieved_achievements')
        if user_achievement.achievement in reg_achievemnets.all():
            response["success"] = False
            response["message"] = u'Вы не можете удалить эту запись.'
            return HttpResponse(json.dumps(response), content_type='application/json')

        try:
            if hasattr(user_achievement, 'proof'):
                user_achievement.proof.delete()
            user_achievement.delete()
        except Exception:
            pass

        return HttpResponse(json.dumps(response), content_type='application/json')


class RatingsView(LoginRequiredMixin, ListView):
    template_name = 'users/ratings.html'

    def get_queryset(self):
        return self.request.user.friends.all()[:10]

    def get_context_data(self, *args, **kwargs):
        ctx = super(RatingsView, self).get_context_data(*args, **kwargs)
        ctx['all_users'] = InLifeUser.objects.filter(is_active=True)\
                                             .order_by('-level')[:10]
        friends_list = [x.pk for x in self.get_queryset()]
        friends_list.append(self.request.user.pk)
        ctx['friends'] = InLifeUser.objects.filter(
            is_active=True, id__in=friends_list).order_by('-level')[:10]
        return ctx


def summary(request, user_id):
    context = Context()
    user = get_object_or_404(InLifeUser, id=user_id)
    context['summ_user'] = user

    big_achievements = UserAchievement.objects.filter(
        user=user, status=UserAchievement.RECEIVED)
    big_achievements = big_achievements.filter(
        Q(achievement__stars_number__lt=1))
    big_achievements = big_achievements.order_by('-recieve_time')
    big_achievements_ids = big_achievements.values_list('id', flat=True)
    recieved_achievments = UserAchievement.objects.filter(user=user, status=UserAchievement.RECEIVED).exclude(id__in=big_achievements_ids)
    context['recieved_achievments'] = recieved_achievments.select_related('achievement').prefetch_related('proof')
    context['big_achievements'] = big_achievements.select_related('achievement').prefetch_related('proof')
    return render(request, 'users/user_summary.html', context)

@login_required
def SocApiPostImage(request):
    response = {}
    try:
        if(request.method != 'POST'): response = {'error': 'Data is Empty!'}
        upload_url = request.POST.get('upload_url')
        image = request.POST.get('image')
        """ send file """
        register_openers()
        datagen, headers = multipart_encode({'photo': default_storage.open(image)})
        req = urllib2.Request(upload_url, datagen, headers)
        response=urllib2.urlopen(req).read()
        if response:
            return HttpResponse(response, content_type='application/json')
    except:
        response = {'error': sys.exc_info()[1].message}
    return HttpResponse(json.dumps(response), content_type='application/json')


class EnquireVKPosting(View):
    def get(self, request, **kwargs):
        response = {
            "isError": False,
            "errorMessage": None,
            "res": None
        }
        try:
            response['res'] = self.process_request(request, **kwargs)
        except Exception, e:
            response["isError"] = True
            response["errorMessage"] = str(e)
        return HttpResponse(json.dumps(response),
                            content_type='application/json')

    def process_request(self, request, **kwargs):
        res = {}
        user_id = int(request.GET['user_id'])
        achievement_id = int(request.GET['achievement_id'])
        user_achievement = UserAchievement.objects.filter(
                user_id=user_id, achievement_id=achievement_id
                ).select_related(
                'achievement'
                ).prefetch_related('userachievementaction_set')[0]
        action = user_achievement.userachievementaction_set.filter()[0]
        #activated achievement
        if user_achievement.is_active() or \
                user_achievement.is_failed() or \
                user_achievement.is_received() or \
                user_achievement.is_peinding_confirmation():

            res['images_count'] = 1
            res['message'] = u"%s %s: %s" % (
                user_achievement.user.get_full_name(),
                action.get_achievement_action_verb(),
                user_achievement.achievement.name)
        # Для всех веток одинаковый код думаю пока лудше сделать через одну ветку
        # #failed achievement
        # elif user_achievement.is_failed():
        #     res['images_count'] = 1
        #     res['message'] = u"%s %s: %s" % (user_achievement.user.get_full_name(), action.get_achievement_action_verb(), user_achievement.achievement.name)
        # #recieved achievement
        # elif user_achievement.is_received():
        #     res['images_count'] = 1
        #     res['message'] = u"%s %s: %s" % (user_achievement.user.get_full_name(), action.get_achievement_action_verb(), user_achievement.achievement.name)
        # #confirmed achievement
        # elif user_achievement.is_peinding_confirmation():
        #     res['images_count'] = 1
        #     res['message'] = u"%s %s: %s" % (user_achievement.user.get_full_name(), action.get_achievement_action_verb(), user_achievement.achievement.name)     
        else:
            raise Exception("you can't share this achievement")
        res["url"] = request.build_absolute_uri(user_achievement.get_absolute_url()) 
        return res

        

class VKPostImages(View):
    response = {}
    def post(self, request, **kwargs):
        response = {
            "isError": False,
            "errorMessage": None,
            "res": None
        }
        try:
            response['res'] = self.process_request(request, **kwargs)
        except Exception, e:
            response["isError"] = True
            response["errorMessage"] = str(e)
        return HttpResponse(json.dumps(response), content_type='application/json')

    def process_request(self, request, **kwargs):
        user_id = int(request.POST['user_id'])
        achievement_id = int(request.POST['achievement_id'])
        user_achievement = UserAchievement.objects.select_related('achievement').get(
            user_id=user_id, achievement_id=achievement_id)
        proofs = Proof.objects.filter(user_achievement=user_achievement)
        proof_photos = ProofPhoto.objects.filter(proof__in=proofs)[:1]
        images = [photo.photo for photo in proof_photos if photo.photo]
        if not images:
            images = [user_achievement.achievement.get_image(), ]
        upload_urls = request.POST.getlist('upload_urls[]')
        
        tockens = []
        for image, upload_url in zip(images, upload_urls):
            vk_resp = self.upload_image(image, upload_url)
            tocken = {
                'server': vk_resp.get('server'),
                'hash': vk_resp.get('hash'),
                'photo': vk_resp.get('photo'),
                }
            tockens.append(tocken)
        return {'tockens': tockens}

    def upload_image(self, image, upload_url):
        image.open()
        register_openers()
        datagen, headers = multipart_encode({'photo': image})
        req = urllib2.Request(upload_url, datagen, headers)
        response = urllib2.urlopen(req).read()
        return json.loads(response)

class UserAchievementConfirmedDetailView(DetailView):
    model = Achievement
    context_object_name = 'achievement'
    template_name = 'achievement/user_achievement_confirmed_detail.html'

    def get_context_data(self, *args, **kwargs):
        context = super(UserAchievementConfirmedDetailView, self).get_context_data(**kwargs)
        user_id = self.kwargs.get('user_id')
        context['user_achievement'] = user_achievement = get_object_or_404(
            UserAchievement, user_id=user_id, achievement=self.object)
        try:
            proof = Proof.objects.get(user_achievement=user_achievement)
        except Proof.DoesNotExist:
            proof = Proof(user_achievement=user_achievement,
                                         proof_type = Proof.P_PHOTO)
        except Proof.MultipleObjectsReturned:
            proof = Proof.objects.filter(user_achievement=user_achievement)[0]
            Proof.objects.filter(user_achievement=user_achievement).exclude(id=proof.id).delete()

        form_kwargs = {
            'instance': user_achievement.achievement,
            'initial':  {'name': user_achievement.achievement.name,
                        'ach_img': user_achievement.achievement.image,
                        'description': proof.description,}
        }
        context['form'] = AchievementAddForm(**form_kwargs)
        context['photo_formset'] = ProofPhotoFormset(queryset=proof.photos.all())
        user = get_object_or_404(InLifeUser, id=user_id)
        context['user_sidebar'] = user
        return context


@login_required
def proof_vote(request, pk):
    proof = Proof.objects.get(pk=pk)
    if request.method == 'GET':
        action = request.GET.get('action')

        if action == 'believe':
            userproofvote, create = UserProofVote.objects.get_or_create(
                user=request.user, proof=proof)
            userproofvote.believe = True
            if create:
                userproofvote.save()
        if action == 'notbelieve':
            userproofvote, create = UserProofVote.objects.get_or_create(
                user=request.user, proof=proof)
            userproofvote.not_believe = True
            if create:
                userproofvote.save()

    response = {'response' : { 'believe': proof.believe_count(),
                               'not_believe': proof.not_believe_count() }}
    return HttpResponse(json.dumps(response), content_type='application/json')


class AchievementEditView(LoginRequiredMixin, UpdateView):
    model = Achievement
    template_name = 'create_achievement/achievement_form.html'
    form_class = AchievementAddForm

    def get_form(self, form_class):
        ach_id = self.kwargs.get('pk')
        if not ach_id or self.request.method == 'POST':
            return form_class(**self.get_form_kwargs())
        try:
            user_achievement = UserAchievement.objects.get(
                user=self.request.user,
                achievement_id=ach_id
                )
        except UserAchievement.DoesNotExist:
            try:
                achievement = Achievement.objects.get(id=ach_id)
                user_achievement = UserAchievement.objects.create(
                    user=self.request.user,
                    achievement=achievement,
                    status=UserAchievement.NOT_ACTIVE
                    )
            except Achievement.DoesNotExist:
                raise Http404
        try:
            proof = Proof.objects.get(user_achievement=user_achievement)
        except Proof.DoesNotExist:
            proof = Proof(user_achievement=user_achievement,
                                         proof_type = Proof.P_PHOTO)

        kwargs = self.get_form_kwargs()
        kwargs.update(
            {'initial': {'name': user_achievement.achievement.name,
                         'description': proof.description,
                         'ach_img': user_achievement.achievement.image}}
            )
        return form_class(**kwargs)

    def form_valid(self, form):
        if self.request.FILES.has_key('someval'):
            self.request.FILES.pop('someval')

        result = super(AchievementEditView, self).form_valid(form)
        self.object.created_by_user = True
        ach_img_name = self.request.POST.get('ach_img')
        self.object.image = self.request.FILES.get(ach_img_name)
        description = self.object.description
        self.object.description = ''
        self.object.save()
        user_achievement = get_object_or_404(UserAchievement,
                                             user=self.request.user,
                                             achievement_id=self.object.pk)
        user_achievement.save()
        proof, _ = Proof.objects.get_or_create(user_achievement=user_achievement,
                                               proof_type = Proof.P_PHOTO)
        proof_form = ProofForm(instance= proof, data=self.request.POST,
                               files=self.request.FILES)
        cover_cropping = form.cleaned_data['cropping']
        if proof_form.is_valid():
            proof = proof_form.save(commit=False)
            proof.description = description
            if proof.is_photo():
                proof_form.save()
                for name, image in self.request.FILES.items():
                    photo = fix_photo_orientation(image)
                    if name == ach_img_name:
                        pos = 1
                        cropping = cover_cropping
                        proof_ph = ProofPhoto(proof=proof, photo=photo,
                                              position=pos, cropping=cropping)
                        proof_ph.save()
                    else:
                        pos = 5
                        proof_ph = ProofPhoto(proof=proof, photo=photo, position=pos)
                        proof_ph.save()
        return result

    def form_invalid(self, form):
        print form.errors
        return super(AchievementEditView, self).form_invalid(form)

    def get_success_url(self):
        return reverse('user_achievement_confirmed_detail',
                       args=[self.request.user.id, self.object.pk])

    def get_context_data(self, **kwargs):
        context = super(AchievementEditView, self).get_context_data(**kwargs)
        context['photo_formset'] = ProofPhotoFormset()
        ach_id = self.kwargs.get('pk')
        achievement = Achievement.objects.get(id=int(ach_id))
        user_achievement = UserAchievement.objects.get(
            user=self.request.user, achievement=achievement)
        context['achievement'] = achievement
        context['user_achievement'] = user_achievement
        if ach_id and achievement.created_by_admin and \
                not self.request.user.is_staff:
            kwargs['form'].fields['name'].widget.attrs.update(
                {'readonly': 'readonly'})

        return context



@login_required
def userachievement_create(request, ach_id):
    achievement = get_object_or_404(Achievement, id=int(ach_id))
    user_achievement, c = UserAchievement.objects.get_or_create(
        user=request.user, achievement=achievement)
    if not user_achievement.is_active():
        user_achievement.activate()
    #спросить что делать если достижение существует
    return redirect(reverse('timeline', args=[request.user.id]))


class AchievementCreateView(LoginRequiredMixin, CreateView):
    model = Achievement
    template_name = 'create_achievement/achievement_form.html'
    form_class = AchievementAddForm

    def post(self, request, *args, **kwargs):
        self.object = None

        if self.request.FILES.has_key('someval'):
            self.request.FILES.pop('someval')

        form_class = self.get_form_class()
        achievement_form = self.get_form(form_class)
        proof_form = ProofForm(self.request.POST, self.request.FILES)
        photo_formset = ProofPhotoFormset(self.request.POST, self.request.FILES)

        forms = [achievement_form, proof_form, photo_formset]

        if all(form.is_valid() for form in forms):
            return self.form_valid(achievement_form, proof_form, photo_formset)
        else:
            return self.form_invalid(achievement_form, proof_form, photo_formset)


    def form_invalid(self, achievement_form, proof_form, photo_formset):
        u"""
        РЕВЬЮ: вместо print'ов необходимо использовать стандартные
        возможности django логгера
        """
        print "achievement_form.errors: ", achievement_form.errors
        print "proof_form.errors: ", proof_form.errors
        print "photo_formset.errors: ", photo_formset.errors
        print "formset.non_form_errors(): ", photo_formset.non_form_errors()
        return self.render_to_response(self.get_context_data(form=achievement_form, proof_form=proof_form, photo_formset=photo_formset))


    def form_valid(self, achievement_form, proof_form, photo_formset):
        self.object = achievement_form.save()
        a_user = self.request.user
        user_id = self.kwargs.get('user_id')
        if user_id:
            a_user = get_object_or_404(InLifeUser, pk=user_id)

        self.object.created_by_user = True
        description = self.object.description
        # self.object.description = ''
        self.object.save()
        user_achievement, created = UserAchievement.objects.get_or_create(
            user=a_user, achievement_id=self.object.pk)
        user_achievement.added()
        user_achievement.activate()
        if not user_id:
            user_achievement.status = UserAchievement.CONFIRMATION
        user_achievement.save()
       
        proof = proof_form.save(commit=False)
        proof.user_achievement = user_achievement
        proof.description = description
        proof.save()

        proof_photos = photo_formset.save()
        for proof_ph in proof_photos:
            proof_ph.proof = proof
            proof_ph.save()

        return HttpResponseRedirect(self.get_success_url())


    def get_success_url(self):
        user_id = self.kwargs.get('user_id')
        if user_id:
            return reverse('timeline', args=[user_id,] )
        else:
            return reverse('user_achievement_confirmed_detail',
                           args=[self.request.user.id, self.object.pk])

    def get_context_data(self, **kwargs):
        context = super(AchievementCreateView, self).get_context_data(**kwargs)

        photos = []
        for media in json.loads(self.request.GET.get('inst_photo', '[]')):
            photos.append(media)

        context['inst_photo'] = photos
        context['max_id'] = self.request.GET.get('max_id', 'not')
        context['access_token'] = self.request.GET.get('access_token', 'not')

        pp = ProofPhoto.objects.filter(user=self.request.user, proof__isnull=True)
        if context['access_token'] == 'not':
            pp.delete()
        else:
            context['photo_formset'] = ProofPhotoFormset(queryset=pp)

        context['photo_bool'] = pp
        if kwargs.get('photo_formset', None):
            context['photo_formset'] = kwargs['photo_formset']
            #invalid formset
        else:
            context['photo_formset'] = ProofPhotoFormset(queryset=pp)

        return context


 
class AchievementForUserCreateView(LoginRequiredMixin, StaffRequiredMixin, CreateView):
    model = Achievement
    template_name = 'create_achievement/create_achievement_for_user.html'
    form_class = AddAchievementToUserForm

    def post(self, request, *args, **kwargs):
        self.object = None


        form_class = self.get_form_class()
        achievement_form = self.get_form(form_class)
        photo_formset = ProofPhotoFormset(self.request.POST, self.request.FILES)

        forms = [achievement_form, photo_formset]

        if all(form.is_valid() for form in forms):
            return self.form_valid(achievement_form, photo_formset)
        else:
            return self.form_invalid(achievement_form, photo_formset)

    def form_invalid(self, achievement_form, photo_formset):
        u"""
        РЕВЬЮ: вместо print'ов необходимо использовать стандартные
        возможности django логгера
        """
        print "achievement_form.errors: ", achievement_form.errors
        print "photo_formset.errors: ", photo_formset.errors
        print "formset.non_form_errors(): ", photo_formset.non_form_errors()
        return self.render_to_response(self.get_context_data(form=achievement_form, photo_formset=photo_formset))


    def form_valid(self, form, photo_formset):
        achievement_user = get_object_or_404(InLifeUser, pk=self.kwargs['user_id'])
        status = int(form.cleaned_data['status'])
        result = super(AchievementForUserCreateView, self).form_valid(form)
        ach_img = self.get_achievement_image()
        self.object.created_by_user = True
        self.object.created_by_admin = True
        if ach_img.get('cropping'):
            self.object.cropping = ach_img['cropping']
        self.object.image = ach_img['image']
        self.object.author = self.request.user
        self.object.save()
        
        user_achievement, created = UserAchievement.objects.get_or_create(
            user=achievement_user, achievement_id=self.object.pk)
        if status == UserAchievement.ACTIVE:
            user_achievement.activate()
            user_achievement.accepted_by_user = False
        elif status == UserAchievement.RECEIVED:
            proof, created = Proof.objects.get_or_create(
                user_achievement=user_achievement
            )
            proof.status = Proof.STATUS_ACCEPTED
            proof.proof_type=Proof.P_PHOTO
            proof.admin=self.request.user
            proof.comment=form.cleaned_data.get("comment", "")
            proof.save()

            photo_formset = ProofPhotoFormset(self.request.POST, self.request.FILES)
            if photo_formset.is_valid():
                proof_photos = photo_formset.save()
                for proof_ph in proof_photos:
                    proof_ph.proof = proof
                    proof_ph.user = None
                    proof_ph.save()

        events_and_types(achievement_user,
                         events=(EMAIL_EVENT.ACHIEVEMENT_ADDITION, ),
                         types=(MessageConf.browser,
                                MessageConf.email,
                                MessageConf.notification, ))

        user_achievement.save()
        return result

    def get_achievement_image(self):
        ach_img = {'image': None, 'cropping': None}
        photo_formset = ProofPhotoFormset(self.request.POST)
        if photo_formset.is_valid():
            photo_forms = filter(lambda form: not form.cleaned_data.get('DELETE', False), photo_formset)
            #photo_forms = [form for form in photo_forms if not form.cleaned_data.get('DELETE', False) ]
            sorted_photo_forms = sorted(photo_forms, key=lambda form: form.cleaned_data.get('position', 99))
            if sorted_photo_forms:
                ach_img['image'] = sorted_photo_forms[0].cleaned_data.get('photo')
                ach_img['cropping'] = sorted_photo_forms[0].cleaned_data.get('cropping')
        return ach_img

    def get_success_url(self):
        user_id = self.kwargs['user_id']
        return reverse('timeline', args=[user_id,] )

    def get_context_data(self, **kwargs):
        context = super(AchievementForUserCreateView, self).get_context_data(**kwargs)
        context['achievement_user'] = get_object_or_404(InLifeUser, pk=self.kwargs['user_id'])
        if not context.get('photo_formset'):
            context['photo_formset'] = ProofPhotoFormset(queryset=ProofPhoto.objects.filter(user=self.request.user, proof__isnull=True))
        return context


class SearchView(FormView):
    form_class = SearchForm
    template_name = 'search/search.html'

    def get_form_kwargs(self):
        """
        Returns the keyword arguments for instantiating the form.
        """
        kwargs = {'initial': self.get_initial()}
        if self.request.method in ('POST', 'PUT'):
            kwargs.update({
                'data': self.request.POST,
                'files': self.request.FILES,
            })
        elif self.request.method in ('GET', ):
            kwargs.update({
                'data': self.request.GET,
            })
        return kwargs

    def get_query(self):
        """
        Returns the query provided by the user.

        Returns an empty string if the query is invalid.
        """
        if self.form.is_valid():
            return self.form.cleaned_data['q']
        return ''

    def get_search_results(self):
        query = self.get_query()
        result = SortedDict()
        if query:
            result['users'] = SearchQuerySet().filter(content_auto=query).models(InLifeUser)
            result['posts'] = SearchQuerySet().filter(content=query).models(UserAchievement)
            result['achievements'] = SearchQuerySet().filter(content=query).models(Achievement)
            result['groups'] = SearchQuerySet().filter(content=query).models(Group)
            for k, q in result.items():
                if q.count():
                    result[k].tab_class = "active"
                    break
        return result
    
    def get_context_data(self, **kwargs):
        context = super(SearchView, self).get_context_data(**kwargs)
        context['query'] = self.get_query()
        context['result'] = self.get_search_results()
        return context

    def get(self, request, *args, **kwargs):
        form_class = self.get_form_class()
        self.form = self.get_form(form_class)
        return self.render_to_response(self.get_context_data(form=self.form))
