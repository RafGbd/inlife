# coding=utf-8
from django import template
from django.core.files.storage import default_storage
from easy_thumbnails.files import get_thumbnailer
import os

u"""
РЕВЬЮ: нужно вынести данный метод из __init__ в отдельный модуль,
и импортировать его в inilife/__init__.py
"""
def render(self, context):
    instance = template.Variable(self.instance).resolve(context)
    if not instance:
        return

    ratiofield = instance._meta.get_field(self.ratiofieldname)
    image = getattr(instance, ratiofield.image_field)  # get imagefield

    if ratiofield.image_fk_field:  # image is ForeignKey
        # get the imagefield
        image = getattr(image, ratiofield.image_fk_field)
        if not image:
            return

    size = (int(ratiofield.width), int(ratiofield.height))
    box = getattr(instance, self.ratiofieldname)

    option = self.option
    if option:
        if option[0] == 'scale':
            width = size[0] * option[1]
            height = size[1] * option[1]
        elif option[0] == 'width':
            width = option[1]
            height = size[1] * width / size[0]
        elif option[0] == 'height':
            height = option[1]
            width = height * size[0] / size[1]
        size = (int(width), int(height))

    if ratiofield.adapt_rotation:
        if (image.height > image.width) != (size[1] > size[0]):
            # box needs rotation
            size = (size[1], size[0])

    thumbnailer = get_thumbnailer(image)
    thumbnail_options = {
        'size': size,
        'box': box,
        'crop': True,
        'detail': True,
        'upscale': self.upscale
    }
    if image and default_storage.exists(image.path):
        return thumbnailer.get_thumbnail(thumbnail_options).url
    elif image:
        return image.url
    
try:
    from image_cropping.templatetags.cropping import CroppingNode
    CroppingNode.render = render
except:
    pass