# coding: utf-8
import urllib2, os, re, hashlib
from datetime import datetime
from uuid import uuid4
from django.template.defaultfilters import slugify
from django.shortcuts import render_to_response
from django.core.files import File
from django.core.files.temp import NamedTemporaryFile
from social_auth.backends.twitter import TwitterBackend
from social_auth.backends.facebook import FacebookBackend
from social_auth.backends.contrib.vk import VKOAuth2Backend
from social_auth.backends.google import GoogleOAuth2Backend
from social_auth.models import UserSocialAuth
from social_auth.utils import setting
from app.const import REGISTRATION_METHOD, SEX
from app.models import InLifeUser
from inlife.settings import USER_PHOTOS_DIRNAME, MEDIA_ROOT

USER_PHOTOS_PATH = os.path.join(MEDIA_ROOT, USER_PHOTOS_DIRNAME)
VK_NO_PHOTO_URL = re.compile('.*camera_[a,b,c]\.gif')

if not os.path.isdir(USER_PHOTOS_PATH):
    os.makedirs(USER_PHOTOS_PATH)

def new_user(backend, response, details, user, is_new, *args, **kwargs):
    if is_new:
        user.registration_method = REGISTRATION_METHOD.SOCIAL_NETWORK
        if backend.__class__ == TwitterBackend:
            user.social_network_page = 'http://twitter.com/%s/' % details['username']
            if 'profile_image_url' in response:
                url = response['profile_image_url']
                if not 'default_profile' in url:
                    url = url.replace('_normal', '_bigger')
                user.photo = os.path.join(USER_PHOTOS_DIRNAME, _save_image_from_url(url))
        elif backend.__class__ == VKOAuth2Backend:
            user.social_network_page = 'http://vk.com/%s' % details['username']
            if 'sex' in response:
                if response['sex'] == 1:
                    user.sex = SEX.FEMALE
                elif response['sex'] == 2:
                    user.sex = SEX.MALE
            else:
                user.sex = SEX.UNDEFINED
            if 'bdate' in response:
                bdate = response['bdate']
                dateformat = '%d.%m' if len(bdate) < 5 else '%d.%m.%Y'
                user.birthdate = datetime.strptime(bdate, dateformat).date()
            if 'photo_max_orig' in response:
                if not VK_NO_PHOTO_URL.match(response['photo_max_orig']):
                    user.photo = os.path.join(USER_PHOTOS_DIRNAME, _save_image_from_url(response['photo_max_orig']))
        elif backend.__class__ == FacebookBackend:
            user.social_network_page = 'http://www.facebook.com/profile.php?id=%s' % response['id']
            h = hashlib.sha256()
            h.update(response['id'])
            filename, photo = get_image_from_url('http://graph.facebook.com/%s/picture?type=large' % response['id'], h.hexdigest()[:16] + '.gif')
            try:
                user.photo.save(filename, photo)
            except Exception, e:
                print "EXEPTION_"
                print e
            
            if 'gender' in response:
                if response['gender'] == 'male':
                    user.sex = SEX.MALE
                elif response['gender'] == 'female':
                    user.sex = SEX.FEMALE
            else:
                user.sex = SEX.UNDEFINED
            if 'email' in response:
                user.email = response['email']
            if 'birthday' in response:
                user.birthdate = datetime.strptime(response['birthday'], '%m/%d/%Y').date()
        elif backend.__class__ == GoogleOAuth2Backend:
            if 'link' in response:
                user.social_network_page = response['link']
            if 'picture' in response:
                user.photo = os.path.join(USER_PHOTOS_DIRNAME, _save_image_from_url(response['picture']))
            if 'gender' in response:
                if response['gender'] == 'male':
                    user.sex = SEX.MALE
                elif response['gender'] == 'female':
                    user.sex = SEX.FEMALE
            else:
                user.sex = SEX.UNDEFINED
        user.save()


def get_username(details, user=None, user_exists=UserSocialAuth.simple_user_exists, *args, **kwargs):
    from social_auth.exceptions import StopPipeline
    if user:
        return {'username': user.username}

    email_as_username = setting('SOCIAL_AUTH_USERNAME_IS_FULL_EMAIL', False)
    do_slugify = setting('SOCIAL_AUTH_SLUGIFY_USERNAMES', False)

    if email_as_username and details.get('email'):
        username = details['email']
    elif details.get('username'):
        username = unicode(details['username'])
    else:
        username = uuid4().get_hex()

    max_length = UserSocialAuth.username_max_length()
    final_username = UserSocialAuth.clean_username(username[:max_length])
    if do_slugify:
        final_username = slugify(final_username)

    if user_exists(username=final_username):
        user = InLifeUser.objects.get(username=final_username)
        try:
            social_user = UserSocialAuth.objects.get(user=user)
        except UserSocialAuth.DoesNotExist:
            provider = 'email'
        else:
            if social_user.provider.startswith('vk'):
                provider = u'ВКонтакте'
            else:
                provider = social_user.provider.title()
        return render_to_response('registration/user_exists.html', {'username': final_username, 'provider': provider})
    return {'username': final_username}


def get_image_from_url(url, filename=None):
    if not filename:
        filename = url.split('/')[-1]
        ext = filename.split('.')[-1]
        h = hashlib.sha256()
        h.update(url)
        filename = '%s.%s' % (h.hexdigest()[:16], ext)
    img_temp = NamedTemporaryFile(delete=True)
    img_temp.write(urllib2.urlopen(url).read())
    img_temp.flush()
    return unicode(filename), File(img_temp)

def _save_image_from_url(url, filename=None):
    if not filename:
        filename = url.split('/')[-1]
        ext = filename.split('.')[-1]
        h = hashlib.sha256()
        h.update(url)
        filename = '%s.%s' % (h.hexdigest()[:16], ext)
    filepath = os.path.join(USER_PHOTOS_PATH, filename)
    #  РЕВЬЮ: прямая запись в fs. Нужно использовать методы django storage
    f = open(filepath, 'w')
    f.write(urllib2.urlopen(url).read())
    f.close()
    return filename
