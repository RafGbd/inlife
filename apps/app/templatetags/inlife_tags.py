# -*- coding: utf-8 -*-
from django import template
from django.template import Context
from django.template import TemplateSyntaxError, Variable, Node, Variable, Library
from django.core.urlresolvers import reverse
from mptt.templatetags.mptt_tags import cache_tree_children
from django.utils.translation import ugettext as _
from app.models import Achievement, UserAchievement, \
                       Category
from app.models import UserAchievementLike
from django.core.cache import cache
from app.raw_queries import trees_with_categorys_achievement_count as trees_raw
from app.utils import get_or_none
from django.utils.safestring import mark_safe
from datetime import timedelta
import datetime
from django.db.models import Q
from itertools import chain
from users.models import Friendship
from apps.configs import achiever_config_value

register = template.Library()


from django.template import loader
from django.conf import settings


template_cache = {}
original_get_template = loader.get_template


def cached_get_template(template_name):
    global template_cache
    t = template_cache.get(template_name, None)
    if not t or settings.DEBUG:
        template_cache[template_name] = t = original_get_template(template_name)
    return t
loader.get_template = cached_get_template


@register.filter
def get(dict, key, default=''):
    """
    Usage:

    view:
    some_dict = {'keyA':'valueA','keyB':{'subKeyA':'subValueA','subKeyB':'subKeyB'},'keyC':'valueC'}
    keys = ['keyA','keyC']
    template:
    {{ some_dict|get:"keyA" }}
    {{ some_dict|get:"keyB"|get:"subKeyA" }}
    {% for key in keys %}{{ some_dict|get:key }}{% endfor %}
    """
    try:
        return dict.get(key, default)
    except:
        return default


class UsrbarSectionNode(template.Node):
    def __init__(self, var_name='trees'):
        self.var_name = var_name

    def render(self, context):
        context[self.var_name] = Category.objects.filter(
            level=0).order_by('tree_id', 'lft')
        return ''


@register.tag
def get_sections(parser, token):
    error = False
    try:
        tag_name, _as, var_name = token.split_contents()
        if _as != 'as':
            error = True
    except:
        error = True

    if error:
        raise template.TemplateSyntaxError, 'get_trees must be of the form, "get_trees as <var_name>"'
    else:
        return UsrbarSectionNode(var_name)


@register.tag
def value_from_settings(parser, token):
    bits = token.split_contents()
    if len(bits) < 2:
        raise TemplateSyntaxError("'%s' takes at least one " \
        "argument (settings constant to retrieve)" % bits[0])
    settingsvar = bits[1]
    settingsvar = settingsvar[1:-1] if settingsvar[0] == '"' else settingsvar
    asvar = None
    bits = bits[2:]
    if len(bits) >= 2 and bits[-2] == 'as':
          asvar = bits[-1]
          bits = bits[:-2]
    if len(bits):
          raise TemplateSyntaxError("'value_from_settings' didn't recognise " \
            "the arguments '%s'" % ", ".join(bits))
    return ValueFromSettings(settingsvar, asvar)

class ValueFromSettings(Node):
    def __init__(self, settingsvar, asvar):
        self.arg = Variable(settingsvar)
        self.asvar = asvar
    def render(self, context):
        ret_val = getattr(settings,str(self.arg))
        if self.asvar:
            context[self.asvar] = ret_val
            return ''
        else:
            return ret_val


@register.simple_tag
def qty(obj, current_user):
    count = UserAchievementLike.objects.filter(action=obj).count()
    checked = False

    if current_user.is_authenticated():
        try:
            if UserAchievementLike.objects.get(action=obj, who_like=current_user):
                checked = True
        except UserAchievementLike.DoesNotExist:
            pass

    t1 = '<div class="likeSend" id="%s"><a href="#"><div class="i_like"><i class="icon-heart"></i><div class="qty">%s</div></div></a></div>' % (obj.pk, count)
    t2 = '<div class="likeSend" id="%s"><a href="#"><div class="i_like red"><i class="icon-heart"></i><div class="qty">%s</div></div></a></div>' % (obj.pk, count)

    if checked:
        return mark_safe(t2)
    return mark_safe(t1)


@register.filter
def end_date(value):
    delta = timedelta(days=value)
    now = datetime.datetime.now()
    return now + delta


@register.filter
def get_usercategory_reached(category, user):
    try:
        return UserCategory.objects.get(category=category, user=user).reached
    except UserCategory.DoesNotExist:
        return False


@register.filter
def get_achiv_count(tree, user):
    #Возвращаем кол-во выполненых достижений юзера в указаном разделе
    return UserAchievement.objects.filter(status=UserAchievement.RECEIVED, achievement__tree_id=tree.tree_id, user=user).count()


@register.filter
def get_char_count(tree, user):
    #Возвращаем кол-во выполненых целей юзера в указаном разделе
    return UserCategory.objects.filter(user=user, category__achievements__tree_id=tree.tree_id, reached=True).count()

@register.filter
def friend_status(one, two):
    #TODO: переписать на один запрос!!!
    status = 'not_friend'
    if one in two.friends.all():
        status = 'friend'
    else:
        try:
            Friendship.objects.get(sender=one, reciever=two, status=Friendship.WAITING_FOR_ANSWER)
            status = 'want_to_friendship'
        except Friendship.DoesNotExist:
            pass
        try:
            Friendship.objects.get(sender=two, reciever=one, status=Friendship.WAITING_FOR_ANSWER)
            status = 'waiting_for_response'
        except Friendship.DoesNotExist:
            pass
    return status


@register.inclusion_tag('sidebar.html', takes_context=True)
def user_sidebar(context, user):    
    context['biggest_achievements'] = user.get_biggest_achievements()
    
    context['user_sidebar'] = user
    groups = []
    # def is_in_group(user_list):
    #     for u in user_list:
    #         if u['user'] == user.pk:
    #             return True
    #     return False
    # for group in Group.objects.all().select_related():
    #     if is_in_group(group.get_users()):
    #         groups.append(group)
    context['groups'] = groups 
    return context


@register.simple_tag
def get_settings(key):
    return achiever_config_value(key)

@register.simple_tag
def get_svg(svg):
    try:
        f = open(svg.path, 'r')
        svg_code = f.read().strip()
        f.close()
        return svg_code
    except (IOError, NotImplementedError) as e:
        return str(e)


@register.inclusion_tag('achievement/achiv_span.html')
def achiv_span(obj, size='', user=None):

    classes = ["achiv", size]
    c = {
        'span_class': '',
        'stars_range': 1
    }

    if isinstance(obj, UserAchievement):
        c['stars_range'] = range(obj.achievement.stars_number)
    elif isinstance(obj, Achievement):
        c['stars_range'] = range(obj.stars_number)

    if hasattr(obj, 'status'):
        if obj.status not in [UserAchievement.RECEIVED, UserAchievement.CONFIRMATION]:
            classes.append("achiv-target")
            c['stars_range'] = range(0)

    c['span_class'] = " ".join(classes)
    return c


@register.inclusion_tag('achievement/achiv_link.html')
def achiv_link(obj, size='', user=None, data_title=False):
    c = {
        'a_class': '',
        'href': '',
        'data_title': '',
        'stars_range': 1,
    }

    classes = ['achiv', size]

    if isinstance(obj, Achievement):
        c['href'] = reverse('achievement', args=(obj.id,))
        c['data_title'] = u"Достижение «%s»" % obj.name if data_title else None
        c['stars_range'] = range(obj.stars_number)

    elif isinstance(obj, UserAchievement):
        c['href'] = obj.get_absolute_url()
        c['data_title'] = u"Достижение «%s»" % obj.achievement.name if data_title else None
        c['stars_range'] = range(obj.achievement.stars_number)

    if hasattr(obj, 'status'):
        if obj.status != UserAchievement.RECEIVED:
            classes.append("achiv-target")
            c['stars_range'] = range(0)

    c['a_class'] = " ".join(classes)

    return c

@register.simple_tag
def this_month():
    today = datetime.date.today()
    print today.strftime("%b")
    return today.strftime("%b")

@register.simple_tag
def this_year():
    today = datetime.date.today()
    return today.strftime("%Y")

@register.simple_tag(takes_context=True)
def active_page_class(context, page):
    if context['request'].path == reverse(page):
        return 'count-li active'
    else:
        return ''
