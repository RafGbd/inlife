# coding: utf-8
from datetime import datetime, timedelta
from django import forms
from django.forms import extras
from django.forms.models import modelformset_factory
from django.forms.models import inlineformset_factory
from django.contrib.auth import get_user_model
from django.utils.translation import ugettext_lazy as _
from django.template.defaultfilters import striptags
from registration.forms import RegistrationFormUniqueEmail
from app.const import SEX_CHOICES
from app.models import Proof, ProofPhoto, Achievement, UserAchievement, ProofPhoto
from image_cropping import ImageCropWidget, ImageRatioField, ImageCropField
from app.widgets import AppImageCropWidget
from tinymce.widgets import TinyMCE
from inlife.utils import AchieverFormMixin
from .widgets import IconColorWidget, WYSIWYGTextWidget
InLifeUser = get_user_model()

class ProofForm(forms.ModelForm):
    #cropping = forms.CharField(widget=forms.HiddenInput(), required=False)
    #ach_img = forms.CharField(widget=forms.HiddenInput())

    description = forms.CharField(widget=forms.TextInput(
        attrs={
            'rows': 14,
            'cols': 40,
            'class': u"form-control",
        }),
        required=False
     )

    class Meta:
        model = Proof
        fields = ('description', )


class SimpleProofPhotoForm(forms.Form):
    photo = forms.ImageField()


class ProofPhotoForm(forms.ModelForm):

    class Meta:
        model = ProofPhoto
        widgets = {
            'photo': ImageCropWidget,
            'position': forms.HiddenInput
        }

class BaseProofPhotoFormset(forms.models.BaseModelFormSet):
    def clean(self):
        if any(self.errors):
            return
        forms_count = len(self.forms) - len(self.deleted_forms)
        if forms_count < 1:
            for deleted_form in self.deleted_forms:
                deleted_form.fields['DELETE'] = False
            raise forms.ValidationError(u"Необходмио выбрать хотя бы одну картинку")

ProofPhotoFormset = modelformset_factory(ProofPhoto, formset=BaseProofPhotoFormset,
    form=ProofPhotoForm,
    fields=('photo','cropping','position'), can_delete=True, extra=0)

class ReplyForm(forms.Form):
    recipient = forms.IntegerField(widget=forms.HiddenInput())
    text = forms.CharField(widget=forms.Textarea(
        attrs={
            'class': u'form-control',
            'rows':"3",
            'cols':"15",
        }),
        label=u'Сообщение'
    )

    def clean_recipient(self):
        data = self.cleaned_data['recipient']
        if not data:
            raise forms.ValidationError()
        try:
            instance = InLifeUser.objects.get(id=data)
        except InLifeUser.DoesNotExist:
            raise forms.ValidationError()
        return instance

class ActionCommentForm(forms.Form):
    comment = forms.CharField(widget=forms.TextInput(
        attrs={
            'class': u'add_comment',
            'placeholder':u'написать комментарий',
            'autocomplete': 'off',
        }),
    )

class CategoryActivateForm(forms.Form):
    def __init__(self, category=None, *args, **kwargs):
        super(CategoryActivateForm, self).__init__(*args, **kwargs)
        self._category = category
        self.max_intensity = category.time_to_complete * 3
        self.min_intensity = category.time_to_complete / 3
        if self.min_intensity < 1:
            self.min_intensity = 1
        self.fields['intensity'].widget.attrs['data-title'] = u"Максимум: %s" % self.max_intensity

    intensity = forms.IntegerField(widget=forms.TextInput(
        attrs={
            'class': "qty_hours hint form-control",
            'autocomplete': 'off',
            'maxlength':"2",
        }),
    )

    def clean(self):
        cleaned_data = super(CategoryActivateForm, self).clean()
        if 'intensity' in cleaned_data:
            intensity = cleaned_data['intensity']
            if intensity > self.max_intensity:
                self._errors['intensity'] = self.error_class(["Максимальная интенсивность: %s" % self.max_intensity ])
            if intensity < self.min_intensity:
                self._errors['intensity'] = self.error_class(["Минимальная интенсивность: %s" % self.min_intensity ])
        else:
            self._errors['intensity'] = self.error_class(["Необходимо указать интенсивность"])
        return cleaned_data
    
class AchievementAddForm(forms.ModelForm):                
    class Meta:
        model = Achievement
        widgets = {
            'points_weight': forms.HiddenInput,
            'time_to_complete': forms.HiddenInput,
            'stars_number': forms.HiddenInput
        }
        #fields 
        exclude = ('date', 'parent', )
        
    short_decscription = forms.CharField(widget=forms.Textarea(
        attrs={
            'rows': 4,
            'cols': 40,
            'class': u"form-control",
        }),
        required=False
    )
    description = forms.CharField(widget=forms.TextInput(
        attrs={
            'rows': 14,
            'cols': 40,
            'class': u"form-control",
        }),
        required=False,
    )    
    name = forms.CharField(widget=forms.TextInput(
        attrs={
            'placeholder': u'Введите название достижения',
            'class': u"form-control",
            'data-bind': "value: ach_name, valueUpdate: 'afterkeydown' "
        }),
    )
    cropping = forms.CharField(widget=forms.HiddenInput(), required=False)
    ach_img = forms.CharField(widget=forms.HiddenInput(), required=False)
    
    def clean_short_decscription(self):
        return striptags(self.cleaned_data['short_decscription'])

    def clean_description(self):
        return self.cleaned_data['description']


class AddAchievementToUserForm(AchievementAddForm):
    """Форма добавления достижения пользователю админом"""
    STATUS_CHOICES = (
        (UserAchievement.ACTIVE, u'Активированно'),
        (UserAchievement.RECEIVED, u'Получено'),
    )
    status = forms.ChoiceField(choices = STATUS_CHOICES, widget=forms.RadioSelect)
    comment = forms.CharField(required=False, widget=forms.TextInput(
        attrs={
            'class': u"form-control",
        }),)
    description = forms.CharField(widget=forms.TextInput(
        attrs={
            'rows': 14,
            'cols': 40,
            'class': u"form-control",
        }),
        required=False
     )
    short_decscription = forms.CharField(widget=forms.Textarea(
        attrs={
            'rows': 4,
            'cols': 40,
            'class': u"form-control",
        }),
        required=False
     )
    time_to_complete = forms.IntegerField(widget=forms.TextInput(
        attrs={
            'class': "qty_hours hint form-control",
            'autocomplete': 'off',
            'maxlength':"2",
        }),
    )
        
class AddAchievementToUserGenTreeForm(AchievementAddForm):   
    parent = forms.ModelChoiceField(queryset=Achievement.objects.filter(), empty_label=u'Выберите категорию')


class SearchForm(forms.Form):
    q = forms.CharField(required=False, label=_('Search'))


class UserAchievementCommentForm(forms.Form):
    comment = forms.CharField(widget=forms.TextInput(
        attrs={
            'class': u'add_comment',
            'placeholder':u'написать комментарий',
            'autocomplete': 'off',
        }),
    )
