# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from image_cropping.widgets import ImageCropWidget, CropWidget, get_attrs
from django.forms.util import flatatt
from django.contrib.admin.widgets import AdminFileWidget
from django.utils.html import format_html, format_html_join, conditional_escape
from django.utils.safestring import mark_safe
from django.utils.encoding import force_text, python_2_unicode_compatible
from django import forms
from django.conf import settings


class AppImageCropWidget(AdminFileWidget, CropWidget):
    def _media(self):
        css = self.Media.css
        js = list(self.Media.js)
        js.remove('image_cropping/image_cropping.js')
        js.append('image_cropping/image_cropping_prev.js')
        #заменяем оригинальный image_cropping на наш, с превьюшкой
        return forms.Media(css=css, js=js)
    media = property(_media)
    template_with_initial = u'''<span class="btn btn-black fileinput-button">
    <span><i class="icon-picture"></i> Выбрать фото
        %(input)s
    </span>
    </span>'''
    template_with_clear = ''

    def render(self, name, value, attrs=None):
        if not attrs:
            attrs = {}
        if value:
            attrs.update(get_attrs(value, name))
        attrs.update({'onchange': 'image_cropping.update_image()'})

        substitutions = {
            'initial_text': self.initial_text,
            'input_text': self.input_text,
            'clear_template': '',
            'clear_checkbox_label': self.clear_checkbox_label,
        }
        template = u'''<span class="btn btn-black fileinput-button">
        <span><i class="icon-picture"></i> Выбрать фото
            %(input)s
        </span>
        </span>'''
        substitutions['input'] = super(forms.ClearableFileInput, self).render(name, value, attrs)

        if value and hasattr(value, "url"):
            template = self.template_with_initial
            substitutions['initial'] = format_html('<a href="{0}">{1}</a>',
                                                   value.url,
                                                   force_text(value))
            if not self.is_required:
                checkbox_name = self.clear_checkbox_name(name)
                checkbox_id = self.clear_checkbox_id(checkbox_name)
                substitutions['clear_checkbox_name'] = conditional_escape(checkbox_name)
                substitutions['clear_checkbox_id'] = conditional_escape(checkbox_id)
                substitutions['clear'] = forms.CheckboxInput().render(checkbox_name, False, attrs={'id': checkbox_id})
                substitutions['clear_template'] = self.template_with_clear % substitutions
        return mark_safe(template % substitutions)




class IconColorWidgetRadioInput(forms.widgets.RadioInput):
    def render(self, name=None, value=None, attrs=None, choices=()):
        name = name or self.name
        value = value or self.value
        attrs = attrs or self.attrs
        if 'id' in self.attrs:
            label_for = format_html(' for="{0}_{1}"  class="achiv mini {2}"', self.attrs['id'], self.index, self.choice_value)
        else:
            label_for = ''
        choice_label = force_text(self.choice_label)
        return format_html('{0}<div><label{1}><i class="icon-trophy"></i></label></div>', self.tag(), label_for)



class IconColorWidgetRenderer(forms.widgets.RadioFieldRenderer):

    def __iter__(self):
        for i, choice in enumerate(self.choices):
            yield IconColorWidgetRadioInput(self.name, self.value, self.attrs.copy(), choice, i)

    def __getitem__(self, idx):
        choice = self.choices[idx] # Let the IndexError propogate
        return IconColorWidgetRadioInput(self.name, self.value, self.attrs.copy(), choice, idx)

    def render(self):
        result = format_html('<div class="choose_color">\n{0}\n</div>',
                           format_html_join('\n', '<div class="choose_color-box">{0}</div>',
                                            [(force_text(w),) for w in self]
                                            ))
        return result


class IconColorWidget(forms.RadioSelect):
    renderer = IconColorWidgetRenderer


class WYSIWYGTextWidget(forms.widgets.TextInput):
    def render(self, name, value, attrs=None):
        if value is None:
            value = ''
        final_attrs = self.build_attrs(attrs, type=self.input_type, name=name)
        if value != '':
            # Only add the 'value' attribute if a value is non-empty.
            final_attrs['value'] = force_text(self._format_value(value))
        field_id =  final_attrs.get("id", "")
        
        field = format_html('<input{0} />', flatatt(final_attrs))
        script = """<script type="text/javascript">
            tinymce.init({
              selector: "#%s",
              plugins: "example, autolink, pagebreak, layer, table, save, advhr, advlink, emotions, iespell, inlinepopups, insertdatetime," +
                       " preview, media, searchreplace, print, contextmenu, paste, directionality, fullscreen, noneditable," +
                       " visualchars, nonbreaking, xhtmlxtras, template, advlist, youtube",
              theme_advanced_buttons1: "bold,italic,underline,strikethrough,|,formatselect,example,iespell,youtube",
              theme_advanced_path : false,
              theme_advanced_resizing : true,
              cleanup : false,
              convert_urls: false,
              theme_advanced_blockformats : "h1, h2, h4",
              extended_valid_elements: "h1, h2, h3, h4, h5, h6, iframe[src|title|width|height|allowfullscreen|frameborder|class|id|data-mce-src], object[classid|width|height|codebase|*], param[name|value|_value|*], embed[type|width|height|src|*]",
            });
        </script>""" % field_id
        return mark_safe(field + script)

    class Media:
        js = ('tiny_mce/tiny_mce.js', 'js/file-input.js')

class DatePickerWidget(forms.DateInput):
    def render(self, name, value, attrs=None):
        if value is None:
            value = ''
        final_attrs = self.build_attrs(attrs, type=self.input_type, name=name)
        if value != '':
            # Only add the 'value' attribute if a value is non-empty.
            final_attrs['value'] = force_text(self._format_value(value))
        field_id =  final_attrs.get("id", "")
        field_name =  final_attrs.get("name", "")
        field_name = field_name.replace('-', '_')
        field =  format_html('<input{0} />', flatatt(final_attrs))
        script = """<script type="text/javascript">
        var %s = $('#%s').datepicker()
         .on('changeDate', function(ev) {
            %s.hide();
        }).data('datepicker');
        </script>""" % (field_name, field_id, field_name)
        return mark_safe(field + script)

    class Media:
        css = {
            'all': ('css/datepicker.css',)
        }
        js = ('js/vendor/bootstrap-datepicker.js',)

class ModelLinkWidget(forms.Widget):
    def __init__(self, obj, attrs=None):
        self.object = obj
        super(ModelLinkWidget, self).__init__(attrs)

    def render(self, name, value, attrs=None):
        if self.object.pk:
            if self.object.user_achievement:
                user_achievement = self.object.user_achievement
                achievement = user_achievement.achievement
                return mark_safe(
                    u'<a target="_blank" href="%s">%s</a>' % \
                          (
                           achievement.get_absolute_url(),
                           achievement.name
                           )
            )
        else:
            return mark_safe(u'')