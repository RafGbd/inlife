# -*- coding: utf-8 -*-
from celery import task
from django.utils import timezone
from app.const import EMAIL_EVENT


@task()
def check_userachievement_fail(user_achievement_id):
    from app.models import UserAchievement
    try:
        user_achievement = UserAchievement.objects.get(id=user_achievement_id)
    except UserAchievement.DoesNotExist, e:
        return
    if user_achievement.is_active():
        if timezone.now() > user_achievement.get_end_date():
            user_achievement.fail()


@task()
def notify_if_still_active(user_achievement_id):
    from app.models import UserAchievement
    from app.utils import send_templated_email
    try:
        user_achievement = UserAchievement.objects.get(id=user_achievement_id)
    except UserAchievement.DoesNotExist, e:
        return
    if user_achievement.is_active():
        try:
            user_email = user_achievement.user.email
        except Exception, e:
            user_email = None
        if user_email:
            send_templated_email(
                EMAIL_EVENT.ACHIEVEMENT_TIME_EXPIRES,
                user_email,
                user_achievement = user_achievement,
            )

@task()
def unactivate_achievement_on_end_date(achievement_id):
    from app.models import Achievement
    try:
        achievement = Achievement.objects.get(id=achievement_id)
    except Achievement.DoesNotExist, e:
        return
    achievement.is_active = False
    achievement.save()


@task()
def rebuild_achievement_tree():
    from app.models import Achievement
    Achievement.objects.rebuild()


@task()
def delete_proof_photo(proof_photo_id):
    """Удаление фоток, если в течении 2х часов её не прикрепили к пруфу"""
    from app.models import Proof, ProofPhoto
    try:
        proof_photo = ProofPhoto.objects.get(id=proof_photo_id)
        if not proof_photo.proof or proof_photo.proof.status == Proof.STATUS_HIDDEN:
            proof_photo.delete()
    except Exception, e:
        pass


@task()
def notify_about_end_date(user):
    from notifications.models import MessageConf
    from app.const import EMAIL_EVENT
    MessageConf.message(user, event=EMAIL_EVENT.EVENT_GROUP,
                        notification_type=MessageConf.email)
