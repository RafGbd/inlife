# coding: utf-8
import json
from django.contrib import admin
from django.contrib.admin import SimpleListFilter, TabularInline
from django.contrib.auth.admin import UserAdmin
from django.contrib.auth.forms import ReadOnlyPasswordHashField
from django.conf import settings
from django.shortcuts import HttpResponse
from django.core.urlresolvers import reverse
from django.utils.translation import ugettext_lazy as _, string_concat
from django import forms
from django.forms.fields import ChoiceField
from django.forms.widgets import SelectMultiple, CheckboxSelectMultiple
from image_cropping import ImageCroppingMixin
from app.models import *
from groups.models import GroupEvent
from .const import EMAIL_EVENT
from .utils import send_templated_email
from feincms.admin import tree_editor
from mptt.admin import MPTTModelAdmin
from .admin_forms import (CategoryAdminForm, AchievementAdminForm,
                          ApproveInterfaceAdminForm, ApproveProofAdminForm)
from django.db.models import F, Q
# from image_cropping import ImageCroppingMixin
from django.http import HttpResponseRedirect


class AchievementAdmin(ImageCroppingMixin, admin.ModelAdmin):
    list_per_page = 5000
    list_filter = ('created_by_user', )


class ApproveInterfaceAdmin(admin.ModelAdmin):
    list_filter = ('proof_type','status')
    list_display = ('get_proof', 'user_timeline')
    fields = ('comment', 'points', 'description', 'stars_number', 'groups')
    readonly_fields = ('user_achievement',)
    form = ApproveInterfaceAdminForm

    def queryset(self, request):
        qs = super(ApproveInterfaceAdmin, self).queryset(request)
        return qs.exclude(is_default=True)

    def get_proof(self, obj):
        return obj

    def user_timeline(self, obj):
        user = obj.get_user()
        if user:
            timeline_url = reverse('timeline', args=[user.id])
            return '<a href="%s">%s</a>' % (timeline_url, user.username)

    user_timeline.allow_tags = True
    user_timeline.short_description = u'Пользовтаель'
    get_proof.short_description = u'Достижения'

    def change_view(self, request, object_id, form_url='', extra_context=None):
        from groups.models import Group, WallAction
        
        proof = Proof.objects.get(id=object_id)
        if proof.user_achievement:
            achievement = proof.user_achievement.achievement
        else:
            achievement = None
        extra_context = extra_context or {}
        if request.method == 'POST':
            if u'_deny' in request.POST:
                proof.status = Proof.STATUS_DECLINED
            elif u'_approve' in request.POST:
                proof.status = Proof.STATUS_ACCEPTED
            elif u'_doubtfully' in request.POST:
                proof.status = Proof.STATUS_DOUBTFULLY

            # я не знаю по какому принципу здесь построен нейминг и что может сломаться если я уберу предыдущий вариант,
            # поэтому я буду не переименовывать, а добавлю ещё ветку if u'comment'.
            # Это решит проблему не сохранения данных в модели

            if proof.status:
                action = proof.user_achievement
                old_selected_groups = set(WallAction.objects.filter(action=action).values_list('group_id', flat=True))
                new_selected_groups = set(request.POST.getlist('groups'))
                to_create = new_selected_groups - old_selected_groups
                to_delete = old_selected_groups - new_selected_groups
                WallAction.objects.filter(action=action, group_id__in = to_delete).delete()
                for group_id in to_create:
                    WallAction.objects.create(action=action, group_id = group_id)



            if u'comment' in request.POST:
                proof.comment = request.POST.get(u'comment', '')
            if u'_comment' in request.POST:
                proof.comment = request.POST.get(u'_comment', '')
            if u'points' in request.POST and request.POST.get('points'):
                proof.points = int(request.POST.get('points'))

            if proof.user_achievement:
                if u'stars_number' in request.POST or u'_stars_number' in request.POST:
                    stars_number = int(
                        request.POST.get(u'stars_number', 0)
                        or
                        request.POST.get(u'_stars_number', 0)
                        )
                    if stars_number:
                        achievement = proof.user_achievement.achievement
                        achievement.stars_number = stars_number
                        achievement.save()

            proof.admin = request.user
            proof.save()
            try:
                user_email = proof.user_achievement.user.email
            except Exception, e:
                user_email = None
            if user_email:
                send_templated_email(
                    EMAIL_EVENT.ADMIN_FEEDBACK,
                    user_email,
                    proof = proof,
                )
            return self.response_change(request, proof)
        else:
            if proof.proof_type == Proof.P_PHOTO:
                extra_context['photos'] = proof.photos.all()
            elif proof.proof_type == Proof.P_VIDEO and proof.video:
                extra_context['video'] = proof.video.url
            extra_context['type_name'] = proof.get_proof_type_display()
            extra_context['achievement'] = proof.user_achievement.achievement if proof.user_achievement else None
        return super(ApproveInterfaceAdmin, self).change_view(request, object_id, form_url, extra_context=extra_context)

    def changelist_view(self, request, extra_context=None):
        # переопределяем дефолтный фильтр для status
        if not request.GET.has_key('status__exact'):
            q = request.GET.copy()
            q['status__exact'] = '0'
            request.GET = q
            request.META['QUERY_STRING'] = request.GET.urlencode()
        return super(ApproveInterfaceAdmin,self).changelist_view(request, extra_context=extra_context)

    def save_model(self, request, obj, form, change):
        if change and obj.is_verified():
            obj.admin = request.user
        obj.save()

    def response_change(self, request, obj):
        # кастомизируем редирект, при измении данных с дашборда.
        response = super(ApproveInterfaceAdmin, self).response_change(request, obj)
        if isinstance(response, HttpResponseRedirect):
            return HttpResponseRedirect('/admin/')

        return response




class UserAchievementAdmin(admin.ModelAdmin):
    list_filter = ('status',)


class CategoryAdmin(tree_editor.TreeEditor):
    form = CategoryAdminForm


class InterestAdmin(admin.ModelAdmin):
    filter_horizontal = ('achievements',)



class FilterValueInlineAdmin(TabularInline):
    model = FilterValue
    extra = 3


class FilterAdmin(admin.ModelAdmin):
    inlines = [FilterValueInlineAdmin, ]


class ApproveProofPhotoInline(ImageCroppingMixin, admin.TabularInline):
    model = ProofPhoto
    extra = 1


class ProofUsersListFilter(SimpleListFilter):
    title = u'Пользователи'
    parameter_name = 'user_id'

    def lookups(self, request, model_admin):
        filters = []
        users = set()
        for proof in Proof.objects.all().exclude(is_default=True).select_related('user_achievement', 'user_achievement__user'):
            users.add(proof.user_achievement.user)
        for user in users:
            filters.append([user.id, user.get_full_name()])
        return filters

    def queryset(self, request, queryset):
        if self.value():
            return queryset.filter(user_achievement__user_id = self.value() )
        else:
            return queryset


class ApproveProofAdmin(admin.ModelAdmin):
    list_select_related = True
    readonly_fields = ('user_achievement',)
    list_display = ('get_proof', 'user_timeline',)
    list_filter = (ProofUsersListFilter, 'status')
    form = ApproveProofAdminForm
    inlines = [
        ApproveProofPhotoInline,
    ]
    
    search_fields = ['user_achievement__achievement__name',
                     'user_achievement__user__username']

    def change_view(self, request, object_id, form_url='', extra_context=None):
        user_achievement = Proof.objects.get(id=object_id).user_achievement
        d = request.POST.get('activate_time_0')
        t = request.POST.get('activate_time_1')
        if user_achievement and d and t:
            dt = datetime.strptime('%s %s' % (d, t, ), '%d.%m.%Y %H:%M:%S')
            user_achievement.activate_time = dt
            user_achievement.save()
        stars_number = request.POST.get('stars_number')
        if user_achievement.achievement and stars_number:
            user_achievement.achievement.stars_number = stars_number
            user_achievement.achievement.save()
        return super(ApproveProofAdmin, self).change_view(
            request, object_id, form_url, extra_context=extra_context)

    def get_proof(self, obj):
        return obj

    def user_timeline(self, obj):
        user = obj.get_user()
        if user:
            timeline_url = reverse('timeline', args=[user.id])
            return '<a href="%s">%s</a>' % (timeline_url, user.username)

    user_timeline.allow_tags = True
    user_timeline.short_description = u'Пользователь'
    get_proof.short_description = u'Достижения'


class DefaultProofAdmin(admin.ModelAdmin):
    list_select_related = True
    readonly_fields = ('user_achievement',)
    inlines = [
        ApproveProofPhotoInline,
    ]

    def queryset(self, request):
        qs = super(DefaultProofAdmin, self).queryset(request)
        return qs.filter(is_default=True)


class ProofPhotoAdmin(ImageCroppingMixin, admin.ModelAdmin):
    pass

admin.site.register(DefaultProof, DefaultProofAdmin)
admin.site.register(ApproveProof, ApproveProofAdmin)
admin.site.register(Achievement, AchievementAdmin)
admin.site.register(Proof, ApproveInterfaceAdmin)
admin.site.register(Category, CategoryAdmin)
admin.site.register(Interest, InterestAdmin)
admin.site.register(Filter, FilterAdmin)
admin.site.register(AchievementFilter)
admin.site.register(ProofPhoto, ProofPhotoAdmin)
