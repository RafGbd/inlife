# coding: utf-8
from django.contrib.admin.templatetags.admin_static import static
from django.utils.encoding import smart_unicode, force_unicode
from django.utils.safestring import  mark_safe  
from itertools import chain
from django import forms
from django.forms.fields import ChoiceField
from django.contrib.auth.forms import ReadOnlyPasswordHashField
from django.forms.widgets import SelectMultiple, CheckboxSelectMultiple
from django.utils.html import escape, conditional_escape
from django.utils.translation import ugettext_lazy as _, string_concat
from django.contrib.admin.widgets import FilteredSelectMultiple, AdminSplitDateTime
from .models import Category, Achievement, Proof
from .widgets import ModelLinkWidget


# class UserCreationForm(forms.ModelForm):
#     password1 = forms.CharField(label=u'Пароль', widget=forms.PasswordInput)
#     password2 = forms.CharField(label=u'Подтверждение пароля', widget=forms.PasswordInput)

#     class Meta:
#         model = InLifeUser

#     def clean_password2(self):
#         password1 = self.cleaned_data.get("password1")
#         password2 = self.cleaned_data.get("password2")
#         if password1 and password2 and password1 != password2:
#             raise forms.ValidationError(u'Пароли не совпадают')
#         return password2

#     def save(self, commit=True):
#         user = super(UserCreationForm, self).save(commit=False)
#         user.set_password(self.cleaned_data['password1'])
#         if commit:
#             user.save()
#         return user


# class UserChangeForm(forms.ModelForm):
#     password = ReadOnlyPasswordHashField()

#     class Meta:
#         model = InLifeUser
#         fields = ('username', 'email', 'first_name', 'middle_name', 'last_name', 'birthdate', 'sex', 'photo', 'registration_method',
#                   'social_network_page', 'friends', 'trust', 'is_active', 'is_staff', 'is_superuser', 'is_premium', 'groups', 'user_permissions', 'last_login', 'date_joined', 'last_clear_pg_date', 'level')

#     def clean_password(self):
#         return self.initial['password']


class AchievementAdminForm(forms.ModelForm):
    class Meta:
        model = Achievement


class BranchFilteredSelectMultiple(FilteredSelectMultiple):
    def render_options(self, choices, selected_choices):

        def render_option(option_value, option_label, level, attrs):
            option_value = force_unicode(option_value)
            selected_html = (option_value in selected_choices) and u' selected="selected"' or ''
            return u'<option value="%s" level="%s" tree_id="%s" lft="%s" rght="%s" %s>%s</option>' % (
                escape(option_value),
                level,
                attrs['tree_id'],
                attrs['lft'],
                attrs['rght'],
                selected_html,
                conditional_escape(force_unicode(option_label)),
            )
        # Normalize to strings.
        selected_choices = set([force_unicode(v) for v in selected_choices])
        output = []
        for option_value, option_label, level, attrs in chain(self.choices, choices):
            if isinstance(option_label, (list, tuple)):
                output.append(u'<optgroup label="%s">' % escape(force_unicode(option_value)))
                for option in option_label:
                    output.append(render_option(*option))
                output.append(u'</optgroup>')
            else:
                output.append(render_option(option_value, option_label, level, attrs))
        return u'\n'.join(output)
    class Media:
        extend = False
        js = (
            static("admin/js/core.js"),
            static("js/SelectFilterBranch.js"),
            static("admin/js/SelectFilter2.js"),
            
            )

class CategoryAchievementChoiceField(forms.ModelMultipleChoiceField):
    def __init__(self, queryset, cache_choices=False, required=False,
                 widget=None, label=None, initial=None,
                 help_text='', instance=None, *args, **kwargs):
        self.instance = instance
        super(forms.ModelMultipleChoiceField, self).__init__(queryset, None,
            cache_choices, required, widget, label, initial, help_text,
            *args, **kwargs)
        # Remove this in Django 1.8
        if isinstance(self.widget, SelectMultiple) and not isinstance(self.widget, CheckboxSelectMultiple):
            msg = _('Hold down "Control", or "Command" on a Mac, to select more than one.')
            self.help_text = string_concat(self.help_text, ' ', msg)

    def _get_choices(self):
        pk = None
        try:
            pk = self.instance.pk
        except AttributeError:
            pass
        achievements_with_chars = Achievement.objects.filter(category__in=Category.objects.exclude(pk=pk)).distinct()
        choices = []
        for ach in self.queryset:
            label = (ach.level * '-') + ach.name
            if ach in achievements_with_chars:
                label += '(!)'
            attrs = {'tree_id': ach.tree_id, 'lft': ach.lft, 'rght': ach.rght}
            choices.append((ach.pk, label, ach.level, attrs))
        return choices

    choices = property(_get_choices, ChoiceField._set_choices)


class CategoryAdminForm(forms.ModelForm):
    class Meta:
        model = Category

    def __init__(self, *args, **kwargs):
        super(CategoryAdminForm, self).__init__(*args, **kwargs)
        instance = kwargs.get('instance', None)
        self.fields['achievements'] = forms.ModelMultipleChoiceField(
            queryset=Achievement.objects.filter(created_by_user=False),
            widget=FilteredSelectMultiple(verbose_name='achievements', is_stacked=False),
            required=False,
        )

class ApproveInterfaceAdminForm(forms.ModelForm):
    from groups.models import Group, WallAction
    
    class Meta:
        model = Proof

    groups = forms.ModelMultipleChoiceField(
        queryset=Group.objects.all(),
        widget=FilteredSelectMultiple(_('groups'), False, attrs={'rows':'10'}))
    
    stars_number = forms.ChoiceField(
        choices= Achievement.STARS_NUMBER_CHIOCES,
        )

    def __init__(self, *args, **kwargs):
        from groups.models import Group, WallAction
        if not kwargs.get('initial'):
            kwargs['initial'] = {}
        
        if 'instance' in kwargs:
            instance = kwargs.get('instance', None)
            if instance.user_achievement:
                kwargs['initial'].update({'stars_number': instance.user_achievement.achievement.stars_number})
                kwargs['initial'].update({'points': instance.get_default_points()})
                action = instance.user_achievement
                selected_groups = set(WallAction.objects.filter(action=action).values_list('group_id', flat=True))
                kwargs['initial']['groups'] = selected_groups
        super(ApproveInterfaceAdminForm, self).__init__(*args, **kwargs)
        #self.fields[key].required = True

class ApproveProofAdminForm(forms.ModelForm):
    source_achievement = forms.CharField(label=u'исходник', required=False)
    stars_number = forms.ChoiceField(
        choices= Achievement.STARS_NUMBER_CHIOCES,
        )

    #activate_time = forms.DateTimeField(widget=AdminSplitDateTime())
    class Meta:
        model = Proof

    def __init__(self, *args, **kwargs):
        from groups.models import Group, WallAction

        if not kwargs.get('initial'):
            kwargs['initial'] = {}
        
        if 'instance' in kwargs:
            instance = kwargs.get('instance', None)
            if instance.user_achievement:
                kwargs['initial'].update({'stars_number': instance.user_achievement.achievement.stars_number})
        super(ApproveProofAdminForm, self).__init__(*args, **kwargs)
        self.fields['source_achievement'].widget = ModelLinkWidget(self.instance)
