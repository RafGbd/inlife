from haystack import indexes

from celery_haystack.indexes import CelerySearchIndex

from app.models import Achievement, UserAchievement


class AchievementIndex(CelerySearchIndex, indexes.Indexable):
    text = indexes.EdgeNgramField(document=True, use_template=True)

    rendered = indexes.CharField(use_template=True, indexed=False)
    
    def get_model(self):
        return Achievement

    def index_queryset(self, using=None):
        """Used when the entire index for model is updated."""
        return self.get_model().objects.all()


class UserAchievementIndex(CelerySearchIndex, indexes.Indexable):
    text = indexes.EdgeNgramField(document=True, use_template=True)

    rendered = indexes.CharField(use_template=True, indexed=False)
    
    def get_model(self):
        return UserAchievement

    def index_queryset(self, using=None):
        """Used when the entire index for model is updated."""
        return self.get_model().objects.all()

