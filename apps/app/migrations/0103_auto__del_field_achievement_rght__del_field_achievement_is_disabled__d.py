# -*- coding: utf-8 -*-
import datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Deleting field 'Achievement.rght'
        db.delete_column(u'app_achievement', 'rght')

        # Deleting field 'Achievement.is_disabled'
        db.delete_column(u'app_achievement', 'is_disabled')

        # Deleting field 'Achievement.is_active'
        db.delete_column(u'app_achievement', 'is_active')

        # Deleting field 'Achievement.is_on_user_tree'
        db.delete_column(u'app_achievement', 'is_on_user_tree')

        # Deleting field 'Achievement.breakthrough'
        db.delete_column(u'app_achievement', 'breakthrough')

        # Deleting field 'Achievement.lft'
        db.delete_column(u'app_achievement', 'lft')

        # Deleting field 'Achievement.time_to_complete'
        db.delete_column(u'app_achievement', 'time_to_complete')

        # Deleting field 'Achievement.landing_button_text'
        db.delete_column(u'app_achievement', 'landing_button_text')

        # Deleting field 'Achievement.landing_image'
        db.delete_column(u'app_achievement', 'landing_image')

        # Deleting field 'Achievement.is_show_landing'
        db.delete_column(u'app_achievement', 'is_show_landing')

        # Deleting field 'Achievement.parent'
        db.delete_column(u'app_achievement', 'parent_id')

        # Deleting field 'Achievement.only_group'
        db.delete_column(u'app_achievement', 'only_group')

        # Deleting field 'Achievement.level'
        db.delete_column(u'app_achievement', 'level')

        # Deleting field 'Achievement.svg_icon'
        db.delete_column(u'app_achievement', 'svg_icon')

        # Deleting field 'Achievement.tree_id'
        db.delete_column(u'app_achievement', 'tree_id')

        # Deleting field 'Achievement.api_svg_icon'
        db.delete_column(u'app_achievement', 'api_svg_icon')

        # Removing M2M table for field filters on 'Achievement'
        db.delete_table('app_achievement_filters')

        # Removing M2M table for field blocking_achievements on 'Achievement'
        db.delete_table('app_achievement_blocking_achievements')


    def backwards(self, orm):

        # User chose to not deal with backwards NULL issues for 'Achievement.rght'
        raise RuntimeError("Cannot reverse this migration. 'Achievement.rght' and its values cannot be restored.")
        # Adding field 'Achievement.is_disabled'
        db.add_column(u'app_achievement', 'is_disabled',
                      self.gf('django.db.models.fields.BooleanField')(default=False),
                      keep_default=False)

        # Adding field 'Achievement.is_active'
        db.add_column(u'app_achievement', 'is_active',
                      self.gf('django.db.models.fields.BooleanField')(default=False),
                      keep_default=False)

        # Adding field 'Achievement.is_on_user_tree'
        db.add_column(u'app_achievement', 'is_on_user_tree',
                      self.gf('django.db.models.fields.BooleanField')(default=False),
                      keep_default=False)

        # Adding field 'Achievement.breakthrough'
        db.add_column(u'app_achievement', 'breakthrough',
                      self.gf('django.db.models.fields.BooleanField')(default=False),
                      keep_default=False)


        # User chose to not deal with backwards NULL issues for 'Achievement.lft'
        raise RuntimeError("Cannot reverse this migration. 'Achievement.lft' and its values cannot be restored.")
        # Adding field 'Achievement.time_to_complete'
        db.add_column(u'app_achievement', 'time_to_complete',
                      self.gf('django.db.models.fields.IntegerField')(default=1),
                      keep_default=False)

        # Adding field 'Achievement.landing_button_text'
        db.add_column(u'app_achievement', 'landing_button_text',
                      self.gf('django.db.models.fields.CharField')(max_length=64, null=True, blank=True),
                      keep_default=False)

        # Adding field 'Achievement.landing_image'
        db.add_column(u'app_achievement', 'landing_image',
                      self.gf('django.db.models.fields.files.ImageField')(max_length=100, null=True, blank=True),
                      keep_default=False)

        # Adding field 'Achievement.is_show_landing'
        db.add_column(u'app_achievement', 'is_show_landing',
                      self.gf('django.db.models.fields.BooleanField')(default=False),
                      keep_default=False)

        # Adding field 'Achievement.parent'
        db.add_column(u'app_achievement', 'parent',
                      self.gf('mptt.fields.TreeForeignKey')(related_name='children', null=True, to=orm['app.Achievement'], blank=True),
                      keep_default=False)

        # Adding field 'Achievement.only_group'
        db.add_column(u'app_achievement', 'only_group',
                      self.gf('django.db.models.fields.BooleanField')(default=False),
                      keep_default=False)


        # User chose to not deal with backwards NULL issues for 'Achievement.level'
        raise RuntimeError("Cannot reverse this migration. 'Achievement.level' and its values cannot be restored.")
        # Adding field 'Achievement.svg_icon'
        db.add_column(u'app_achievement', 'svg_icon',
                      self.gf('django.db.models.fields.files.FileField')(max_length=100, null=True, blank=True),
                      keep_default=False)


        # User chose to not deal with backwards NULL issues for 'Achievement.tree_id'
        raise RuntimeError("Cannot reverse this migration. 'Achievement.tree_id' and its values cannot be restored.")
        # Adding field 'Achievement.api_svg_icon'
        db.add_column(u'app_achievement', 'api_svg_icon',
                      self.gf('django.db.models.fields.files.FileField')(max_length=100, null=True, blank=True),
                      keep_default=False)

        # Adding M2M table for field filters on 'Achievement'
        db.create_table(u'app_achievement_filters', (
            ('id', models.AutoField(verbose_name='ID', primary_key=True, auto_created=True)),
            ('achievement', models.ForeignKey(orm[u'app.achievement'], null=False)),
            ('achievementfilter', models.ForeignKey(orm[u'app.achievementfilter'], null=False))
        ))
        db.create_unique(u'app_achievement_filters', ['achievement_id', 'achievementfilter_id'])

        # Adding M2M table for field blocking_achievements on 'Achievement'
        db.create_table(u'app_achievement_blocking_achievements', (
            ('id', models.AutoField(verbose_name='ID', primary_key=True, auto_created=True)),
            ('from_achievement', models.ForeignKey(orm[u'app.achievement'], null=False)),
            ('to_achievement', models.ForeignKey(orm[u'app.achievement'], null=False))
        ))
        db.create_unique(u'app_achievement_blocking_achievements', ['from_achievement_id', 'to_achievement_id'])


    models = {
        u'app.achievement': {
            'Meta': {'object_name': 'Achievement'},
            'author': ('django.db.models.fields.related.ForeignKey', [], {'blank': 'True', 'related_name': "'created_achievements'", 'null': 'True', 'to': u"orm['users.InLifeUser']"}),
            'bonuses': ('django.db.models.fields.related.ManyToManyField', [], {'to': u"orm['users.MetricBonus']", 'symmetrical': 'False', 'blank': 'True'}),
            'created_by_admin': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'created_by_user': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'cropping': ('django.db.models.fields.CharField', [], {'max_length': '255', 'blank': 'True'}),
            'date': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now'}),
            'description': ('ckeditor.fields.RichTextField', [], {'blank': 'True'}),
            'end_date': ('django.db.models.fields.DateTimeField', [], {'null': 'True', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'image': ('django.db.models.fields.files.ImageField', [], {'max_length': '100', 'null': 'True', 'blank': 'True'}),
            'interests': ('django.db.models.fields.related.ManyToManyField', [], {'to': u"orm['app.Interest']", 'symmetrical': 'False', 'blank': 'True'}),
            'is_event': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '50'}),
            'points_weight': ('django.db.models.fields.IntegerField', [], {'default': '0'}),
            'short_decscription': ('django.db.models.fields.TextField', [], {'blank': 'True'}),
            'stars_number': ('django.db.models.fields.IntegerField', [], {'default': '1'})
        },
        u'app.achievementfilter': {
            'Meta': {'object_name': 'AchievementFilter'},
            'ach_filter': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['app.Filter']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'values': ('django.db.models.fields.related.ManyToManyField', [], {'to': u"orm['app.FilterValue']", 'symmetrical': 'False'})
        },
        u'app.category': {
            'Meta': {'object_name': 'Category'},
            'achievements': ('mptt.fields.TreeManyToManyField', [], {'symmetrical': 'False', 'to': u"orm['app.Achievement']", 'null': 'True', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'image': ('django.db.models.fields.files.ImageField', [], {'max_length': '100', 'null': 'True', 'blank': 'True'}),
            'info': ('tinymce.models.HTMLField', [], {'blank': 'True'}),
            'is_disabled': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'level': ('django.db.models.fields.PositiveIntegerField', [], {'db_index': 'True'}),
            'lft': ('django.db.models.fields.PositiveIntegerField', [], {'db_index': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'parent': ('mptt.fields.TreeForeignKey', [], {'blank': 'True', 'related_name': "'children'", 'null': 'True', 'to': u"orm['app.Category']"}),
            'rght': ('django.db.models.fields.PositiveIntegerField', [], {'db_index': 'True'}),
            'svg_icon': ('django.db.models.fields.files.FileField', [], {'max_length': '100', 'null': 'True', 'blank': 'True'}),
            'tree_id': ('django.db.models.fields.PositiveIntegerField', [], {'db_index': 'True'})
        },
        u'app.filter': {
            'Meta': {'object_name': 'Filter'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '255'})
        },
        u'app.filtervalue': {
            'Meta': {'object_name': 'FilterValue'},
            'ach_filter': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'values'", 'to': u"orm['app.Filter']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'value': ('django.db.models.fields.CharField', [], {'max_length': '255'})
        },
        u'app.interest': {
            'Meta': {'object_name': 'Interest'},
            'achievements': ('django.db.models.fields.related.ManyToManyField', [], {'to': u"orm['app.Achievement']", 'symmetrical': 'False', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '150'})
        },
        u'app.proof': {
            'Meta': {'object_name': 'Proof'},
            'admin': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['users.InLifeUser']", 'null': 'True', 'blank': 'True'}),
            'comment': ('django.db.models.fields.TextField', [], {'null': 'True'}),
            'description': ('tinymce.models.HTMLField', [], {'null': 'True', 'blank': 'True'}),
            'from_phone': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'is_default': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'photo': ('django.db.models.fields.files.ImageField', [], {'max_length': '100', 'null': 'True', 'blank': 'True'}),
            'points': ('django.db.models.fields.PositiveIntegerField', [], {'null': 'True', 'blank': 'True'}),
            'proof_type': ('django.db.models.fields.IntegerField', [], {'default': '1'}),
            'status': ('django.db.models.fields.IntegerField', [], {'default': '0'}),
            'user_achievement': ('django.db.models.fields.related.OneToOneField', [], {'to': u"orm['app.UserAchievement']", 'unique': 'True', 'null': 'True', 'blank': 'True'}),
            'video': ('django.db.models.fields.files.FileField', [], {'max_length': '100', 'null': 'True', 'blank': 'True'})
        },
        u'app.proofphoto': {
            'Meta': {'ordering': "['position']", 'object_name': 'ProofPhoto'},
            'cropping': ('django.db.models.fields.CharField', [], {'max_length': '255', 'blank': 'True'}),
            'date': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'photo': ('django.db.models.fields.files.ImageField', [], {'max_length': '100'}),
            'position': ('django.db.models.fields.IntegerField', [], {'null': 'True', 'blank': 'True'}),
            'proof': ('django.db.models.fields.related.ForeignKey', [], {'blank': 'True', 'related_name': "'photos'", 'null': 'True', 'to': u"orm['app.Proof']"}),
            'user': ('django.db.models.fields.related.ForeignKey', [], {'blank': 'True', 'related_name': "'proof_photos'", 'null': 'True', 'to': u"orm['users.InLifeUser']"})
        },
        u'app.userachievement': {
            'Meta': {'unique_together': "(('user', 'achievement'),)", 'object_name': 'UserAchievement'},
            'accepted_by_user': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'achievement': ('mptt.fields.TreeForeignKey', [], {'to': u"orm['app.Achievement']"}),
            'activate_time': ('django.db.models.fields.DateTimeField', [], {'null': 'True', 'blank': 'True'}),
            'comment': ('django.db.models.fields.CharField', [], {'max_length': '1024', 'null': 'True', 'blank': 'True'}),
            'date': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now', 'db_index': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'intensity': ('django.db.models.fields.IntegerField', [], {'null': 'True', 'blank': 'True'}),
            'is_derivative_page_viewed': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'is_hidden': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'recieve_time': ('django.db.models.fields.DateTimeField', [], {'null': 'True', 'blank': 'True'}),
            'status': ('django.db.models.fields.IntegerField', [], {'default': '1', 'db_index': 'True'}),
            'user': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'achievements'", 'to': u"orm['users.InLifeUser']"})
        },
        u'app.userachievementcomment': {
            'Meta': {'ordering': "['date']", 'object_name': 'UserAchievementComment'},
            'comment': ('django.db.models.fields.TextField', [], {'blank': 'True'}),
            'date': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'user': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['users.InLifeUser']"}),
            'user_achievement': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'comments'", 'to': u"orm['app.UserAchievement']"})
        },
        u'app.userachievementlike': {
            'Meta': {'object_name': 'UserAchievementLike'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'user_achievement': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'likes'", 'to': u"orm['app.UserAchievement']"}),
            'who_like': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['users.InLifeUser']"})
        },
        u'app.userproofvote': {
            'Meta': {'object_name': 'UserProofVote'},
            'believe': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'not_believe': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'proof': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "u'proof_votes'", 'to': u"orm['app.Proof']"}),
            'user': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "u'user_votes'", 'to': u"orm['users.InLifeUser']"})
        },
        u'app.watchuserachievement': {
            'Meta': {'object_name': 'WatchUserAchievement'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'user': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['users.InLifeUser']"}),
            'user_achievement': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['app.UserAchievement']"}),
            'viewed_at': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now', 'null': 'True', 'blank': 'True'})
        },
        u'auth.group': {
            'Meta': {'object_name': 'Group'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '80'}),
            'permissions': ('django.db.models.fields.related.ManyToManyField', [], {'to': u"orm['auth.Permission']", 'symmetrical': 'False', 'blank': 'True'})
        },
        u'auth.permission': {
            'Meta': {'ordering': "(u'content_type__app_label', u'content_type__model', u'codename')", 'unique_together': "((u'content_type', u'codename'),)", 'object_name': 'Permission'},
            'codename': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'content_type': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['contenttypes.ContentType']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '50'})
        },
        u'contenttypes.contenttype': {
            'Meta': {'ordering': "('name',)", 'unique_together': "(('app_label', 'model'),)", 'object_name': 'ContentType', 'db_table': "'django_content_type'"},
            'app_label': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'model': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '100'})
        },
        u'users.inlifeuser': {
            'Meta': {'object_name': 'InLifeUser'},
            'birthdate': ('django.db.models.fields.DateField', [], {'null': 'True', 'blank': 'True'}),
            'char_to_premium': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['app.Category']", 'null': 'True', 'blank': 'True'}),
            'cropping': ('django.db.models.fields.CharField', [], {'max_length': '255', 'blank': 'True'}),
            'date_joined': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now', 'blank': 'True'}),
            'email': ('django.db.models.fields.EmailField', [], {'max_length': '254', 'null': 'True', 'blank': 'True'}),
            'facebook_friends_imported': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'first_name': ('django.db.models.fields.CharField', [], {'max_length': '30', 'blank': 'True'}),
            'first_view': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'friends': ('django.db.models.fields.related.ManyToManyField', [], {'to': u"orm['users.InLifeUser']", 'symmetrical': 'False', 'blank': 'True'}),
            'google_friends_imported': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'groups': ('django.db.models.fields.related.ManyToManyField', [], {'to': u"orm['auth.Group']", 'symmetrical': 'False', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'is_active': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'is_interesting': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'is_premium': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'is_public_action_in_fb': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'is_public_action_in_tw': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'is_public_action_in_vk': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'is_show_location': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'is_staff': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'is_superuser': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'last_clear_pg_date': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now'}),
            'last_login': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now'}),
            'last_name': ('django.db.models.fields.CharField', [], {'max_length': '30', 'blank': 'True'}),
            'latitude': ('django.db.models.fields.FloatField', [], {'null': 'True', 'blank': 'True'}),
            'level': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['users.Level']", 'null': 'True', 'on_delete': 'models.SET_NULL', 'blank': 'True'}),
            'longitude': ('django.db.models.fields.FloatField', [], {'null': 'True', 'blank': 'True'}),
            'middle_name': ('django.db.models.fields.CharField', [], {'max_length': '30', 'blank': 'True'}),
            'password': ('django.db.models.fields.CharField', [], {'max_length': '128'}),
            'photo': ('django.db.models.fields.files.ImageField', [], {'max_length': '100', 'blank': 'True'}),
            'registration_method': ('django.db.models.fields.IntegerField', [], {'default': '0'}),
            'sex': ('django.db.models.fields.IntegerField', [], {'default': '0'}),
            'social_network_page': ('django.db.models.fields.URLField', [], {'max_length': '200', 'null': 'True', 'blank': 'True'}),
            'trust': ('django.db.models.fields.IntegerField', [], {'default': '50', 'blank': 'True'}),
            'twitter_friends_imported': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'user_permissions': ('django.db.models.fields.related.ManyToManyField', [], {'to': u"orm['auth.Permission']", 'symmetrical': 'False', 'blank': 'True'}),
            'username': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '30'}),
            'vk_friends_imported': ('django.db.models.fields.BooleanField', [], {'default': 'False'})
        },
        u'users.level': {
            'Meta': {'object_name': 'Level'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'number': ('django.db.models.fields.IntegerField', [], {'unique': 'True'}),
            'required_points': ('django.db.models.fields.IntegerField', [], {'default': '0'})
        },
        u'users.metricbonus': {
            'Meta': {'object_name': 'MetricBonus'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'metric': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['users.UserMetric']"}),
            'points': ('django.db.models.fields.IntegerField', [], {'default': '0'})
        },
        u'users.usermetric': {
            'Meta': {'object_name': 'UserMetric'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'title': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'type': ('django.db.models.fields.IntegerField', [], {'default': '0'})
        }
    }

    complete_apps = ['app']