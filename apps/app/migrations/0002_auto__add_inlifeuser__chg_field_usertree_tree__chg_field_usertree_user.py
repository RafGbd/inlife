# -*- coding: utf-8 -*-
import datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding model 'InLifeUser'
        db.create_table(u'app_inlifeuser', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('password', self.gf('django.db.models.fields.CharField')(max_length=128)),
            ('last_login', self.gf('django.db.models.fields.DateTimeField')(default=datetime.datetime.now)),
            ('is_superuser', self.gf('django.db.models.fields.BooleanField')(default=False)),
            ('username', self.gf('django.db.models.fields.CharField')(unique=True, max_length=30)),
            ('email', self.gf('django.db.models.fields.EmailField')(unique=True, max_length=254)),
            ('first_name', self.gf('django.db.models.fields.CharField')(max_length=30, blank=True)),
            ('middle_name', self.gf('django.db.models.fields.CharField')(max_length=30, blank=True)),
            ('last_name', self.gf('django.db.models.fields.CharField')(max_length=30, blank=True)),
            ('sex', self.gf('django.db.models.fields.IntegerField')(default=0)),
            ('birthdate', self.gf('django.db.models.fields.DateField')(null=True, blank=True)),
            ('photo', self.gf('django.db.models.fields.files.ImageField')(max_length=100, blank=True)),
            ('date_joined', self.gf('django.db.models.fields.DateField')(default=datetime.datetime.now, blank=True)),
            ('registration_method', self.gf('django.db.models.fields.IntegerField')(default=0)),
            ('social_network_page', self.gf('django.db.models.fields.URLField')(max_length=200, null=True, blank=True)),
            ('is_active', self.gf('django.db.models.fields.BooleanField')(default=False)),
            ('is_staff', self.gf('django.db.models.fields.BooleanField')(default=False)),
        ))
        db.send_create_signal(u'app', ['InLifeUser'])

        # Adding M2M table for field groups on 'InLifeUser'
        db.create_table(u'app_inlifeuser_groups', (
            ('id', models.AutoField(verbose_name='ID', primary_key=True, auto_created=True)),
            ('inlifeuser', models.ForeignKey(orm[u'app.inlifeuser'], null=False)),
            ('group', models.ForeignKey(orm[u'auth.group'], null=False))
        ))
        db.create_unique(u'app_inlifeuser_groups', ['inlifeuser_id', 'group_id'])

        # Adding M2M table for field user_permissions on 'InLifeUser'
        db.create_table(u'app_inlifeuser_user_permissions', (
            ('id', models.AutoField(verbose_name='ID', primary_key=True, auto_created=True)),
            ('inlifeuser', models.ForeignKey(orm[u'app.inlifeuser'], null=False)),
            ('permission', models.ForeignKey(orm[u'auth.permission'], null=False))
        ))
        db.create_unique(u'app_inlifeuser_user_permissions', ['inlifeuser_id', 'permission_id'])


        # Changing field 'UserTree.tree'
        db.alter_column(u'app_usertree', 'tree_id', self.gf('mptt.fields.TreeForeignKey')(to=orm['app.Achievement']))

        # Changing field 'UserTree.user'
        db.alter_column(u'app_usertree', 'user_id', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['app.InLifeUser']))

        # Changing field 'UserAchievement.achievement'
        db.alter_column(u'app_userachievement', 'achievement_id', self.gf('mptt.fields.TreeForeignKey')(to=orm['app.Achievement']))

        # Changing field 'UserAchievement.user'
        db.alter_column(u'app_userachievement', 'user_id', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['app.InLifeUser']))

    def backwards(self, orm):
        # Deleting model 'InLifeUser'
        db.delete_table(u'app_inlifeuser')

        # Removing M2M table for field groups on 'InLifeUser'
        db.delete_table('app_inlifeuser_groups')

        # Removing M2M table for field user_permissions on 'InLifeUser'
        db.delete_table('app_inlifeuser_user_permissions')


        # Changing field 'UserTree.tree'
        db.alter_column(u'app_usertree', 'tree_id', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['app.Achievement']))

        # Changing field 'UserTree.user'
        db.alter_column(u'app_usertree', 'user_id', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['auth.User']))

        # Changing field 'UserAchievement.achievement'
        db.alter_column(u'app_userachievement', 'achievement_id', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['app.Achievement']))

        # Changing field 'UserAchievement.user'
        db.alter_column(u'app_userachievement', 'user_id', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['auth.User']))

    models = {
        u'app.achievement': {
            'Meta': {'object_name': 'Achievement'},
            'description': ('django.db.models.fields.TextField', [], {'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'level': ('django.db.models.fields.PositiveIntegerField', [], {'db_index': 'True'}),
            'lft': ('django.db.models.fields.PositiveIntegerField', [], {'db_index': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '50'}),
            'parent': ('mptt.fields.TreeForeignKey', [], {'blank': 'True', 'related_name': "'children'", 'null': 'True', 'to': u"orm['app.Achievement']"}),
            'rght': ('django.db.models.fields.PositiveIntegerField', [], {'db_index': 'True'}),
            'tree_id': ('django.db.models.fields.PositiveIntegerField', [], {'db_index': 'True'})
        },
        u'app.inlifeuser': {
            'Meta': {'object_name': 'InLifeUser'},
            'birthdate': ('django.db.models.fields.DateField', [], {'null': 'True', 'blank': 'True'}),
            'date_joined': ('django.db.models.fields.DateField', [], {'default': 'datetime.datetime.now', 'blank': 'True'}),
            'email': ('django.db.models.fields.EmailField', [], {'unique': 'True', 'max_length': '254'}),
            'first_name': ('django.db.models.fields.CharField', [], {'max_length': '30', 'blank': 'True'}),
            'groups': ('django.db.models.fields.related.ManyToManyField', [], {'to': u"orm['auth.Group']", 'symmetrical': 'False', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'is_active': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'is_staff': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'is_superuser': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'last_login': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now'}),
            'last_name': ('django.db.models.fields.CharField', [], {'max_length': '30', 'blank': 'True'}),
            'middle_name': ('django.db.models.fields.CharField', [], {'max_length': '30', 'blank': 'True'}),
            'password': ('django.db.models.fields.CharField', [], {'max_length': '128'}),
            'photo': ('django.db.models.fields.files.ImageField', [], {'max_length': '100', 'blank': 'True'}),
            'registration_method': ('django.db.models.fields.IntegerField', [], {'default': '0'}),
            'sex': ('django.db.models.fields.IntegerField', [], {'default': '0'}),
            'social_network_page': ('django.db.models.fields.URLField', [], {'max_length': '200', 'null': 'True', 'blank': 'True'}),
            'user_permissions': ('django.db.models.fields.related.ManyToManyField', [], {'to': u"orm['auth.Permission']", 'symmetrical': 'False', 'blank': 'True'}),
            'username': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '30'})
        },
        u'app.userachievement': {
            'Meta': {'object_name': 'UserAchievement'},
            'achievement': ('mptt.fields.TreeForeignKey', [], {'to': u"orm['app.Achievement']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'status': ('django.db.models.fields.IntegerField', [], {'default': '0'}),
            'user': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'achievements'", 'to': u"orm['app.InLifeUser']"})
        },
        u'app.usertree': {
            'Meta': {'object_name': 'UserTree'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'tree': ('mptt.fields.TreeForeignKey', [], {'to': u"orm['app.Achievement']"}),
            'user': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'trees'", 'to': u"orm['app.InLifeUser']"})
        },
        u'auth.group': {
            'Meta': {'object_name': 'Group'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '80'}),
            'permissions': ('django.db.models.fields.related.ManyToManyField', [], {'to': u"orm['auth.Permission']", 'symmetrical': 'False', 'blank': 'True'})
        },
        u'auth.permission': {
            'Meta': {'ordering': "(u'content_type__app_label', u'content_type__model', u'codename')", 'unique_together': "((u'content_type', u'codename'),)", 'object_name': 'Permission'},
            'codename': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'content_type': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['contenttypes.ContentType']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '50'})
        },
        u'contenttypes.contenttype': {
            'Meta': {'ordering': "('name',)", 'unique_together': "(('app_label', 'model'),)", 'object_name': 'ContentType', 'db_table': "'django_content_type'"},
            'app_label': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'model': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '100'})
        }
    }

    complete_apps = ['app']