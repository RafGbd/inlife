# -*- coding: utf-8 -*-
import datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding model 'MetricBonus'
        db.create_table(u'app_metricbonus', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('metric', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['app.UserMetric'])),
            ('points', self.gf('django.db.models.fields.IntegerField')(default=0)),
        ))
        db.send_create_signal(u'app', ['MetricBonus'])
        # Deleting field 'Achievement.metric'
        db.delete_column(u'app_achievement', 'metric_id')

        # Deleting field 'Achievement.points'
        db.delete_column(u'app_achievement', 'points')

        # Adding M2M table for field bonuses on 'Achievement'
        db.create_table(u'app_achievement_bonuses', (
            ('id', models.AutoField(verbose_name='ID', primary_key=True, auto_created=True)),
            ('achievement', models.ForeignKey(orm[u'app.achievement'], null=False)),
            ('metricbonus', models.ForeignKey(orm[u'app.metricbonus'], null=False))
        ))
        db.create_unique(u'app_achievement_bonuses', ['achievement_id', 'metricbonus_id'])
        # Adding field 'Proof.id'
        db.add_column(u'app_proof', u'id',
                      self.gf('django.db.models.fields.AutoField')(primary_key=True, default=0),
                      keep_default=False)
        # Adding field 'Proof.verified'
        db.add_column(u'app_proof', 'verified',
                      self.gf('django.db.models.fields.BooleanField')(default=False),
                      keep_default=False)

        # Changing field 'Proof.user_achievement'
        db.alter_column(u'app_proof', 'user_achievement_id', self.gf('django.db.models.fields.related.OneToOneField')(to=orm['app.UserAchievement'], unique=True))

    def backwards(self, orm):
        # Deleting model 'MetricBonus'
        db.delete_table(u'app_metricbonus')


        # User chose to not deal with backwards NULL issues for 'Achievement.metric'
        #raise RuntimeError("Cannot reverse this migration. 'Achievement.metric' and its values cannot be restored.")
        # Adding field 'Achievement.points'
        db.add_column(u'app_achievement', 'points',
                      self.gf('django.db.models.fields.IntegerField')(default=0),
                      keep_default=False)

        # Removing M2M table for field bonuses on 'Achievement'
        db.delete_table('app_achievement_bonuses')

        # Deleting field 'Proof.id'
        db.delete_column(u'app_proof', u'id')

        # Deleting field 'Proof.verified'
        db.delete_column(u'app_proof', 'verified')


        # Changing field 'Proof.user_achievement'
        db.alter_column(u'app_proof', 'user_achievement_id', self.gf('django.db.models.fields.related.OneToOneField')(to=orm['app.UserAchievement'], unique=True, primary_key=True))

    models = {
        u'app.achievement': {
            'Meta': {'object_name': 'Achievement'},
            'bonuses': ('django.db.models.fields.related.ManyToManyField', [], {'to': u"orm['app.MetricBonus']", 'symmetrical': 'False', 'blank': 'True'}),
            'description': ('django.db.models.fields.TextField', [], {'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'level': ('django.db.models.fields.PositiveIntegerField', [], {'db_index': 'True'}),
            'lft': ('django.db.models.fields.PositiveIntegerField', [], {'db_index': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '50'}),
            'parent': ('mptt.fields.TreeForeignKey', [], {'blank': 'True', 'related_name': "'children'", 'null': 'True', 'to': u"orm['app.Achievement']"}),
            'rght': ('django.db.models.fields.PositiveIntegerField', [], {'db_index': 'True'}),
            'tree_id': ('django.db.models.fields.PositiveIntegerField', [], {'db_index': 'True'})
        },
        u'app.inlifeuser': {
            'Meta': {'object_name': 'InLifeUser'},
            'birthdate': ('django.db.models.fields.DateField', [], {'null': 'True', 'blank': 'True'}),
            'date_joined': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now', 'blank': 'True'}),
            'email': ('django.db.models.fields.EmailField', [], {'max_length': '254', 'null': 'True', 'blank': 'True'}),
            'first_name': ('django.db.models.fields.CharField', [], {'max_length': '30', 'blank': 'True'}),
            'groups': ('django.db.models.fields.related.ManyToManyField', [], {'to': u"orm['auth.Group']", 'symmetrical': 'False', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'is_active': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'is_staff': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'is_superuser': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'last_login': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now'}),
            'last_name': ('django.db.models.fields.CharField', [], {'max_length': '30', 'blank': 'True'}),
            'middle_name': ('django.db.models.fields.CharField', [], {'max_length': '30', 'blank': 'True'}),
            'password': ('django.db.models.fields.CharField', [], {'max_length': '128'}),
            'photo': ('django.db.models.fields.files.ImageField', [], {'max_length': '100', 'blank': 'True'}),
            'registration_method': ('django.db.models.fields.IntegerField', [], {'default': '0'}),
            'sex': ('django.db.models.fields.IntegerField', [], {'default': '0'}),
            'social_network_page': ('django.db.models.fields.URLField', [], {'max_length': '200', 'null': 'True', 'blank': 'True'}),
            'user_permissions': ('django.db.models.fields.related.ManyToManyField', [], {'to': u"orm['auth.Permission']", 'symmetrical': 'False', 'blank': 'True'}),
            'username': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '30'})
        },
        u'app.metricbonus': {
            'Meta': {'object_name': 'MetricBonus'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'metric': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['app.UserMetric']"}),
            'points': ('django.db.models.fields.IntegerField', [], {'default': '0'})
        },
        u'app.proof': {
            'Meta': {'object_name': 'Proof'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'photo': ('django.db.models.fields.files.ImageField', [], {'max_length': '100', 'null': 'True', 'blank': 'True'}),
            'proof_type': ('django.db.models.fields.IntegerField', [], {}),
            'user_achievement': ('django.db.models.fields.related.OneToOneField', [], {'to': u"orm['app.UserAchievement']", 'unique': 'True'}),
            'verified': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'video': ('django.db.models.fields.files.FileField', [], {'max_length': '100', 'null': 'True', 'blank': 'True'})
        },
        u'app.userachievement': {
            'Meta': {'object_name': 'UserAchievement'},
            'achievement': ('mptt.fields.TreeForeignKey', [], {'to': u"orm['app.Achievement']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'status': ('django.db.models.fields.IntegerField', [], {'default': '0'}),
            'user': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'achievements'", 'to': u"orm['app.InLifeUser']"})
        },
        u'app.usermetric': {
            'Meta': {'object_name': 'UserMetric'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'title': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'type': ('django.db.models.fields.IntegerField', [], {'default': '0'})
        },
        u'app.usermetricvalue': {
            'Meta': {'object_name': 'UserMetricValue'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'metric': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'values'", 'to': u"orm['app.UserMetric']"}),
            'user': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'metrics'", 'to': u"orm['app.InLifeUser']"}),
            'value': ('django.db.models.fields.IntegerField', [], {'default': '0'})
        },
        u'app.usermetricvaluechange': {
            'Meta': {'object_name': 'UserMetricValueChange'},
            'change': ('django.db.models.fields.IntegerField', [], {}),
            'datetime': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'metric_value': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'changes'", 'to': u"orm['app.UserMetricValue']"})
        },
        u'app.usertree': {
            'Meta': {'object_name': 'UserTree'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'tree': ('mptt.fields.TreeForeignKey', [], {'to': u"orm['app.Achievement']"}),
            'user': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'trees'", 'to': u"orm['app.InLifeUser']"})
        },
        u'auth.group': {
            'Meta': {'object_name': 'Group'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '80'}),
            'permissions': ('django.db.models.fields.related.ManyToManyField', [], {'to': u"orm['auth.Permission']", 'symmetrical': 'False', 'blank': 'True'})
        },
        u'auth.permission': {
            'Meta': {'ordering': "(u'content_type__app_label', u'content_type__model', u'codename')", 'unique_together': "((u'content_type', u'codename'),)", 'object_name': 'Permission'},
            'codename': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'content_type': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['contenttypes.ContentType']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '50'})
        },
        u'contenttypes.contenttype': {
            'Meta': {'ordering': "('name',)", 'unique_together': "(('app_label', 'model'),)", 'object_name': 'ContentType', 'db_table': "'django_content_type'"},
            'app_label': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'model': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '100'})
        }
    }

    complete_apps = ['app']
