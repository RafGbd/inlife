# -*- coding: utf-8 -*-
from django.core.urlresolvers import reverse
from django.contrib.auth import get_user_model
from django.utils.formats import date_format
from django.utils.dateformat import DateFormat
from django.utils.html import linebreaks
from django.db.models import Q
from rest_framework import serializers
from postman.models import Message
from apps.configs import achiever_config_value
from users.models import Friendship
from app.const import ACTION_VERBS
from .utils import get_or_none
from .models import UserAchievement, \
                    UserAchievementComment, \
                    Category, \
                    Achievement

InLifeUser = get_user_model()


from actions.models import UserAchievementAction

class MessageUserSerializer(serializers.ModelSerializer):
    avatar = serializers.SerializerMethodField('get_avatar')
    avatar_class = serializers.SerializerMethodField('get_avatar_class')
    full_name = serializers.SerializerMethodField('get_full_name')

    def get_avatar_class(self, obj):
        return 'avatar ' + obj.avatar_class()
        # return u" ".join(["avatar", obj.avatar_class()])


    def get_avatar(self, obj):
        thumb = obj.get_cropped_avatar()
        if not thumb:
            thumb = achiever_config_value('default_user_avatar')
        return thumb.url

    def get_full_name(self, obj):
        return u'%s %s' % (obj.first_name, obj.last_name)

    class Meta:
        model = InLifeUser
        fields = ('id', 'full_name', 'first_name', 'last_name', 'avatar', 'avatar_class')

class MessageSerializer(serializers.ModelSerializer):
    sender = MessageUserSerializer()
    recipient = MessageUserSerializer()
    sent_at = serializers.DateTimeField(format='iso-8601')
    body = serializers.SerializerMethodField('get_body')

    def get_body(self, obj):
        return linebreaks(obj.body)
    
    class Meta:
        model = Message
        fields = ('id', 'body', 'sender', 'recipient', 'sent_at')


class CommentUserSerializer(serializers.ModelSerializer):
    avatar = serializers.SerializerMethodField('get_avatar')
    link = serializers.SerializerMethodField('get_link')
    avatar_class = serializers.SerializerMethodField('get_avatar_class')

    def get_avatar(self, obj):
        thumb = obj.get_cropped_avatar()
        if not thumb:
            thumb = achiever_config_value('default_user_avatar')
        return thumb.url

    def get_link(self, obj):
        return reverse('timeline', args=(obj.id,))

    def get_avatar_class(self, obj):
        return 'avatar ' + obj.avatar_class()

    class Meta:
        model = InLifeUser
        fields = ('first_name', 'last_name', 'avatar', 'link', 'username', 'avatar_class')
        
class InLifeUserSerializer(serializers.ModelSerializer):
    avatar = serializers.SerializerMethodField('get_avatar')
    link = serializers.SerializerMethodField('get_link')
    name = serializers.SerializerMethodField('get_name')
    avatar_class = serializers.SerializerMethodField('get_avatar_class')

    def get_avatar(self, obj):
        thumb = obj.get_cropped_avatar()
        if not thumb:
            thumb = achiever_config_value('default_user_avatar')
        return thumb.url

    def get_link(self, obj):
        return reverse('timeline', args=(obj.id,))
    
    def get_name(self, obj):
        return obj.get_full_name()

    def get_avatar_class(self, obj):
        return 'avatar ' + obj.avatar_class()

    class Meta:
        model = InLifeUser
        fields = ('id', 'name', 'first_name', 'last_name', 'avatar', 'link', 'avatar_class')


class UserAchievementCommentSerializer(serializers.ModelSerializer):
    user = CommentUserSerializer()
    user_achievement_id = serializers.SerializerMethodField('get_user_achievement_id')
    editable = serializers.SerializerMethodField('is_editable')
    comment_id = serializers.SerializerMethodField('get_comment_id')
    date = serializers.SerializerMethodField('get_date')

    def get_user_achievement_id(self, obj):
        return obj.user_achievement_id

    def get_comment_id(self, obj):
        return obj.id

    def get_date(self, obj):
        date = DateFormat(obj.date)
        return '%s %s, %s:%s' % (date.format('j'), date.format('E'), date.format('H'), date.format('i'))

    def is_editable(self, obj):
        return self.context['request_user'].id == obj.user_id

    class Meta:
        model = UserAchievementComment
        fields = ('date', 'comment', 'comment_id', 'user', 'user_achievement_id', 'editable')


class UserAchievementSerializer(serializers.ModelSerializer):
    class Meta:
        model = UserAchievement
        fields = ('id', 'url', 'date', 'timestamp', 'image', 'icon_class',
                  'span_class', 'stars_number', 'name', 'action', 'comment_form_url', 'user',
                  'is_hidden', 'timeline_pic_class', 'proof_annotation', 'accept_choice',
                  'status', 'achievemnt_id', 'comments', 'comments_count','likes', 'has_comments',
                  'like_url', 'unlike_url', 'is_event', 'event_remaining_time')

    #id = serializers.SerializerMethodField('get_id')
    url = serializers.SerializerMethodField('achievement_url')
    date = serializers.SerializerMethodField('get_date')
    timestamp = serializers.SerializerMethodField('get_timstamp')
    image = serializers.SerializerMethodField('get_image')
    icon_class = serializers.SerializerMethodField('get_icon_class')
    span_class = serializers.SerializerMethodField('get_span_class')
    stars_number = serializers.SerializerMethodField('get_stars_number')
    action = serializers.SerializerMethodField('get_action')
    name = serializers.SerializerMethodField('get_name')
    comment_form_url = serializers.SerializerMethodField('get_comment_form_url')
    user = serializers.SerializerMethodField('get_user') #InLifeUserSerializer()
    timeline_pic_class = serializers.SerializerMethodField('get_timeline_pic_class')
    proof_annotation = serializers.SerializerMethodField('get_proof_annotation')
    accept_choice = serializers.SerializerMethodField('has_accept_choice')
    status = serializers.SerializerMethodField('get_status')
    achievemnt_id = serializers.SerializerMethodField('get_achievemnt_id')
    likes = serializers.SerializerMethodField('get_likes')
    comments = serializers.SerializerMethodField('get_comments')
    comments_count = serializers.SerializerMethodField('get_comments_count')
    has_comments = serializers.SerializerMethodField('is_has_comments')
    is_guide = serializers.SerializerMethodField('get_is_guide')
    like_url = serializers.SerializerMethodField('get_like_url')
    unlike_url = serializers.SerializerMethodField('get_unlike_url')
    is_event = serializers.SerializerMethodField('get_is_event')
    event_remaining_time = serializers.SerializerMethodField('get_event_remaining_time')

    def get_id(self, obj):
        return obj.id

    def get_date(self, obj):
        if obj.date:
            date = DateFormat(obj.date)
            return {
                    'day': date.format('j'),
                    'month': date.format('M'),
                    'year': date.format('Y'),
                    }
    
    def get_timstamp(self, obj):
        return DateFormat(obj.date).format('U')  

    def get_image(self, obj):
        thumb = None
        if obj.is_active():
            #для активированных достижений фон-заглушка
            #todo: сделать livesetting
            return u"/static/img/original-p.jpg"
        if obj.is_received() or obj.is_peinding_confirmation() or obj.is_declined():
            #если пользователь загрузил пруф, не вожно какой
            #показываем его
            try:
                thumb = obj.proof.get_photo_timeline_thumb()
            except Exception, e:
                pass
        if not thumb:
            #если ничего нет показываем картинку достижения
            thumb = obj.achievement.get_timeline_thumb()
        if thumb:
            return thumb.url

    def get_icon_class(self, obj):
        if obj.is_failed():
            return 'icon-remove'
        else:
            return 'icon-star'

    def get_span_class(self, obj):
        #icon color
        classes = ['achiv', ]
        if obj.is_failed():
            classes.append('fail')
        return " ".join(classes)

    def get_stars_number(self, obj):
        if not obj.is_failed():
            return obj.achievement.stars_number
        else:
            return 1

    def get_action(self, obj):
        if obj.is_declined():
            return u'отклонено достижение'
        return u'%s достижение' % ACTION_VERBS['PERFECT_FORM'][obj.status]         
    
    def get_name(self, obj):
        return obj.achievement.name
    
    def achievement_url(self, obj):
        return obj.get_absolute_url()

    def get_comment_form_url(self, obj):
        return reverse('comment_achievement', args=[obj.id])
        
    def get_timeline_pic_class(self, obj):
        classes = ['timeline_pic', ]
        if obj.is_received() or obj.is_failed():
            try:
                proof = obj.proof
                if proof.is_accepted():
                    classes.append('trusted')
                elif proof.is_declined():
                    classes.append('trusted')
                elif proof.is_doubtfully():
                    classes.append('trusted')
            except Exception, e:
                pass
        return " ".join(classes)

    def get_proof_annotation(self, obj):
        annotation = {
            'icon_class': u'',
            'data_title': u"",
            'span_class': u'',
        }
        if obj.is_failed():
            """
            РЕВЬЮ: верстки не должно быть в питон коде
            """
            annotation['icon_class'] = u'icon-remove-sign'
            annotation['span_class'] = u'icon_sign rejected_sign hint'
            if obj.is_declined():
                #Провалено, потому что админ отклонил подтверждение
                annotation['data_title'] = u"Отклонено"
            else:
                #просто провалено
                annotation['data_title'] = u"Провалено"
        elif obj.is_received():
            try:
                proof = obj.proof
                if proof.is_accepted():
                    annotation['icon_class'] = u'icon-ok-sign'
                    annotation['span_class'] = u'icon_sign trusted_sign hint'
                    annotation['data_title'] = u"Подтверждено"
                elif proof.is_doubtfully():
                    annotation['icon_class'] = u'icon-ok-sign'
                    annotation['span_class'] = u'icon_sign not_trusted_sign hint'
                    annotation['data_title'] = u"Сомнительно"
            except Exception, e:
                pass
        return annotation

    def has_accept_choice(self, obj):
        if obj.achievement.created_by_admin:
            if not obj.accepted_by_user:
                return True
        return False

    def get_status(self, obj):
        status = ''
        if obj.is_received():
            status = 'recieved'
        elif obj.is_active():
            status = 'activated'
        return status

    def get_achievemnt_id(self, obj):
        return obj.achievement.id

    def has_notification(self):
        from notifications.models import MessageConf
        from app.const import EMAIL_EVENT
        return MessageConf.message(
            user=None, event=EMAIL_EVENT.ACHIEVEMENT_COMMENT,
            notification_type=MessageConf.other_notification)

    def get_likes(self, obj):
        likes_count = 0
        classes = ['i_like', ]

        if hasattr(obj, 'request_user_liked') and obj.request_user_liked:
            classes.append('red')

        if hasattr(obj, 'likes_count') and self.has_notification():
            likes_count = obj.likes_count

        return {
            'count': likes_count,
            'classes': " ".join(classes)
        }

    def get_user(self, obj):
        user = obj.user
        if self.has_accept_choice(obj) and obj.achievement.author:
            user = obj.achievement.author

        def get_avatar(obj):
            thumb = obj.get_cropped_avatar()
            if not thumb:
                thumb = achiever_config_value('default_user_avatar')
            return thumb.url

        user_data = {
            'id': user.id,
            'name': user.get_full_name(),
            'first_name': user.first_name,
            'last_name': user.last_name,
            'avatar': get_avatar(user),
            'link': reverse('timeline', args=(user.id,)),
            'avatar_class': 'avatar ' + user.avatar_class(),
        }
        return user_data


    def get_comments(self, obj):
        if not hasattr(obj, '_comments'):
            return []
        serializer_class = UserAchievementCommentSerializer
        serializer = serializer_class(obj._comments, many=True, context=self.context)
        return serializer.data

    def get_comments_count(self, obj):
        if not self.has_notification():
            return ''
        elif hasattr(obj, 'comments_count'):
            return obj.comments_count
        else:
            return obj.comments.count()

    def is_has_comments(self, obj):
        return obj.is_received() or obj.is_peinding_confirmation() or obj.is_failed() #or obj.is_activated()

    def get_like_url(self, obj):
        return reverse('like_achievement', args=[obj.id, ])

    def get_unlike_url(self, obj):
        return reverse('unlike_achievement', args=[obj.id, ])

    def get_is_event(self, obj):
        return obj.achievement.is_event

    def get_event_remaining_time(self, obj):
        if obj.achievement.is_event and obj.achievement.end_date:
            return obj.achievement.get_event_remaining_time()
        else:
            return None


class FriendsInLifeUserSerializer(InLifeUserSerializer):
    friendship_status = serializers.SerializerMethodField('get_friendship_status')
    achievements = serializers.SerializerMethodField('get_achievements')

    def get_friendship_status(self, obj):
        friendship_outcoming = self.context.get('friendship_outcoming', [])
        if obj.pk in friendship_outcoming:
            return 'waiting_for_response'
        if self.context.get('status', None):
            return self.context['status']
        return 'not_friend'

    def serialize_achievement(self, user_achievement):
        return {
            'href': user_achievement.get_absolute_url(),
            'title': u'Достижение «%s»' % user_achievement.achievement.name,
            'stars_number': user_achievement.achievement.stars_number
        }
    
    def get_achievements(self, obj):
        achievements = obj.get_biggest_achievements()
        return [self.serialize_achievement(ach) for ach in achievements]
    
    class Meta:
        model = InLifeUser
        fields = ('id', 'name', 'first_name', 'last_name', 'avatar', 'link', 'avatar_class', 'friendship_status', 'achievements')
