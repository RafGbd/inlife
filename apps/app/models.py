# -*- coding: utf-8 -*-
from django.db import models

from django.utils import timezone
from django.core.urlresolvers import reverse
from django.dispatch import receiver
from django.db.models.signals import post_save, post_delete, pre_delete
from cacheops.invalidation import invalidate_obj
from mptt.models import MPTTModel, TreeForeignKey, TreeManyToManyField
from mptt.managers import TreeManager
from registration.signals import user_registered
from inlife.settings import AUTH_USER_MODEL, CROP_SIZE
from app.const import EMAIL_EVENT, ACTION_VERBS, ACHIEVEMENT_FORMS
from datetime import timedelta
from tinymce.models import HTMLField
from ckeditor.fields import RichTextField
from inlife.utils import proof_file, proof_image, inlifeuser_image, \
    category_image, achievement_image, invalidate_template_cache, group_image, landing_image
from app.tasks import (check_userachievement_fail, notify_if_still_active,
                       unactivate_achievement_on_end_date, delete_proof_photo)
from apps.configs import achiever_config_value
from image_cropping import ImageCropField, ImageRatioField
from easy_thumbnails.files import get_thumbnailer
from easy_thumbnails.exceptions import EasyThumbnailsError
import importlib
from users.models import UserMetric, UserMetricValue, UserMetricValueChange
from actions.models import UserAchievementAction
from djangosphinx.models import SphinxSearch


def get_class(cls):
    module_name, class_name = cls.split(".")
    somemodule = importlib.import_module(module_name.replace(':', '.'))
    return (getattr(somemodule, class_name))


class AchievementManager(models.Manager):

    def registration_list(self):
        choices = []
        for ach in self.filter(created_by_user=False):
            label = '-' + ach.name
            choices.append((ach.pk, label))
        return choices

    def created_by_staff(self):
        #Достижения созданные персоналом сайта, через админку
        return self.filter(created_by_user=False)


class Achievement(models.Model):

    STARS_NUMBER_CHIOCES = [(i, i) for i in range(1, 9)]

    """Достижение(элемент дерева) абстрактное"""
    name = models.CharField(max_length=50)
    description = RichTextField(blank=True)
    short_decscription = models.TextField(blank=True)
    points_weight = models.IntegerField(default=0, verbose_name=u'Вес в поинтах')
    bonuses = models.ManyToManyField(
        'users.MetricBonus', verbose_name=u'Бонусы',
        help_text=u'Бонусы изменяют значения метрик \
                    пользователя при получении достижения',
        blank=True
    )
    interests = models.ManyToManyField(
        'app.Interest', verbose_name=u'Интересы',
        blank=True
    )
    image = ImageCropField(
        verbose_name=u'картинка для таймлайна',
        upload_to=achievement_image,
        blank=True, null=True
    )
    cropping = ImageRatioField('image', '69x29', size_warning=True)
    stars_number = models.IntegerField(
        verbose_name=u'кол-во звёзд',
        choices=STARS_NUMBER_CHIOCES,
        default=1
    )
    created_by_user = models.BooleanField(u'создано польз-лем', default=False)
    #создано через http://achiever.ru/achievements/create/ 'achievement_create'
    created_by_admin = models.BooleanField(u'создано админом', default=False)
    #создано через http://achiever.ru/achievements/create/<user_id>/'
    #create_achievement_for_user', причем created_by_user тоже True
    date = models.DateTimeField(default=timezone.now)
    is_event = models.BooleanField(u'Событие группы', default=False)
    end_date = models.DateTimeField(
        verbose_name=u'Дата завершения события',
        blank=True, null=True
    )
    author = models.ForeignKey(
        'users.InLifeUser',
        related_name='created_achievements',
        null=True, blank=True
    )
    #автор достижения

    objects = AchievementManager()

    class Meta:
        verbose_name = u'достижение'
        verbose_name_plural = u'достижения'

    def __unicode__(self):
        return self.name

    def get_image(self):
        if self.image:
            return self.image
        else:
            return achiever_config_value('default_achievement_image')

    def get_full_image_url(self):
        return getattr(self.get_image(), 'url')

    def get_timeline_thumb(self):
        thumb = None
        if self.image:
            try:
                thumb = get_thumbnailer(self.image).get_thumbnail({
                    'size': (690, 290),
                    'box': self.cropping,
                    'crop': True,
                    'detail': True,
                })
            except EasyThumbnailsError, e:
                pass
                #что-то не так с изображением, обнуляем его
                #print e
                #self.image = None
                #self.save()
        if not thumb:
            thumb = achiever_config_value('default_achievement_image')
        return thumb
    
    def get_achievement_of_month_thumb(self):
        return self.get_timeline_thumb()

    def get_list_box_thumb(self):
        if self.image:
            try:
                thumb = get_thumbnailer(self.image).get_thumbnail({
                    'size': (220, 90),
                    'box': self.cropping,
                    'crop': True,
                    'detail': True,
                })
                return thumb
            except EasyThumbnailsError, e:
                pass
                #что-то не так с изображением, обнуляем его
                #print e
                #self.image = None
                self.save()
        return None

    def get_children_count(self):
        return self.children.count()
        
    def get_absolute_url(self):
        return reverse('achievement', args=[self.pk])

    def get_intensity(self):
        if self.is_event and self.end_date:
            delta = self.end_date - timezone.now()
            intensity = delta.days
            return intensity if intensity > 0 else 0
        return self.time_to_complete

    def get_event_remaining_time(self):
        # assert self.is_event
        # assert self.end_date
        delta = self.end_date - timezone.now()
        return delta.days + 1 if delta.total_seconds() > 0 else 0


class UserAchievement(models.Model):
    """Достижение пользователя"""
    NOT_AVAILABLE = 0
    NOT_ACTIVE = 1
    ACTIVE = 2
    CONFIRMATION = 3
    RECEIVED = 4
    FAILED = 5
    ADDED = 6
    STATUS_CHOICES = (
        (NOT_AVAILABLE, u'Недоступно'),
        (NOT_ACTIVE, u'Неактивно'),
        (ACTIVE, u'Активированно'),
        (CONFIRMATION, u'Подтверждается'),
        (RECEIVED, u'Получено'),
        (FAILED, u'Провалено'),
        (ADDED, u'Добавлено'),
    )
    status = models.IntegerField(choices=STATUS_CHOICES, default=NOT_ACTIVE, db_index=True)
    user = models.ForeignKey(AUTH_USER_MODEL, related_name='achievements')
    achievement = models.ForeignKey('app.Achievement')
    activate_time = models.DateTimeField(null=True, blank=True, verbose_name=u'Дата и время активации')
    recieve_time = models.DateTimeField(null=True, blank=True, verbose_name=u'Дата и время получения')
    date = models.DateTimeField(u'Дата и время изменения', default=timezone.now, db_index=True)
    intensity = models.IntegerField(u'Интенсивность, в днях', null=True, blank=True)
    comment = models.CharField(max_length=1024, null=True, blank=True)
    accepted_by_user = models.BooleanField(u'принято пользователем', default=True) # for created_by_admin achievements
    is_hidden = models.BooleanField('', default=False)
    is_derivative_page_viewed = models.BooleanField(u'Производное достижение просмотрено', default=False)

    class Meta:
        verbose_name = u'Достижение пользователя'
        verbose_name_plural = u'Достижения пользователей'
        unique_together = (('user', 'achievement'),)

    def __unicode__(self):
        user, achievement_name, status = None, None, None
        try:
            user = self.user
            achievement_name = self.achievement.name
            status = self.get_status_display()
        except Achievement.DoesNotExist:
            achievement_name = "Not exist, id: " + str(self.achievement_id)
        return u"<%s|%s|%s>" % (user, achievement_name, status)

    def set_derivative_page_viewed(self):
        self.is_derivative_page_viewed = True
        self.save()
        return ''

    def activate(self, activate_time=None):
        if self.achievement.is_event:
            if self.achievement.end_date:
                assert timezone.now() < self.achievement.end_date
            end_date = self.end_date - timedelta(days=1)
            notify_about_end_date.apply_async(args=[self.user,], eta=end_date)

        self.status = UserAchievement.ACTIVE
        self.accepted_by_user = True
        self.activate_time = activate_time or timezone.now()
        self.likes.all().delete()
        self.comments.all().delete()
        try:
            self.proof.delete()
        except Proof.DoesNotExist, e:
            pass
        self.save()

    def unactivate(self):
        action = UserAchievementAction.objects.filter(user=self.user,
                                                 action=self.status,
                                                 achievement=self.achievement,
                                                 user_achievement=self)[:1].get()
        if action:
            action.delete()
        self.status = UserAchievement.NOT_ACTIVE
        self.activate_time = None
        self.recieve_time = None
        self.intensity = None
        self.comment = ''
        self.accepted_by_user = False
        self.save()

    def fail(self):
        self.status = UserAchievement.FAILED
        self.save()

    def reciev(self, recieve_time=None):
        #при получении достижения:
        self.status = UserAchievement.RECEIVED
        self.recieve_time = recieve_time or self.recieve_time or timezone.now()
        #Добавление к баллам пользователя веса достижения в баллах
        points_metric, created = UserMetric.objects.get_or_create(title=u'Баллы', type=UserMetric.TYPE_SUM)
        points_user_metric_val, created = UserMetricValue.objects.get_or_create(user=self.user, metric=points_metric)
        points = self.proof.points or 0
        points_user_metric_val_chage, metric_val_chage_created =  UserMetricValueChange.objects.get_or_create(
            metric_value=points_user_metric_val,
            object_id=self.proof.id,
            defaults={'change': points, 'content_object':self.proof}
        )
        if not metric_val_chage_created:
            points_user_metric_val_chage.change = points
            points_user_metric_val_chage.save()
        #Получение пользователем нового уровня(если хватает баллов)
        self.user.reach_new_level()
        self.save()
        # посылка сообщений от админа
        from notifications.models import events_and_types, MessageConf
        events_and_types(self.user, events=(EMAIL_EVENT.ADMIN_FEEDBACK, ),
                         types=(MessageConf.browser,
                                MessageConf.email,
                                MessageConf.notification, ))

    def added(self):
        self.status = UserAchievement.ADDED
        self.save()

    def is_not_active(self):
        return self.status == UserAchievement.NOT_ACTIVE
    
    def is_received(self):
        if self.status == UserAchievement.RECEIVED:
            return True
        else:
            return False

    def is_failed(self):
        if self.status == UserAchievement.FAILED:
            return True
        else:
            return False

    def is_declined(self):
        """
        Провалено. Отклонено. Когда админ посчитал доказательство не убедительным.
        """
        try:
            proof = self.proof
        except Exception, e:
            proof = None

        return proof and self.status == UserAchievement.FAILED

    def is_active(self):
        if self.status == UserAchievement.ACTIVE:
            return True
        else:
            return False

    def is_peinding_confirmation(self):
        return self.status == UserAchievement.CONFIRMATION


    def get_end_date(self):
        if self.status != UserAchievement.ACTIVE:
            return None
        if not self.activate_time:
            return None
        delta = timedelta(days=int(self.intensity))
        end_date = self.activate_time + delta
        return end_date


    def get_remaining_time(self):
        if self.status != UserAchievement.ACTIVE:
            return None
        else:
            end_date = self.get_end_date()
            remaining_time = end_date - timezone.now()
            if remaining_time.total_seconds() < 0:
                return None
            return {'days': remaining_time.days,
                    'hours': remaining_time.seconds // 3600,
                    'minutes': (remaining_time.seconds // 60) % 60, }

    def check_fail(self):
        if self.status == UserAchievement.ACTIVE:
            if not self.activate_time:
                self.fail()
                return
            end_date = self.get_end_date()
            remaining_time = end_date - timezone.now()
            if remaining_time.total_seconds() < 0:
                self.fail()


    def get_absolute_url(self):
        if self.is_peinding_confirmation() or self.is_received() or self.is_failed():
            return reverse('user_achievement_confirmed_detail', args=[self.user.id, self.achievement.pk])
        else:
            return reverse('achievement', args=(self.achievement.id,))


    def get_achievement_action(self):
        return self.userachievementaction_set.all()[0]

    def get_timeline_thumb(self):
        if self.is_received() or self.is_peinding_confirmation() or self.is_failed():
            try:
                return self.proof.get_photo_timeline_thumb()
            except Exception, e:
                pass
        return self.achievement.get_timeline_thumb()

    def get_proof_comment(self):
        try:
            return self.proof.comment
        except Exception, e:
            return ''

    def get_perfect_verb(self):
        return ACTION_VERBS['PERFECT_FORM'][self.status]

    def get_action_verb(self):
        return ACTION_VERBS[self.user.sex][self.status]

    def get_achievement_action_verb(self):
        return u"%s %s" % (self.get_action_verb(),
                           ACHIEVEMENT_FORMS[self.status])

    def get_full_achievement_action_verb(self):
        return u"%s %s %s" % (self.user.get_full_name(),
                              self.get_achievement_action_verb(),
                              self.achievement.name)

    def get_proof_annotation(self):
        obj = self
        annotation = {
            'icon_class': u'',
            'data_title': u"",
            'span_class': u'',
        }
        if obj.is_failed():
            #  РЕВЬЮ: верстки в питон коде быть не должно
            annotation['icon_class'] = u'icon-remove-sign'
            annotation['span_class'] = u'icon_sign rejected_sign hint'
            if obj.is_declined():
                #Провалено, потому что админ отклонил подтверждение
                annotation['data_title'] = u"Отклонено"
            else:
                #просто провалено
                annotation['data_title'] = u"Провалено"
        elif obj.is_received():
            try:
                proof = obj.proof
                if proof.is_accepted():
                    annotation['icon_class'] = u'icon-ok-sign'
                    annotation['span_class'] = u'icon_sign trusted_sign hint'
                    annotation['data_title'] = u"Подтверждено"
                elif proof.is_doubtfully():
                    annotation['icon_class'] = u'icon-ok-sign'
                    annotation['span_class'] = u'icon_sign not_trusted_sign hint'
                    annotation['data_title'] = u"Сомнительно"
            except Exception, e:
                pass
        return annotation

    def save(self, *args, **kwargs):
        self.date = timezone.now()
        super(UserAchievement, self).save(*args, **kwargs)
        #сохранение действия пользователя
        if self.status in (self.ACTIVE,
                           self.RECEIVED,
                           self.FAILED,
                           self.CONFIRMATION,
                           self.ADDED):
            UserAchievementAction.create_action(self)


class UserAchievementComment(models.Model):
    class Meta:
        verbose_name = u'UserAchievement Comment'
        verbose_name_plural = u'UserAchievement comments'
        ordering = ['date']

    user_achievement = models.ForeignKey(UserAchievement, related_name='comments')
    user = models.ForeignKey('users.InLifeUser')
    date = models.DateTimeField(auto_now_add=True)
    comment = models.TextField(blank=True)

@receiver(post_delete, sender=UserAchievementComment)
@receiver(post_save, sender=UserAchievementComment)
def comment_invalidate_user_ach(sender, instance, *args, **kwargs):
    try:
        invalidate_obj(instance.user_achievement)
    except Exception, e:
        pass
    


class UserAchievementLike(models.Model):
    user_achievement = models.ForeignKey(UserAchievement, related_name="likes")
    who_like = models.ForeignKey('users.InLifeUser')

    def __unicode__(self):
        return u"%s like %s" % (self.who_like, self.user_achievement)

@receiver(post_delete, sender=UserAchievementLike)
@receiver(post_save, sender=UserAchievementLike)
def like_invalidate_user_ach(sender, instance, *args, **kwargs):
    try:
        invalidate_obj(instance.user_achievement)
    except Exception, e:
        pass

class WatchUserAchievement(models.Model):
    user = models.ForeignKey('users.InLifeUser')
    user_achievement = models.ForeignKey(UserAchievement)
    viewed_at = models.DateTimeField('', null=True, blank=True, default=timezone.now)


class Proof(models.Model):
    P_UNFOUNDED = 0
    P_PHOTO = 1
    P_VIDEO = 2
    P_ADMIN = 3
    TYPE_CHOICES = (
        (P_UNFOUNDED, u'Голословно'),
        (P_PHOTO, u'Фотография'),
        (P_VIDEO, u'Видео'),
    )
    STATUS_NOT_VERIFIED = 0
    STATUS_ACCEPTED = 1
    STATUS_DECLINED = 2
    STATUS_DOUBTFULLY = 3
    STATUS_HIDDEN = 4
    STATUS_CHOICES = ((STATUS_NOT_VERIFIED, u'Не проверено'),
                      (STATUS_ACCEPTED, u'Принято'),
                      (STATUS_DECLINED, u'Отклонено'),
                      (STATUS_DOUBTFULLY, u'Сомнительно'),
                      (STATUS_HIDDEN, u'Скртытый'),
                      )
    COLOR_DEFAULT = u''
    COLOR_RED = u'achiv'
    COLOR_GREEN = u'achiv achiv-green'
    COLOR_BLUE = u'achiv achiv-collect'
    COLOR_VIOLET = u'achiv achiv-violet'
    COLOR_GRAY = u'achiv achiv-gray'

    user_achievement = models.OneToOneField(UserAchievement, primary_key=False, blank=True, null=True)
    proof_type = models.IntegerField(choices=TYPE_CHOICES, default=P_PHOTO)
    status = models.IntegerField(u'Статус', default=STATUS_NOT_VERIFIED, choices=STATUS_CHOICES)
    photo = models.ImageField(upload_to=proof_image, blank=True, null=True)
    video = models.FileField(upload_to=proof_file, blank=True, null=True)
    comment = models.TextField(u'комментарий', null=True)
    admin = models.ForeignKey('users.InLifeUser', verbose_name=u'Админ', help_text=u'который проверил', limit_choices_to={'is_superuser': True}, null=True, blank=True)
    from_phone = models.BooleanField(u'С телефона', default=False)
    points = models.PositiveIntegerField(u'Очки', help_text=u'Количество очков за достижение', blank=True, null=True)
    description = HTMLField(u'Описание', blank=True, null=True)
    is_default = models.BooleanField(u'По умолчанию', default=False)

    def get_user(self):
        if self.user_achievement:
            return self.user_achievement.user

    def is_accepted(self):
        return self.status == Proof.STATUS_ACCEPTED

    def is_declined(self):
        return self.status == Proof.STATUS_DECLINED

    def is_doubtfully(self):
        return self.status == Proof.STATUS_DOUBTFULLY

    def is_hidden(self):
        return self.status == Proof.STATUS_HIDDEN

    def is_photo(self):
        return self.proof_type == Proof.P_PHOTO

    def is_not_verified(self):
        return self.status == Proof.STATUS_NOT_VERIFIED

    def is_verified(self):
        return self.status in (Proof.STATUS_ACCEPTED, Proof.STATUS_DECLINED, Proof.STATUS_DOUBTFULLY)

    def get_photo(self):
        try:
            proof_photo = self.photos.all()[0]
            return proof_photo.photo
        except IndexError:
            return None
        
    def believe_count(self):
        try:
            return self.proof_votes.filter(believe=True).annotate(models.Count('id')).get().id__count
        except:
            return 0

    def not_believe_count(self):
        try:
            return self.proof_votes.filter(not_believe=True).annotate(models.Count('id')).get().id__count
        except:
            return 0

    def get_default_points(self):
        if self.user_achievement:
            # Если есть поинты у достижения берем поинты у него, если нет - у цвета
            return self.user_achievement.achievement.points_weight or 0
        return 0

    def save(self, *args, **kwargs):
        if self.user_achievement:
            self.is_default = False
        super(Proof, self).save(*args, **kwargs)
        if self.user_achievement:
            if self.is_not_verified():
                self.user_achievement.recieve_time = timezone.now()
            if self.proof_type == Proof.P_UNFOUNDED:
                pass
            elif self.status == Proof.STATUS_ACCEPTED:
                self.user_achievement.recieve_time = timezone.now()
            elif self.status == Proof.STATUS_DOUBTFULLY:
                self.user_achievement.recieve_time = timezone.now()
            elif self.status == Proof.STATUS_DECLINED:
                self.user_achievement.fail()
            self.user_achievement.save()

            if not self.is_hidden() and not self.is_declined():
                self.user_achievement.recieve_time = timezone.now()
                self.user_achievement.reciev()


    def __unicode__(self):
        if self.user_achievement:
            if self.proof_type == Proof.P_UNFOUNDED:
                return u"%s (%s)" % (self.user_achievement.achievement.name, Proof.TYPE_CHOICES[0][1])
            else:
                return u"%s (%s, %s)" % (self.user_achievement.achievement.name, self.get_proof_type_display(), self.get_status_display())
        else:
            return u"Подтверждение по умолчанию"

    def get_photo_timeline_thumb(self):
        assert self.is_photo()
        photos = self.photos.all()
        if photos:
            photo_obj = photos[0]
            photo = photo_obj.photo
            cropping = photo_obj.cropping
            #print cropping
            try:
                thumb = get_thumbnailer(photo).get_thumbnail({
                    'size': (630, 290),
                    'box': cropping,
                    'crop': True,
                    'detail': True,
                })
                return thumb
            except EasyThumbnailsError, e:
                pass
                #что-то не так с изображением, удаляем и пробуем снова
                #print e
                #photo_obj.delete()
                #return self.get_photo_timeline_thumb()
        raise Exception('Proof photos not found')

    class Meta:
        verbose_name = u'Подтверждение'
        verbose_name_plural = u'Подтверждения'

@receiver(post_delete, sender=Proof)
@receiver(post_save, sender=Proof)
def proof_invalidate_user_ach(sender, instance, *args, **kwargs):
    try:
        invalidate_obj(instance.user_achievement)
    except Exception, e:
        pass



@receiver(post_delete, sender=Proof)
@receiver(post_save, sender=Proof)
def proof_invalidate_user_ach(sender, instance, *args, **kwargs):
    try:
        invalidate_obj(instance.user_achievement)
    except Exception, e:
        pass


class ProofPhoto(models.Model):
    proof = models.ForeignKey('Proof', related_name='photos', blank=True, null=True)
    user = models.ForeignKey('users.InLifeUser', related_name='proof_photos', null=True, blank=True)
    photo = models.ImageField(upload_to=proof_image)
    date = models.DateTimeField(u'Дата загрузки', default=timezone.now)
    position = models.IntegerField(u'Позиция', blank=True, null=True)
    cropping = ImageRatioField('photo', '69x29', size_warning=True)

    class Meta:
        ordering = ['position']

    def save(self, *args, **kwargs):
        if self.id:
            created = False
        else:
            created = True

        super(ProofPhoto, self).save(*args, **kwargs)

        if created:
            delete_proof_photo.apply_async(args=[self.pk,], countdown = 60 * 60 * 2)


"""
РЕВЬЮ: непонятного назначения прокси классы
"""
class ApproveProof(Proof):
    class Meta:
        proxy = True
        verbose_name = u'Подтверждение достижения'
        verbose_name_plural = u'Подтверждения достижений'
        app_label = u'etc'

class DefaultProof(Proof):
    class Meta:
        proxy = True
        verbose_name = u'Подтверждение по умолчанию'
        verbose_name_plural = u'Подтверждения по умолчанию'
        app_label = u'etc'


class Category(MPTTModel):
    class Meta:
        verbose_name = u'Категория'
        verbose_name_plural = u'Категории'

    def __unicode__(self):
        return self.name

    parent = TreeForeignKey('self', null=True, blank=True, related_name='children')
    name = models.CharField(u'Название', max_length=100)
    info = HTMLField(blank=True, verbose_name=u'Информация')
    achievements = models.ManyToManyField(
        'app.Achievement',
        verbose_name=u'Достижения', blank=True, null=True)
    svg_icon = models.FileField(verbose_name=u'SVG-иконка', upload_to='svg_icons/', blank=True, null=True)
    is_disabled = models.BooleanField(u'Заблокировано', default=False)
    image = models.ImageField(u'Изображение', upload_to=category_image, null=True, blank=True)  

    def get_root_name(self):
        root = self.get_root()
        if root:
            return root.name

    def get_image(self):
        return self.image

    def get_absolute_url(self):
        return reverse('category_detail', args=[self.pk])

    @classmethod
    def chars_to_choose(cls):
        return cls.objects.filter(achievements__isnull=False).distinct()


class Interest(models.Model):
    class Meta:
        verbose_name = u'Интерес'
        verbose_name_plural = u'Интересы'

    def __unicode__(self):
        return u"%s" % self.name

    name = models.CharField(max_length=150)
    """
    РЕВЬЮ: в many2many зависимости указано through вместо related_name
    """

    achievements = models.ManyToManyField(
        'Achievement',
        through=Achievement.interests.through,
        blank=True
    )
    #through тут чтобы можно было редактировать m2m для обоих моделей
    #/admin/app/achievement/<id>/ "Интересы:"
    #/admin/app/interest/<id>/ "Achievements:"


class Filter(models.Model):
    """
    Название фильтра
    """
    name = models.CharField(u'Название фильтра', max_length=255)

    def __unicode__(self):
        return self.name

    class Meta:
        verbose_name = u'Фильтр'
        verbose_name_plural = u'Фильтры'


class FilterValue(models.Model):
    """
    Варианты для фильтра
    """
    value = models.CharField(u'Вариант значения фильтра', max_length=255)
    ach_filter = models.ForeignKey(Filter, related_name='values', verbose_name=u'Фильтр')

    def __unicode__(self):
        return self.value


class AchievementFilter(models.Model):
    """
    Фильтр достижений
    """
    ach_filter = models.ForeignKey(Filter, verbose_name=u'Фильтр')
    values = models.ManyToManyField(FilterValue, verbose_name=u'Значение')

    def get_values_unicode(self):
        return u' '.join([value.__unicode__() for value in self.values.all()])

    def __unicode__(self):
        return self.ach_filter.__unicode__() + u' : ' + self.get_values_unicode()

    class Meta:
        verbose_name = u'Фильтр достижений'
        verbose_name_plural = u'Фильтры достижений'



class UserProofVote(models.Model):
    class Meta:
        verbose_name = u'голос пользователя'
        verbose_name_plural = u'голоса пользователей'

    user = models.ForeignKey('users.InLifeUser', related_name=u'user_votes')
    proof = models.ForeignKey(Proof, related_name=u'proof_votes')
    believe = models.BooleanField(u'верю', blank=True)
    not_believe = models.BooleanField(u'не верю', blank=True) 



####################################################################
#                                                                  #
#                                                                  #    
#                     /MODELS                                      #
#                                                                  #
#                                                                  #
####################################################################
