# -*- coding: utf-8 -*-
from datetime import timedelta
from importlib import import_module
from django.contrib.auth.models import AbstractBaseUser, PermissionsMixin, UserManager
from django.core.mail import send_mail
from django.core.urlresolvers import reverse
from django.core.exceptions import MultipleObjectsReturned, ObjectDoesNotExist
from django.dispatch import receiver
from django.utils import timezone
from django.db.models.signals import m2m_changed, post_save
from registration.signals import user_registered, user_activated
from django.db.models import Q
from django.db import connection, models
from image_cropping import ImageCropField, ImageRatioField
from easy_thumbnails.files import get_thumbnailer
from easy_thumbnails.exceptions import EasyThumbnailsError
from apps.configs import achiever_config_value
from guide.models import GuideUser, GuideStep
from users.const import REGISTRATION_METHOD, REGISTRATION_METHOD_CHOICES, SEX, SEX_CHOICES
from users.user import new_user
from inlife.utils import inlifeuser_image
from app.const import EMAIL_EVENT
from django.conf import settings
from .validators import validate_trust

from django.contrib.contenttypes.models import ContentType
from django.contrib.contenttypes import generic

relation_limits = {'model__in': ('proof', )}


class FakeQS(list):

    def __init__(self, iterable):
        super(FakeQS, self).__init__(iterable)


class Subscriber(models.Model):
    email = models.EmailField(u'Почта', max_length=254, unique=True)

    class Meta:
        verbose_name = u'Подписчик'
        verbose_name_plural = u'Подписчики'


def get_class(cls):
    module_name, class_name = cls.split(".")
    somemodule = import_module(module_name.replace(':', '.'))
    return (getattr(somemodule, class_name))


def dictfetchall(cursor):
    "Returns all rows from a cursor as a dict"
    desc = cursor.description
    return [
        dict(zip([col[0] for col in desc], row))
        for row in cursor.fetchall()
    ]


class InLifeUserQuerySet(models.query.QuerySet):
    pass

class InLifeUserManager(UserManager):
    def get_query_set(self):
        return InLifeUserQuerySet(self.model)

    def get_not_related(self, user):
        """return active, not already friends"""

        return (self.filter(self._get_not_related_query(user))
                    .order_by('level')
                )

    def _get_not_related_query(self, user):

        return Q(Q(is_active=True),
                 ~Q(id=user.id),
                 ~Q(first_name=u'', last_name=u''),
                 ~Q(id__in=user.friends.all))

    def possible_friendships(self, user):

        return (self.filter(self._get_possible_friendships_query(user))
                    .order_by('-friendship_rate__rate')
                )

    def _get_possible_friendships_query(self, user):

        return Q(Q(friendship_rate_rel__user1=user)
                 | Q(friendship_rate__user2=user),
                 ~Q(id=user.id),
                 ~Q(id__in=user.friends.all)
                 )

    def staff(self):
        return self.filter(is_staff=True, is_active=True)

    def active(self):
        return self.filter(is_active=True)


class FriendsMixin(object):

    def add_friend(self, user):
        from inlife import graph
        try:
            friendship = Friendship.objects.get(
                sender=user, reciever=self, status=Friendship.WAITING_FOR_ANSWER)
            friendship.status = Friendship.ACCEPTED
            friendship.save()

            users = graph.db.labels.get('User')
            self_node = users.get(_id=self.pk)[0]
            user_node = users.get(_id=user.pk)[0]
            self_node.relationships.create("Friend", user_node)

            return 'accepted'

        except Friendship.DoesNotExist:
            if user not in user.friends.all():
                try:
                    Friendship.objects.get(Q(sender=self,
                                             reciever=user,
                                             status=Friendship.WAITING_FOR_ANSWER)
                                           | Q(sender=user,
                                               reciever=self,
                                               status=Friendship.WAITING_FOR_ANSWER))
                except Friendship.DoesNotExist:
                    Friendship.objects.create(sender=self, reciever=user)

            return 'added'


    def remove_friend(self, user):
        try:
            friendship = Friendship.objects.get(Q(sender=self,
                                                  reciever=user,
                                                  status=Friendship.ACCEPTED)
                                                | Q(sender=user,
                                                    reciever=self,
                                                    status=Friendship.ACCEPTED))
        except Friendship.DoesNotExist:
            pass
        else:
            friendship.cancel(user)

        from inlife import graph
        query = """match (u:User{_id: %s})-[r:FRIEND]-(f:User{_id= %s})
                   delete r""" % (self.id, user.id)
        graph.db.query(query)

    def change_friendship_status(self, user, begin_status, end_status):
        # Friendship не находился в варианте 
        try:
            friendship = Friendship.objects.get(sender=self,
                                                reciever=user,
                                                status=begin_status)
        except Friendship.DoesNotExist:
            friendship = Friendship.objects.get(
                sender=user, reciever=self, status=begin_status)

        friendship.status = end_status
        friendship.save()

    def get_possible_friends(self, page=1):
        u"""
        Метод возвращает список возможных друзей.
        Сперва в списке идут друзья из соц сетей, отсортированные по кол-ву
        соц. сетей, в которых есть дружба, затем идут друзья знакомых,
        отсортированные по кол-ву общих друзей.
        Данный метод поддерживает постраничный вывод и возвращает объект,
        который мимикрирует под QuerySet (поддерживает итерацию
        и имеет метод count).
        """
        from inlife import graph
        social_friends_data = {}
        regular_friends_data = {}

        limit_end = settings.PAGINATE_FRIEND*int(page)
        limit_start = limit_end - settings.PAGINATE_FRIEND

        social_count_query = """match (me:User{_id: %s})-[:SOCIAL_FRIEND]-(f)
                                return count(distinct f)""" % self.pk

        regular_count_query = """match (me:User{_id: %s})-[:FRIEND]-(mf)-[:FRIEND]-(u:User)
                                 where not (me)-[:FRIEND]-(u) and not me._id = u._id
                                 return count(distinct u)""" % self.pk

        social_query = """match (me:User{_id: %s})-[r:SOCIAL_FRIEND]-(f)
                          where not (me)-[:FRIEND]-(f)
                          return f._id, count(r)
                          order by count(r) desc \n""" % self.pk

        regular_query = """match (me:User{_id: %s})-[:FRIEND]-(mf)-[:FRIEND]-(u:User)
                           where not (me)-[:FRIEND]-(u) and not me._id = u._id
                           return u._id, count(mf)
                           order by count(mf) desc \n""" % self.pk

        social_count = graph.db.query(social_count_query).elements[0][0]
        regular_count = graph.db.query(regular_count_query).elements[0][0]

        # ---------[query]-
        # --social-mutual--
        if limit_start >= social_count:
            limit_start = limit_start - social_count
            limit_end = limit_end - social_count
            social_query_paginated = regular_query + 'skip %s limit %s' % (limit_start,
                                                                           limit_end)
            social_friends = graph.db.query(social_query_paginated)
            social_friends_data = {el[0]: el[1]
                                   for el in social_friends}

        # -[query]---------
        # --social-mutual--
        elif limit_end <= social_count:
            regular_query_paginated = social_query + 'skip %s limit %s' % (limit_start,
                                                                           settings.PAGINATE_FRIEND)
            regular_friends = graph.db.query(regular_query_paginated)
            regular_friends_data = {el[0]: el[1]
                                    for el in regular_friends}
        # -----[query]-----
        # --social-mutual--
        else:
            limit_end = limit_end - social_count
            social_query_paginated = social_query + 'skip %s limit %s' % (limit_start,
                                                                          settings.PAGINATE_FRIEND)
            regular_query_paginated = regular_query + 'limit %s' % limit_end
            social_friends = graph.db.query(social_query_paginated)
            regular_friends = graph.db.query(regular_query_paginated)

            social_friends_data = {el[0]: el[1]
                                   for el in social_friends}
            regular_friends_data = {el[0]: el[1]
                                    for el in regular_friends}

        users = InLifeUser.objects.filter(
            id__in=sum(
                [social_friends_data.keys(),
                 regular_friends_data.keys()],
                [])
        )

        for user in users:
            user.mf_count = regular_friends_data.get(user.id, 0)
            user.social_count = social_friends_data.get(user.id, 0)
        users = FakeQS(sorted(users,
                              key=lambda user: (user.social_count, user.mf_count),
                              reverse=True))
        users.count = lambda: social_count + regular_count

        return users


class InLifeUser(AbstractBaseUser, FriendsMixin, PermissionsMixin):
    class Meta:
        verbose_name = u'Пользователь'
        verbose_name_plural = u'Пользователи'

    username = models.CharField(u'Ник', max_length=30, unique=True, blank=False)
    email = models.EmailField(u'Почта', max_length=254, blank=True, null=True)
    first_name = models.CharField(u'Имя', max_length=30, blank=True)
    middle_name = models.CharField(u'Отчество', max_length=30, blank=True)
    last_name = models.CharField(u'Фамилия', max_length=30, blank=True)
    sex = models.IntegerField(u'Пол', blank=False, default=SEX.UNDEFINED, choices=SEX_CHOICES)
    birthdate = models.DateField(u'Дата рождения', blank=True, null=True)
    photo = ImageCropField(u'Фото', upload_to=inlifeuser_image, blank=True)
    date_joined = models.DateTimeField(u'Дата регистрации', blank=True, default=timezone.now)
    registration_method = models.IntegerField(u'Способ регистрации', default=REGISTRATION_METHOD.EMAIL, choices=REGISTRATION_METHOD_CHOICES)
    social_network_page = models.URLField(u'Страница в социальной сети', blank=True, null=True)
    is_active = models.BooleanField(u'Активен', default=False)
    is_staff = models.BooleanField(default=False)
    level = models.ForeignKey('Level', verbose_name=u'Уровень', blank=True, null=True, on_delete=models.SET_NULL)
    friends = models.ManyToManyField('InLifeUser', verbose_name=u'Друзья', blank=True)
    trust = models.IntegerField(u'Доверие', blank=True, default=0, validators=[validate_trust])
    first_view = models.BooleanField(default=False, editable=False)
    is_public_action_in_vk = models.BooleanField(u'я хочу выкладвывать свои достижения во Вконтакте', default=True)
    is_public_action_in_fb = models.BooleanField(u'я хочу выкладвывать свои достижения во Facebook', default=True)
    is_public_action_in_tw = models.BooleanField(u'я хочу выкладвывать свои достижения во Twitter', default=True)
    is_show_location = models.BooleanField(u'показывать мое местоположение', default=True)
    is_interesting = models.BooleanField(u'интересный', default=False)
    latitude = models.FloatField(u'широта', blank=True, null=True)
    longitude = models.FloatField(u'долгота', blank=True, null=True)
    is_premium = models.BooleanField(u'премиум', default=False)
    cropping = ImageRatioField('photo', '100x100', size_warning=True)#, allow_fullsize=True)
    last_clear_pg_date = models.DateTimeField(u'обнуления прогресс бара', default=timezone.now, help_text=u'Дата последнего обнуления прогресс бара')
    char_to_premium = models.ForeignKey('app.Category', blank=True, null=True)

    # For social friends search button
    vk_friends_imported = models.BooleanField(default=False)
    facebook_friends_imported = models.BooleanField(default=False)
    google_friends_imported = models.BooleanField(default=False)
    twitter_friends_imported = models.BooleanField(default=False)

    USERNAME_FIELD = 'username'
    REQUIRED_FIELDS = ['email', ]

    objects = InLifeUserManager()


    def __unicode__(self):
        return self.username

    def save(self, *args, **kwargs):
        from inlife import graph
        if self.pk is None:
            level, created = Level.objects.get_or_create(number=1)
            self.level = level
            super(InLifeUser, self).save(*args, **kwargs)
            points_metric, created = UserMetric.objects.get_or_create(title=u'Баллы', type=UserMetric.TYPE_SUM)
            user_metric_val, created = UserMetricValue.objects.get_or_create(user=self, metric=points_metric)
            user_metric_val.save()
        else:
            super(InLifeUser, self).save(*args, **kwargs)
            
        # g_node = graph.db.nodes.create(id=self.pk)
        # g_node.labels.add('User')

    def friends_imported(self, backend):
        """
        Mark social network as searched
        """
        setattr(self,
                {'vk-oauth': 'vk_friends_imported',
                 'google-oauth2': 'google_friends_imported',
                 'facebook': 'facebook_friends_imported',
                 'twitter': 'twitter_friends_imported'
                 }[backend],
                True)
        self.save()

    @property
    def all_social_networks_searched(self):
        """
        Check that all social networks had been searched for friends
        """
        return (self.vk_friends_imported
                and self.facebook_friends_imported
                and self.twitter_friends_imported
                and self.google_friends_imported)

    def get_short_name(self):
        return self.username

    def get_full_name(self):
        if self.first_name or self.last_name:
            return u' '.join([x for x in (self.first_name, self.last_name) if x])
        else:
            return self.username

    def email_user(self, subject, message, from_email=None):
        send_mail(subject, message, from_email, [self.email])

    def get_points_metric(self):
        points_metric, created = UserMetric.objects.get_or_create(title=u'Баллы', type=UserMetric.TYPE_SUM)
        user_metric_val, created = UserMetricValue.objects.get_or_create(user=self, metric=points_metric)
        return user_metric_val

    def get_user_points(self):
        return self.get_points_metric().value

    def get_next_level_points(self):
        next_levels = (Level.objects.filter(required_points__gt=self.get_user_points())
                                    .order_by('required_points'))
        try: 
            next_level = next_levels[0]
            return next_level.required_points
        except IndexError:
            return

    def get_progress(self):
        user_points = self.get_user_points()
        next_level_points = self.get_next_level_points()
        if not next_level_points:
            return 100
        try:
            progress = 100 * (user_points - self.level.required_points) / (next_level_points - self.level.required_points)
        except ZeroDivisionError:
            progress = 0
        return progress

    def get_status(self):
        points_metric, created = UserMetric.objects.get_or_create(title=u'Баллы', type=UserMetric.TYPE_SUM)
        user_metric_val, created = UserMetricValue.objects.get_or_create(user=self, metric=points_metric)
        points = user_metric_val.value
        higher_levels = Level.objects.filter(required_points__gt=points).order_by('required_points')[:1]
        points_to_next_level = higher_levels[0].required_points
        progress = 100 * (points - self.level.required_points) / (points_to_next_level - self.level.required_points)
        status = {'points':points, 
                  'points_to_next_level':points_to_next_level, 
                  'current_level':self.level.number, 
                  'progress':progress}            
        return status


    def get_level_scale_grads(self):
        user_points = self.get_points_metric().value
        next_levels = Level.objects.filter(required_points__gt=user_points).order_by('required_points')
        if not next_levels:
            return (180, 180)
        next_level = next_levels[0]
        try:
            scale_grads = (user_points - self.level.required_points) * 360 / (next_level.required_points - self.level.required_points)
        except ZeroDivisionError:
            # TODO: Тут нужно писать в лог, ибо такой ситуации происходить не должно
            scale_grads = (0, 0)
        if scale_grads <= 180:
            return (scale_grads, 0)
        else:
            return (180, scale_grads - 180)

    def reach_new_level(self):
        user_points = self.get_points_metric().value
        new_levels = Level.objects.filter(required_points__lte=user_points).order_by('-required_points')
        if not new_levels:
            new_level = self.level
        else:
            new_level = new_levels[0]
        if self.level != new_level:
            self.level = new_level
            self.save()

    def get_biggest_achievements(self):
        qs = self.achievements.filter(
            status=get_class('app:models.UserAchievement').RECEIVED
            ).order_by('-achievement__stars_number', '-recieve_time')[:10]
        # TODO: Задать вопрос, тут странно всё
        # qs = self.achievements.filter(
        #     status=get_class('app:models.UserAchievement').RECEIVED).exclude(
        #     Q(proof__points__isnull=True)).order_by('-proof__points').select_related()
        # qs2 = self.achievements.filter(
        #     status=get_class('app:models.UserAchievement').CONFIRMATION)
        # qs = (qs | qs2)[:10]
        return qs

    def get_largest_achievements(self):
        achievements = self.achievements.filter(status__in=[get_class('app:models.UserAchievement').RECEIVED,get_class('app:models.UserAchievement').ACTIVE]).order_by('-achievement__points_weight').select_related('achievement')
        achs = [ a.achievement for a in achievements ]
        #achievements = Achievement.objects.filter(pk__in=set(achievements))
        return achs

    def get_active_largest_achievements(self):
        achievements = self.achievements.filter(status=get_class('app:models.UserAchievement')).order_by('-achievement__points_weight').select_related('achievement')
        achs = [ a.achievement for a in achievements ]
        #achievements = Achievement.objects.filter(pk__in=set(achievements))
        return achs

    def get_interlocutors(self):
        """
            Возвращает пользователей, с которыми были беседы,
            к каждому пользовател добавляет дату последнего сообщения
        """
        return InLifeUser.objects.raw(
            """
            select * from users_inlifeuser user INNER JOIN
            (
                select user_id, max(sent_at) as sent_at from
                (
                    select sender_id AS user_id, sent_at
                        FROM postman_message sender
                        WHERE recipient_id = %s
                    UNION
                    select recipient_id AS user_id, sent_at
                        FROM postman_message recipient
                        WHERE sender_id = %s
                ) l_m
                group by user_id
            ) last_messages ON (user.id = last_messages.user_id)
            ORDER BY sent_at DESC

            """, [self.id, self.id]
        )
   
    def check_premium(self):
        if not self.is_premium:
            self.is_premium = True
            self.save()
        return self.is_premium

    def get_friendship_offers_count(self):
        return self.friendship_incoming.filter(status=Friendship.WAITING_FOR_ANSWER).count()

    def get_news_count(self):
        from app.models import WatchUserAchievement, UserAchievement
        reg_achievs_recieved = achiever_config_value('post_registration_recieved_achievements')
        ua = UserAchievement.objects.exclude(
            achievement__in=reg_achievs_recieved.all())
        interesting_ids = list(InLifeUser.objects.filter(
                is_interesting=True).values_list('id', flat=True))
        seen = list(WatchUserAchievement.objects.filter(user=self).values_list(
                'user_achievement_id', flat=True))
        return ua.filter(user__in=interesting_ids).exclude(pk__in=seen).count()

    def get_message_count(self):
        from postman.models import Message
        return Message.objects.inbox_unread_count(self)

    def get_cropped_avatar(self):
        if self.photo:
            try:
                thumb = get_thumbnailer(self.photo).get_thumbnail({
                    'size': (200, 200),
                    'box': self.cropping,
                    'crop': True,
                    'detail': True,
                })
                return thumb
            except EasyThumbnailsError, e:
                pass
                #что-то не так с изображением, обнуляем его
                #print e
                #self.photo = None
                #self.save()
        elif achiever_config_value('default_user_avatar'):
            return achiever_config_value('default_user_avatar')
        return

    def get_sidebar_avatar(self):
        if self.photo:
            try:
                thumb = get_thumbnailer(self.photo).get_thumbnail({
                    'size': (190, 0),
                    'box': None,
                    'crop': False,
                    'detail': True,
                })
                return thumb
            except EasyThumbnailsError, e:
                #что-то не так с изображением, обнуляем его
                #self.photo = None
                self.save()
        return

    def get_timeline_url(self):
        return reverse('timeline', args=[self.pk])

    def get_absolute_url(self):
        return self.get_timeline_url()
    
    def avatar_class(self):
        classes = []
        # if self.is_premium:
        #     classes.append("vip_user")
        if self.is_staff:
            classes.append("admin")
            classes.append("vip_user")
        return " ".join(classes)

user_registered.connect(new_user)


@receiver(post_save, sender=InLifeUser, dispatch_uid="apps.users.models")
def post_registration_actions(sender, instance, created, **kwargs):
    if created:
        # посылка сообщений от админа
        from notifications.models import events_and_types, MessageConf
        events_and_types(instance, events=(EMAIL_EVENT.REGISTRATION, ),
                         types=(MessageConf.notification, MessageConf.email, ))

        UserAchievement = get_class('app:models.UserAchievement')
        reg_achievs_recieved = achiever_config_value('post_registration_recieved_achievements').all()
        reg_groups = achiever_config_value('registration_groups').all()

        for achievement in reg_achievs_recieved:
            user_achievement, created = UserAchievement.objects.get_or_create(user=instance, achievement=achievement)

        user_achievements = UserAchievement.objects.filter(user=instance)
        #премиум при регистрации
        instance.is_premium = True
        instance.save()
        # активирование по порядку, LF-438
        action_time = timezone.now()
        from app.models import Proof

        for rec_achiev in user_achievements.filter(achievement__in=reg_achievs_recieved):
            default_proof = Proof.objects.filter(is_default=True)[0]
            default_proof_photos = default_proof.photos.all()
            default_proof.pk = None
            default_proof.user_achievement = rec_achiev
            default_proof.is_default = False
            default_proof.status = default_proof.STATUS_NOT_VERIFIED
            default_proof.save()
            default_proof.status = default_proof.STATUS_ACCEPTED
            for photo in default_proof_photos:
                photo.proof = default_proof
                photo.pk = None
                photo.save()
            default_proof.save()
        
        action_time += timedelta(seconds=1)
        # TODO: восстановить функционал, try except, проверка на премиума при регистрации
        action_time += timedelta(seconds=1)
        for act_achiev in user_achievements.filter(achievement_id__in=reg_achievs_active):
            act_achiev.activate(action_time)
        from groups.models import GroupMemebreship, Group
        for group in reg_groups:
            GroupMemebreship.objects.get_or_create(group=group, user=instance)
        GuideUser.objects.get_or_create(user=instance, step=GuideStep.objects.get(number=1))


def friend_added(sender, **kwargs):
    if kwargs['action'] == "pre_add":
        if kwargs['pk_set']:
            if kwargs['instance'].pk == list(kwargs['pk_set'])[0]:
                raise models.exceptions.ValidationError(u"user catn't be friend of himself")

m2m_changed.connect(friend_added, sender=InLifeUser.friends.through)

@receiver(user_activated)
def set_first_view(sender, user, request, **kwargs):
    user.first_view = True
    user.save()

class Level(models.Model):
    class Meta:
        verbose_name = u'Уровень'
        verbose_name_plural = u'Уровни'

    def __unicode__(self):
        return u"Уровень  %s" % self.number

    number = models.IntegerField(unique=True)
    required_points = models.IntegerField(verbose_name=u'Необходимо поинтов для получения', default=0)
    
class UserMetric(models.Model):
    TYPE_SUM = 0
    TYPE_MAXIMUM = 1
    TYPE_CHOICES = ((TYPE_SUM, u'Сумма'),
                    (TYPE_MAXIMUM, u'Максимум'))

    class Meta:
        verbose_name = u'Метрика'
        verbose_name_plural = u'Метрики'

    def __unicode__(self):
        return self.title

    def is_sum(self):
        return self.type == UserMetric.TYPE_SUM

    def is_maximum(self):
        return self.type == UserMetric.TYPE_MAXIMUM

    title = models.CharField(u'Название', max_length=100)
    type = models.IntegerField(u'Тип', choices=TYPE_CHOICES, default=TYPE_SUM)

class UserMetricValue(models.Model):
    class Meta:
        verbose_name = u'Значение метрики'
        verbose_name_plural = u'Значения метрик'
        ordering = ['value']

    def __unicode__(self):
        return u"%s: %s | %s" % (self.user.username, self.metric.title, self.value)

    user = models.ForeignKey(InLifeUser, verbose_name=u'Пользователь', related_name='metrics')
    metric = models.ForeignKey(UserMetric, verbose_name=u'Метрика', related_name='values')
    value = models.IntegerField(verbose_name=u'Значение', default=0)

    def get_title(self):
        return self.metric.title

    def save(self, *args, **kwargs):
        super(UserMetricValue, self).save(*args, **kwargs)
        self.user.reach_new_level()


class UserMetricValueChange(models.Model):
    class Meta:
        verbose_name = u'Изменение значения метрики'
        verbose_name_plural = u'Изменения значений метрик'

    def __unicode__(self):
        if self.metric_value.metric.is_sum():
            return u"%s: %s %+i" % (self.metric_value.user, self.metric_value.metric.title, self.change)
        else:
            return u"%s: %s = %i" % (self.metric_value.user, self.metric_value.metric.title, self.change)

    metric_value = models.ForeignKey(UserMetricValue, verbose_name=u'Значение', related_name='changes')
    change = models.IntegerField(u'Изменение', blank=False, null=False)
    datetime = models.DateTimeField(u'Дата изменения', blank=True, default=timezone.now)
    content_type = models.ForeignKey(ContentType, limit_choices_to=relation_limits)
    object_id = models.PositiveIntegerField()
    content_object = generic.GenericForeignKey('content_type', 'object_id')

    def save(self, *args, **kwargs):
        super(UserMetricValueChange, self).save(*args, **kwargs)
        if self.metric_value.metric.is_sum():
            self.metric_value.value = sum([mvch.change for mvch in self.metric_value.changes.all()])
        else:
            self.metric_value.value = self.change
        self.metric_value.save()


class MetricBonus(models.Model):
    class Meta:
        verbose_name = u'Бонус к метрике'
        verbose_name_plural = u'Бонусы к метрикам'

    def __unicode__(self):
        if self.metric.is_sum():
            return u"%s %+i" % (self.metric.title, self.points)
        else:
            return u"%s = %i" % (self.metric.title, self.points)

    metric = models.ForeignKey('UserMetric', verbose_name=u'Метрика')
    points = models.IntegerField(u'Баллы', default=0)


class Friendship(models.Model):
    WAITING_FOR_ANSWER = 0
    ACCEPTED = 1
    DECLINED = 2
    REJECTED = 5
    CANCELLED_BY_SENDER = 3
    CANCELLED_BY_RECIEVER = 4
    STATUS_CHOICES = ((WAITING_FOR_ANSWER, u'Ожидает ответа'),
                      (ACCEPTED, u'Принята'),
                      (DECLINED, u'Отклонена'),
                      (REJECTED, u'Отозвана'),
                      (CANCELLED_BY_SENDER, u'Прервана отправителем'),
                      (CANCELLED_BY_RECIEVER, u'Прервана получателем'))

    class Meta:
        verbose_name = u'Дружба'
        verbose_name_plural = u'Дружба'

    def __unicode__(self):
        return u"%s -> %s (%s)" % (self.sender, self.reciever, self.get_status_display())

    sender = models.ForeignKey(InLifeUser, verbose_name=u'Отправитель', related_name='friendship_outcoming')
    reciever = models.ForeignKey(InLifeUser, verbose_name=u'Получатель', related_name='friendship_incoming')
    status = models.IntegerField(u'Статус', choices=STATUS_CHOICES, default=WAITING_FOR_ANSWER)

    def cancel(self, user):
        error = False
        if user == self.sender:
            self.status = Friendship.CANCELLED_BY_SENDER
        elif user == self.reciever:
            self.status = Friendship.CANCELLED_BY_RECIEVER
        if not error:
            self.save()

    def save(self, *args, **kwargs):
        super(Friendship, self).save(*args, **kwargs)
        if self.status == Friendship.ACCEPTED:
            self.sender.friends.add(self.reciever)
            self.reciever.friends.add(self.sender)
        elif self.status == Friendship.CANCELLED_BY_SENDER or self.status == Friendship.CANCELLED_BY_RECIEVER:
            self.sender.friends.remove(self.reciever)
            self.reciever.friends.remove(self.sender)

            
class TrustChange(models.Model):
    REASON_UNFOUNDED = 0
    REASON_DECLINED = 1
    REASON_ACCEPTED = 2
    REASON_DOUBTFULLY = 3
    REASON_CHOICES = ((REASON_UNFOUNDED, u'Голословное подтверждение'),
                      (REASON_DECLINED, u'Отклоненное подтверждение'),
                      (REASON_ACCEPTED, u'Проверенное подтверждение'),
                      (REASON_DOUBTFULLY, u'Сомнительное подтверждение'),
                      )

    class Meta:
        verbose_name = u'Изменение доверия'
        verbose_name_plural = u'Изменения доверия'

    def __unicode__(self):
        return u"%s: %+i (%s)" % (self.user.username, self.change, self.get_reason_display())

    datetime = models.DateTimeField(u'Дата', default=timezone.now, blank=True)
    user = models.ForeignKey(InLifeUser, verbose_name=u'Пользователь')
    change = models.IntegerField(u'Величина изменения')
    reason = models.IntegerField(u'Причина', choices=REASON_CHOICES)
    proof = models.ForeignKey('app.Proof', verbose_name=u'Подтверждение', blank=True, null=True)

    def save(self, *args, **kwargs):
        super(TrustChange, self).save(*args, **kwargs)
        Proof = get_class('app:models.Proof')
        points = self.proof.user_achievement.achievement.points_weight * 0.1
        if self.proof.proof_type == Proof.P_UNFOUNDED:
            self.user.trust -= points

        if self.proof.proof_type == Proof.P_PHOTO or self.proof.proof_type == Proof.P_VIDEO:
            self.user.trust += points

        if self.user.trust > 100:
            self.user.trust = 100
        if self.user.trust < 0:
            self.user.trust = 0

        self.user.save()

#signals
@receiver(post_save, sender=Friendship)
def notify_friendship(sender, instance, **kwargs):
    if instance.status == Friendship.WAITING_FOR_ANSWER:
        from notifications.models import events_and_types, MessageConf
        events_and_types(instance.reciever, events=(EMAIL_EVENT.FRIENDSHIP, ),
                         types=(MessageConf.browser, MessageConf.email, ))
