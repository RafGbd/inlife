from django.conf import settings
from django.contrib.sites.models import RequestSite, Site
from django.contrib.auth import get_user_model
from django.template.loader import render_to_string
from django.template import Context, Template
from django.utils.safestring import mark_safe 
from registration import signals
from registration.forms import RegistrationForm
from registration.models import RegistrationProfile, RegistrationManager
from registration.backends.default import DefaultBackend
from common.models import EmailTemplate
from app.const import EMAIL_EVENT
User = user_model = get_user_model()


def send_inlife_activation_email(self, site):
    ctx_dict = Context({'activation_key': self.activation_key,
                'expiration_days': settings.ACCOUNT_ACTIVATION_DAYS,
                'site': site})
    template = EmailTemplate.objects.get(event=EMAIL_EVENT.REGISTRATION, active=True)
    subject_template = Template(template.subject)
    subject = subject_template.render(ctx_dict)
    # Email subject *must not* contain newlines
    subject = ''.join(subject.splitlines())
    message_template = Template(mark_safe(template.body_html))
    message = message_template.render(ctx_dict)

    self.user.email_user(subject, message, settings.DEFAULT_FROM_EMAIL)

RegistrationProfile.send_activation_email = send_inlife_activation_email

class EmailBackend(DefaultBackend):
    def register(self, request, **kwargs):
        form_kwargs = {'password': kwargs['password'],'username': kwargs['email'],}
        for field in set([user_model.USERNAME_FIELD,] + list(user_model.REQUIRED_FIELDS)):
            if field in kwargs:
                form_kwargs[field] = kwargs[field]

        if Site._meta.installed:
            site = Site.objects.get_current()
        else:
            site = RequestSite(request)
        new_user = RegistrationProfile.objects.create_inactive_user(form_kwargs, site)
        signals.user_registered.send(sender=self.__class__,
            user=new_user,
            request=request)
        return new_user
