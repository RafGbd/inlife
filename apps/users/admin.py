# coding: utf-8
from django.contrib import admin
from image_cropping import ImageCroppingMixin
from django.contrib.auth.admin import UserAdmin
from django.contrib.admin import TabularInline
from django.utils.translation import ugettext_lazy as _

from .admin_forms import UserCreationForm, UserChangeForm
from users.models import InLifeUser, Subscriber, UserMetric, UserMetricValue, \
                         UserMetricValueChange, Level, Friendship, TrustChange


class UserMetricValueInlineAdmin(TabularInline):
    model = UserMetricValue
    extra = 1

class InLifeUserAdmin(ImageCroppingMixin, UserAdmin):
    form = UserChangeForm
    add_form = UserCreationForm
    list_display = ('username', 'id', 'first_name', 'last_name' ,'email', "is_staff", )
    inlines = [UserMetricValueInlineAdmin, ]
    list_filter = ('is_active', 'is_staff', 'is_superuser', 'sex', 'registration_method')
    fieldsets = (
        (None, {'fields': ('username', 'password')}),
        (_('Personal info'), {'fields': ('first_name', 'middle_name', 'last_name', 'birthdate', 'sex', 'photo', 'cropping',
                'email', 'registration_method', 'social_network_page', 'friends', 'trust', 'level', 'char_to_premium', "is_interesting",)}),
        (_('Permissions'), {'fields': ('is_active', 'is_staff', 'is_superuser', 'is_premium',
                                       'groups', 'user_permissions'),
                            'classes': ('collapse',)}),
        (_('Important dates'), {'fields': ('last_login', 'date_joined', 'last_clear_pg_date')}),
    )
    search_fields = ('email', 'username', 'first_name', 'middle_name', 'last_name')


class UserMetricValueChangeAdmin(admin.ModelAdmin):
    list_display = ['metric_value', 'change', 'datetime']


admin.site.register(InLifeUser, InLifeUserAdmin)
admin.site.register(Subscriber)
admin.site.register(UserMetric)
admin.site.register(UserMetricValue)
admin.site.register(UserMetricValueChange, UserMetricValueChangeAdmin)
admin.site.register(Level)
admin.site.register(Friendship)
admin.site.register(TrustChange)
