# coding: utf-8
from .models import InLifeUser, friend_added
from django import forms
from django.utils.translation import ugettext_lazy as _
from django.contrib.auth.forms import ReadOnlyPasswordHashField

class UserCreationForm(forms.ModelForm):
    password1 = forms.CharField(label=u'Пароль', widget=forms.PasswordInput)
    password2 = forms.CharField(label=u'Подтверждение пароля', widget=forms.PasswordInput)

    class Meta:
        model = InLifeUser

    def clean_password2(self):
        password1 = self.cleaned_data.get("password1")
        password2 = self.cleaned_data.get("password2")
        if password1 and password2 and password1 != password2:
            raise forms.ValidationError(u'Пароли не совпадают')
        return password2

    def save(self, commit=True):
        user = super(UserCreationForm, self).save(commit=False)
        user.set_password(self.cleaned_data['password1'])
        if commit:
            user.save()
        return user


class UserChangeForm(forms.ModelForm):
    password = ReadOnlyPasswordHashField(label= _("Password"),
        help_text= _("Raw passwords are not stored, so there is no way to see "
                    "this user's password, but you can change the password "
                    "using <a href=\"password/\">this form</a>."))

    class Meta:
        model = InLifeUser
        fields = ('username', 'email', 'first_name', 'middle_name', 'last_name', 'birthdate', 'sex', 'photo', 'cropping', 'registration_method',
                  'social_network_page', 'friends', 'trust', 'is_active', 'is_staff', 'is_superuser', 'is_premium', 'groups', 'user_permissions', 'last_login', 'date_joined', 'last_clear_pg_date', 'level', 'char_to_premium', "is_interesting", )

    def clean_password(self):
        return self.initial['password']

    def clean_friends(self):
        friends = self.cleaned_data.get('friends', [])
        if friends:
                friend_added(
                sender=self.instance.friends,
                instance=self.instance,
                action="pre_add",
                reverse=False,
                model=self.instance.friends.model,
                pk_set=[friend.id for friend in friends]
            )
        return self.cleaned_data.get('friends', [])

def _get_parents(achievement):
    if achievement:
        return "%s > %s" % (_get_parents(achievement.parent), achievement.name)
    else:
        return ''

