# -*- coding: utf-8 -*-
import json, os, sys
from django.contrib.auth.decorators import login_required
from django.contrib.auth import logout
from social_auth.decorators import dsa_view
from social_auth.utils import setting
from social_auth.views import auth_process
from social_auth.backends.twitter import TwitterAuth, TWITTER_AUTHORIZATION_URL
from django.utils.http import is_safe_url
from django.shortcuts import render, HttpResponse, HttpResponseRedirect
from django.shortcuts import redirect
from django.shortcuts import get_object_or_404
from django.core.urlresolvers import reverse
from django.conf import settings
from django.template.response import TemplateResponse
from django.views.decorators.debug import sensitive_post_parameters
from django.views.decorators.cache import never_cache
from django.views.decorators.csrf import csrf_protect
from django.shortcuts import render_to_response
from django.shortcuts import resolve_url
from registration.views import register as reg
from django.contrib.auth import REDIRECT_FIELD_NAME, login as auth_login, logout as auth_logout, get_user_model
from django.contrib.auth import authenticate
from app.utils import get_or_none
from users.forms import InlifeRegistrationForm, InlifeAuthenticationForm
from users.forms import ProfileForm
from django.contrib.auth import get_user_model 
InLifeUser = get_user_model()

@sensitive_post_parameters()
@csrf_protect
@never_cache
def login(request, template_name='registration/login.html',
          redirect_field_name=REDIRECT_FIELD_NAME,
          authentication_form=InlifeAuthenticationForm,
          current_app=None, extra_context=None):
    """
    Displays the login form and handles the login action.
    """
    redirect_to = request.REQUEST.get(redirect_field_name, '')
    if request.method == "POST":
        #логинится по полю username, а из формы прилетает email, поэтому копируем это поле
        if "username" not in request.POST:
            post_data =  dict(request.POST.items() + [ ('username', (request.POST.get('email')))])
        else:
            post_data = request.POST
        form = authentication_form(data=post_data)
        if form.is_valid():
            # Ensure the user-originating redirection url is safe.
            if not is_safe_url(url=redirect_to, host=request.get_host()):
                redirect_to = resolve_url(settings.LOGIN_REDIRECT_URL)

            # Okay, security check complete. Log the user in.
            auth_login(request, form.get_user())

            if request.session.test_cookie_worked():
                request.session.delete_test_cookie()

            return HttpResponseRedirect(redirect_to)
    else:
        form = authentication_form(request)

    request.session.set_test_cookie()

    context = {
        'form': form,
        redirect_field_name: redirect_to,
    }
    if extra_context is not None:
        context.update(extra_context)
    return TemplateResponse(request, template_name, context)


def register(request):
    return reg(request, form_class=InlifeRegistrationForm, backend = 'users.backends.EmailBackend')


def login_or_register(request):
    """центральная форма на landing.html если усть пользователь-логин, нет-регистрация"""
    if request.method == "POST":
        user = get_or_none(InLifeUser, username=request.POST.get('email'))
        if user:
            return login(request)
    return register(request)

@login_required
def profile(request):
    if request.method == 'GET':
        form = ProfileForm(instance=request.user)
    elif request.method == 'POST':
        form = ProfileForm(request.POST, request.FILES, instance=request.user)
        if form.is_valid():
            form.save()
            return redirect(reverse('home'))
    return render(request, 'users/profile.html', {
                                            'form': form,
                                            })
    
@dsa_view(setting('SOCIAL_AUTH_COMPLETE_URL_NAME', 'socialauth_complete'))
def inlife_auth(request, backend):
    logout(request)
    return auth_process(request, backend)


def auth_twitter(request):
    if request.GET.get('finish', None):
        return HttpResponse('<script>self.close();</script>')
    auth = TwitterAuth(request, '%s?%s' % (reverse('auth_twitter'), 'finish=1'))
    token = auth.unauthorized_token()
    return HttpResponseRedirect('%s?oauth_token=%s' % (TWITTER_AUTHORIZATION_URL, token.key))

@login_required
def get_premium(request):
    user = request.user
    user.is_premium = True
    user.save()
    return HttpResponse('{"success": true}')
