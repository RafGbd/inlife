
from django.conf.urls import patterns, include, url
from django.conf import settings
from django.contrib import admin
from django.views.generic import ListView
from django.views.decorators.csrf import csrf_exempt
from users.views import *
from django.contrib.auth.views import password_reset, password_reset_confirm
from app.views import LoginRedirectView
from users.forms import InlifePasswordResetForm, InlifeSetPasswordForm


admin.autodiscover()

urlpatterns = patterns('',
    url(r'^accounts/profile/$', 'users.views.profile', name='profile'),
    url(r'^accounts/register/$', 'users.views.login_or_register', name="register"),
 	url(r'^accounts/password/reset/$',
 		password_reset,
 		{'password_reset_form': InlifePasswordResetForm},
 		name="password_reset"
 		),
	url(r'^accounts/password/reset/confirm/(?P<uidb36>[0-9A-Za-z]{1,13})-(?P<token>[0-9A-Za-z]{1,13}-[0-9A-Za-z]{1,20})/$',
        password_reset_confirm,
        {'set_password_form': InlifeSetPasswordForm},
        name='password_reset_confirm'
        ),
    url(r'^accounts/logout/$', 'django.contrib.auth.views.logout', {'next_page': '/'}, name='auth_logout'),
    url(r'^accounts/login/$', 'users.views.login', name="login"),
    url(r'^login/redirect/$', LoginRedirectView.as_view(), name='login_redirect'),
    url(r'^login/(?P<backend>[^/]+)/$', inlife_auth, name='inlife_socialauth_begin'), #hook for logging out user.
    url(r'^accounts/', include('registration.backends.default.urls')),
    url(r'^auth_twitter/$', 'users.views.auth_twitter', name='auth_twitter'),
    url(r'^get_premium/$', 'users.views.get_premium', name='get_premium'),
)