# -*- coding: utf-8 -*-
from django.core.exceptions import ValidationError


def validate_trust(value):
    if value > 100:
        raise ValidationError(u'Доверие не может быть больше 100')