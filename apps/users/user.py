from datetime import date
from registration.signals import user_activated
from django.contrib.auth import login, authenticate
from django.shortcuts import render_to_response
from app.const import EMAIL_EVENT


def new_user(sender, user, request, **kwargs):
    user.first_name = request.POST.get('first_name', '')
    user.last_name = request.POST.get('last_name', '')
    if request.POST.get('sex'):
        user.sex = request.POST.get('sex')
    birthdate_year = request.POST.get('birthdate_year')
    birthdate_month =  request.POST.get('birthdate_month')
    birthdate_day = request.POST.get('birthdate_day')
    if birthdate_year and birthdate_month and birthdate_day:
        user.birthdate = date(int(birthdate_year), int(birthdate_month), int(birthdate_day))
    photo = request.FILES.get('photo', None)
    if photo:
        user.photo.save(photo.name, photo)
    user.save()


def send_post_registration_email(sender, user, request, **kwargs):
    from app.utils import send_templated_email
    try:
        user_email = user.email
    except Exception, e:
        user_email = None
    if user_email:
        send_templated_email(
            EMAIL_EVENT.POST_REGISTRATION,
            user_email,
            user=user,
            )


def login_on_activation(sender, user, request, **kwargs):
    """Logs in the user after activation"""
    user.backend = 'django.contrib.auth.backends.ModelBackend'
    login(request, user)

# Registers the function with the django-registration user_activated signal
user_activated.connect(login_on_activation)