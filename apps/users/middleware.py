from social_auth.middleware import SocialAuthExceptionMiddleware
from social_auth.exceptions import SocialAuthBaseException
from django.shortcuts import render_to_response

class InlifeSocialAuthExceptionMiddleware( SocialAuthExceptionMiddleware):
    def process_exception(self, request, exception):
        if isinstance(exception, SocialAuthBaseException):
            message = self.get_message(request, exception)
            return render_to_response('registration/social_auth_error.html', {'message': message})