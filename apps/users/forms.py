# coding: utf-8
from django import forms
from django.utils.translation import ugettext_lazy as _
from django.contrib.auth.forms import PasswordResetForm
from users.models import InLifeUser, Subscriber
from image_cropping import ImageCropWidget
from app.widgets import AppImageCropWidget
from django.forms import extras
from django.contrib.auth.forms import AuthenticationForm, SetPasswordForm
from datetime import datetime, timedelta


class SubscriberForm(forms.ModelForm):
    class Meta:
        model = Subscriber

    email = forms.EmailField(widget=forms.TextInput(
                                        attrs={
                                            'class': 'form-control input-lg',
                                            'placeholder': u"Электронная почта"
                                        }
                                        ),
                             label='')

class InlifeRegistrationForm(forms.Form):
    email = forms.EmailField(widget=forms.TextInput(
                                        attrs={
                                            'class': 'form-control input-lg',
                                            'placeholder': u"Электронная почта"
                                        }
                                        ),
                             label=_("Email address"))
    password = forms.CharField(widget=forms.PasswordInput(
                                        attrs={
                                            'class': 'form-control input-lg',
                                            'placeholder': u"Пароль"
                                            },
                                        render_value=False
                                        ),
                                label=_("Password"))

    def clean_email(self):
        """
        Validate that the supplied email address is unique for the
        site.
        
        """
        if InLifeUser.objects.filter(email__iexact=self.cleaned_data['email']):
            raise forms.ValidationError(_("This email address is already in use. Please supply a different email address."))
        return self.cleaned_data['email']
    
class ProfileForm(forms.ModelForm):

    def __init__(self, *args, **kwargs):
        super(ProfileForm, self).__init__(*args, **kwargs)
        self.fields['photo'].required = False
        self.fields['photo'].widget = AppImageCropWidget()

    class Meta:
        model = InLifeUser
        fields = ('first_name', 'last_name', 'sex', 'birthdate', 'email',
                  'is_public_action_in_vk', 'is_public_action_in_fb', 'is_public_action_in_tw',
                  'is_show_location', 
                  'photo', 'latitude', 'longitude', 'cropping')
        widgets = {
                    'photo': AppImageCropWidget,
                    }

    first_name = forms.CharField(label=u'Имя',
                    widget=forms.TextInput(
                        attrs={
                            'placeholder': u'Ваше имя',
                            'class': 'form-control',
                            }
                        ),
                    required=True)
    
    last_name = forms.CharField(label=u'Фамилия',
                    widget=forms.TextInput(
                        attrs={
                            'placeholder': u'Ваша фамилия',
                            'class': 'form-control',
                            }
                        ),
                    required=False)
    birthdate = forms.DateField(label=u'Дата рождения', required=False, widget=extras.SelectDateWidget(years=[y for y in reversed(range(1960, (datetime.now()-timedelta(days=365)*10).year))]))
    email = forms.EmailField(label=u'Электронная почта', required=False, widget=forms.TextInput(
        attrs={
            'placeholder': u'Ваш email',
            'class': 'form-control',
            }
    ))
    latitude = forms.CharField(widget=forms.HiddenInput(), required=False)
    longitude = forms.CharField(widget=forms.HiddenInput(), required=False)

    def clean(self):
        cleaned_data = super(ProfileForm, self).clean()
        self.all_fields_filled = True
        for field in self.fields:
            if self.fields[field].required:
                if not cleaned_data.get(field, None):
                    self.fields[field].empty_alert = True
                    self.all_fields_filled = False
        return cleaned_data

    #django can't convert string u'' to FloatField
    def clean_latitude(self):
        data = self.cleaned_data['latitude']
        if not data:
            data = None
        return data

    def clean_longitude(self):
        data = self.cleaned_data['longitude']
        if not data:
            data = None
        return data

class InlifePasswordResetForm(PasswordResetForm):
    email = forms.EmailField(label=_("Email"),
                             max_length=254,
                             widget=forms.TextInput(attrs={
                                                "placeholder": u"Ваша электронная почта",
                                                "class": u"form-control input-lg",
                                                }
                                                ))


class InlifeAuthenticationForm(AuthenticationForm):
    username = forms.CharField(widget=forms.TextInput(
                                        attrs={
                                            'class': 'form-control input-lg',
                                            'placeholder': u"Электронная почта"
                                        }
                                        ),
                             label=_("Email address"))
    password = forms.CharField(widget=forms.PasswordInput(
                                        attrs={
                                            'class': 'form-control input-lg',
                                            'placeholder': u"Пароль"
                                            },
                                        render_value=False
                                        ),
                                label=_("Password"))
        

class InlifeSetPasswordForm(SetPasswordForm):
    new_password1 = forms.CharField(
        label=_("New password"),
        widget=forms.PasswordInput(
                                    attrs={
                                            'class': 'form-control input-lg',
                                            'placeholder': _("New password"),
                                            },
                                        render_value=False
                                        ),
        max_length=128,
    )
    new_password2 = forms.CharField(
        label=_("New password confirmation"),
        widget=forms.PasswordInput(
                                    attrs={
                                            'class': 'form-control input-lg',
                                            'placeholder': _("New password confirmation"),
                                            },
                                        render_value=False
                                        ),
        max_length=128,
    )
