# coding: utf-8
class REGISTRATION_METHOD:
    EMAIL = 0
    SOCIAL_NETWORK = 1

REGISTRATION_METHOD_CHOICES = [(REGISTRATION_METHOD.EMAIL, u'Почта'),
                               (REGISTRATION_METHOD.SOCIAL_NETWORK, u'Социальная сеть')]

class SEX:
    UNDEFINED = 0
    MALE = 1
    FEMALE = 2

SEX_CHOICES = [(SEX.UNDEFINED, u'Не указан'),
               (SEX.MALE, u'Мужской'),
               (SEX.FEMALE, u'Женский')]


class EMAIL_EVENT:
    REGISTRATION = 0
    LEVEL_UP = 1
    CHOICES = ((REGISTRATION, u'Регистрация пользователя'),
               (LEVEL_UP, u'Достижение уровня'))