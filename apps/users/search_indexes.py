from haystack import indexes

from celery_haystack.indexes import CelerySearchIndex

from users.models import InLifeUser


class UserIndex(CelerySearchIndex, indexes.Indexable):
    text = indexes.CharField(document=True, model_attr='get_full_name')
    rendered = indexes.CharField(use_template=True, indexed=False)

    content_auto = indexes.EdgeNgramField(model_attr='get_full_name')
    
    def get_model(self):
        return InLifeUser

    def index_queryset(self, using=None):
        """Used when the entire index for model is updated."""
        return self.get_model().objects.all()
