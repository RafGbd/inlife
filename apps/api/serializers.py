# -*- coding: utf-8 -*-
from django.core.files.storage import default_storage
from django.core.files.base import ContentFile
from rest_framework import serializers
from app.models import Achievement, UserAchievement, UserAchievementAction, ProofPhoto
from inlife.settings import STATIC_URL
from .models import UserDevice, UploadedFile
from easy_thumbnails.files import get_thumbnailer
from django.utils.html import strip_tags
from HTMLParser import HTMLParser
from cairosvg import svg2png
from io import BytesIO
from PIL import Image
import os
from time import time


class GoalSerializer(serializers.ModelSerializer):

    name = serializers.SerializerMethodField('get_goal_name')
    description = serializers.SerializerMethodField('get_goal_description')
    background = serializers.SerializerMethodField('get_goal_background')
    icon = serializers.SerializerMethodField('get_goal_icon')

    class Meta:
        model = Achievement
        fields = ('id', 'name', 'description', 'tree_id', 'background', 'icon')

    def get_goal_name(self, obj):
        h = HTMLParser()
        return h.unescape(strip_tags(obj.name))

    def get_goal_description(self, obj):
        h = HTMLParser()
        return h.unescape(strip_tags(obj.description))

    def get_goal_background(self, obj):            
        thumbnailer = get_thumbnailer(obj.landing_image)
        thumbnail_options = {
                              'size': (800, 282),               
                              'crop': True,
                              'detail': True,
                            }
        try:            
            thumbnail = thumbnailer.get_thumbnail(thumbnail_options)            
            return self.context['request'].build_absolute_uri(thumbnail.url)
        except Exception as e:
            return str(e)

    def get_goal_icon(self,obj):
        try:          
            size = 100,100
            size_in_str = '{}x{}'.format(size[0],size[1])
            thumb_tail = '.' + size_in_str + '.png'
            thumb_filename = obj.api_svg_icon.file.name + thumb_tail
            thumb_url = obj.api_svg_icon.url + thumb_tail
            if default_storage.exists(thumb_filename):
                return self.context['request'].build_absolute_uri(thumb_url)
            else:
                new_file = ContentFile('')
                svg2png(file_obj=obj.api_svg_icon.file, write_to=new_file)
                default_storage.save(thumb_filename, new_file)                    
                return self.context['request'].build_absolute_uri(thumb_url)
        except Exception as e:
            return str(e)


class UserDeviceSerializer(serializers.ModelSerializer):
    class Meta:
        model = UserDevice
        fields = ('deviceKey', 'model', 'simKey', 'uniqueKey') 


class UploadedFileDataSerializer(serializers.ModelSerializer):
    comment = serializers.CharField(source='comment', required=False)
    thumbnail = serializers.SerializerMethodField('get_thumbnail_url')
    image = serializers.SerializerMethodField('get_image_url')

    class Meta:
        model = UploadedFile
        fields = ('id', 'accuracy', 'createDate', 'latitude', 'longitude', 'location', 'reportTypeID','totalBytes', 
                   'velocity', 'achievementID', 'fileName', 'thumbnail', 'image')

    def get_thumbnail_url(self,obj):
        thumbnailer = get_thumbnailer(obj.file)
        thumbnail_options = {
                              'size': (640, 640),               
                              'crop': 'center',
                              'detail': True,
                            }
        try:
            thumbnail = thumbnailer.get_thumbnail(thumbnail_options)
            return self.context['request'].build_absolute_uri(thumbnail.url)
        except Exception as e:
            return str(e)

    def get_image_url(self,obj):
        return self.context['request'].build_absolute_uri(obj.file.url)

class ProofPhotoSerializer(serializers.ModelSerializer):

    class Meta:
        model = ProofPhoto
        fields = (
                'id',
                'createDate',
                'reportTypeID',
                'achievementID',
                'thumbnail',
                'image',
                )

    thumbnail = serializers.SerializerMethodField('get_thumbnail_url')
    image = serializers.SerializerMethodField('get_image_url')
    reportTypeID = serializers.SerializerMethodField('get_reportTypeID')
    achievementID = serializers.SerializerMethodField('get_achievementID')
    createDate = serializers.SerializerMethodField('get_createDate') 

    def get_reportTypeID(self, obj):
        return obj.proof.proof_type

    def get_achievementID(self, obj):
        return obj.proof.user_achievement.achievement_id

    def get_createDate(self, obj):
        if obj.date:
            return obj.date.isoformat()
        return "1970-01-01T00:00:00.000+0000"

    def get_thumbnail_url(self,obj):
        thumbnailer = get_thumbnailer(obj.photo)
        thumbnail_options = {
                              'size': (640, 640),               
                              'crop': 'center',
                              'detail': True,
                            }
        try:
            thumbnail = thumbnailer.get_thumbnail(thumbnail_options)
            return self.context['request'].build_absolute_uri(thumbnail.url)
        except Exception as e:
            return str(e)

    def get_image_url(self,obj):
        return self.context['request'].build_absolute_uri(obj.photo.url)


class UserAchievementSerializer(serializers.ModelSerializer):
    class Meta:
        model = UserAchievement
        fields = ('id', 'name', 'description', 'activate_time', 'dueDate',
                'icon', 'icon_url', 'status', 'thumbnail', 'update_time', 'comment', 'achievement', 'goal')

    dueDate = serializers.SerializerMethodField('get_due_date')
    activate_time = serializers.SerializerMethodField('get_activate_time')
    name = serializers.SerializerMethodField('get_achievement_name')
    description = serializers.SerializerMethodField('get_achievement_description')
    icon = serializers.SerializerMethodField('get_achievement_icon')
    id = serializers.SerializerMethodField('get_achievement_id')
    icon_url = serializers.SerializerMethodField('get_achievement_icon_url')
    thumbnail = serializers.SerializerMethodField('get_last_thumbnail')
    update_time = serializers.SerializerMethodField('get_update_time')
    goal = serializers.SerializerMethodField('get_goal')
    achievement = serializers.SerializerMethodField('get_achievement_name')
    status = serializers.SerializerMethodField('get_api_status')
    comment = serializers.SerializerMethodField('get_comment')


    def get_goal(self, obj):
        root = obj.achievement.get_root()
        if root.pk != obj.achievement.pk:
            return root.pk
        else: 
            return None

    def get_update_time(self, obj):
        return obj.date

    def get_last_thumbnail(self, obj):
        try:
            thumb = obj.get_timeline_thumb()
            return thumb.url
        except Exception, e:
            print e
        return None

    def get_due_date(self, obj):
        return obj.get_end_date()

    def get_activate_time(self, obj):
        obj.activate_time
    
    def get_achievement_id(self, obj):
        return obj.achievement.id

    def get_achievement_name(self, obj):
        h = HTMLParser()
        return h.unescape(strip_tags(obj.achievement.name))

    def get_achievement_description(self, obj):
        h = HTMLParser()
        return h.unescape(strip_tags(obj.achievement.short_decscription))

    def get_achievement_icon(self, obj):
        return 'icon-star'

    def get_api_status(self, obj):
        if obj.is_received():
            try:
                proof = obj.proof
                if proof.is_accepted():
                    """
                    РЕВЬЮ: вместо чисел нужно использовать константы
                    """
                    return 3 #утверждено
                elif proof.is_doubtfully():
                    return 2 #утверждено сомнительно
            except Exception, e:
                pass
        elif obj.is_failed():
            return 4 #не утверждено
        return 1 #в обработке
    
    def get_achievement_icon_url(self, obj):
        """
        РЕВЬЮ: переменные color & icon не определены
        """
        filename = 'red_star.png'.format(color, icon, '')
        return STATIC_URL + 'i/achievement_icons/' + filename

    def get_comment(self, obj):
        comment = ''
        try:
            comment = obj.proof.comment
        except Exception, e:
            pass
        return comment

class AchievementSerializer(serializers.ModelSerializer):
    class Meta:
        model = Achievement