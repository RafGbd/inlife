from django.conf.urls import patterns, url
from rest_framework.urlpatterns import format_suffix_patterns
from .views import GoalList, GoalUserAchievementsList, FileUploadView, RegisterUserDevice,\
                   login, file_upload, UploadedFileList, TimelineView, UserAchievementView, \
                   DeleteAction, UserAchievementDetailView
from django.views.decorators.csrf import csrf_exempt


urlpatterns = patterns('api.views',
    url(r'^goals/$', GoalList.as_view(), name='goals_list'),
    url(r'^get-settings/$', 'get_settings', name='get_settings'),
    url(r'^achievements/(?P<goal_id>[0-9]+)$', GoalUserAchievementsList.as_view(), name='goal_achievement'),
    url(r'^achievements/(?P<ach_id>[0-9]+)/delete$', DeleteAction.as_view(), name='delete_action'),
    url(r'^enqueue/(?P<device_key>[-\w]+)$', 'enqueue', name='enqueue'),
    url(r'^get-uploaded-bytes/(?P<upload_ticket>[a-fA-F\d]{40})$', 'get_uploaded_bytes', name='get_uploaded_bytes'),
    url(r'^upload/(?P<upload_ticket>[a-fA-F\d]{40})$', 'file_upload'),
    url(r'^upload/(?P<upload_ticket>[a-fA-F\d]{40})/(?P<filename>.+)$', FileUploadView.as_view(), name='upload'),
    url(r'^register/$', RegisterUserDevice.as_view(), name='register_device'),
    url(r'^reports/$', UploadedFileList.as_view(), name='report_list'),
    url(r'^reports/(?P<report_id>\d+)$', UploadedFileList.as_view(), name='report'),
    url(r'^timeline/$', TimelineView.as_view(), name='api_timeline'),
    url(r'^achievement/(?P<ach_id>[0-9]+)$', UserAchievementDetailView.as_view(), name='api_get_achievement'),
    url(r'^achievement/$', UserAchievementView.as_view(), name='api_put_achievement'),
)


urlpatterns += patterns('',
    url(r'^login/', csrf_exempt(login)),
)
# Format suffixes
urlpatterns = format_suffix_patterns(urlpatterns, allowed=['json', 'api'])
