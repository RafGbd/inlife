# -*- coding: utf-8 -*-
# Create your views here.
import time
import hashlib
from dateutil import parser as date_parser
from urllib import unquote

from django.http import Http404
from django.core.files.storage import default_storage
from django.core.files.base import ContentFile
from django.utils.html import strip_tags
from django.utils import timezone
from django.core.exceptions import PermissionDenied
from django.shortcuts import get_object_or_404
from django.db.models import BooleanField, IntegerField
from rest_framework import generics
from rest_framework import views
from rest_framework import parsers
from rest_framework import permissions
from rest_framework import status, exceptions
from rest_framework.authtoken.views import ObtainAuthToken
from rest_framework.authtoken.models import Token
from rest_framework.decorators import api_view, permission_classes, parser_classes
from rest_framework.reverse import reverse
from rest_framework.response import Response
from inlife.utils import api_image
from app.models import Achievement, UserAchievement, UserAchievementAction, Proof, ProofPhoto
from apps.configs.models import ApiConfig
from .serializers import GoalSerializer, \
                         UserAchievementSerializer, \
                         UserDeviceSerializer, \
                         UploadedFileDataSerializer, \
                         ProofPhotoSerializer, \
                         AchievementSerializer
from .models import UploadedFile
from .parsers import FileParser


@api_view(['GET', 'POST'])
@parser_classes((FileParser,))
def file_upload(request, upload_ticket):
    u'''
    Принимает файл по номеру заявки на прием файла - upload_ticket 
    '''
    content = {
        "isError": False,
        "errorMessage": None,
        "res": [
        ]
    }
    try:
        file_obj = request.FILES['file']
        uploaded_file = UploadedFile.objects.get(uploadTicket=upload_ticket)
        uploaded_file.file.save(api_image(u"image.jpg"), file_obj)
        if uploaded_file.achievementID:
            user_achievement = UserAchievement.objects.get(user=request.user, achievement_id=uploaded_file.achievementID)
            proof, created = Proof.objects.get_or_create(
                                    user_achievement=user_achievement,
                                    proof_type=Proof.P_PHOTO,
                                    from_phone=True,
                                )
            if uploaded_file.createDate:
                date = date_parser.parse(uploaded_file.createDate)
            else:
                date = timezone.now()
            proof_photo = ProofPhoto(proof=proof, photo=uploaded_file.file, date=date)

            proof.save()
            proof_photo.save()        
    except Exception, e:
        content['isError'] = True
        content['errorMessage'] = str(e)
    return Response(content)


@api_view(('GET',))
def api_root(request, format=None):
    return Response({
        'goals': reverse('api:goals_list', request=request, format=format),
    })


class InlifeExceptionMixin(object):
    def handle_exception(self, exc):
        content = {
            "isError": True,
            "errorMessage": None,
            "res": [],
        }
        """
        Handle any exception that occurs, by returning an appropriate response,
        or re-raising the error.
        """
        print exc
        if isinstance(exc, exceptions.Throttled):
            # Throttle wait header
            self.headers['X-Throttle-Wait-Seconds'] = '%d' % exc.wait

        if isinstance(exc, (exceptions.NotAuthenticated,
                            exceptions.AuthenticationFailed)):
            # WWW-Authenticate header for 401 responses, else coerce to 403
            auth_header = self.get_authenticate_header(self.request)

            if auth_header:
                self.headers['WWW-Authenticate'] = auth_header
            else:
                exc.status_code = status.HTTP_403_FORBIDDEN

        if isinstance(exc, exceptions.APIException):
            content['errorMessage'] = exc.detail
            return Response(content,
                            status=exc.status_code,
                            exception=True)
        elif isinstance(exc, Http404):
            content['errorMessage'] = 'Not found'
            return Response(content,
                            status=status.HTTP_404_NOT_FOUND,
                            exception=True)
        elif isinstance(exc, PermissionDenied):
            content['errorMessage'] = 'Permission denied'
            return Response(content,
                            status=status.HTTP_403_FORBIDDEN,
                            exception=True)

        else:
            content['errorMessage'] = str(exc)
            return Response(content,
                            status=status.HTTP_500_INTERNAL_SERVER_ERROR,
                            exception=True)

        
class GoalList(InlifeExceptionMixin, generics.ListCreateAPIView):
    u'''
    Возвращает все цели(корневые достижения из дерева, level==0)
    '''
    permission_classes = (permissions.IsAuthenticated,)
    queryset = Achievement.objects.filter(created_by_user=False)[:1]
    serializer_class = GoalSerializer

    def get(self, request, format=None):
        serializer = self.serializer_class(self.queryset, context={'request': request}, many=True)
        res = [
            {
                "id": 666, 
                "name": u"Твои достижения", 
                "description": "", 
                "tree_id": 666, 
                "background": "http://test.achiever.s3.amazonaws.com/achievements/9b397ec.jpg.800x282_q85_crop_detail.jpg", 
                "icon": "The 'api_svg_icon' attribute has no file associated with it."
            },
        ]
        content = {
            "isError": False,
            "errorMessage": None,
            'user': unicode(request.user),
            "res": res,
        }
        return Response(content)


class GoalUserAchievementsList(InlifeExceptionMixin, generics.ListCreateAPIView):
    u'''
    Возвращает все активированные достижения конкретной цели
    '''
    permission_classes = (permissions.IsAuthenticated,)
    serializer_class = UserAchievementSerializer

    def get_queryset(self, goal_id):
        # try:
        #     goal = Achievement.objects.get(id=goal_id, level=0)
        # except Achievement.DoesNotExist:
        #     raise Http404
        return UserAchievement.objects.filter(
                user=self.request.user.pk,
                status__in=[
                    UserAchievement.ACTIVE,]
            )

    def get(self, request, goal_id, format=None):
        queryset = self.get_queryset(goal_id=goal_id)
        serializer = self.serializer_class(queryset, many=True, context={'request': request})
        content = {
            "isError": False,
            "errorMessage": None,
            'user': unicode(request.user),
            "res": serializer.data,
        }
        return Response(content)


class RegisterUserDevice(InlifeExceptionMixin, generics.ListCreateAPIView):
    u'''
    Регистриует устройство, выдает device_key котоый нужен для загрузки файлов
    '''
    permission_classes = (permissions.IsAuthenticated,)

    def post(self, request, format=None):
        content = {
            "isError": False,
            "errorMessage": None,
            'user': unicode(request.user),
        }
        serializer = UserDeviceSerializer(data=request.DATA)
        if serializer.is_valid():
            user_device = serializer.object
            user_device.user = request.user
            user_device.save()
        else:
            content['isError'] = True
            content["errorMessage"]= serializer.errors,
        return Response(content)


@api_view(('GET',))
def get_settings(request):
    CONFIG_TYPES = {
        IntegerField: "int",
        BooleanField: "bool",
    }
    api_configs = ApiConfig.get_solo()
    res = []

    for config_key in api_configs._meta.get_all_field_names():
        field = api_configs._meta.get_field_by_name(config_key)[0]
        conf = {
            "key": config_key,
            "type": CONFIG_TYPES.get(field.__class__, ""),
            "value": getattr(api_configs, config_key)
        }
        res.append(conf)
    content = {
        "isError": False,
        "errorMessage": None,
        "res": res,
    }
    return Response(content)


@api_view(['POST', ])
#@permission_classes((permissions.IsAuthenticated,))
def enqueue(request, device_key):
    u'''
    По ключу устройства device_key принимает и верифицирует заявку UploadedFiledata на отправку файла, 
    возвращает uploadTicket для приема собственно файла  
    '''
    content = {
        "isError": False,
        "errorMessage": None,
        "res": [
        ]
    }
    _hash = hashlib.sha1()
    timestamp = str(int(time.time()) * 1000)
    serializer = UploadedFileDataSerializer(data=request.DATA)
    if serializer.is_valid():
        uploaded_file = serializer.object
        filename = api_image(u"test.jpg")        
        _hash.update(filename + device_key + timestamp)
        key = _hash.hexdigest()
        uploaded_file.uploadTicket = key
        uploaded_file.deviceKey = device_key
        uploaded_file.file.save(filename, ContentFile(''), save=False)
        uploaded_file.save()
        content['res'].append({"uploadticket": uploaded_file.uploadTicket, },)
        content['res'] = uploaded_file.uploadTicket
    else:
        content['isError'] = True
        content["errorMessage"]= serializer.errors
    return Response(content)


@api_view(['GET', ])
#@permission_classes((permissions.IsAuthenticated,))
def get_uploaded_bytes(request, upload_ticket):
    content = {
        "isError": False,
        "errorMessage": None,
        "res": [
        ]
    }
    try:
        uploaded_file= UploadedFile.objects.get(uploadTicket=upload_ticket)
        size = uploaded_file.file.size
        content['res'] = size
    except Exception, e:
        content['isError'] = True
        content['errorMessage'] = e
    return Response(content)


class FileUploadView(InlifeExceptionMixin, views.APIView):
    parser_classes = (parsers.FileUploadParser,)

    def put(self, request, upload_ticket, filename, format=None):
        content = {
            "isError": False,
            "errorMessage": None,
            "res": [
            ]
        }
        file_obj = request.FILES['file']
        uploaded_file = UploadedFile.objects.get(uploadTicket=upload_ticket)
        uploaded_file.file.save(filename, file_obj)
        user_achievement = UserAchievement.objects.get(user=request.user, achievement_id=uploaded_file.achievementID).select_related()
        proof, created = Proof.object.get_or_create(user_achievement=user_achievement, proof_type=Proof.P_PHOTO)
        proof.from_phone = True
        if not created:
            proof.save()
        proof_photo = ProofPhoto(proof=proof, photo=uploaded_file.file)
        proof_photo.save()
        if not user_achievement.achievement.image:
            user_achievement.achievement.image = proof_photo.photo
            user_achievement.achievement.save()
        return Response(content)


class InlifeApiObtainAuthToken(ObtainAuthToken):

    def post(self, request):
        content = {
            "isError": False,
            "errorMessage": None,
            "res": [
            ]
        }
        serializer = self.serializer_class(data=request.DATA)
        if serializer.is_valid():
            token, created = Token.objects.get_or_create(user=serializer.object['user'])
            content['res'].append({'token': token.key})
            return Response(content)
        content['isError'] = True
        content['errorMessage'] = "Login error"
        content['res'].append(serializer.errors)
        return Response(content, status=status.HTTP_400_BAD_REQUEST)


login = InlifeApiObtainAuthToken.as_view()

class UploadedFileList(InlifeExceptionMixin, generics.ListCreateAPIView):
    u'''
    Выдает заявки на загрузку файла(включая те, по которым файлы не загружались), порциями (параметр limit>0) или все сразу(если ничего не указано). 
    Может выдавать конкретную заявку по id, или заявки по конкретному achievementID.
    '''
    permission_classes = (permissions.IsAuthenticated,)    
    serializer_class = ProofPhotoSerializer

    def get_queryset(self, report_id, achievementID=None, limit=None):
        if limit:
            try:
                limit = int(limit)
            except Exception as e:
                raise exceptions.ParseError     
            finally:
                if limit <= 0: raise exceptions.ParseError

        if report_id:
            try:
                return UploadedFile.objects.get(pk=report_id)
            except UploadedFile.DoesNotExist:
                raise Http404
        elif achievementID:
            queryset = ProofPhoto.objects.filter(
                proof__user_achievement__achievement_id=achievementID,
                proof__user_achievement__user=self.request.user
            )
            # queryset = UploadedFile.objects.filter(achievementID=achievementID)[:limit]
            return queryset 
        else:
            return UploadedFile.objects.all()[:limit]

    def get(self, request, report_id=None, format=None):        
        achievementID = self.request.QUERY_PARAMS.get('achievementID')
        limit = self.request.QUERY_PARAMS.get('limit')        
        is_many = False if report_id else True
        serializer = self.serializer_class(self.get_queryset(report_id=report_id, achievementID=achievementID, limit=limit), context={'request': request}, many=is_many)
        content = {
            "isError": False,
            "errorMessage": None,
            'user': unicode(request.user),
            "res": serializer.data,
        }
        return Response(content) 

class TimelineView(generics.ListCreateAPIView):
    u'''
    Выдает все Achievement у которых есть загруженное фото
    '''

    permission_classes = (permissions.IsAuthenticated,)    
    serializer_class = UserAchievementSerializer
    #category_serializer_class = UserCategoryActionSerializer
    
    def from_date(self):
        date = None
        time_offset = self.request.GET.get('updateTime', None)
        if time_offset:
            try:
                date = date_parser.parse(time_offset)
            except Exception:
                pass
        return date

    def get_achievement_queryset(self):
        from_date = self.from_date()

        qs = UserAchievement.objects.exclude(is_hidden=True).filter(
                user=self.request.user,
                status__in=[UserAchievement.ACTIVE, UserAchievement.CONFIRMATION, UserAchievement.RECEIVED, UserAchievement.FAILED]
            ).extra(
                select={
                    'request_user_liked': '%s IN (SELECT app_userachievementlike.who_like_id FROM app_userachievementlike WHERE app_userachievementlike.user_achievement_id = app_userachievement.id)',
                }, select_params=[self.request.user.id, ]
            ).extra(
                select={
                    'likes_count': 'SELECT COUNT(*) FROM app_userachievementlike WHERE app_userachievementlike.user_achievement_id = app_userachievement.id',
                    'comments_count': 'SELECT COUNT(*) FROM app_userachievementcomment WHERE app_userachievementcomment.user_achievement_id = app_userachievement.id',
                },
            ).select_related(
                'achievement',
                'proof',
                'user'
            ).prefetch_related(
                'proof__photos',
            )
        if from_date:
            qs = qs.filter(date__gt=from_date)
        return qs.order_by('-date')
    
    def get(self, request):
        queryset = self.get_achievement_queryset()
        serializer = self.serializer_class(queryset, many=True, context={'request': request})
        content = {
            "isError": False,
            "errorMessage": None,
            'user': unicode(request.user),
            "res": serializer.data,
        }
        return Response(content)

class UserAchievementView(InlifeExceptionMixin, views.APIView):
    u'''
    Загрузка достижения пользователем
    '''
    permission_classes = (permissions.IsAuthenticated,)
    serializer_class = UserAchievementSerializer

    def put(self, request, format=None):
        content = {
            "isError": False,
            "errorMessage": None,
            'user': unicode(request.user),
        }
        if request.DATA.get('achievement', False):
            user_achievement = UserAchievement.objects.get(user=request.user, achievement=request.DATA['achievement'])
            serializer = self.serializer_class(user_achievement, data=request.DATA, context={'request': request})
        else:
            description = request.DATA.get('description', u"")
            achievement = Achievement(name=request.DATA['name'], description=description, created_by_user=True)
            achievement.save()
            user_achievement, created = UserAchievement.objects.get_or_create(user=request.user, achievement=achievement)
            user_achievement.added()
            user_achievement.save()     
            serializer = self.serializer_class(user_achievement, data=request.DATA, context={'request': request})

        if serializer.is_valid():
            serializer.object.activate()
            serializer.save()
            content['res'] = serializer.data
        else:
            content['isError'] = True
            content["errorMessage"]= serializer.errors,
        return Response(content)

class DeleteAction(InlifeExceptionMixin, views.APIView):
    u'''
    Удаление события из таймлайна
    '''
    permission_classes = (permissions.IsAuthenticated,)

    def get(self, request, ach_id):
        content = {
            "isError": False,
            "errorMessage": None,
            'user': unicode(request.user),
        }
        action = get_object_or_404(UserAchievementAction, user=request.user, achievement_id=ach_id)
        try:
            if hasattr(action, 'user_achievement'):
                if hasattr(action.user_achievement, 'proof'):
                    action.user_achievement.proof.delete()
                action.user_achievement.delete()
            action.delete()
        except action.DoesNotExist:
            pass
        return Response(content)


class UserAchievementDetailView(InlifeExceptionMixin, views.APIView):
    #permission_classes = (permissions.IsAuthenticated,)

    def get(self, request, ach_id):
        content = {
            "isError": False,
            "errorMessage": None,
            'user': unicode(request.user),
        }
        achievement = get_object_or_404(Achievement, id=ach_id)
        serializer = AchievementSerializer(achievement)
        content['res'] = serializer.data
        return Response(content)

