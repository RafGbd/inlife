# -*- coding: utf-8 -*-# -*- coding: utf-8 -*-
from django.db import models
from django.conf import settings
from easy_thumbnails.fields import ThumbnailerImageField

class UserDevice(models.Model):
    user = models.ForeignKey(settings.AUTH_USER_MODEL)
    deviceKey = models.CharField(max_length=128)
    model = models.CharField(max_length=128)
    simKey = models.CharField(max_length=128, blank=True, null=True)
    uniqueKey = models.CharField(max_length=128)
    
    class Meta:
        verbose_name = u'UserDevice'
        verbose_name_plural = u'UserDevices'

class UploadedFile(models.Model):    
    PENDING = 0
    ACCEPTED = 1 
    DECLINED = 2 
    STATE_CHOICES =[(PENDING, u'В процессе подтверждения'),
                    (ACCEPTED, u'Подтвержено'),
                    (DECLINED, u'Отклонено')]
    
    accuracy = models.CharField(max_length=128, blank=True, null=True)
    achievementID = models.CharField(max_length=128, blank=True, null=True)
    createDate = models.CharField(max_length=128, blank=True, null=True)
    fileName = models.CharField(max_length=128, blank=True, null=True)
    hash = models.CharField(max_length=128, blank=True, null=True)
    latitude = models.CharField(max_length=128, blank=True, null=True)
    longitude = models.CharField(max_length=128, blank=True, null=True)
    location  = models.CharField(max_length=128, blank=True, null=True)
    reportTypeID = models.CharField(max_length=128, blank=True, null=True)    
    totalBytes = models.CharField(max_length=128, blank=True, null=True)
    velocity = models.CharField(max_length=128, blank=True, null=True)
    deviceKey = models.CharField(max_length=128)
    uploadTicket = models.CharField(max_length=128)
    file = models.FileField(upload_to='api-files/')    