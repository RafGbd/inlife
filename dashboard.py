# -*- coding: utf-8 -*-
"""
This file was generated with the customdashboard management command, it
contains the two classes for the main dashboard and app index dashboard.
You can customize these classes as you want.

To activate your index dashboard add the following to your settings.py::
    ADMIN_TOOLS_INDEX_DASHBOARD = 'inlife.dashboard.CustomIndexDashboard'

And to activate the app index dashboard::
    ADMIN_TOOLS_APP_INDEX_DASHBOARD = 'inlife.dashboard.CustomAppIndexDashboard'
"""

from django.utils.translation import ugettext_lazy as _
from django.core.urlresolvers import reverse

from admin_tools.dashboard import modules, Dashboard, AppIndexDashboard
from admin_tools.utils import get_admin_site_name
from django.utils.text import capfirst


class CustomIndexDashboard(Dashboard):
    """
    Custom index dashboard for inlife.
    """
    def __init__(self, **kwargs):
            Dashboard.__init__(self, **kwargs)
            # self.children.append(CustomAppList(
            #     _('Applications'),
            #     exclude=(
            #         'django.contrib.*',
            #         'app.models.Achievement',
            #         'app.models.ApproveProof',
            #         'app.models.AchievementByUser',
            #         'app.models.UserGenTree',
            #         'app.models.Category',
            #         'groups.models.Group',
            #         'users.models.InLifeUser',
            #         'app.models.Proof',
            #         ),
            # ))

            self.children.append(modules.Group(
                        title=u'Приложения',
                        display="accordion",
                        children=[
                            modules.ModelList(
                                title=u'Управление контентом',
                                models=(
                                    'app.models.Achievement',
                                    'app.models.Category',
                                    )
                            ),
                            modules.ModelList(
                                title=u'Проверка подтверждений',
                                models=(
                                'app.models.Proof',
                                )
                            ),
                            modules.ModelList(
                                title=u'Управление группами',
                                models=('groups.models.Group',
                                        'groups.models.GroupDiscussion',
                                        'groups.models.AchievementOfMonth',)
                            ),
                            modules.ModelList(
                            title=u'Автоматические контакты с пользователем',
                                models=('common.models.EmailTemplate',)
                            ),
                            modules.ModelList(
                                title=u'Модерация общения',
                                models=('groups.models.DiscussionReply',)
                            ),
                            modules.ModelList(
                                title=u'Личная переписка',
                                models=('postman.models.Message',
                                        'postman.models.PendingMessage',)
                            ),
                            modules.ModelList(
                                title=u'Пользовательская активность',
                                models=('app.models.AchievementByUser',
                                        'app.models.ApproveProof',
                                        'app.models.Proof',
                                        'users.models.UserMetricValue',
                                        'users.models.Friendship',)
                            ),
                            modules.ModelList(
                                title=u'База пользователей',
                                models=('users.models.InLifeUser',
                                        'registration.models.RegistrationProfile',
                                        'social_auth.db.django_models.UserSocialAuth',)
                            ),
                            modules.ModelList(
                                title=u'Настройка поощрений',
                                models=('users.models.UserMetric',
                                        'users.models.Level',)
                            ),
                            modules.ModelList(
                                title=u'Обучалка',
                                models=('guide.models.GuideUser',
                                        'guide.models.GuideStep',)
                            ),
                            modules.ModelList(
                                title=u'Прочее',
                                models=('app.models.ProofPhoto',
                                        'app.models.UserAchievement',
                                        'app.models.Interest',
                                        'app.models.Filter',
                                        'app.models.AchievementFilter',
                                        'app.models.LandingItem',
                                        'social_auth.db.django_models.Association',
                                        'social_auth.db.django_models.Nonce',
                                        'users.models.TrustChange',
                                        'users.models.Subscriber',)
                            ),
                        ]
                    ))

            self.children.append(modules.Group(
                    title=u'Сортированные приложения',
                    display="accordion",
                    children=[
                        modules.ModelList(
                            title=u'Управление контентом',
                            models=(
                                'app.models.Achievement',
                                'app.models.Category',
                                )
                            ),
                        modules.ModelList(
                            title=u'Управление группами',
                            models=('groups.models.Group',
                                    'groups.models.GroupDiscussion',
                                    'groups.models.AchievementOfMonth',)
                            ),
                        modules.ModelList(
                            title=u'Модерация общения',
                            models=('groups.models.DiscussionReply',)
                            ),
                        modules.ModelList(
                            title=u'Пользовательская активность',
                            models=('app.models.AchievementByUser',
                                    'users.models.UserMetricValue',
                                    'users.models.Friendship',)
                            ),
                        ]
                    ))

            self.children.append(ProofModule(title=u'Подтверждение достижений',))

    def init_with_context(self, context):
        site_name = get_admin_site_name(context)
        # append a link list module for "quick links"
        self.children.append(modules.LinkList(
            _('Quick links'),
            layout='inline',
            draggable=False,
            deletable=False,
            collapsible=False,
            children=[
                [_('Return to site'), '/'],
                [_('Change password'),
                 reverse('%s:password_change' % site_name)],
                [_('Log out'), reverse('%s:logout' % site_name)],
                [_('Clear cache'), reverse('clear_cache')],
            ]
        ))

        # append an app list module for "Applications"
        # self.children.append(modules.AppList(
        #     _('Applications'),
        #     exclude=('django.contrib.*',),
        # ))

        # append an app list module for "Administration"
        self.children.append(modules.AppList(
            _('Administration'),
            models=('django.contrib.*',),
        ))

        # append a recent actions module
        self.children.append(modules.RecentActions(_('Recent Actions'), 5))


class CustomAppIndexDashboard(AppIndexDashboard):
    """
    Custom app index dashboard for inlife.
    """

    # we disable title because its redundant with the model list module
    title = ''

    def __init__(self, *args, **kwargs):
        AppIndexDashboard.__init__(self, *args, **kwargs)

        # append a model list module and a recent actions module
        self.children += [
            modules.ModelList(self.app_title, self.models),
            modules.RecentActions(
                _('Recent Actions'),
                include_list=self.get_app_content_types(),
                limit=5
            )
        ]

    def init_with_context(self, context):
        """
        Use this method if you need to access the request context.
        """
        return super(CustomAppIndexDashboard, self).init_with_context(context)


class CustomAppList(modules.DashboardModule, modules.AppListElementMixin):
    '''
    Кастомный АппЛист. По-умолчанию не отображается в дашборде, надо достать из "Модулей".
    '''
    title = _('CustomApplications')
    template = 'admin/admin_tools/custom_app_list.html'
    models = None
    exclude = None
    include_list = None
    exclude_list = None
    enabled = False

    def __init__(self, title=None, **kwargs):
        self.models = list(kwargs.pop('models', []))
        self.exclude = list(kwargs.pop('exclude', []))
        self.include_list = kwargs.pop('include_list', []) # deprecated
        self.exclude_list = kwargs.pop('exclude_list', []) # deprecated
        super(CustomAppList, self).__init__(title, **kwargs)

    def init_with_context(self, context):
        if self._initialized:
            return
        items = self._visible_models(context['request'])
        apps = {}
        for model, perms in items:
            app_label = model._meta.app_label
            if app_label not in apps:
                apps[app_label] = {
                    'title': capfirst(app_label.title()),
                    'url': self._get_admin_app_list_url(model, context),
                    'models': []
                }
            model_dict = {}
            model_dict['title'] = capfirst(model._meta.verbose_name_plural)
            if perms['change']:
                model_dict['change_url'] = self._get_admin_change_url(model, context)
            if perms['add']:
                model_dict['add_url'] = self._get_admin_add_url(model, context)
            apps[app_label]['models'].append(model_dict)

        apps_sorted = apps.keys()
        apps_sorted.sort()
        for app in apps_sorted:
            # sort model list alphabetically
            apps[app]['models'].sort(lambda x, y: cmp(x['title'], y['title']))
            self.children.append(apps[app])
        self._initialized = True


class ProofModule(modules.DashboardModule):
    def is_empty(self):
        pass

    def __init__(self, **kwargs):
        super(ProofModule, self).__init__(**kwargs)
        self.template = 'admin/admin_tools/proof.html'

    def init_with_context(self, context):
        from apps.app.models import Achievement, Proof
        # Отдаем по 10 значений, чтобы не загромождать дашборд
        self.proof_list = Proof.objects.filter(status=0).order_by('-pk')[:10]
        self.stars_number_choices = Achievement.STARS_NUMBER_CHIOCES
