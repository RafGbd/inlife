# coding: utf-8
from __future__ import absolute_import, unicode_literals

import copy

from django.core.exceptions import ValidationError
from django.forms.fields import Field, FileField
from django.forms.util import flatatt, ErrorDict, ErrorList
from django.forms.widgets import Media, media_property, TextInput, Textarea
from django.utils.datastructures import SortedDict
from django.utils.html import conditional_escape, format_html
from django.utils.html import format_html_join
from django.utils.encoding import smart_text, force_text, python_2_unicode_compatible
from django.utils.safestring import mark_safe
from django.utils import six
from django.core.cache import cache
from django.utils.http import urlquote

from inlife.settings import USER_PHOTOS_DIRNAME

from datetime import datetime
from hashlib import md5
import trans


def invalidate_template_cache(fragment_name, *variables):
    args = md5(u':'.join([urlquote(var) for var in variables]))
    cache_key = 'template.cache.%s.%s' % (fragment_name, args.hexdigest())
    cache.delete(cache_key)


def image_path(instance, filename, dirname):
    """
    Returns a file path where to save a photo
    """
    ext = filename.rsplit('.')[-1].lower()
    hash = md5(filename.encode('trans') + '%s' % datetime.now())
    new_filename = '%s.%s' % (hash.hexdigest()[3:10], ext)
    return u'%s/%s' % (dirname, new_filename)


def achievement_image(instance, filename):
    return image_path(instance, filename, 'achievements')

def proof_image(instance, filename):
    return image_path(instance, filename, 'proofs/photos')

def proof_file(instance, filename):
    return image_path(instance, filename, 'proofs/videos')

def inlifeuser_image(instance, filename):
    return image_path(instance, filename, USER_PHOTOS_DIRNAME)

def category_image(instance, filename):
    return image_path(instance, filename, 'categorys')

def group_image(instance, filename):
    return image_path(instance, filename, 'groups')

def landing_image(instance, filename):
    return image_path(instance, filename, 'landing')

def notification_image(filename):
    return image_path(instance, filename, 'notifications')

def api_image(filename):
    return image_path(None, filename, 'api/photos')

def settings_image(filename):
    return image_path(None, filename, 'settings')


class AchieverErrorList(ErrorList):
    def __str__(self):
        return self.as_ul()

    def as_ul(self):
        if not self: return ''
        return format_html('{0}',
                           format_html_join('',
                            '''
                            <br>
                                <div class="alert alert-danger">
                                    <i class="icon-warning-sign"></i>
                                    <button type="button" class="close" data-dismiss="alert">&times;</button>
                                    {0}
                                </div>''',
                                            ((force_text(e),) for e in self)
                                            )
                           )

class AchieverFormMixin(object):
    def as_inlife(self):
        """
            Генерирует форму в соответствии с нашей версткой
        """
        for name, field in self.fields.items():
            self[name].label_tag = self.label_tag
            field.label_tag = self.label_tag

        return self._html_output(
            normal_row="""
                <div class="form-group" %(html_class_attr)s>
                    %(label)s
                    <div class="col-xs-9">
                        %(field)s
                        %(errors)s
                    </div>
                </div>
            """,
            error_row = """
                <br>
                <div class="alert alert-danger">
                    <i class="icon-warning-sign"></i>
                    <button type="button" class="close" data-dismiss="alert">&times;</button>
                    %s
                </div>
            """,
            row_ender=' ',
            help_text_html='%s',
            errors_on_separate_row=False)

    def _html_output(self, normal_row, error_row, row_ender, help_text_html, errors_on_separate_row):
        "Helper function for outputting HTML. Used by as_table(), as_ul(), as_p()."
        top_errors = self.non_field_errors() # Errors that should be displayed above all fields.
        output, hidden_fields = [], []

        for name, field in self.fields.items():
            html_class_attr = ''
            bf = self[name]
            bf_errors = AchieverErrorList([conditional_escape(error) for error in bf.errors]) # Escape and cache in local variable.
            if bf.is_hidden:
                if bf_errors:
                    top_errors.extend(['(Hidden field %s) %s' % (name, force_text(e)) for e in bf_errors])
                hidden_fields.append(six.text_type(bf))
            else:
                # Create a 'class="..."' atribute if the row should have any
                # CSS classes applied.
                css_classes = bf.css_classes()
                if css_classes:
                    html_class_attr = ' class="%s"' % css_classes

                if errors_on_separate_row and bf_errors:
                    output.append(error_row % force_text(bf_errors))

                if field.help_text:
                    help_text = help_text_html % force_text(field.help_text)
                else:
                    help_text = ''

                if bf.label:
                    label = conditional_escape(force_text(bf.label))
                    # Only add the suffix if the label does not end in
                    # punctuation.
                    if self.label_suffix:
                        if label[-1] not in ':?.!':
                            label = format_html('{0}{1}', label, self.label_suffix)
                    label = self.label_tag(bf, label, help_text) or ''
                else:
                    label = ''

                output.append(normal_row % {
                    'errors': force_text(bf_errors),
                    'label': force_text(label),
                    'field': six.text_type(bf),
                    'help_text': help_text,
                    'html_class_attr': html_class_attr
                })

        if top_errors:
            output.insert(0, error_row % force_text(top_errors))

        if hidden_fields: # Insert any hidden fields in the last row.
            str_hidden = ''.join(hidden_fields)
            if output:
                last_row = output[-1]
                # Chop off the trailing row_ender (e.g. '</td></tr>') and
                # insert the hidden fields.
                if not last_row.endswith(row_ender):
                    # This can happen in the as_p() case (and possibly others
                    # that users write): if there are only top errors, we may
                    # not be able to conscript the last row for our purposes,
                    # so insert a new, empty row.
                    last_row = (normal_row % {'errors': '', 'label': '',
                                              'field': '', 'help_text':'',
                                              'html_class_attr': html_class_attr})
                    output.append(last_row)
                output[-1] = last_row[:-len(row_ender)] + str_hidden + row_ender
            else:
                # If there aren't any rows in the output, just append the
                # hidden fields.
                output.append(str_hidden)
        return mark_safe('\n'.join(output))

    def label_tag(self, bf, contents=None, help_text='', attrs=None):
        """
        Wraps the given contents in a <label>, if the field has an ID attribute.
        contents should be 'mark_safe'd to avoid HTML escaping. If contents
        aren't given, uses the field's HTML-escaped label.

        If attrs are given, they're used as HTML attributes on the <label> tag.
        """
        contents = contents or conditional_escape(bf.label)
        widget = bf.field.widget
        id_ = widget.attrs.get('id') or bf.auto_id
        if id_:
            attrs = attrs and flatatt(attrs) or ''
            contents = format_html('<label class="col-xs-3 control-label" for="{0}"{1}>{2}<br><small>{3}</small></label>',
                                   widget.id_for_label(id_), attrs, contents, help_text
                                   )
        return mark_safe(contents)