# -*- coding: utf-8 -*-
import os
import sys
import mimetypes

mimetypes.add_type("image/svg+xml", ".svg", True)
mimetypes.add_type("image/svg+xml", ".svgz", True)

gettext = lambda s: s
PROJECT_PATH = os.path.abspath(os.path.dirname(__file__))
sys.path.insert(0, os.path.join(PROJECT_PATH, "../libs"))
sys.path.insert(0, os.path.join(PROJECT_PATH, "../apps"))

DEBUG = False
TEMPLATE_DEBUG = DEBUG

ADMINS = (
    # ('Your Name', 'your_email@example.com'),
)
MANAGERS = ADMINS

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.mysql', # Add 'postgresql_psycopg2', 'mysql', 'sqlite3' or 'oracle$
        'NAME': 'inlife',                      # Or path to database file if using sqlite3.
        'USER': 'master',                      # Not used with sqlite3.
        'PASSWORD': 'redc1own45',                  # Not used with sqlite3.
        'HOST': 'inlifetest.cplcb3d07due.eu-west-1.rds.amazonaws.com',                      # Set to emp$
        'PORT': '',                      # Set to empty string for default. Not used with sqlite3.
    }
}

CACHES = {
   'default': {
       'BACKEND': 'django.core.cache.backends.memcached.MemcachedCache',
       'LOCATION': '127.0.0.1:11211',
   }
}

BROKER_URL = 'django://'

# Hosts/domain names that are valid for this site; required if DEBUG is False
# See https://docs.djangoproject.com/en/1.5/ref/settings/#allowed-hosts
ALLOWED_HOSTS = []

# Local time zone for this installation. Choices can be found here:
# http://en.wikipedia.org/wiki/List_of_tz_zones_by_name
# although not all choices may be available on all operating systems.
# In a Windows environment this must be set to your system time zone.
TIME_ZONE = 'Asia/Yekaterinburg'

# Language code for this installation. All choices can be found here:
# http://www.i18nguy.com/unicode/language-identifiers.html
LANGUAGE_CODE = 'ru'

SITE_ID = 1

ACCOUNT_ACTIVATION_DAYS = 2
AUTH_USER_EMAIL_UNIQUE = True
EMAIL_HOST = 'localhost'
EMAIL_PORT = 1025
EMAIL_HOST_USER = ''
EMAIL_HOST_PASSWORD = ''
EMAIL_USE_TLS = False
DEFAULT_FROM_EMAIL = 'reg@example.com'

# If you set this to False, Django will make some optimizations so as not
# to load the internationalization machinery.
USE_I18N = True

# If you set this to False, Django will not format dates, numbers and
# calendars according to the current locale.
USE_L10N = True

# If you set this to False, Django will not use timezone-aware datetimes.
USE_TZ = True

MEDIA_ROOT = os.path.join(PROJECT_PATH, "..", "media")
MEDIA_URL = "/media/"
USER_PHOTOS_DIRNAME = "photos"

STATIC_ROOT = ''#os.path.join(PROJECT_PATH, "..", "static")

AWS_ACCESS_KEY_ID = 'AKIAJA36MDQ7AOZG3ABA'
AWS_SECRET_ACCESS_KEY = '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'
AWS_STORAGE_BUCKET_NAME = 'test.achiever'
AWS_S3_HOST = 's3-eu-west-1.amazonaws.com'
AWS_S3_USE_SSL = False
AWS_PRELOAD_METADATA = True
AWS_QUERYSTRING_EXPIRE = 60 * 60 * 24 * 365 * 10 # файл по url будет доступен ~10 лет
STATIC_URL = '/static/'

# Neo4j configuration
NEO4J_URL = 'http://localhost:7474/db/data/'
NEO4J_LOGIN = 'username'
NEO4J_PASSWORD = 'password'


# Additional locations of static files
STATICFILES_DIRS = (
    os.path.join(PROJECT_PATH, "../static"),
    # Put strings here, like "/home/html/static" or "C:/www/django/static".
    # Always use forward slashes, even on Windows.
    # Don't forget to use absolute paths, not relative paths.
)

# List of finder classes that know how to find static files in
# various locations.
STATICFILES_FINDERS = (
    'django.contrib.staticfiles.finders.FileSystemFinder',
    'django.contrib.staticfiles.finders.AppDirectoriesFinder',
)

# Make this unique, and don't share it with anybody.
SECRET_KEY = '46nn*q=88o&f)8@a_8a7_kx%qwhpkwq+ls=f9l)05b^fmchlk+'

# List of callables that know how to import templates from various sources.
TEMPLATE_LOADERS = (
    #('django.template.loaders.cached.Loader', (
        'django.template.loaders.filesystem.Loader',
        'django.template.loaders.app_directories.Loader',
    #)),
)

TEMPLATE_CONTEXT_PROCESSORS = (
    'django.contrib.auth.context_processors.auth',
    'django.core.context_processors.static',
    'django.core.context_processors.request',
    'app.context_processors.debug',
    'app.context_processors.reply_form',
    'django.core.context_processors.csrf',
)

COMPRESS_ENABLED = False

MIDDLEWARE_CLASSES = (
    'django.middleware.common.CommonMiddleware',
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'users.middleware.InlifeSocialAuthExceptionMiddleware',
    'inlife.middleware.CloseSiteMiddleware',
    'inlife.middleware.UserNameMiddleware',
    # Uncomment the next line for simple clickjacking protection:
    'django.middleware.clickjacking.XFrameOptionsMiddleware',
)

ROOT_URLCONF = 'inlife.urls'

# Python dotted path to the WSGI application used by Django's runserver.
WSGI_APPLICATION = 'inlife.wsgi.application'

TEMPLATE_DIRS = (
    os.path.join(PROJECT_PATH, "..", "templates"),
)

SESSION_ENGINE = 'redis_sessions.session'


INSTALLED_APPS = (
    #django-admin-tools
    'admin_tools',
    'admin_tools.theming',
    'admin_tools.menu',
    'admin_tools.dashboard',
    #django
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.sites',
    'django.contrib.messages',
    'django.contrib.staticfiles',
    'django.contrib.admin',
    #3-th party
    'south',
    'mptt',
    'registration',
    'social_auth',
    'social_friends_finder',
    'postman',
    'feincms',
    'kombu.transport.django',
    'raven.contrib.django.raven_compat',
    'rest_framework',
    'rest_framework.authtoken',
    'easy_thumbnails',
    'image_cropping',
    'filebrowser',
    'tinymce',
    'djangosphinx',
    'storages',
    'cacheops',
    'haystack',
    'celery_haystack',
    'debug_toolbar',
    'redactor',
    'ckeditor',
    'chosen',
    'solo',
    #our
    'app',
    'apps.achievements',
    'apps.configs',
    'actions',
    'groups',
    'api',
    'fileupload',
    'common',
    'guide',
    'notifications',
    'friends',
    'users', # ! this must be in the end !
    # Uncomment the next line to enable the admin:
    # 'django.contrib.admin',
    # Uncomment the next line to enable admin documentation:
    # 'django.contrib.admindocs',

#    'gunicorn',
)
AUTH_USER_MODEL = 'users.InLifeUser'
SOCIAL_AUTH_USER_MODEL = AUTH_USER_MODEL
FEINCMS_USE_PAGE_ADMIN=False 
DEFAULT_TRUST = 50
MIN_TRUST = 30
TRUST_INCREASE = 5
TRUST_PENALTY = -5

AUTHENTICATION_BACKENDS = (
    'social_auth.backends.twitter.TwitterBackend',
    'social_auth.backends.facebook.FacebookBackend',
    'social_auth.backends.google.GoogleOAuthBackend',
    'social_auth.backends.google.GoogleOAuth2Backend',
    'social_auth.backends.contrib.vk.VKOAuth2Backend',
    'django.contrib.auth.backends.ModelBackend',
)

VK_APP_ID = '3492399'
VK_API_SECRET = 'OMyuR4tZd6CHqpaLxZax'
VK_EXTRA_SCOPE = ['photos', 'wall', ]
FACEBOOK_APP_ID = '514681578573636'
FACEBOOK_API_SECRET = '2211d1c8217e961e2943ce72e86c60ec'
TWITTER_CONSUMER_KEY = '3v2VctYMOJuHuWV9ZEkJg'
TWITTER_CONSUMER_SECRET = 'svqJZJ8JuxLWXJC2CnoUluAFtPqCzbjUFtauxRenvg'
GOOGLE_OAUTH2_CLIENT_ID = '335376993573-6ahhig73jf8qcijlh00kr4gen61rrcig.apps.googleusercontent.com'
GOOGLE_OAUTH2_CLIENT_SECRET = '6vFyRaTdlnYx3t3v4jBx-zNG'
GOOGLE_OAUTH_EXTRA_SCOPE = ['https://www.google.com/m8/feeds/']

INVITE_PROVIDERS = ['google-oauth2']

VK_EXTRA_DATA = ['bdate', 'sex', 'photo_max_orig']

SOCIAL_AUTH_PIPELINE = (
    'social_auth.backends.pipeline.social.social_auth_user',
    'app.auth_pipeline.get_username',
    'social_auth.backends.pipeline.user.create_user',
    'social_auth.backends.pipeline.social.associate_user',
    'social_auth.backends.pipeline.social.load_extra_data',
    'social_auth.backends.pipeline.user.update_user_details',
    'app.auth_pipeline.new_user'
)

LOGIN_URL = '/accounts/login/'
LOGIN_ERROR_URL = '/'
LOGIN_REDIRECT_URL = SOCIAL_AUTH_LOGIN_REDIRECT_URL = '/login/redirect/'
SOCIAL_AUTH_UID_LENGTH = 222
SOCIAL_AUTH_NONCE_SERVER_URL_LENGTH = 200
SOCIAL_AUTH_ASSOCIATION_SERVER_URL_LENGTH = 135
SOCIAL_AUTH_ASSOCIATION_HANDLE_LENGTH = 125
SOCIAL_AUTH_PROTECTED_USER_FIELDS = ['first_name', 'last_name']


POSTMAN_AUTO_MODERATE_AS = True

REST_FRAMEWORK = {
    # Use hyperlinked styles by default.
    # Only used if the `serializer_class` attribute is not set on a view.
    'DEFAULT_MODEL_SERIALIZER_CLASS':
        'rest_framework.serializers.HyperlinkedModelSerializer',
    
    'DEFAULT_AUTHENTICATION_CLASSES': (
        'rest_framework.authentication.BasicAuthentication',
        'rest_framework.authentication.SessionAuthentication',
        'rest_framework.authentication.TokenAuthentication',
    ),
    # Use Django's standard `django.contrib.auth` permissions,
    # or allow read-only access for unauthenticated users.
    #'DEFAULT_PERMISSION_CLASSES': [
    #    'rest_framework.permissions.DjangoModelPermissionsOrAnonReadOnly'
    #],
}

#filebrowser
ADMIN_MEDIA_PREFIX = '/static/admin/'

ADMIN_TOOLS_INDEX_DASHBOARD = 'dashboard.CustomIndexDashboard'
ADMIN_TOOLS_APP_INDEX_DASHBOARD = 'dashboard.CustomAppIndexDashboard'

CACHEBUST='?cachebust=test'
#TinyMCE
# TINYMCE_JS_URL = STATIC_URL + "tiny_mce/tiny_mce.js"
# TINYMCE_JS_ROOT = STATIC_URL + "/tiny_mce"

TINYMCE_SPELLCHECKER = False
TINYMCE_PLUGINS = [
    'pagebreak',
    'layer',
    'table',
    'save',
    'advhr',
    'advimage',
    'advlink',
    'emotions',
    'iespell',
    'inlinepopups',
    'insertdatetime',
    'preview',
    'media',
    'searchreplace',
    'print',
    'contextmenu',
    'paste',
    'directionality',
    'fullscreen',
    'noneditable',
    'visualchars',
    'nonbreaking',
    'xhtmlxtras',
    'template',
    'wordcount',
    'advlist',
]

TINYMCE_DEFAULT_CONFIG={
    'theme' : "advanced",
    'plugins' : ",".join(TINYMCE_PLUGINS),
    'language' : 'ru',
    'theme_advanced_buttons1': "save,newdocument,|,bold,italic,underline,strikethrough,|,justifyleft,justifycenter,justifyright,justifyfull,formatselect,fontselect,fontsizeselect",
    'theme_advanced_buttons2': "cut,copy,paste,pastetext,pasteword,|,search,replace,|,bullist,numlist,|,outdent,indent,blockquote,|,undo,redo,|,link,unlink,anchor,image,cleanup,code,|,insertdate,inserttime,preview,|,forecolor,backcolor",
    'theme_advanced_buttons3': "tablecontrols,|,hr,removeformat,visualaid,|,sub,sup,|,charmap,emotions,iespell,media,advhr,|,print,|,ltr,rtl,|,fullscreen",
    'theme_advanced_buttons4': "insertlayer,moveforward,movebackward,absolute,|,styleprops,|,cite,abbr,acronym,del,ins,attribs,|,visualchars,nonbreaking,template,pagebreak",
    'theme_advanced_toolbar_location' : "top",
    'theme_advanced_toolbar_align' : "left",
    'theme_advanced_statusbar_location' : "bottom",
    'theme_advanced_resizing' : True,
    'table_default_cellpadding': 2,
    'table_default_cellspacing': 2,
    'cleanup_on_startup' : False,
    'cleanup' : False,
    'paste_auto_cleanup_on_paste' : False,
    'paste_block_drop' : False,
    'paste_remove_spans' : False,
    'paste_strip_class_attributes' : False,
    'paste_retain_style_properties' : "",
    'forced_root_block' : False,
    'force_br_newlines' : False,
    'force_p_newlines' : False,
    'remove_linebreaks' : False,
    'convert_newlines_to_brs' : False,
    'inline_styles' : False,
    'relative_urls' : False,
    'formats' : {
        'alignleft' : {'selector' : 'p,h1,h2,h3,h4,h5,h6,td,th,div,ul,ol,li,table,img', 'classes' : 'align-left'},
        'aligncenter' : {'selector' : 'p,h1,h2,h3,h4,h5,h6,td,th,div,ul,ol,li,table,img', 'classes' : 'align-center'},
        'alignright' : {'selector' : 'p,h1,h2,h3,h4,h5,h6,td,th,div,ul,ol,li,table,img', 'classes' : 'align-right'},
        'alignfull' : {'selector' : 'p,h1,h2,h3,h4,h5,h6,td,th,div,ul,ol,li,table,img', 'classes' : 'align-justify'},
        'strikethrough' : {'inline' : 'del'},
        'italic' : {'inline' : 'em'},
        'bold' : {'inline' : 'strong'},
        'underline' : {'inline' : 'u'}
    },
}

from easy_thumbnails.conf import Settings as thumbnail_settings
THUMBNAIL_PROCESSORS = (
    'image_cropping.thumbnail_processors.crop_corners',
) + thumbnail_settings.THUMBNAIL_PROCESSORS
#IMAGE_CROPPING_THUMB_SIZE = (300, 300)
IMAGE_CROPPING_SIZE_WARNING = True

# A sample logging configuration. The only tangible logging
# performed by this configuration is to send an email to
# the site admins on every HTTP 500 error when DEBUG=False.
# See http://docs.djangoproject.com/en/dev/topics/logging for
# more details on how to customize your logging configuration.
LOGGING = {
    'version': 1,
    'disable_existing_loggers': False,
    'root': {
        'level': 'WARNING',
        'handlers': ['sentry'],
    },
    'filters': {
        'require_debug_false': {
            '()': 'django.utils.log.RequireDebugFalse'
        }
    },
    'handlers': {
        'mail_admins': {
            'level': 'ERROR',
            'filters': ['require_debug_false'],
            'class': 'django.utils.log.AdminEmailHandler'
        },
        'sentry': {
            'level': 'ERROR',
            'class': 'raven.contrib.django.raven_compat.handlers.SentryHandler',
        },
    },
    'loggers': {
        'django.request': {
            'handlers': ['mail_admins'],
            'level': 'ERROR',
            'propagate': True,
        },
    }
}

APPEND_SLASH = False 

PAGINATE_FRIEND = 50
if not os.path.exists(MEDIA_ROOT + '/uploads'):
    os.makedirs(MEDIA_ROOT + '/uploads')

CROP_SIZE = (630, 290, )

#THUMBNAIL_DEFAULT_STORAGE = 'easy_thumbnails.storage.ThumbnailFileSystemStorage'
THUMBNAIL_DEFAULT_STORAGE = 'storages.backends.s3boto.S3BotoStorage'

CACHEOPS = {
    'users.inlifeuser': ('get', 60*15),
    'guide.*': ('all', 60 * 60),
    'easy_thumbnails.*': ('all', 60*60),
    'app.category': ('all', 60*60),
    'app.achievement': ('all', 60*60),
    'app.likeuserachievementaction': ('all', 60*60),
    'app.userachievementactioncomment': ('all', 60*60),
    'app.proofphoto': ('all', 60*60),
    'app.userachievement' : ('all', 60*60),
}
HAYSTACK_SIGNAL_PROCESSOR = 'celery_haystack.signals.CelerySignalProcessor'
FILE_UPLOAD_PERMISSIONS = 0755

ELASTICSEARCH_INDEX_SETTINGS = {
    'settings': {
        "analysis": {
            "analyzer": {
                "ru_analyzer": {
                    "type": "custom",
                    "tokenizer": "standard",
                    "filter": ["lowercase", "russian_morphology", "english_morphology", "ru_stopwords"]
                },
                "ngram_analyzer": {
                    "type": "custom",
                    "tokenizer": "lowercase",
                    "filter": ["haystack_ngram"]
                },
                "edgengram_analyzer": {
                    "type": "custom",
                    "tokenizer": "lowercase",
                    "filter": ["haystack_edgengram"]
                }
            },
            "tokenizer": {
                "haystack_ngram_tokenizer": {
                    "type": "nGram",
                    "min_gram": 3,
                    "max_gram": 15,
                },
                "haystack_edgengram_tokenizer": {
                    "type": "edgeNGram",
                    "min_gram": 2,
                    "max_gram": 15,
                    "side": "front"
                }
            },
            "filter": {
                "ru_stopwords": {
                    "type": "stop",
                    "stopwords": "а,без,более,бы,был,была,были,было,быть,в,вам,вас,весь,во,вот,все,всего,всех,вы,где,да,даже,для,до,его,ее,если,есть,еще,же,за,здесь,и,из,или,им,их,к,как,ко,когда,кто,ли,либо,мне,может,мы,на,надо,наш,не,него,нее,нет,ни,них,но,ну,о,об,однако,он,она,они,оно,от,очень,по,под,при,с,со,так,также,такой,там,те,тем,то,того,тоже,той,только,том,ты,у,уже,хотя,чего,чей,чем,что,чтобы,чье,чья,эта,эти,это,я,a,an,and,are,as,at,be,but,by,for,if,in,into,is,it,no,not,of,on,or,such,that,the,their,then,there,these,they,this,to,was,will,with"
                },
                "haystack_ngram": {
                    "type": "nGram",
                    "min_gram": 3,
                    "max_gram": 15
                },
                "haystack_edgengram": {
                    "type": "edgeNGram",
                    "min_gram": 2,
                    "max_gram": 15
                }
            }
        }
    }
}

ELASTICSEARCH_DEFAULT_ANALYZER = "ru_analyzer"

INSTAGRAM_CLIENTID = 'c72e89a0ee294a10af7fc395662e92e4'
INSTAGRAM_CLIENTSECRET = '72f80c3181a64d7aa4e08e1013a677d1'
INSTAGRAM_REDIRECTURI = 'http://test.achiever.ru/instagramback/'

REDACTOR_OPTIONS = {
    'removeStyles': True,
    'removeClasses': True,
    'lang': 'ru',
    }
REDACTOR_UPLOAD = 'uploads/'

CKEDITOR_CONFIGS = {
    'default': {
        'forcePasteAsPlainText': True,
        'toolbar': 'Full',
        'language': 'ru',
        'resize_enabled': True,
        'width': "900",
        'extraPlugins': 'horizontalrule',
        'toolbar_Full': [
            {'name': 'document',    'items': [ 'Source','-','Save','NewPage','DocProps','Preview','Print','-','Templates' ] },
            {'name': 'clipboard',   'items': [ 'Cut','Copy','Paste','PasteText','PasteFromWord','-','Undo','Redo' ] },
            {'name': 'editing',     'items': [ 'Find','Replace','-','SelectAll','-','SpellChecker', 'Scayt' ] },
            {'name': 'basicstyles', 'items': [ 'Bold','Italic','Underline','Strike','Subscript','Superscript','-','RemoveFormat' ] },
            {'name': 'paragraph',   'items': [ 'NumberedList','BulletedList','-','Outdent','Indent','-','Blockquote','CreateDiv','-','JustifyLeft','JustifyCenter','JustifyRight','JustifyBlock','-','BidiLtr','BidiRtl' ] },
            {'name': 'links',       'items': [ 'Link','Unlink','Anchor' ] },
            {'name': 'insert',      'items': [ 'Image','Flash','Table','HorizontalRule','Smiley','SpecialChar','PageBreak' ] },
            {'name': 'styles',      'items': [ 'Styles','Format','Font','FontSize' ] },
            {'name': 'colors',      'items': [ 'TextColor','BGColor' ] },
            {'name': 'tools',       'items': [ 'ShowBlocks', '-', 'Maximize',  ] }
        ],
    },
}

CKEDITOR_UPLOAD_PATH = "uploads/"

try:
    from local_settings import *
except ImportError:
    pass
