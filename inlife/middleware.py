import re
from django.shortcuts import render_to_response, redirect
from django.core.urlresolvers import reverse

extra_url = (r'^/settings/.*',
             r'^/closesite/.*',
             r'/accounts/login/',
             # r'^/admin/.*',
             # r'^/admin_tools/.*',
             # r'^/clear_cache/$',
             )

class CloseSiteMiddleware(object):

    def process_request(self, request):
#        if config_value('Site','CLOSE_SITE'):
#            for item in extra_url:
#                if re.match(item, request.path):
#                    return None
#            return redirect(reverse('closesite'))
        return None


class UserNameMiddleware(object):

    def process_request(self, request):
        if request.user.is_authenticated():
            if not request.user.first_name and request.path != reverse('profile'):
                return redirect(reverse('profile'))
        return None


