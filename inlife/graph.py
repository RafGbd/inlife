from django.conf import settings
from neo4jrestclient.client import GraphDatabase


db = GraphDatabase(settings.NEO4J_URL,
                   settings.NEO4J_LOGIN,
                   settings.NEO4J_PASSWORD)

def _create_indexes():
    db.query("CREATE INDEX ON :User(_id)")

_create_indexes()
