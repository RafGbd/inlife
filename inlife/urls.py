from django.conf.urls import patterns, include, url
from django.conf import settings
from django.contrib import admin
from django.views.generic import ListView
from django.views.decorators.csrf import csrf_exempt
from app.views import *
from filebrowser.sites import site
admin.autodiscover()


urlpatterns = patterns(
    '',
    url(r'', include('apps.achievements.urls')),

    url(r'^$', HomeView.as_view(), name='home'),
    url(r'^rules/$', RulesView.as_view(), name='rules'),
    url(r'^clear_cache/$', 'app.views.clear_cache', name='clear_cache'),
    url(r'^closesite/$', 'app.views.closesite', name='closesite'),
    url(r'^start/$', StartView.as_view(), name='start'),
    url(r'^ab_test/$', ABView.as_view(), name='ab_test'),
    url(r'^friends/', include('apps.friends.urls')),
    url(r'^mytrees/get_children/(?P<ach_id>\d+)$', GetAchievementChildren.as_view(), name='get_achivement_children'),
    #url(r'^mytrees/(?P<tree_pk>\d+)/(?P<pk>\d+)/$', UserAchievementDetailView.as_view(), name='user_achievement_detail'),
    url(r'^achievements/create/$', AchievementCreateView.as_view(), name='achievement_create'),
    url(r'^achievements/(?P<ach_id>\d+)/\?callback=(?P<callback>.+)$', 'app.views.achievement_detail', name='achievement_with_callback'),
    url(r'^achievements/(?P<ach_id>\d+)/$', 'app.views.achievement_detail', name='achievement'),
    url(r'^achievements/(?P<ach_id>\d+)/activate/$', 'app.views.activate_achievement', name='activate_achievement'),
    url(r'^achievements/(?P<ach_id>\d+)/unactivate/$', 'app.views.unactivate_achievement', name='unactivate_achievement'),
    url(r'^achievements/(?P<ach_id>\d+)/confirm/$', 'app.views.confirm_achievement', name='confirm_achievement'),
    url(r'^achievements/(?P<ach_id>\d+)/confirm/upload_photo/$', 'app.views.upload_proof_photo', name='upload_proof_photo'),
    url(r'^achievements/(?P<user_ach_id>\d+)/comment/$', CommentAchievementView.as_view(), name='comment_achievement'),
    url(r'^achievements/(?P<user_ach_id>\d+)/like/$', LikeAchievementView.as_view(), name='like_achievement'),
    url(r'^achievements/(?P<user_ach_id>\d+)/unlike/$', UnLikeAchievementView.as_view(), name='unlike_achievement'),
    url(r'^achievements/(?P<user_ach_id>\d+)/accept/$', AcceptAchievementView.as_view(), name='accept_achievement'),
    url(r'^achievements/(?P<user_ach_id>\d+)/delete/$', DeleteAchievementView.as_view(), name='delete_achievement'),
    url(r'^confirm/upload_temp_proof_photo/$', 'app.views.upload_temp_proof_photo', name='upload_temp_proof_photo'),
    url(r'^confirm/delete_temp_proof_photo/(?P<obj_id>\d+)/$',
        'app.views.delete_temp_proof_photo', name='delete_temp_proof_photo'),
    url(r'^admin/', include(admin.site.urls)),
    url(r'^admin_tools/', include('admin_tools.urls')),
    url(r'^settings/', include('lsettings.urls')),
    url(r'^proof/(?P<pk>\d+)/$', 'app.views.proof_vote', name='proof_vote'),

    url(r'^conversations/$', 'app.views.conversations', name='conversations'),
    url(r'^conversations/count/$', ConversationsCountView.as_view(), name='conversations_count'),
    url(r'^conversations/check$', 'app.views.conversations_check', name='conversations_check'),

    url(r'^comment/delete/(?P<obj_type>[\-\w]+)/(?P<pk>\d+)/$', CommentDeleteView.as_view(), name='comment_delete'),
    url(r'^ratings/$', RatingsView.as_view(), name='ratings'),
    url(r'^share/enquire_posting_vk/$', EnquireVKPosting.as_view(), name='enquire_posting_vk'),
    url(r'^share/vk_post_images/$', VKPostImages.as_view(), name='vk_post_images'),
    url(r'^soc_post_image/$', SocApiPostImage, name='soc_post_image'),
    url(r'^(?P<user_id>\d+)/achievement/(?P<pk>\d+)/$', UserAchievementConfirmedDetailView.as_view(), name='user_achievement_confirmed_detail'),
    url(r'^(?P<user_id>\d+)/achievement/(?P<pk>\d+)/vk_post_images/$', VKPostImages.as_view(), name='vk_post_images'),
    url(r'^(?P<user_id>\d+)/achievement/(?P<ach_id>\d+)/data/$', UserAchievmentDataView.as_view(), name='achievment_data'),

    url(r'^edit_achievement/(?P<pk>\d+)/$', AchievementEditView.as_view(), name='user_edit_achievement'),
    url(r'^achievements/create/(?P<user_id>\d+)/$', AchievementForUserCreateView.as_view(), name='create_achievement_for_user'),
    url(r'^userachievements/create/(?P<ach_id>\d+)/$', 'app.views.userachievement_create', name='userachievement_create'),

    url(r'^instagram/$', 'app.views.instagram', name='instagram'),
    url(r'^instagramback/$', 'app.views.instagramback', name='instagramback'),
    url(r'^nextphotos/$', 'app.views.nextphotos', name='nextphotos'),

    url(r'^(?P<user_id>\d+)/summary/$', 'app.views.summary', name='user_summary'),
    #(r'^search/', include('haystack.urls')),
    url(r'^search/$', SearchView.as_view(), name='search'),
    url(r'', include('users.urls')),
    url(r'', include('guide.urls')),
    url(r'', include('actions.urls')),
    url(r'^notifications/', include('notifications.urls')),
    url(r'^messages/', include('postman.urls')),
    url(r'^api/', include('api.urls', namespace='api')),
    url(r'^groups/', include('groups.urls')),
    url(r'', include('social_auth.urls')),
    #(r'^tinymce/', include('tinymce.urls')),
    #url(r'^admin/filebrowser/', include(site.urls)),
    url(r'^fileupload/', include('fileupload.urls')),
    url(r'^redactor/', include('redactor.urls')),
    url(r'^ckeditor/', include('ckeditor.urls')),
    url(r'^api-auth/', include('rest_framework.urls', namespace='rest_framework')),
    )
    
if settings.DEBUG:
    urlpatterns += patterns(
        '', url(r'^media/(?P<path>.*)$', 'django.views.static.serve', {
                'document_root': settings.MEDIA_ROOT, }),
        )
    from django.contrib.staticfiles.urls import staticfiles_urlpatterns
    urlpatterns += staticfiles_urlpatterns()
else:
    from django.shortcuts import render

    def handler_custom404(request, *args, **kwargs):
        return render(request, "404.html", {})

    def handler_custom500(request, *args, **kwargs):
        return render(request, "500.html", {})

    handler404 = 'inlife.urls.handler_custom404'
    handler500 = 'inlife.urls.handler_custom500'
