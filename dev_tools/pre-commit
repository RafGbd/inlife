#!/usr/bin/env python

import os
import re
import subprocess
import sys

from subprocess import Popen, PIPE

def exit_on_error(func):
    def wrapper(*args, **kw):
        return_code = func(*args, **kw)
        if return_code:
            sys.exit(return_code)
        return return_code
    return wrapper


def pylint_check():
    """Checks your git commit with Pylint!"""
    # Run the git command that gets the filenames of every file that has been
    # locally modified since the last commit.

    # Threshold for code to pass the Pylint test. 10 is the highest score Pylint
    # will give to any peice of code.
    PYLINT_PASS_THRESHOLD = 7

    def is_py_script(filename):
        """Returns True if a file is a python executable."""
        if not os.access(filename, os.X_OK):
            return False
        else:
            try:
                first_line = open(filename, "r").next().strip()
                return "#!" in first_line and "python" in first_line
            except StopIteration:
                return False

    sub = Popen("git diff --staged --name-only HEAD".split(),
                stdout=PIPE)
    sub.wait()

    # Filter out non-python or deleted files.
    py_files_changed = [file
                        for file in [f.strip() for f in sub.stdout.readlines()]
                        if (file.endswith(".py") and os.path.exists(file))
                            or is_py_script(file)]

    # Run Pylint on each file, collect the results, and display them for the
    # user.
    results = {}
    for file in py_files_changed:
        pylint = Popen(("pylint -f text %s" % file).split(),
                       stdout=PIPE)
        pylint.wait()

        output = pylint.stdout.read()
        print output

        results_re = re.compile(r"Your code has been rated at ([\d\.]+)/10")
        results[file] = float(results_re.findall(output)[0])

    # Display a summary of the results (if any files were checked).
    if len(results.values()) > 0:
        print "==============================================="
        print "Final Results:"
        for file in results:
            result = results[file]
            grade = "FAIL" if result < PYLINT_PASS_THRESHOLD else "pass"
            print "[ %s ] %s: %.2f/10" % (grade, file, result)

    # If any of the files failed the Pylint test, exit nonzero and stop the
    # commit from continuing.
    if any([(result < PYLINT_PASS_THRESHOLD)
            for result in results.values()]):
        print "git: fatal: commit failed, Pylint tests failing."
        sys.exit(0)


@exit_on_error
def clean_print_calls_and_debug_points(all_files):


    modified = re.compile('^(?:M|A)(\s+)(?P<name>.*)')

    CHECKS = [
        {
            'output': 'Checking for pdbs...',
            'command': 'grep -n "import pdb" %s',
            'ignore_files': ['.*pre-commit'],
            'print_filename': True,
            },
        {
            'output': 'Checking for pudbs...',
            'command': 'grep -n "pudb" %s',
            'ignore_files': ['.*pre-commit'],
            'print_filename': True,
            },
        {
            'output': 'Checking for ipdbs...',
            'command': 'grep -n "import ipdb" %s',
            'ignore_files': ['.*pre-commit'],
            'print_filename': True,
            },
        {
            'output': 'Checking for print statements...',
            'command': 'grep -n " print " %s',
            'match_files': ['.*\.py$'],
            'ignore_files': ['.*migrations.*', '.*management/commands.*', '.*manage.py', '.*/scripts/.*'],
            'print_filename': True,
            },
        {
            'output': 'Checking for console.log()...',
            'command': 'grep -n console.log %s',
            'match_files': ['.*yipit/.*\.js$'],
            'print_filename': True,
            },
        ]

    def check_files(files, check, repo_root):

        def matches_file(file_name, match_files):
            return any(re.compile(match_file).match(file_name) for match_file in match_files)

        result = 0
        print check['output']
        for file_name in files:
            if not 'match_files' in check or matches_file(file_name, check['match_files']):
                if not 'ignore_files' in check or not matches_file(file_name, check['ignore_files']):
                    process = subprocess.Popen(check['command'] % (repo_root + '/' + file_name), stdout=subprocess.PIPE, stderr=subprocess.PIPE, shell=True)
                    out, err = process.communicate()
                    if out or err:
                        if check['print_filename']:
                            prefix = '\t%s:' % file_name
                        else:
                            prefix = '\t'
                        output_lines = ['%s%s' % (prefix, line) for line in out.splitlines()]
                        print '\n'.join(output_lines)
                        if err:
                            print err
                        result = 1
        return result

    p = subprocess.Popen(['git', 'rev-parse', '--show-toplevel'], stdout=subprocess.PIPE)
    out, _ = p.communicate()
    repo_root = out.splitlines()[0]
    
    files = []
    if all_files:
        for root, dirs, file_names in os.walk('.'):
            for file_name in file_names:
                files.append(os.path.join(root, file_name))
    else:
        p = subprocess.Popen(['git', 'status', '--porcelain'], stdout=subprocess.PIPE)
        out, err = p.communicate()
        for line in out.splitlines():
            match = modified.match(line)
            if match:
                files.append(match.group('name'))
    
    result = 0
    
    p = subprocess.Popen(['find', repo_root, '-name', 'manage.py'], stdout=subprocess.PIPE)
    out, _ = p.communicate()
    manage = out.splitlines()[0]
    
    print 'Running Django Code Validator...'
    return_code = subprocess.call('$VIRTUAL_ENV/bin/python %s validate' % manage, shell=True)
    result = return_code or result
    
    if result:
        return result
    
    for check in CHECKS:
        result = check_files(files, check, repo_root) or result

    return result

if __name__ == '__main__':
    all_files = False
    if len(sys.argv) > 1 and sys.argv[1] == '--all-files':
        all_files = True
    clean_print_calls_and_debug_points(all_files)
    pylint_check()
    sys.exit(0)

