/*публикация записи на стену ВК
 * data - ответ сервера метода photos.saveWallPhoto, если false публикуется ссылка
 * на страницу.
 * message - текст сообщения.
 */
function VkWallPost(data, message){
	var result = false;
	VK.Api.call('account.getAppPermissions', {}, function(r){ 
		var attachments = location.origin+location.pathname;
		if(data){ attachments = data.response[0].id+','+location.origin+location.pathname; }
		var permissions = r.response;
		if(permissions & 8192){
			VK.Api.call('wall.post', {message: message, 
									  attachments: attachments
									 }, function(r){
				if(r.response){
					console.log(r.response.post_id);
					result = true;
				}
			});
		} else {
			VK.Auth.login(function(){VkWallPost(data);}, 8197);	
		}
	});
	return result;
}
//стандартный widget ВК
function VkWallPostNotApi(){
	window.open('http://vk.com/share.php?url='+location.href, '_blank', 
		'width=800,height=400,resizable=yes,scrollbars=yes,status=yes');
	return false;
}
/*
 * Метод использует плагин jQuery Cookie http://plugins.jquery.com/cookie/
 * Публикация изображений во ВК
 */
function VkWallPostWithImage(imagePath, message){
	var result = false;
	var csrftoken = $.cookie('csrftoken');
	if(VK._apiId === null)
	{
		setTimeout(function(){VkWallPostWithImage(imagePath, message);}, 1);
		return false;
	}
	VK.Api.call('account.getAppPermissions', {}, function(r){
		var permissions = r.response;
		if((permissions & 4)!=0 & (permissions & 8192)!=0){
			if(imagePath == ""){
				result = VkWallPost(false, message);
			} else {
				VK.Api.call('photos.getWallUploadServer', {}, function(r){
					if(!r.response) { return false; }
					else{
						  var upload_url = r.response.upload_url;
						$.post("/soc_post_image/", 
							{
								image: imagePath,
								upload_url: upload_url,
								csrfmiddlewaretoken: csrftoken 
							}, 
							saveWallPhoto, "json");
					}
				});
			}
		} else {
			VK.Auth.login(function(){VkWallPostWithImage(imagePath);}, 8197);
		}
	});
	var saveWallPhoto = function(data){
		console.log(data);
		VK.Api.call('photos.saveWallPhoto', {
			server: data.server,
			hash: data.hash,
			photo: data.photo
		},
		function(r){ result = VkWallPost(r, message); });
	};
	return result;
}

/*
 * Публикация изображения или сообщения на стену во FB
 */

function FbWallPostWithImage(imgURL, message){
	var result = false;
	var loginStatus = null;
	FB.getLoginStatus(function(r){loginStatus = r.status});

	if(loginStatus != 'unknown' && loginStatus != null){
		if(imgURL == ""){
			FB.api('/me/feed', 'post', {
				message: message
			}, function(response){
			
			    if (!response || response.error) {
			        console.log('Error occured');
			    } else {
			        console.log('Post ID: ' + response.id);
			        result = true;
			    }
			});
		}
		else {
			FB.api('/me/photos', 'post', {
			    message:message,
			    url:imgURL        
			}, function(response){
			
			    if (!response || response.error) {
			        console.log('Error occured');
			    } else {
			        console.log('Post ID: ' + response.id);
			        result = true;
			    }
			});
		}	
	} else {
		FB.login(function(response) {
			if(response.status === "connected") {
				FbWallPostWithImage(imgURL, message);
			}
		}, {perms:'read_stream,publish_stream,offline_access,publish_actions,user_photos ', 
			score:'read_stream,publish_stream,offline_access,publish_actions,user_photos '
		   });
	}
	return result;
}
/*
 * Вспомогательные функции
 */
function GetQueryStringParams(sParam){
    var sPageURL = window.location.search.substring(1);
    var sURLVariables = sPageURL.split('&');
    for (var i = 0; i < sURLVariables.length; i++) 
    {
        var sParameterName = sURLVariables[i].split('=');
        if (sParameterName[0] == sParam) 
        {
            return sParameterName[1];
        }
    }
}
var getCallbacks = function(){
	var result = 0;
	var callback = GetQueryStringParams('callback');
	callback = $.parseJSON(decodeURIComponent(callback));
	for(var key in callback){
		console.log(callback[key].function, callback[key].params);
		var fn = window[callback[key].function];
		if(typeof fn === 'function'){
			if(fn(callback[key].params.imagePath, callback[key].params.message)){
				result = result + 1;
			}
		}
	}
};
var clearQueryStringParams = function(){
	if (uri.indexOf("?") > 0) {
	    var clean_uri = uri.substring(0, uri.indexOf("?"));
	    window.history.replaceState({}, document.title, clean_uri);
	}
};
window.onload = getCallbacks;
