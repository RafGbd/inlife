$(function() {

    var options = {
            empty_value: 'null',
            indexed: true,  // the data in tree is indexed by values (ids), not by labels
            on_each_change: '/admin/app/achievement/get_subtree/', // this file will be called with 'id' parameter, JSON data must be returned
            choose: function(level) {
                return 'Choose level ' + level;
            },
            set_value_on: 'each',
            loading_image: '/static/i/ajax-load.gif',
            show_multiple: 10, // if true - will set the size to show all options
            choose: '' // no choose item
        };

        var displayParents = function() {
            var labels = []; // initialize array
            $(this).siblings('select') // find all select
                           .find(':selected') // and their current options
                             .each(function() { labels.push($(this).text()); }); // and add option text to array
            //$('<div>').text(this.value + ':' + labels.join(' > ')).appendTo('#parent-result');  // and display the labels
            }

    $.getJSON('/admin/app/achievement/get_subtree/', function(tree) { // initialize the tree by loading the file first
        $('input[name=parent]').optionTree(tree, options).change(displayParents);
    });
});