$(document).ready(function(){
	window.createMap = false;
	var treeHistory = $.cookie("treeViewHistory");
	if(treeHistory!=undefined){
		treeHistory = $.parseJSON(treeHistory);
		$("#tree_navtab").click();
		for(key in treeHistory){
			//alert(treeHistory[key]);
			$("#"+treeHistory[key]).click();
			$("label[for='"+treeHistory[key]+"']").click();
		}
	}
	window.createMap = true;
	
	if(location.hash !==''){
		$(".nav.nav-pills li a[href='"+location.hash+"']").tab("show");
	} else {
		$(".nav.nav-pills li a:first").tab("show");
	}
	$(".nav.nav-pills li a").on("shown", function(e) {
      location.hash = e.target.hash.substr(1); 
      return false ;
	});
	$(window).bind( 'hashchange', function(e) { 
		if(location.hash !==''){
			$(".nav.nav-pills li a[href='"+location.hash+"']").tab("show");
		} else {
			$(".nav.nav-pills li a:first").tab("show");
		}
	});
});
function sortOnKeys(dict) {
    var sorted = [];
    for(var key in dict) {
        sorted[sorted.length] = key;
    }
    sorted.sort();

    var tempDict = {};
    for(var i = 0; i < sorted.length; i++) {
        tempDict[sorted[i]] = dict[sorted[i]];
    }

    return tempDict;
}
// implement JSON.stringify serialization
JSON.stringify = JSON.stringify || function (obj) {
    var t = typeof (obj);
    if (t != "object" || obj === null) {
        // simple data type
        if (t == "string") obj = '"'+obj+'"';
        return String(obj);
    }
    else {
        // recurse array or object
        var n, v, json = [], arr = (obj && obj.constructor == Array);
        for (n in obj) {
            v = obj[n]; t = typeof(v);
            if (t == "string") v = '"'+v+'"';
            else if (t == "object" && v !== null) v = JSON.stringify(v);
            json.push((arr ? "" : '"' + n + '":') + String(v));
        }
        return (arr ? "[" : "{") + String(json) + (arr ? "]" : "}");
    }
};
function createTreeMap(obj) {
	$.removeCookie('treeViewHistory')
	var $obj = $(obj),
		map = {};
	map[$obj.attr("name")] = $obj.attr("id");
	$obj.parents(".branch").each(function(){
		if($(this).attr("id")!="group1")
		{
			var $this = $(this).prev(".ach");
			map[$this.attr("name")] = $this.attr("id");
		}
	});
	map = sortOnKeys(map);
	$.cookie("treeViewHistory", JSON.stringify(map));
}