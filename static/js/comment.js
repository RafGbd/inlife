function CommentModel(comment, self) {
    this.comment_id = comment['comment_id'];
    this.id = comment['action_id'];
    this.editable = comment['editable'];
    self.user = comment['user'];
    this.user = comment['user'];
    this.date = comment['date'];
    this.comment = comment['comment'];
    var words = this.comment.split(" ")
    this.is_long = words.length > 10;
    this.short_comment = this.is_long ? words.slice(0,10).join(" ") : this.comment;
    this.addComment = self.addComment
    // флаг, показывать ли этот коммент, используется для сворачивания
    // длинной ленты, меняется экшном
    this.visible = ko.observable(true);
    // показывать ли коммент целиком, или только первые десять слов
    this.showFull = ko.observable(!this.is_long);
    this.getComment = function() {
    	return this.comment //this.showFull() ? this.comment : this.short_comment;
    }
    this.show = function() {
	this.showFull(true);
    }
}

function CommentViewModel() {
    var self = this;
    this.actions = ko.observableArray();
    self.submitting = ko.observable(false);
    self.loading = ko.observable(false);
    self.comment_form_url = ko.observable('');

    this.loadActions();
}

CommentViewModel.prototype.addComment = function(form) {
    var self = this;
    var $form = $(form);
    var csrf_token = $form.find('input[name="csrfmiddlewaretoken"]').val();
    var comment = $form.find('textarea[name="comment"]').val();
    var url = $form.attr('action');

    $.ajax({
        type: 'POST',
        url: url,
        data: {
            csrfmiddlewaretoken: csrf_token,
            comment: comment
        },
        dataType: 'json',
        beforeSend: function(){
            self.submitting(true); // включим крутилку
        },
        complete: function(){
            self.submitting(false);
        },
        success: function(response){
            if (response.success) {
                // Добавим комментарий в запись таймлайна, рендеринг нокаут возьмет
                // на себя.
                self.actions.push(new CommentModel(response.data, self));
                $form.find('textarea[name="comment"]').val('');
            }
        }
    });
    return false;
};


/**
 * Загрузить с сервера коменты и наполнить ими массив actions.
 */
CommentViewModel.prototype.loadActions = function(){
    var self = this;

    function updateActions(response) {
        var actions = response['comments'];
	self.comment_form_url = response['comment_form_url'];
	// костыль
	jQuery('#comment_form').attr('action', response['comment_form_url'])
	
        for (var i = 0; i < actions.length; i++) {
            self.actions.push(new CommentModel(actions[i], self));
        }
    }
    $.ajax({
        type: 'GET',
        url: (location.pathname + '/data/').replace('//', '/'),
        dataType: 'text',
        cache: false,
        beforeSend: function(){
            self.loading(true);
        },
        complete: function(){
            self.loading(false);
        },
        success: function(response) {
            updateActions(jQuery.parseJSON(response));
        }
    });
};
