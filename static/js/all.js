$(document).ready(function(){
	$("[data-toggle='tooltip']").tooltip();
	$(".btn-tooltip").tooltip();
	
	$('.carousel').carousel();

	function change_landing_tab(e) {
		e.preventDefault();
		$('.main_tabs a').removeClass('current');

		$(this).addClass('current');
		var backImage = $(this).attr('href');
		var buttonText = $(this).attr('button-text');
		$('.landing_image').css('background-image', 'url('+backImage+')');
    }

    $('.main_tabs a').click(change_landing_tab);
    $('.main_tabs a').hover(change_landing_tab);
    
	$('.nav-pills a, .nav-tabs a').click(function (e) {
		e.preventDefault();
		$(this).tab('show');
	});
	$('.likeSend').click(function(e) {
		e.preventDefault();
		var like;
		var block = $(this);
		if (block.children().children().attr('class') == 'i_like') {
			like = 'like';
		} else {
			like = 'unlike';
		}
		$.get('/achievements/'+block.attr('id')+'/'+like+'/', function(data) {
			block.html(data);
		});
	});
	
	$('.achiv_tree label').click(function (e) {
		var thisId = $(this).parent().attr('id');
		$('#'+thisId + ' .leaf').removeClass('current');
		$(this).addClass('current');
	});
	
	$(function() {
		$(".knob").knob();
	});
	
	$('.timeline_comments .icon-remove-sign').click(function(e) {
		$(this).parent().remove();
	});

	$('[rel=href] button, [rel=href] a').click(function(event){
		$( $(this).attr('href') ).modal('show');
		event.stopPropagation ? event.stopPropagation() : (event.cancelBubble=true);
	});
	
	$('[rel=href]').click(function(){
		location.href = $(this).data('href');
	});
	
	// intro
	$('#intro-1').modal('show');
	

	// $('#intro-1 .btn-primary').click(function(){
	// 	$('#intro-1').modal('hide');
	// 	$('#intro-2').modal('show');
	// });
	
	$('#intro-2 .btn-primary').click(function(){
		$('#intro-2').modal('hide');
	});
	$('.instagram-modal .list-inline li'). click(function(){
		$(this).toggleClass('active');
	});
	$('#register_button').click(function(e){
		e.preventDefault()
		$(document).scrollTop( $("#reg-form").offset().top );
		$('#id_email').focus();
	});
});