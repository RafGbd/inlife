ko.observableArray.fn.pushAll = function(valuesToPush) {
    var underlyingArray = this();
    this.valueWillMutate();
    ko.utils.arrayPushAll(underlyingArray, valuesToPush);
    this.valueHasMutated();
    return this;
};

function DiscussionViewModel(discussion) {
    var self = this;

    self.submitting = ko.observable(false);
    self.replies = ko.observableArray();
    self.key = location.pathname;

    self.updateFromCache();

    this.updater = function() {
        date = self.last_reply_time();
        self.update_data(date);
    };

    setInterval(this.updater, 2000);
}

DiscussionViewModel.prototype.updateLocalCache = function (data) {
    var localObj, self = this;
    if (localStorage.hasOwnProperty(self.key)) {
	localObj = JSON.parse(localStorage.getItem(self.key));
	Array.prototype.push.apply(localObj.localArray, data);
    } else {
	localObj = {};
	localObj.localArray = data;
    }
    localStorage.setItem(self.key, JSON.stringify(localObj));
}

DiscussionViewModel.prototype.updateFromCache = function () {
    var self = this;
    if (localStorage.hasOwnProperty(self.key)) {
	localObj = JSON.parse(localStorage.getItem(self.key));
	var time = localObj.localArray[localObj.localArray.length-1].sent_at;

        for (var i = 0; i < localObj.length; i++) {
            self.replies.push(
                new ReplyViewModel(data.replies[i], self)
            );
        }
	this.update_data(time);
    } else {
	this.update_data();
    }
}

DiscussionViewModel.prototype.update_data = function(date){
    var self = this;
    request_params = {}
    
    if (date) {
        request_params['date'] = date;
    }
    
    $.getJSON((location.pathname + '/data/').replace('//', '/'), request_params)
    .done(function(data) {
        if (data) {
            if (data.replies.length) {
		self.updateLocalCache(data.replies);
		console.dir(data.replies);
                for (var i = 0; i < data.replies.length; i++) {
                    self.replies.push(
                        new ReplyViewModel(data.replies[i], self)
                    );
                }
            }
        }
    });
};

DiscussionViewModel.prototype.last_reply_time = function (){
    var self = this;
    var replies = self.replies();
    var last_reply = replies[replies.length-1];
    if (last_reply)
        return last_reply.date
}


DiscussionViewModel.prototype.addReply = function(form) {
    var self = this;
    var $form = $(form);
    var csrf_token = $form.find('input[name="csrfmiddlewaretoken"]').val();
    var message = $form.find('textarea[name="message"]').val();
    var url = $form.attr('action');

    if (message) {
        $.ajax({
            type: 'POST',
            url: url,
            data: {
                csrfmiddlewaretoken: csrf_token,
                message: message
            },
            dataType: 'json',
            beforeSend: function(){
                self.submitting(true); // включим крутилку
            },
            complete: function(){
                self.submitting(false);
            },
            success: function(response){
                self.updater()
                // Добавим комментарий в запись таймлайна, рендеринг нокаут возьмет
                // на себя.
                $form.find('textarea[name="message"]').val('');
            }
        });
    }
    return false;
};


function ReplyViewModel(reply, discussion) {
    var self = this;
    self.discussion = discussion;

    self.author = reply.author;
    self.date = reply.date;
    self.date_repr = reply.date_repr;
    self.message = reply.message;
}