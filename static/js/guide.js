/**
 * Created by katya on 13.01.14.
 */
$(document).ready(function () {
    $('.guide_button').click(function (e) {
        e.preventDefault();
        var tag = $(this);
        $.get('/next_step/?user_id=' + tag.attr('user_id') + '&step='+ tag.data('step'), function (data) {
            console.log(data);
            $('.guide_continue').hide();

            if (data.url) {
                window.location = data.url;
            }
//            if (data.character_bar) {
//                $('.main_section :first').html('стрелка')
//            }
            if (data.step) {
                var step = parseInt(data.step);
                if (step===3) {
                    $('.blank').addClass('intro intro-1');
                    $('.timeline:eq(2)').addClass('intro intro-2');
                    $('.timeline:eq(3)').addClass('intro intro-3');
                } else if (step===4) {
                    $('.pie_wrap:eq(0)').addClass('intro intro-6');
                    $('.intro-7').show();
                    $('.intro-8').show();
                    $('.derivative-page-text').addClass('intro intro-9');

                } else if (step===5) {
                    $('.collect_achievments:eq(1)').addClass('intro intro-4')
                } else if (step===6) {
                    $('.col-xs-3:first').addClass('intro intro-5')
                }

            }
        });
        $('#intro-1').modal('hide');
    });
    $('.guide_close .close').click(function (e) {
        e.preventDefault();
        var tag = $(this);
        $.get('/guide_close/?user_id=' + tag.attr('user_id'), function (data) {
            console.log(data);
        });
    });
    $('.guide_continue').click(function (e) {
        e.preventDefault();
        var tag = $(this);
        window.location = tag.attr('url');
    });
});