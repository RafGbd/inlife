var engine = {
    posts : [],
    target : null,
    busy : false,
    render : function(obj){
	return "<li class='secondphoto' ><img data-url='" + obj[0] + "' src='" + obj[1] + "'/><div class='check icon-ok'></div></li>";
    },
    init : function(posts, target) {
	if (!target) { return; }
	this.target = $(target);
	this.append(posts);
	var that = this;
	$(this.target).scroll(function() {
	    if ($(document).height() - $(window).height() <= $(window).scrollTop()) {
		that.scrollPosition = $(window).scrollTop();
		that.get();
	    }
	});
    },
    append : function(posts){
	posts = (posts instanceof Array) ? posts : [];
	for (var i=0; i<posts.length; i++) {
	    this.target.append(this.render(posts[i]));
	}
 	if (this.scrollPosition !== undefined && this.scrollPosition !== null) {
	    $(window).scrollTop(this.scrollPosition);
	}
    },
    get : function() {
	var max_id = $("#max_id").text(),
	access_token = $("#access_token").text();
	if (!this.target || this.busy) return;
	this.setBusy(true);
	var that = this;
	if(max_id != "not") {
	    $.getJSON('/nextphotos/', { "max_id": max_id, "access_token": access_token },
		      function(data){
			  that.append(data.photos);
			  $("#max_id").text(data.max_id)
			  that.setBusy(false);
		      }
		     );
	}
    },
    showLoading : function(bState){
	var loading = $('#loading');
	if (bState) {
	    $(this.target).append(loading);
	    loading.show('slow');
	} else {
	    $('#loading').hide();
	}
    },
    setBusy : function(bState){ this.showLoading(this.busy = bState); }
};
