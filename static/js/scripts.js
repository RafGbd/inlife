/**
 * Created by katya on 16.01.14.
 */
$(document).ready(function() {
    $('.send-message').click(function() {
        $('i.icon-user').html(' ' + $(this).data('user_name'));
        $('#id_recipient').val($(this).data('id'));
        $('#myModal form').attr('action', $(this).data('href'));
    });
    $('.add-to-friend').click(function(e) {
        e.preventDefault();
        var tag = $(this);
        $.ajax({
            type: "POST",
            url: "/friends/action/",
            data: {action:"add", id: tag.data('id')}
        }).done(function() {
                console.log('success add to friend');
                tag.replaceWith('<span class="aside-status">Запрос отправлен</span>')
            });
    });
});