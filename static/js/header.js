/* Количества - в меню */

function HeaderActionModel(action) {
    var self = this;
    self.count_class = action.class
    self.title = action.title
    self.header_count = action.count
    self.url = action.url
    self.active = ko.computed(function() {
	return location.pathname == self.url ? 'count-li active' : ''
    });
}


function HeaderViewModel() {
    var self = this;

    self.counters = ko.observableArray();

    self.updateActions = function(actions) {
	self.counters.removeAll();
        for (var i = 0; i < actions.length; i++) {
	    self.counters.push(new HeaderActionModel(actions[i]));
        };
    };

    self.loadActions = function () {
	$.get("/notifications/counts/", {}, self.updateActions);
    };

    self.dir_log = function(into_ob) {
	console.dir(info_ob);
    };

    self.loadActions();
    setInterval(self.loadActions, 20000);
}


$(document).ready(function() {
    var hvm = new HeaderViewModel();
    ko.applyBindings(hvm, document.getElementById('header_counts'));
});
