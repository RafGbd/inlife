var Notify = {},
    n_id = 0;


function showNotification(notification, i){
    var theme = notification.theme,
        link = notification.link;

    Notify["notification" + n_id] = new Notification(theme, notification);
    Notify["notification" + n_id].onclick = function() { document.location.href=link };
    n_id++;
}


function checkNotification(){
    var timeout = 5000;

    $.getJSON( "/notifications/check/", function( r ) {
        if (r.notifications.length){
            for (var i = 0; i < r.notifications.length; i++) {
                showNotification(r.notifications[i], i+1);  
            };
        }
       
    });
    setTimeout(checkNotification, timeout);
}


function start_checking(){
    if ( arguments.callee._is_called ){
        return null;
    }
    console.log('_is_not_called');
    arguments.callee._is_called = true;
    
    checkNotification();
}

function getCurrentPermission() {
    Notification.requestPermission(
        function(result) {
            console.log(result);
            if (result.toLowerCase() == "granted"){
                start_checking();
            }
            $('*').unbind('mousemove keydown scroll click', getCurrentPermission);
        }
    );
    
};

(function ($) {
    $(document).ready(function () {
        $('*').bind('mousemove keydown scroll click', getCurrentPermission);
        //только на событие пользователя
    });
}) (jQuery)
