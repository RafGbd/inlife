var upload_counter = 1,
    type_obj = {'image/png': true,
		'image/jpeg': true,
		'image/jpg': true};

    function max_photo_formset_id() {
        var id = -1;
        $('.proof_photo_form').each(function() {
            var id_attr = $( this ).attr('id');
            var this_id = parseInt(id_attr.split('_')[2]);
            if (this_id > id) 
                id = this_id;
        })
        return id
    };

    var selectPhoto = function(e, el) {

	// Проверка если формат не укан в type_obj то мы фото никуда не шлём
	if(!(el.type in type_obj || typeof(el.type) == 'undefined')) { return false; }

        var upload_id = upload_counter;
        upload_counter += 1;
        var fd = new FormData();
	try {
	    fd.append('photo', el.dataset.url)
	} catch(except) {
	    fd.append('photo', el)		
	}

        fd.append('csrfmiddlewaretoken', $('[name=csrfmiddlewaretoken]').first().val())
        $.ajax({
          type: 'POST',
          xhr: function() {
              myXhr = $.ajaxSettings.xhr();
              if(myXhr.upload){
                myXhr.upload.addEventListener('progress', function(prog) {
                  var value = ~~((prog.loaded / prog.total) * 100) * 0.8; //хак из-за замирания на 95%
                  $('li.temp.'+upload_id).find('.progress-bar.progress-bar-info').css('width', value + '%');
                }, false);
              }
              return myXhr;
          },
          url: proof_photo_upload_url,
          data: fd,
          processData: false,
          contentType: false,
          dataType: "json",
          beforeSend: function() {

            var reader = new FileReader();
              reader.onload = function (e) {
              var img_src = e.target.result;
              var $ul = $('ul.photo_achiv.unstyled'),
                  $li = $('<li>', {'class': "temp " + upload_id}),
                  $img = $('<img>', {
                    'class': "hint",
                    'src': img_src,
                    'data-placement': "bottom",
                    'data-toggle': "tooltip"
                  }),
                  $a = $('<a>', {'class': "icon-remove", 'href':"#"});
                  $div = $('<div>', {'class': "in-progress"}),
                  $progress_box = $('<div>', {'class': "progress-box"}),
                  $progress_bar_wrap = $('<div>', {'class': "progress progress-striped"}),
                  $progress_bar = $("<div>", {
                                        'class': "progress-bar progress-bar-info",
                                        'style': "width: 0%",
                                        'aria-valuemax': "100",
                                        'aria-valuemin': "0",
                                        'aria-valuenow': "40",
                                        'role':"progressbar"
                                      });
              $progress_bar.appendTo($progress_bar_wrap);
              $progress_bar_wrap.appendTo($progress_box);

              $img.appendTo($div);
              $progress_box.appendTo($div);
              $div.appendTo($li);
              $li.appendTo($ul);
            }
              if(typeof(el.dataset) == 'undefined') {
                  reader.readAsDataURL(el);
              }
          },
          success: function(response) {
             $('li.temp.'+upload_id).remove();
             if ( response.success && response.photo ) {
		 
		 $('.not-first-condition').removeClass('hide');
		 $('.first-condition').addClass('hide');

                var has_active_img = $('ul.photo_achiv.unstyled li.current').length;

                var photo = response.photo,
                    $ul = $('ul.photo_achiv.unstyled'),
                    $div = $('<div>'),
                    $li = $('<li>'),
                    $a = $('<a>', {'class': "icon-remove", 'href':"#"});
                    new_form_counter = max_photo_formset_id() + 1,
                    new_form_id = "photo_form_" + new_form_counter;
                  
                var $img = $('<img>', {
                  'src': photo.attrs['data-thumbnail-url'],
                  'form-id': '#' + new_form_id,
                  'class': "hint",
                  'width': 90,
                  'height': 60,
                  'data-placement': "bottom",
                  'data-title': "Сделать обложкой",
                  'data-index': 0
                });
                // creating thumb
                $a.appendTo($div);
                $img.appendTo($div);
                $div.appendTo($li);
                $li.appendTo($ul);

                //creating form
                var $formset_container = $('#photo_formsets_container');
                var $div_form_wrapper = $('<div>', {
                  'id': new_form_id,
                  'class': "proof_photo_form",
                  'style': "display: none;"
                });
                var $proof_id_input = $('<input>', {
                  'id': "id_form-" + new_form_counter + "-id",
                  'type': "hidden",
                  'value': photo.id,
                  'name': "form-" + new_form_counter  + "-id"
                });
                $proof_id_input.appendTo($div_form_wrapper);

                var $delete_input = $('<input>', {
                  'id': "id_form-" + new_form_counter + "-DELETE",
                  'type': "checkbox",
                  'name': "form-" + new_form_counter + "-DELETE"
                });
                var $delete_input_wrap = $('<div>', {
                  'class': "delete_input_wrap",
                  'style': "display: none;"
                });
                $delete_input.appendTo($delete_input_wrap);
                $delete_input_wrap.appendTo($div_form_wrapper);

                var $position_input = $('<input>', {
                  'id': "id_form-"+ new_form_counter +"-position",
                  'type': "hidden",
                  'value': "10",
                  'name': "form-"+ new_form_counter +"-position"
                });
                var $position_input_wrap = $('<div>', {
                  'class': "position_input_wrap",
                  'style': "display: none;"
                });
                $position_input.appendTo($position_input_wrap);
                $position_input_wrap.appendTo($div_form_wrapper);
                var $photo_input_div_wrap = $('<div>', {
                  'style': "display: none;"
                });
                var $photo_input_p_wrap = $('<p>', {'class':"file-upload"});
                var $photo_input = $('<input>', {
                  'type': "file",
                  'id': "id_form-" + new_form_counter + "-photo",
                  'class': "crop-thumb",
                  'name': "form-" + new_form_counter + "-photo",
                  'data-thumbnail-url': photo.attrs['data-thumbnail-url'],
                  'data-org-width': photo.attrs['data-org-width'],
                  'data-org-height': photo.attrs['data-org-height'],
                  'data-field-name': "form-" + new_form_counter + "-photo",
                });
                $photo_input.appendTo($photo_input_p_wrap);
                $photo_input_p_wrap.appendTo($photo_input_div_wrap);
                $photo_input_div_wrap.appendTo($div_form_wrapper);


                var $cropping_input = $('<input>', {
                  'id': "id_form-" + new_form_counter + "-cropping",
                  'class': "image-ratio",
                  'type': "text",
                  'name': "form-" + new_form_counter + "-cropping",
                  'maxlength': "255",
                  'data-width': "69",
                  'data-size-warning': "true",
                  'data-my-name': "cropping",
                  'data-image-field': "photo",
                  'data-height': "29",
                  'data-allow-fullsize': "false",
                  'data-adapt-rotation': "false"
                });

                $cropping_input.appendTo($div_form_wrapper);

                $div_form_wrapper.appendTo($formset_container);

                var $initial_forms = $('input[id^=id_][id$=-INITIAL_FORMS]'),
                    $total_forms = $('input[id^=id_][id$=-TOTAL_FORMS]');
                $initial_forms.val( parseInt($initial_forms.val()) + 1 );
                $total_forms.val( parseInt($total_forms.val()) + 1 );

                 image_cropping.init(true);

                if (!has_active_img) $img.trigger('click');
             }
          },
          error: function(response) {
            console.log(response);
          }
        });
    };

  $(document).on('change', "#id_select_photo", function(e) {
      for(i=0;i < this.files.length;i++) {
	  selectPhoto(i, this.files[i]);
      }
  });
  $(document).on('click', "#instagram_image", function() {
      $("#instagram-modal .active img").each(selectPhoto);
  });

  $('ul.photo_achiv.unstyled').on('click', 'img', function(event){
      event.preventDefault();
      var $img = $(this),
          $proof_photo_form = $( $img.attr('form-id') );
      $('.proof_photo_form').hide();
      $proof_photo_form.show();
      console.log($proof_photo_form);
      
      //setting selected image as first in order
      $('input[id^=id_][id$=-position]').each(function() {
        $(this).val(10);
      });
      var $position_input = $proof_photo_form.find('input[id^=id_][id$=-position]');
      console.log($position_input);
      $position_input.val(0);
      //
      
      $('ul.photo_achiv.unstyled li').removeClass('current');
      $img.closest( "li" ).addClass('current');
  });
    
  $(document).on('click', 'a.icon-remove', function(event){
      event.preventDefault();
      var $current_li = $('ul.photo_achiv.unstyled li.current'),
          $this = $(this),
          $wrapper = $this.parent(),
          $this_li = $wrapper.parent(),
          $img = $wrapper.find('img'),
          $proof_photo_form = $( $img.attr('form-id') ),
          $input_wrapper = $proof_photo_form.find('.delete_input_wrap'),
          $delete_input = $input_wrapper.find('input:checkbox'),
          $photo_id = $('#id_form-' + $delete_input.attr('id').split('-')[1] + '-id').val();
          $photo_div = $('#photo_form_-' + $delete_input.attr('id').split('-')[1]);

      // $.ajax({
      //     type: 'POST',
      //     url: '/confirm/delete_temp_proof_photo/' + $photo_id + '/',
      //     data: {'csrfmiddlewaretoken': $('[name=csrfmiddlewaretoken]').first().val()},
      //     dataType: "json",
      // 	  success: function(response) {
      // 	      if(response.status = 'ok') {
		  $proof_photo_form.hide();
		  // $photo_div.remove();
		  if ($current_li.is($this_li) || ($current_li.length == 0 )){
		      $this_li.remove();
		      $current_li = $('ul.photo_achiv.unstyled li').first();
		  }
		  $img_to_click = $current_li.find('img');
		  $this_li.remove();
		  $delete_input.prop('checked', true);
		  $wrapper.remove();

                  // var $initial_forms = $('input[id^=id_][id$=-INITIAL_FORMS]'),
                  //     $total_forms = $('input[id^=id_][id$=-TOTAL_FORMS]');
                  // $initial_forms.val( parseInt($initial_forms.val()) - 1 );
                  // $total_forms.val( parseInt($total_forms.val()) - 1 );

		  if($img_to_click.length > 0) {
		      $img_to_click.trigger('click');
		  }
	      // }
	      if($('.photo_achiv li').length == 0) {
		  $('.not-first-condition').addClass('hide');
		  $('.first-condition').removeClass('hide');
	      }
      // 	  },
      //     error: function(response) { console.log(response); }
      // })

      return false
  });
  
  $('ul.photo_achiv.unstyled li img').first().trigger('click');
