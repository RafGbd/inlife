var DropFunc = function() {
    var dropZone = $('.dropZone')

    dropZone[0].ondragover = function() {
	dropZone.addClass('hover');
	return false;
    };
    
    dropZone[0].ondragleave = function() {
	dropZone.removeClass('hover');
	return false;
    };

    dropZone[0].ondrop = function(e) {
	e.preventDefault();
	var files = e.dataTransfer.files;
	for(i=0;i < files.length;i++) {
	    selectPhoto(i, files[i]);
	}
    }
}
