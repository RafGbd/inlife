ko.bindingHandlers.scroll = {

  updating: true,

  init: function(element, valueAccessor, allBindingsAccessor) {
      var self = this
      self.updating = true;
      ko.utils.domNodeDisposal.addDisposeCallback(element, function() {
            $(window).off("scroll.ko.scrollHandler")
            self.updating = false
      });
  },

  update: function(element, valueAccessor, allBindingsAccessor){
    var props = allBindingsAccessor().scrollOptions
    var offset = props.offset ? props.offset : "0"
    var loadFunc = props.loadFunc
    var load = ko.utils.unwrapObservable(valueAccessor());
    var self = this;

    if(load){
      element.style.display = "";
      $(window).on("scroll.ko.scrollHandler", function(){
        if(($(document).height() - offset <= $(window).height() + $(window).scrollTop())){
          if(self.updating){
            loadFunc()
            self.updating = false;
          }
        }
        else{
          self.updating = true;
        }
      });
    }
    else{
        element.style.display = "none";
        $(window).off("scroll.ko.scrollHandler")
        self.updating = false
    }
  }
 }

/**
 * Таймлайн.
 */

/**
 * Достижение - строка в таймлайне
 */
function TimelineActionModel(action, tab) {
    var self = this;
    self.tab = tab; // parent TimelineTabModel

    self.id = action.id;
    self.name = action.name;
    self.date = action.date;
    self.timestamp = action.timestamp;
    self.user = action.user;
    self.url = action.url;
    self.image = action.image;
    self.action = action.action;
    self.span_class = action.span_class;
    self.icon_class = action.icon_class;
    self.stars_number = action.stars_number;
    self.timeline_pic_class = action.timeline_pic_class;
    self.proof_annotation = action.proof_annotation;
    self.status = action.status;
    self.achievemnt_id = action.achievemnt_id;
    self.likes = ko.observable(action.likes);
    self.commenturl = self.url + '#comments_tab';
    self.comment_form_url = action.comment_form_url;
    self.comments_count = action.comments_count;
    self.accept_choice = ko.observable(action.accept_choice);
    self.is_hidden = ko.observable(action.is_hidden);
    self.tabs = ko.observableArray(action.tabs);
    self.has_comments = action.has_comments;
    self.like_url = action.like_url;
    self.unlike_url = action.unlike_url;
    self.is_event = action.is_event;
    self.event_remaining_time = action.event_remaining_time;

    // флаг включает крутилку загрузки при отправке комментария
    self.submitting = ko.observable(false);

    // флаг показывает, нужно ли выводить все комментарии
    self.showAllComments = ko.observable(false);

    // текст ссылки (чтобы ее скрыть, присвойте пустую строку)
    self.toggleCommentsLink = ko.observable('');

    self.comments = ko.observableArray();
    for (var i = 0; i < action.comments.length; i++) {
        self.comments.push(
            new TimelineCommentModel(action.comments[i])
        );
    }
    self.frameComments(); // вызовем сами в первый раз
    // а дальше автоматически при изменении
    // бинд нужен, т. к. иногда нокаут вызывает ф-ю в контексте непонятно чего
    self.comments.subscribe(self.frameComments.bind(self));

    // флаг того, что пользователю, просматривающему таймлайн,
    // нравится это достижение
    self.iLike = ko.observable(action.likes.classes.indexOf('red') !== -1);
}

/**
 * Обработчик формы добавления комментария.
 */
TimelineActionModel.prototype.addComment = function (form) {
    var self = this;
    var $form = $(form);
    var csrf_token = $form.find('input[name="csrfmiddlewaretoken"]').val();
    var comment = $form.find('input[name="comment"]').val();
    var url = $form.attr('action');

    $.ajax({
        type: 'POST',
        url: url,
        data: {
            csrfmiddlewaretoken: csrf_token,
            comment: comment
        },
        dataType: 'json',
        beforeSend: function () {
            self.submitting(true); // включим крутилку
        },
        complete: function () {
            self.submitting(false);
        },
        success: function (response) {
            if (response.success) {
                var data = response.data;

                // Добавим комментарий в запись таймлайна, рендеринг нокаут возьмет
                // на себя.
                self.comments.push(new TimelineCommentModel(data));
                $form.find('input[name="comment"]').val('');
            }
        }
    });

    return false;
};

/**
 * Реализует функции Показать, Скрыть предыдущие комментарии.
 *
 * Меняет флаг visible у каждого из комментов. Если стоит флаг showAllComments
 * у экшна, показывает все комментарии и меняет надпись toggleCommentsLink
 * на нужную.
 *
 * Вызывается в контексте Action
 */
TimelineActionModel.prototype.frameComments = function () {
    var self = this;
    var comments = self.comments();
    var commentsLimit = self.tab.timeline.numComments;
    var showAll = self.showAllComments();

    if (comments.length > commentsLimit) {
        self.toggleCommentsLink(
            showAll ? 'Скрыть предыдущие' : 'Показать предыдущие'
        );
    }
    else {
        self.toggleCommentsLink('');
    }

    // Покажем или последине ИКС с конца или все, если флаг showAll
    for (var i = comments.length - 1, k = 0; i >= 0; i--, k++) {
        comments[i].visible(k < commentsLimit || showAll);
    }
};

/**
 * Обработчик ссылки Показать / Скрыть комменты
 */
TimelineActionModel.prototype.toggleComments = function () {
    this.showAllComments(!this.showAllComments());
    this.frameComments();
};

/**
 * Лайк
 */
TimelineActionModel.prototype.toggleLike = function () {
    var self = this;
    var action = this.iLike() ? 'unlike' : 'like';
    var url = this.iLike() ? self.unlike_url : self.like_url;
    console.log(url);
    $.get(url);

    // красное сердечко
    this.iLike(!this.iLike());

    // и коррекция числа
    var likes = this.likes();
    action == 'like' ? likes['count']++ : likes['count']--;
    this.likes(likes);
};

/**
 * Удалить комментарий
 *
 * Вызывается в контексте Экшна для легкого доступа к массиву comments.
 * Можно было бы сделать функцией самого комментария, но так проще.
 */
TimelineActionModel.prototype.removeComment = function (comment) {
    var id = comment.comment_id.split('_')[1];
    $.get('/comment/delete/ach/' + id + '/');

    // удалим комментарий из массива comments
    this.comments.remove(comment);
    // в кеше лежит версия с неудаленным экшном
    this.clearLocalCache();
};

/**
 * Показать, скрыть свое достижение из таймлайна.
 */
TimelineActionModel.prototype.toggleHide = function () {
    var self = this;
    var action = self.is_hidden() ? 'show' : 'hide';
    $.get('/actions/' + self.id + '/' + action + '/', function () {
        self.is_hidden(!self.is_hidden());
    });
};

/**
 * Удалить достижение из таймлайна
 */
TimelineActionModel.prototype._delete = function () {
    var self = this;
    if (!confirm('Вы уверены, что хотите удалить запись?')) {
        return;
    }

    var csrf_token = $('input[name="csrfmiddlewaretoken"]').first().val();

    $.post('/achievements/' + self.id + '/delete/',
        {csrfmiddlewaretoken: csrf_token},
        function (response) {
            if (response.success) {
                self.timeline.actions.remove(self);
                self.timeline.clearLocalCache();
            }
            else {
                alert(response.message);
            }

        });
};

/**
 * Поделиться
 */
TimelineActionModel.prototype.share_vk = function () {
    /*
    отдельная функция для шаринга в вк, тк
    "для предотвращения блокировки popup-окна, она должна вызываться в обработчике пользовательского события"
    */
    var self = this;
    function wk_post_wrap () {
        VKPostAchievement(self.achievemnt_id, self.user.id);
    }
    VK.Auth.getLoginStatus( function(response) {
      if (response.session) {
        VK.Api.call('account.getAppPermissions', {}, function(r){
            var permissions = r.response;
            if ( (permissions & 4) !=0 & (permissions & 8192) != 0) {
                console.log("has permissions");
                wk_post_wrap();
            } else {
                console.log("no permissions");
                //self.tyed_to_login = true;
                VK.Auth.login(wk_post_wrap, 8197);
            }
        });
      } else {
        VK.Auth.login(wk_post_wrap, 8197);
      } 
    });

}
TimelineActionModel.prototype.share = function (method) {
    var self = this;
    var ach_url = encodeURIComponent(location.origin + (
        this.url.charAt(0) == '/' ? this.url : ('/' + this.url)));

    if (method == 'facebook') {
        window.open(
            'https://www.facebook.com/sharer/sharer.php?u=' + ach_url,
            'facebook-share-dialog',
            'width=626,height=436');
    }
    else if (method == 'twitter') {
        window.open('https://twitter.com/share?url=' + ach_url,
            '',
            'width=626,height=436');
    }
    else if (method == 'google') {
        window.open('https://plus.google.com/share?url=' + ach_url, '',
            'menubar=no,toolbar=no,resizable=yes,scrollbars=yes,height=600,width=600');
    }
};

/**
 * Принять достижение добавленное админом
 */
TimelineActionModel.prototype._accept = function () {
    var self = this;
    self.accept_choice(false);
    
    var csrf_token = $('input[name="csrfmiddlewaretoken"]').first().val();

    $.post('/achievements/' + self.id + '/accept/',
        {csrfmiddlewaretoken: csrf_token},
        function (response) {
            if (response.success) {
                self.timeline.clearLocalCache();
            }
            else {
                alert(response.message);
                self.accept_choice(true);
            }

        });
};

/**
 * Отклонить достижение добавленное админом
 */
 // то же самое что и _delete
 TimelineActionModel.prototype._decline = function() {
 var self = this;
 console.log(self);
 console.log("_decline");
 };
/**
 * Удалить достижение со стены группы
 */
TimelineActionModel.prototype._delete_from_wall = function () {
    var self = this;
    var csrf_token = $('input[name="csrfmiddlewaretoken"]').first().val();
    if (!confirm('Вы уверены, что хотите удалить запись?')) {
        return;
    }
    url = (location.pathname + '/achievements/' + self.id + '/delete/').replace('//', '/');
    $.post(url,
        {csrfmiddlewaretoken: csrf_token},
        function (response) {
            if (response.success) {
                self.timeline.actions.remove(self);
                self.timeline.clearLocalCache();
            }
            else {
                alert(response.message);
            }

        });
};

/**
 * Комментарий к достижению.
 */
function TimelineCommentModel(comment) {
    var self = this;
    self.comment_id = comment['comment_id'];
    self.editable = comment['editable'];
    self.user = comment['user'];
    self.comment = comment['comment'];
    var words = self.comment.split(" ")
    self.is_long = words.length > 10;
    self.short_comment = self.is_long ? words.slice(0, 10).join(" ") : self.comment;

    // флаг, показывать ли этот коммент, используется для сворачивания
    // длинной ленты, меняется экшном
    self.visible = ko.observable(true);

    // показывать ли коммент целиком, или только первые десять слов
    self.showFull = ko.observable(!self.is_long);

    self.getComment = function () {
        return self.showFull() ? self.comment : self.short_comment;
    }

    self.show = function () {
        self.showFull(true);
    }
}

/**
 * Главный класс таймлайна.
 *
 * Таймлайн работает с достижениями, выводит их в табах.
 */
function TimelineTabModel(tab, timeline) {
    var self = this;

    this.timeline = timeline;
    this.name = tab.tab_name;
    this.tab_id = tab.tab_id;
    this.next = tab.next;

    this.isSelected = ko.computed(function() {
       return this === timeline.selectedTab();  
    }, this);

    var tab_actions = []
    for (var i = 0; i < tab.items.length; i++) {
        tab_actions.push( new TimelineActionModel(tab.items[i], self) );
    };

    this.actions = ko.observableArray(tab_actions);

}

TimelineTabModel.prototype.updateTab = function(tab) {
    var self = this;
    self.next = tab.next;

    var tab_actions = []
    for (var i = 0; i < tab.items.length; i++) {
        tab_actions.push( new TimelineActionModel(tab.items[i], self) );
    };
    
    ko.utils.arrayPushAll(self.actions(), tab_actions);
    self.actions.valueHasMutated();
}

TimelineTabModel.prototype.loadMore = function() {
    var self = this;

    if (self.next) {
        $.ajax({
            type: 'GET',
            url: self.next,
            context: self,
            dataType: 'json',
            cache: false,
            beforeSend: function () {
                self.timeline.loading(true);
            },
            complete: function () {
                self.timeline.loading(false);
            },
            success: function (response) {
                self.updateTab(response[0]);
            }
        });

    }
}

/**
 * Главный класс таймлайна.
 *
 * Таймлайн работает с достижениями, выводит их в табах.
 */
function TimelineViewModel(userIsOwner, numComments, isGuide) {
    var self = this;
    
    self.selectedTab = ko.observable();

    self.tabs = ko.observableArray();

    this.ativeTab = ko.computed(function() {
        var selectedTab = self.selectedTab();
        if (selectedTab)
            return selectedTab
        return {}   
    }, this);
    
    self.like = '';
    self.loading = ko.observable(false);
    self.isGuide = ko.observable(isGuide);

    // просматривающий пользователь - владелец таймлайна
    self.isowner = userIsOwner;

    self.loadTimelineData();

    self.scrolled = function(){
        var selectedTab = self.selectedTab();
        if (selectedTab)
            selectedTab.loadMore(); 
    };
}

TimelineViewModel.prototype.loadTimelineData = function (){
    var self = this;

    var cacheKey = 'tl_actions_' + document.location.host + document.location.pathname;
    
    function localStorageAvail() {
        try {
            return 'localStorage' in window && window['localStorage'] !== null;
        } catch (e) {
            return false;
        }
    };

    function updateTimelineData(data){
        var resp_tabs = [];
        $.each(data, function( i, tab ) {
            resp_tabs.push( new TimelineTabModel(tab, self) );
        });
        self.tabs.removeAll() 
        ko.utils.arrayPushAll(self.tabs(), resp_tabs);
        self.tabs.valueHasMutated();
        self.selectedTab(self.tabs()[0]);
    };

    function updateLocalCahce(data){
        if (localStorageAvail()) {
            try {
                localStorage.removeItem(cacheKey);
                localStorage.setItem(cacheKey, JSON.stringify(data));
            }
            catch (e) {
                console.log(e);
            }
        }
    };

    function getLocalCahce(){
        if (localStorageAvail()) {
            try {
                cached_data = JSON.parse(localStorage.getItem(cacheKey));
            } catch(e) {
                cached_data = null;
            }
        }
        if (!cached_data){
            cached_data = [];
        }
        return cached_data
    };



    $.ajax({
        type: 'GET',
        url: (location.pathname + '/data/').replace('//', '/'),
        context: self,
        dataType: 'json',
        cache: false,
        beforeSend: function () {
            self.loading(true);
            try {
                updateTimelineData(getLocalCahce());
            } catch(e){
                console.log(e);
            }
            
            //показываем данные из кеша, пока выполняется запрос
        },
        complete: function () {
            self.loading(false);
        },
        success: function (data) {
            updateTimelineData(data);
            updateLocalCahce(data);
        }
    });
}