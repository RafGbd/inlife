function UserModel(user, friends_model) {
    var self = this;
    self.friends_model = friends_model; // parent FriendsViewModel
    self.id = user.id;
    self.name = user.name;
    self.first_name = user.first_name;
    self.last_name = user.last_name;
	self.full_name = ko.computed(function() {
        return self.first_name + " " + self.last_name;
    }, self);
    self.link = user.link;
    self.achievements = user.achievements;
    self.avatar_class = user.avatar_class;
    self.avatar = user.avatar;
    self.friendship_status = ko.observable(user.friendship_status);
    self.submitting = ko.observable(false);
	self.is_friend = ko.computed(function() {
		return (self.friendship_status() == 'friend')
	}, self);
	self.is_not_friend = ko.computed(function() {
		return (self.friendship_status() == 'not_friend')
	}, self);
	self.is_waiting_for_response = ko.computed(function() {
		return (self.friendship_status() == 'waiting_for_response')
	}, self);
	self.is_want_to_friendship = ko.computed(function() {
		return (self.friendship_status() == 'want_to_friendship')
	}, self);
}
UserModel.prototype.send_data = function(action, id) {
        var self = this;
	$.ajax({
	    type: "POST",
	    url: (location.pathname + '/action/').replace('//', '/'),
	    data: {action: action, id: id},
	    success: function(data) {
		if(data.friendship == 'accepted' && action == 'add' ) {
		    self.friendship_status('friend');
		    self.friends_model.friends.push(self);
		    self.friends_model.users.remove(self);
		    action = "accept";
		}
		self.friends_model.update_action(action, id);
	    }
	});
}
UserModel.prototype.add_to_friends = function(){
    //мы делаем пришлашение
    this.friendship_status('waiting_for_response');
    this.send_data("add", this.id);
}
UserModel.prototype.remove_from_friends = function(){
    //мы удаляем из друзей
    if (confirm('Вы уверены?')) {
        this.friendship_status('not_friend');
        this.send_data("remove", this.id);
        this.friends_model.users.push(this);
        this.friends_model.friends.remove(this);
    } else {
        // Do nothing!
    }

}
UserModel.prototype.reject_friendship_offer = function(){
    //мы отменяем приглашение, сделанне нами
    this.friendship_status('not_friend');
    this.send_data("reject", this.id);
}
UserModel.prototype.accept_offer = function(){
    //мы принимаем приглашение
    this.friendship_status('friend');
    this.send_data("accept", this.id);
    this.friends_model.friends.push(this);
    this.friends_model.friendship_offerings.remove(this);
    if(this.friends_model.friendship_offerings.length == 0) {
	$('a[href*=who_wants]').parent().removeClass('active');
    }
    // console.dir(this.friends_model.friendship_offerings.length);
}
UserModel.prototype.decline_offer = function(){
    //мы отклоняем приглашение, сделанное нам
    this.friendship_status('not_friend');
    this.send_data("decline", this.id);
    this.friends_model.friendship_offerings.remove(this);
}
function FriendsViewModel() {
    var self = this;

    self.loading = ko.observable(false);

    self.friends = ko.observableArray();
    self.friends_count = ko.computed(function() {
		return self.friends().length
	}, self);
    self.friendship_offerings = ko.observableArray();
    self.friendship_offerings_count = ko.computed(function() {
		return self.friendship_offerings().length
	}, self);
	self.users = ko.observableArray();
	self.search_empty = ko.observable(false);
    self.pages = ko.observableArray();
    self.active_page = ko.observable(1);
    self.users_count = ko.observable(false);

    self.friends_class = ko.computed(function() {
	if(self.friends_count() > 0) {
	    return 'active'	    
	}
	return ''
    }, self);
    self.search_class = ko.computed(function() {
	if(self.friends_count() == 0) {
	    return 'active'	    
	}
	return ''
    }, self);

    self.loadData();
}

FriendsViewModel.prototype.nextPage = function(page) {
    var self = this;
    self.search_empty(false);
    self.active_page(page);
    

    function updateUsers(response) {
        self.users.splice(0, self.users().length);
        var users = response.users;
        if (users.length == 0)
            self.search_empty(true);
        for (var i = 0; i < users.length; i++) {
            self.users.push(new UserModel(users[i], self));
        }
    }

    $.ajax({
        type: 'GET',
        url: (location.pathname + '/page/').replace('//', '/'),
        dataType: 'text',
        data: { 'page': page },
        cache: false,
        success: function(response) {
            updateUsers(jQuery.parseJSON(response));
            $('.hint').tooltip();
        }
    });
    
    $("html, body").animate({ scrollTop: 0 }, "fast");

}

FriendsViewModel.prototype.searchFriends = function(formElement) {
    var self = this;
    self.search_empty(false);
    function updateUsers(response) {
    	self.users.splice(0, self.users().length);
        var users = response.users;
        if (users.length == 0)
        	self.search_empty(true);
        for (var i = 0; i < users.length; i++) {
            self.users.push(new UserModel(users[i], self));
        }
    }
	var $form = $(formElement);
	var q = $form.find("input[name='q']").val();
    $('.pagination').hide();
	if (q) {
	    $.ajax({
	        type: 'GET',
	        url: (location.pathname + '/search/').replace('//', '/'),
	        dataType: 'text',
	        data: { 'q': q },
	        cache: false,
	        beforeSend: function(){
	            self.loading(true);
	        },
	        complete: function(){
	            self.loading(false);
	        },
	        success: function(response) {
	            updateUsers(jQuery.parseJSON(response));
	            $('.hint').tooltip();
	        }
	    });
	}
}

FriendsViewModel.prototype.update_action = function (action, id) {
    var localObj2 = {}, localObj = {}, friends, f_o, friendship_offerings, self = this;
    var key = 'fr_users_' + document.location.host + document.location.pathname;

    friends = JSON.parse(localStorage.getItem('friends')).localArray;
    friendship_offerings = JSON.parse(localStorage.getItem('friendship_offerings')).localArray;
    if (action == "remove") {
    	for(i = 0; i < friends.length; i++) {
    	    if(friends[i].id == id) {
		z = i == 0 ? 1: i;
    		friends.splice(i, z)[0];
    		break;
    	    }
    	}
    }
    if (action == "accept") {
    	for(j = 0; j < friendship_offerings.length; j++) {
    	    if(friendship_offerings[j].id == id) {
		friendship_offerings[j].friendship_status = 'friend';
		z = j == 0 ? 1: j;
		friends.push(friendship_offerings.splice(j, z)[0]);
    		break;
    	    }
    	}
	localObj2.localArray = friendship_offerings;
	localStorage.setItem('friendship_offerings', JSON.stringify(localObj2));
    }
    localObj.localArray = friends;
    localStorage.setItem('friends', JSON.stringify(localObj));
}

FriendsViewModel.prototype.updateLocalCache = function (data, key) {
    var localObj, self = this,
    localObj = {};
    localObj.localArray = data;
    localStorage.setItem(key, JSON.stringify(localObj));
}

FriendsViewModel.prototype.loadData = function(){
    var self = this;

    function localStorageAvail() {
        try {
            return 'localStorage' in window && window['localStorage'] !== null;
        } catch (e) {
            return false;
        }
    }

    function updateUsers(response) {
        var stor_friends = 0,
	    friends = response.friends;

	if (localStorage.hasOwnProperty("friends")) {
	    var stor_friends = JSON.parse(localStorage.getItem('friends')).localArray.length;
	}
	if (stor_friends != friends.length) {
	    self.updateLocalCache(friends, 'friends');
            self.friends.splice(0, self.friends().length); // clear before refill
            for (var i = 0; i < friends.length; i++) {
		self.friends.push(new UserModel(friends[i], self));
            }
	}

        var friendship_offerings = response.friendship_offerings;
	self.updateLocalCache(friendship_offerings, 'friendship_offerings')
        self.friendship_offerings.splice(0, self.friendship_offerings().length); // clear before refill
        for (var i = 0; i < friendship_offerings.length; i++) {
            self.friendship_offerings.push(new UserModel(friendship_offerings[i], self));
        }
        var users = response.users;
        self.users.splice(0, self.users().length); // clear before refill
        for (var i = 0; i < users.length; i++) {
            self.users.push(new UserModel(users[i], self));
        }
        for (var i = 1; i <= Math.ceil(response.users_count/response.users_limit); i++ ){
            self.pages.push({'page':i});
        }
    }

    self.clearLocalCache = function() {
        if (localStorageAvail()) {
            localStorage.removeItem(key);
        }
    };    

    var key = 'fr_users_' + document.location.host + document.location.pathname;

    if (localStorage.hasOwnProperty("friends")) {
	friends = JSON.parse(localStorage.getItem('friends')).localArray;
        self.friends.splice(0, self.friends().length); // clear before refill
        for (var i = 0; i < friends.length; i++) {
            self.friends.push(new UserModel(friends[i], self));
        }
    }
    $.ajax({
        type: 'GET',
        url: (location.pathname + '/data/').replace('//', '/'),
        dataType: 'text',
        cache: false,
        beforeSend: function(){
	    self.loading(true);
        },
        complete: function(){
	    self.loading(false);
        },
        success: function(response) {
	    updateUsers(jQuery.parseJSON(response));
	    $('.hint').tooltip();
	    if (localStorageAvail()) {
                try {
		    localStorage.setItem(key, response);
                }
                catch (e) {}
		
                // автоматическая очистка кеша при изменении данных
                self.friends.subscribe(function(newValue){
		    self.clearLocalCache();
                });
                self.friendship_offerings.subscribe(function(newValue){
		    self.clearLocalCache();
                });
                self.users.subscribe(function(newValue){
		    self.clearLocalCache();
                });
	    }
        }
    });
};

var FriendsInit = function() {

    ko.applyBindings(new FriendsViewModel(), document.getElementById('friends_for_bindings'));

    $('body').on("click", '.sendMessage', function(){
        $('i.icon-user').html(' ' + $(this).data('user_name'));
        $('#id_recipient').val($(this).data('id'));
        $('#myModal form').attr('action', $(this).data('href'));
    });

    var inviteTpl = Handlebars.compile($('#invite-friends-tpl').html());

    if (window.location.hash !='') {
        var friends_reqest = $.ajax({
                url: '/friends/social_auth/invite/' + window.location.hash.substr(1) + '/data/',
                method: 'GET'
        });
        friends_reqest.done(function ( friends ) {
            $('#invite-friends-list div.modal-body').html(inviteTpl({friends: friends}));
            $('#invite-friends-list').modal('show');

            var invite_links = $('#invite-friends-list a.friend-invite');
            invite_links.click(function (e) {
                e.preventDefault();
                var that = $(this);
                var btn_success = function () {
                    that.removeClass('btn-primary-else');
                    that.addClass('btn-success');
                    that.text(' Приглашен');
                    that.prepend('<i class="icon-comment"></i>');
                    that.attr('disabled', true);
                };
                $.ajax({
                    type: 'POST',
                    url: $(this).attr('href'),
                    data: {action: 'invite',
                           email: $(this).data('email')}
                }).done(btn_success);
            });
            var add_links = $('#invite-friends-list a.friend-add');
            add_links.click(function (e) {
                e.preventDefault();
                var that = $(this);
                var btn_success = function () {
                    that.removeClass('btn-primary');
                    that.addClass('btn-success');
                    that.text(' Приглашен');
                    that.prepend('<i class="icon-comment"></i>');
                    that.attr('disabled', true);
                };
                $.ajax({
                    type: 'POST',
                    url: $(this).attr('href'),
                    data: {action: 'add',
                           id: $(this).data('id')}
                }).done(btn_success);
            });
        });
        friends_reqest.fail(function( jqXHR, textStatus ) {
            console.log( "Request failed: " + textStatus );
        });
    }
}
