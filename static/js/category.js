ko.observableArray.fn.pushAll = function(valuesToPush) {
    var underlyingArray = this();
    this.valueWillMutate();
    ko.utils.arrayPushAll(underlyingArray, valuesToPush);
    this.valueHasMutated();
    return this;
};

function ChildCategoryViewModel(category, parent_category) {
    var self = this;
    self.parent_category = parent_category;

    self.name = category.name;
    self.info = category.info;
    self.achievements = category.achievements;
    self.image = category.image;
    self.is_disabled = category.is_disabled;

    self.is_active = ko.computed(function() {
        return self.parent_category.active_child() == self
    });

}

function CategoryViewModel() {
    var self = this;
    self.children = ko.observableArray();
    self.active_child = ko.observable();

    self.select_child = function(child) {
        if (!child.is_disabled) {
            self.active_child(child);
        }
    };

    $.getJSON((location.pathname + '/data/').replace('//', '/'), function(category_data) {
        for (var i = 0; i < category_data.children.length; i++) {
            self.children.push(
                new ChildCategoryViewModel(category_data.children[i], self)
            );
        }

        self.active_child(self.children()[0]);
    });
}


