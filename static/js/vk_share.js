function VKPostAchievement(achievement_id, user_id){
    this.achievement_id = achievement_id;
    this.user_id = user_id;
    this.message = ''
    this.url = ''
    this.images_count = 0;
    this.attachments = [];

    console.log("starting post achievement to vk");
    console.log("achievement_id: " + achievement_id);
    console.log("user_id: " + user_id);


    

    this.enquire_posting = function(){
        var self = this;
        console.log("requesting images count and posting permissions from achiever server");
        $.ajax({
            context: self,
            type: "get",
            url: '/share/enquire_posting_vk/',
            data: { 
                achievement_id: self.achievement_id,
                user_id: self.user_id
            },
            success: self.process_enquire_response
        });
    }
    this.process_enquire_response = function(response){
        var self = this;
        console.log("processing response from achiever server");
        console.log("response:");
        console.log(response);
        if (!response.isError){
            console.log('succes response');
            self.message = response.res.message;
            self.images_count = response.res.images_count;
            self.url = response.res.url;
            console.log("self.message: ");
            console.log(self.message);
            console.log("self.images_count: ");
            console.log(self.images_count);
            
            VK.Api.call('account.getAppPermissions', {}, self.init_upload_urls);

        } else {
            console.log('error');
            console.log(response.errorMessage);
        }
    }

    this.init_upload_urls = function() {
        var self = this;
        this.upload_urls = [];
        for (var i = self.images_count - 1; i >= 0; i--) {
            VK.Api.call('photos.getWallUploadServer', {}, self.collect_url);
        };
    }
    
    this.collect_url = function(r){
        var self = this;
        console.log("collect_url");
        if (!r.response) {
          //ошибка для данного сервера, уменьшаем кол-во
          this.images_count -= 1;
        } else {
          console.log(r.response.upload_url);
          this.upload_urls.push(r.response.upload_url);
        }
        
        if (this.upload_urls.length >= this.images_count) {
          self.upload_photos();
        }
    }
    this.upload_photos = function(){
        console.log('upload_photos');
        var self = this;
        console.log(self);
        $csrf = $( 'input[name="csrfmiddlewaretoken"]' );
        $.ajax({
            context: self,
            type: "post",
            url: '/share/vk_post_images/',
            data: { 
                achievement_id: self.achievement_id,
                user_id: self.user_id,
                upload_urls: self.upload_urls,
                csrfmiddlewaretoken: $csrf.val()
            },
            success: self.save_wall_photos
        });
    }
    this.save_wall_photos = function(response) {
        console.log("save_wall_photos");
        var self = this;
        if (!response.isError){
            var tockens = response.res.tockens;
            self.tockens_count = tockens.length;
            var tockens_length = tockens.length;
            this.attachments = [];
            for (var i = tockens_length - 1; i >= 0; i--) {
                tocken = tockens[i];
                VK.Api.call('photos.saveWallPhoto', tocken, self.push_attachment);
            }
        } else {
            console.log('error');
            console.log(response.errorMessage);
        }
    }
    this.push_attachment = function(r) {
        console.log("push_attachment");
        var self = this;
        //нужно предусмотреть случай ошибки
        this.attachments.push(r.response[0].id);
  
        if (this.attachments.length == this.tockens_count) {
          console.log(this.attachments);
          self.wall_post();
        }
    }
    
    this.wall_post = function(data){
        console.log("wall_post");
        var self = this;
        self.attachments.push(self.url);
        str_attachments = self.attachments.join(",")
       
        VK.Api.call('wall.post', 
        {
            message: self.message, 
            attachments: attachments
        }, 
        function(r){
            if(r.response){
                console.log(r.response.post_id);
            } else {
                console.error(r);
            }
        });
    }



    self.enquire_posting();

}