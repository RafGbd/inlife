function ChatViewModel(user_id, interlocutor_id) {
    var self = this;
    this.user_id = user_id;
    this.interlocutor_id = interlocutor_id;
    
    this.messages = ko.observableArray([]);
    
    self.submitting = ko.observable(false);
    self.loading = ko.observable(true);

    self.key = '_chat_user_' + self.interlocutor_id

    self.updateMassegeFromCache();
    scrollDown();

    this.updater = function() {
        time = self.last_message_time();
        self.update_messages(time);
    };
    
    setInterval(this.updater, 2000);
}

ChatViewModel.prototype.update_messages = function(time){
    var self = this;
    request_params = { 'chat_user_id': this.interlocutor_id }
    
    if (time) {
        request_params['time'] = time;
    }

    $.get("/conversations/check", request_params)
    .done(function(data) {
        if (data.success) {
	    if (data.sender_count) {
	    	$('.chat_user span').text('');
	    	for(i=0; i<data.sender_count.length;i++) {
	    	    $('.user_' + data.sender_count[i].sender_id + ' span').text(data.sender_count[i].sender__count);
	    	}
	    }
            if (data.messages.length) {
		self.updateLocalCache(data.messages);
                ko.utils.arrayPushAll(self.messages(), data.messages);
                self.messages.valueHasMutated();
                scrollDown();
            }
        }
	self.loading(false);
    });
};

ChatViewModel.prototype.last_message_time = function (){
    var self = this;
    var messages = self.messages()
    var last_message = messages[messages.length-1];
    return last_message.sent_at
}

ChatViewModel.prototype.updateLocalCache = function (data) {
    var localObj, self = this;
    if (localStorage.hasOwnProperty(self.key)) {
	localObj = JSON.parse(localStorage.getItem(self.key));
	Array.prototype.push.apply(localObj.localArray, data);
    } else {
	localObj = {};
	localObj.localArray = data;
    }
    localStorage.setItem(self.key, JSON.stringify(localObj));
}

ChatViewModel.prototype.updateMassegeFromCache = function () {
    var self = this;
    if (localStorage.hasOwnProperty(self.key)) {
	localObj = JSON.parse(localStorage.getItem(self.key));
	var time = localObj.localArray[localObj.localArray.length-1].sent_at;

        ko.utils.arrayPushAll(self.messages(), localObj.localArray);
        self.messages.valueHasMutated();
	this.update_messages(time);
    } else {
	this.update_messages();
    }
}

ChatViewModel.prototype.send_message = function(form){
    var self = this;
    var $form = $("#reply_form");
    var csrf_token = $form.find('input[name="csrfmiddlewaretoken"]').val();
    var text = $form.find('textarea[name="text"]').val().replace(/\n$/m, '');
    var recipient = self.interlocutor_id;
    var url = $form.attr('action');
    self.submitting(true);
    if(text.length < 1) {
        self.submitting(false);
        $form.find('textarea[name="text"]').val('');
	return false;
    }
    $.ajax({
        type: 'POST',
        url: url,
        data: {
            csrfmiddlewaretoken: csrf_token,
            recipient: recipient,
            text: text,
        },
        dataType: 'json',
        beforeSend: function(){
            self.submitting(true); // включим крутилку
        },
        complete: function(){
            self.submitting(false);
        },
        success: function(response){
            if (response.success) {
                $form.find('textarea[name="text"]').val('');
            }
        }
    });
    return false;

}

function scrollDown(){
    var message_box = document.getElementById("message_box");
    if (message_box){
       message_box.scrollTop = message_box.scrollHeight; 
    }
};