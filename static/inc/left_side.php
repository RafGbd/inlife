<aside class="span3 user left_separator" >
	<div id="leftside">
		<div class="name"><b>Константин</b>Константинов</div>
		<div class="photo"><img src="/i/user_photo.jpg" /></div>
		
		<h5>Уровень доверия 60%</h5>
		<div class="progress">
		  <div class="bar" style="width: 60%;"></div>
		</div>
		
		<h5>Крупнейшие достижения</h5>
		<div class="achievements">
			<div class="achiv mini hint" data-title="Достижение «Чемпион»">
				<i class="icon-trophy"></i>
			</div>
			<div class="achiv mini hint" data-title="Достижение «Звезда спорта»">
				<i class="icon-star"></i>
			</div>
			<div class="achiv mini hint activated" data-title="Достижение «Большое сердце»">
				<i class="icon-heart"></i>
			</div>
			<div class="achiv mini hint" data-title="Достижение «Листочек»">
				<i class="icon-leaf"></i>
			</div>
			<div class="achiv mini hint" data-title="Достижение «Молния»">
				<i class="icon-bolt"></i>
			</div>
			<div class="achiv mini hint" data-title="Достижение «Пивной живот»">
				<i class="icon-beer"></i>
			</div>
			<div class="achiv mini hint" data-title="Достижение «Особый»">
				<i class="icon-asterisk"></i>
			</div>
		</div>
		<h5 class="hint">Интересы</h5>
		<div class="interests">
			<span class="label">Спорт</span>
			<span class="label">Философия</span>
			<span class="label">Греко-римская борьба</span>
			<span class="label">Бокс</span>
			<span class="label">Психология</span>
			<span class="label">Математика</span>
		</div>
	</div>
</aside>