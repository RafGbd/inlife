<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<title>InRealLife</title>

<link href="/css/irlstyle.css" rel="stylesheet">
<script src="/js/jquery-1.8.3.min.js"></script>
<script src="/js/bootstrap.min.js"></script>
<script src="/js/common.js"></script>

<!--[if lt IE 9]>
<script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
<![endif]-->
</head>

<body>
	<header>
		<div class="navbar navbar-inverse navbar-fixed-top">
			<div class="navbar-inner">
				<div class="container">
					<a class="brand" href="/">InRealLife</a> <span class="tagline"><span>|</span>Создай цель своей жизни</span>
					<ul class="nav pull-right">
						<li>
							<a class="icon-group hint" data-placement="bottom" data-title="Друзья" href="/html/friends.php"></a>
						</li>
						<li class="attention">
							<a class="icon-rss hint" data-placement="bottom" data-title="Новости" href="/html/news.php"></a>
							<div class="count_badge cb_red">1</div>
						</li>
						<li>
							<a class="icon-bar-chart hint" data-placement="bottom" data-title="Статистика" href="/html/ratings.php"></a>
						</li>
						<li class="attention">
							<a class="icon-comments hint" data-placement="bottom" data-title="Сообщения" href="/html/messages.php"></a>
							<div class="count_badge cb_red">3</div>
						</li>
						<li>
							
							<div class="btn-group">
								<a class="icon-cog btn dropdown-toggle" data-toggle="dropdown" data-placement="bottom" href="/html/settings.php"></a>
								<ul class="dropdown-menu">
									<li><a href="settings.php"><i class="icon-user"></i> Профиль</a></li>
									<li><a href="landing_page.php"><i class="icon-off"></i> Выход</a></li>
								</ul>
							</div>
						</li>
					</ul>
				</div>
			</div>
		</div>
	</header>

	<div id="wrap">
		<div class="user_bar">
			<div class="container">
			
				<div class="row">
					<div class="span4">
						<div class="pie_wrap">
							<div class="pie">
								<ul>
									<li><p style="-webkit-transform:rotate(160deg);-moz-transform:rotate(180deg);-ms-accelerator-transform:rotate(180deg);transform:rotate(180deg);"><span></span></p></li> 	
									<li><p style="-webkit-transform:rotate(0deg);-moz-transform:rotate(90deg);-ms-accelerator-transform:rotate(90deg);transform:rotate(90deg);"><span></span></p></li>
								</ul>
							</div>
							<div class="pie_inside">
								<img src="/i/user_avatar.jpg"/>
							</div>
							<div class="count_badge">3</div>
						</div>
						<div class="name"><b>Константин</b>Константинов</div>
					</div>
					<div class="span2 main_section">
						<a href="/html/tree.php">
							<div class="pie_wrap">
								<div class="pie">
									<ul>
										<li><p style="-webkit-transform:rotate(180deg);-moz-transform:rotate(180deg);-ms-accelerator-transform:rotate(180deg);transform:rotate(180deg);"><span></span></p></li> 	
										<li><p style="-webkit-transform:rotate(90deg);-moz-transform:rotate(90deg);-ms-accelerator-transform:rotate(90deg);transform:rotate(90deg);"><span></span></p></li>
									</ul>
								</div>
								<span class="pie_inside">
									<span>75</span>
								</span>
								<div class="count_badge cb_white">3</div>
							</div>
							<div class="name"><b>Тело</b>Хоккеист</div>
						</a>
					</div>
					<div class="span2 main_section">
						<a href="">
							<div class="pie_wrap">
								<div class="pie">
								</div>
								<span class="pie_inside add_target hint" data-placement="bottom" data-title="Добавить цель">
									<i class="icon-plus"></i>
								</span>
							</div>
							<div class="name"><b>Дух</b>Нет цели</div>
						</a>
					</div>
					<div class="span2 main_section">
						<a href="#">
							<div class="pie_wrap">
								<div class="pie">
								</div>
								<span class="pie_inside add_target hint" data-placement="bottom" data-title="Добавить цель">
									<i class="icon-plus"></i>
								</span>
							</div>
							<div class="name"><b>Тело</b>Нет цели</div>
						</a>
					</div>
					<div class="span2 main_section">
						<a href="">
							<div class="pie_wrap">
								<div class="pie">
								</div>
								<span class="pie_inside add_target hint" data-placement="bottom" data-title="Добавить цель">
									<i class="icon-plus"></i>
								</span>
							</div>
							<div class="name"><b>Общество</b>Нет цели</div>
						</a>
					</div>
				</div>
			</div>
		</div>
		<div class="container">
		